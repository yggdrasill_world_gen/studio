from bawr.config import *

class Env( Environment ):
    pass
    
class FontAwesomeIcons( IconSet ):
    src = 'fa_icons'

class YggdrasillIcons( IconSet ):
    src = 'icons'
    options = {}


class MyFont( Font ):
    copyright = "Copyright 2022 Florian Oetke"
    name = "yggdrasill-icons"
    family = "yggdrasill-icons"
    start_code = 0xe000
    collections = (YggdrasillIcons, FontAwesomeIcons)


class MyCppFontH( CppFontHeader ):
    source = MyFont
    constexpr = True
    name = "yggdrasill_icons.hpp"
    namespace = "yggdrasill::icons"
    macros = True
    macro_prefix = "YGGDRASILL_ICON_"

class MyCppFontEmbed( CppEmbedded ):
    source = "${BAWR_OUTPUT_DIR}/yggdrasill-icons.ttf"
    name = "yggdrasill_icon_data"
    namespace = "yggdrasill::icons"
