#pragma once

#define YGGDRASILL_ICON_BRANCHING_COPY                   "\ue000"
#define YGGDRASILL_ICON_BRANCHING_COPY_ONCE              "\ue001"
#define YGGDRASILL_ICON_BRANCHING_EMPTY                  "\ue002"
#define YGGDRASILL_ICON_BRANCHING_EMPTY_ONCE             "\ue003"
#define YGGDRASILL_ICON_BRANCHING_LINEAR                 "\ue004"
#define YGGDRASILL_ICON_DELETE_BRANCH                    "\ue005"
#define YGGDRASILL_ICON_GOTO_LAST_CHANGE                 "\ue006"
#define YGGDRASILL_ICON_PLAY_FROM_START                  "\ue007"
#define YGGDRASILL_ICON_TOOL_ELEVATION                   "\ue008"
#define YGGDRASILL_ICON_TOOL_ELEVATION_HILL              "\ue009"
#define YGGDRASILL_ICON_TOOL_ELEVATION_MANUAL            "\ue00a"
#define YGGDRASILL_ICON_TOOL_ELEVATION_MOUNTAIN          "\ue00b"
#define YGGDRASILL_ICON_TOOL_ELEVATION_OCEAN             "\ue00c"
#define YGGDRASILL_ICON_TOOL_ELEVATION_WATER             "\ue00d"
#define YGGDRASILL_ICON_TOOL_MEASURE                     "\ue00e"
#define YGGDRASILL_ICON_TOOL_MESH_SUBDIVISION            "\ue00f"
#define YGGDRASILL_ICON_TOOL_PLATE_ASSIGNMENT            "\ue010"
#define YGGDRASILL_ICON_TOOL_PLATE_VELOCITY              "\ue011"
#define YGGDRASILL_ICON_TOOL_SELECT                      "\ue012"
#define YGGDRASILL_ICON_AD                               "\ue013"
#define YGGDRASILL_ICON_ADDRESS_BOOK                     "\ue014"
#define YGGDRASILL_ICON_ADDRESS_CARD                     "\ue015"
#define YGGDRASILL_ICON_ADJUST                           "\ue016"
#define YGGDRASILL_ICON_AIR_FRESHENER                    "\ue017"
#define YGGDRASILL_ICON_ALIGN_CENTER                     "\ue018"
#define YGGDRASILL_ICON_ALIGN_JUSTIFY                    "\ue019"
#define YGGDRASILL_ICON_ALIGN_LEFT                       "\ue01a"
#define YGGDRASILL_ICON_ALIGN_RIGHT                      "\ue01b"
#define YGGDRASILL_ICON_ALLERGIES                        "\ue01c"
#define YGGDRASILL_ICON_AMBULANCE                        "\ue01d"
#define YGGDRASILL_ICON_AMERICAN_SIGN_LANGUAGE_INTERPRETING "\ue01e"
#define YGGDRASILL_ICON_ANCHOR                           "\ue01f"
#define YGGDRASILL_ICON_ANGLE_DOUBLE_DOWN                "\ue020"
#define YGGDRASILL_ICON_ANGLE_DOUBLE_LEFT                "\ue021"
#define YGGDRASILL_ICON_ANGLE_DOUBLE_RIGHT               "\ue022"
#define YGGDRASILL_ICON_ANGLE_DOUBLE_UP                  "\ue023"
#define YGGDRASILL_ICON_ANGLE_DOWN                       "\ue024"
#define YGGDRASILL_ICON_ANGLE_LEFT                       "\ue025"
#define YGGDRASILL_ICON_ANGLE_RIGHT                      "\ue026"
#define YGGDRASILL_ICON_ANGLE_UP                         "\ue027"
#define YGGDRASILL_ICON_ANGRY                            "\ue028"
#define YGGDRASILL_ICON_ANKH                             "\ue029"
#define YGGDRASILL_ICON_APPLE_ALT                        "\ue02a"
#define YGGDRASILL_ICON_ARCHIVE                          "\ue02b"
#define YGGDRASILL_ICON_ARCHWAY                          "\ue02c"
#define YGGDRASILL_ICON_ARROW_ALT_CIRCLE_DOWN            "\ue02d"
#define YGGDRASILL_ICON_ARROW_ALT_CIRCLE_LEFT            "\ue02e"
#define YGGDRASILL_ICON_ARROW_ALT_CIRCLE_RIGHT           "\ue02f"
#define YGGDRASILL_ICON_ARROW_ALT_CIRCLE_UP              "\ue030"
#define YGGDRASILL_ICON_ARROW_CIRCLE_DOWN                "\ue031"
#define YGGDRASILL_ICON_ARROW_CIRCLE_LEFT                "\ue032"
#define YGGDRASILL_ICON_ARROW_CIRCLE_RIGHT               "\ue033"
#define YGGDRASILL_ICON_ARROW_CIRCLE_UP                  "\ue034"
#define YGGDRASILL_ICON_ARROW_DOWN                       "\ue035"
#define YGGDRASILL_ICON_ARROW_LEFT                       "\ue036"
#define YGGDRASILL_ICON_ARROW_RIGHT                      "\ue037"
#define YGGDRASILL_ICON_ARROW_UP                         "\ue038"
#define YGGDRASILL_ICON_ARROWS_ALT_H                     "\ue039"
#define YGGDRASILL_ICON_ARROWS_ALT_V                     "\ue03a"
#define YGGDRASILL_ICON_ARROWS_ALT                       "\ue03b"
#define YGGDRASILL_ICON_ASSISTIVE_LISTENING_SYSTEMS      "\ue03c"
#define YGGDRASILL_ICON_ASTERISK                         "\ue03d"
#define YGGDRASILL_ICON_AT                               "\ue03e"
#define YGGDRASILL_ICON_ATLAS                            "\ue03f"
#define YGGDRASILL_ICON_ATOM                             "\ue040"
#define YGGDRASILL_ICON_AUDIO_DESCRIPTION                "\ue041"
#define YGGDRASILL_ICON_AWARD                            "\ue042"
#define YGGDRASILL_ICON_BABY_CARRIAGE                    "\ue043"
#define YGGDRASILL_ICON_BABY                             "\ue044"
#define YGGDRASILL_ICON_BACKSPACE                        "\ue045"
#define YGGDRASILL_ICON_BACKWARD                         "\ue046"
#define YGGDRASILL_ICON_BACON                            "\ue047"
#define YGGDRASILL_ICON_BACTERIA                         "\ue048"
#define YGGDRASILL_ICON_BACTERIUM                        "\ue049"
#define YGGDRASILL_ICON_BAHAI                            "\ue04a"
#define YGGDRASILL_ICON_BALANCE_SCALE_LEFT               "\ue04b"
#define YGGDRASILL_ICON_BALANCE_SCALE_RIGHT              "\ue04c"
#define YGGDRASILL_ICON_BALANCE_SCALE                    "\ue04d"
#define YGGDRASILL_ICON_BAN                              "\ue04e"
#define YGGDRASILL_ICON_BAND_AID                         "\ue04f"
#define YGGDRASILL_ICON_BARCODE                          "\ue050"
#define YGGDRASILL_ICON_BARS                             "\ue051"
#define YGGDRASILL_ICON_BASEBALL_BALL                    "\ue052"
#define YGGDRASILL_ICON_BASKETBALL_BALL                  "\ue053"
#define YGGDRASILL_ICON_BATH                             "\ue054"
#define YGGDRASILL_ICON_BATTERY_EMPTY                    "\ue055"
#define YGGDRASILL_ICON_BATTERY_FULL                     "\ue056"
#define YGGDRASILL_ICON_BATTERY_HALF                     "\ue057"
#define YGGDRASILL_ICON_BATTERY_QUARTER                  "\ue058"
#define YGGDRASILL_ICON_BATTERY_THREE_QUARTERS           "\ue059"
#define YGGDRASILL_ICON_BED                              "\ue05a"
#define YGGDRASILL_ICON_BEER                             "\ue05b"
#define YGGDRASILL_ICON_BELL_SLASH                       "\ue05c"
#define YGGDRASILL_ICON_BELL                             "\ue05d"
#define YGGDRASILL_ICON_BEZIER_CURVE                     "\ue05e"
#define YGGDRASILL_ICON_BIBLE                            "\ue05f"
#define YGGDRASILL_ICON_BICYCLE                          "\ue060"
#define YGGDRASILL_ICON_BIKING                           "\ue061"
#define YGGDRASILL_ICON_BINOCULARS                       "\ue062"
#define YGGDRASILL_ICON_BIOHAZARD                        "\ue063"
#define YGGDRASILL_ICON_BIRTHDAY_CAKE                    "\ue064"
#define YGGDRASILL_ICON_BLENDER_PHONE                    "\ue065"
#define YGGDRASILL_ICON_BLENDER                          "\ue066"
#define YGGDRASILL_ICON_BLIND                            "\ue067"
#define YGGDRASILL_ICON_BLOG                             "\ue068"
#define YGGDRASILL_ICON_BOLD                             "\ue069"
#define YGGDRASILL_ICON_BOLT                             "\ue06a"
#define YGGDRASILL_ICON_BOMB                             "\ue06b"
#define YGGDRASILL_ICON_BONE                             "\ue06c"
#define YGGDRASILL_ICON_BONG                             "\ue06d"
#define YGGDRASILL_ICON_BOOK_DEAD                        "\ue06e"
#define YGGDRASILL_ICON_BOOK_MEDICAL                     "\ue06f"
#define YGGDRASILL_ICON_BOOK_OPEN                        "\ue070"
#define YGGDRASILL_ICON_BOOK_READER                      "\ue071"
#define YGGDRASILL_ICON_BOOK                             "\ue072"
#define YGGDRASILL_ICON_BOOKMARK                         "\ue073"
#define YGGDRASILL_ICON_BORDER_ALL                       "\ue074"
#define YGGDRASILL_ICON_BORDER_NONE                      "\ue075"
#define YGGDRASILL_ICON_BORDER_STYLE                     "\ue076"
#define YGGDRASILL_ICON_BOWLING_BALL                     "\ue077"
#define YGGDRASILL_ICON_BOX_OPEN                         "\ue078"
#define YGGDRASILL_ICON_BOX_TISSUE                       "\ue079"
#define YGGDRASILL_ICON_BOX                              "\ue07a"
#define YGGDRASILL_ICON_BOXES                            "\ue07b"
#define YGGDRASILL_ICON_BRAILLE                          "\ue07c"
#define YGGDRASILL_ICON_BRAIN                            "\ue07d"
#define YGGDRASILL_ICON_BREAD_SLICE                      "\ue07e"
#define YGGDRASILL_ICON_BRIEFCASE_MEDICAL                "\ue07f"
#define YGGDRASILL_ICON_BRIEFCASE                        "\ue080"
#define YGGDRASILL_ICON_BROADCAST_TOWER                  "\ue081"
#define YGGDRASILL_ICON_BROOM                            "\ue082"
#define YGGDRASILL_ICON_BRUSH                            "\ue083"
#define YGGDRASILL_ICON_BUG                              "\ue084"
#define YGGDRASILL_ICON_BUILDING                         "\ue085"
#define YGGDRASILL_ICON_BULLHORN                         "\ue086"
#define YGGDRASILL_ICON_BULLSEYE                         "\ue087"
#define YGGDRASILL_ICON_BURN                             "\ue088"
#define YGGDRASILL_ICON_BUS_ALT                          "\ue089"
#define YGGDRASILL_ICON_BUS                              "\ue08a"
#define YGGDRASILL_ICON_BUSINESS_TIME                    "\ue08b"
#define YGGDRASILL_ICON_CALCULATOR                       "\ue08c"
#define YGGDRASILL_ICON_CALENDAR_ALT                     "\ue08d"
#define YGGDRASILL_ICON_CALENDAR_CHECK                   "\ue08e"
#define YGGDRASILL_ICON_CALENDAR_DAY                     "\ue08f"
#define YGGDRASILL_ICON_CALENDAR_MINUS                   "\ue090"
#define YGGDRASILL_ICON_CALENDAR_PLUS                    "\ue091"
#define YGGDRASILL_ICON_CALENDAR_TIMES                   "\ue092"
#define YGGDRASILL_ICON_CALENDAR_WEEK                    "\ue093"
#define YGGDRASILL_ICON_CALENDAR                         "\ue094"
#define YGGDRASILL_ICON_CAMERA_RETRO                     "\ue095"
#define YGGDRASILL_ICON_CAMERA                           "\ue096"
#define YGGDRASILL_ICON_CAMPGROUND                       "\ue097"
#define YGGDRASILL_ICON_CANDY_CANE                       "\ue098"
#define YGGDRASILL_ICON_CANNABIS                         "\ue099"
#define YGGDRASILL_ICON_CAPSULES                         "\ue09a"
#define YGGDRASILL_ICON_CAR_ALT                          "\ue09b"
#define YGGDRASILL_ICON_CAR_BATTERY                      "\ue09c"
#define YGGDRASILL_ICON_CAR_CRASH                        "\ue09d"
#define YGGDRASILL_ICON_CAR_SIDE                         "\ue09e"
#define YGGDRASILL_ICON_CAR                              "\ue09f"
#define YGGDRASILL_ICON_CARAVAN                          "\ue0a0"
#define YGGDRASILL_ICON_CARET_DOWN                       "\ue0a1"
#define YGGDRASILL_ICON_CARET_LEFT                       "\ue0a2"
#define YGGDRASILL_ICON_CARET_RIGHT                      "\ue0a3"
#define YGGDRASILL_ICON_CARET_SQUARE_DOWN                "\ue0a4"
#define YGGDRASILL_ICON_CARET_SQUARE_LEFT                "\ue0a5"
#define YGGDRASILL_ICON_CARET_SQUARE_RIGHT               "\ue0a6"
#define YGGDRASILL_ICON_CARET_SQUARE_UP                  "\ue0a7"
#define YGGDRASILL_ICON_CARET_UP                         "\ue0a8"
#define YGGDRASILL_ICON_CARROT                           "\ue0a9"
#define YGGDRASILL_ICON_CART_ARROW_DOWN                  "\ue0aa"
#define YGGDRASILL_ICON_CART_PLUS                        "\ue0ab"
#define YGGDRASILL_ICON_CASH_REGISTER                    "\ue0ac"
#define YGGDRASILL_ICON_CAT                              "\ue0ad"
#define YGGDRASILL_ICON_CERTIFICATE                      "\ue0ae"
#define YGGDRASILL_ICON_CHAIR                            "\ue0af"
#define YGGDRASILL_ICON_CHALKBOARD_TEACHER               "\ue0b0"
#define YGGDRASILL_ICON_CHALKBOARD                       "\ue0b1"
#define YGGDRASILL_ICON_CHARGING_STATION                 "\ue0b2"
#define YGGDRASILL_ICON_CHART_AREA                       "\ue0b3"
#define YGGDRASILL_ICON_CHART_BAR                        "\ue0b4"
#define YGGDRASILL_ICON_CHART_LINE                       "\ue0b5"
#define YGGDRASILL_ICON_CHART_PIE                        "\ue0b6"
#define YGGDRASILL_ICON_CHECK_CIRCLE                     "\ue0b7"
#define YGGDRASILL_ICON_CHECK_DOUBLE                     "\ue0b8"
#define YGGDRASILL_ICON_CHECK_SQUARE                     "\ue0b9"
#define YGGDRASILL_ICON_CHECK                            "\ue0ba"
#define YGGDRASILL_ICON_CHEESE                           "\ue0bb"
#define YGGDRASILL_ICON_CHESS_BISHOP                     "\ue0bc"
#define YGGDRASILL_ICON_CHESS_BOARD                      "\ue0bd"
#define YGGDRASILL_ICON_CHESS_KING                       "\ue0be"
#define YGGDRASILL_ICON_CHESS_KNIGHT                     "\ue0bf"
#define YGGDRASILL_ICON_CHESS_PAWN                       "\ue0c0"
#define YGGDRASILL_ICON_CHESS_QUEEN                      "\ue0c1"
#define YGGDRASILL_ICON_CHESS_ROOK                       "\ue0c2"
#define YGGDRASILL_ICON_CHESS                            "\ue0c3"
#define YGGDRASILL_ICON_CHEVRON_CIRCLE_DOWN              "\ue0c4"
#define YGGDRASILL_ICON_CHEVRON_CIRCLE_LEFT              "\ue0c5"
#define YGGDRASILL_ICON_CHEVRON_CIRCLE_RIGHT             "\ue0c6"
#define YGGDRASILL_ICON_CHEVRON_CIRCLE_UP                "\ue0c7"
#define YGGDRASILL_ICON_CHEVRON_DOWN                     "\ue0c8"
#define YGGDRASILL_ICON_CHEVRON_LEFT                     "\ue0c9"
#define YGGDRASILL_ICON_CHEVRON_RIGHT                    "\ue0ca"
#define YGGDRASILL_ICON_CHEVRON_UP                       "\ue0cb"
#define YGGDRASILL_ICON_CHILD                            "\ue0cc"
#define YGGDRASILL_ICON_CHURCH                           "\ue0cd"
#define YGGDRASILL_ICON_CIRCLE_NOTCH                     "\ue0ce"
#define YGGDRASILL_ICON_CIRCLE                           "\ue0cf"
#define YGGDRASILL_ICON_CITY                             "\ue0d0"
#define YGGDRASILL_ICON_CLINIC_MEDICAL                   "\ue0d1"
#define YGGDRASILL_ICON_CLIPBOARD_CHECK                  "\ue0d2"
#define YGGDRASILL_ICON_CLIPBOARD_LIST                   "\ue0d3"
#define YGGDRASILL_ICON_CLIPBOARD                        "\ue0d4"
#define YGGDRASILL_ICON_CLOCK                            "\ue0d5"
#define YGGDRASILL_ICON_CLONE                            "\ue0d6"
#define YGGDRASILL_ICON_CLOSED_CAPTIONING                "\ue0d7"
#define YGGDRASILL_ICON_CLOUD_DOWNLOAD_ALT               "\ue0d8"
#define YGGDRASILL_ICON_CLOUD_MEATBALL                   "\ue0d9"
#define YGGDRASILL_ICON_CLOUD_MOON_RAIN                  "\ue0da"
#define YGGDRASILL_ICON_CLOUD_MOON                       "\ue0db"
#define YGGDRASILL_ICON_CLOUD_RAIN                       "\ue0dc"
#define YGGDRASILL_ICON_CLOUD_SHOWERS_HEAVY              "\ue0dd"
#define YGGDRASILL_ICON_CLOUD_SUN_RAIN                   "\ue0de"
#define YGGDRASILL_ICON_CLOUD_SUN                        "\ue0df"
#define YGGDRASILL_ICON_CLOUD_UPLOAD_ALT                 "\ue0e0"
#define YGGDRASILL_ICON_CLOUD                            "\ue0e1"
#define YGGDRASILL_ICON_COCKTAIL                         "\ue0e2"
#define YGGDRASILL_ICON_CODE_BRANCH                      "\ue0e3"
#define YGGDRASILL_ICON_CODE                             "\ue0e4"
#define YGGDRASILL_ICON_COFFEE                           "\ue0e5"
#define YGGDRASILL_ICON_COG                              "\ue0e6"
#define YGGDRASILL_ICON_COGS                             "\ue0e7"
#define YGGDRASILL_ICON_COINS                            "\ue0e8"
#define YGGDRASILL_ICON_COLUMNS                          "\ue0e9"
#define YGGDRASILL_ICON_COMMENT_ALT                      "\ue0ea"
#define YGGDRASILL_ICON_COMMENT_DOLLAR                   "\ue0eb"
#define YGGDRASILL_ICON_COMMENT_DOTS                     "\ue0ec"
#define YGGDRASILL_ICON_COMMENT_MEDICAL                  "\ue0ed"
#define YGGDRASILL_ICON_COMMENT_SLASH                    "\ue0ee"
#define YGGDRASILL_ICON_COMMENT                          "\ue0ef"
#define YGGDRASILL_ICON_COMMENTS_DOLLAR                  "\ue0f0"
#define YGGDRASILL_ICON_COMMENTS                         "\ue0f1"
#define YGGDRASILL_ICON_COMPACT_DISC                     "\ue0f2"
#define YGGDRASILL_ICON_COMPASS                          "\ue0f3"
#define YGGDRASILL_ICON_COMPRESS_ALT                     "\ue0f4"
#define YGGDRASILL_ICON_COMPRESS_ARROWS_ALT              "\ue0f5"
#define YGGDRASILL_ICON_COMPRESS                         "\ue0f6"
#define YGGDRASILL_ICON_CONCIERGE_BELL                   "\ue0f7"
#define YGGDRASILL_ICON_COOKIE_BITE                      "\ue0f8"
#define YGGDRASILL_ICON_COOKIE                           "\ue0f9"
#define YGGDRASILL_ICON_COPY                             "\ue0fa"
#define YGGDRASILL_ICON_COPYRIGHT                        "\ue0fb"
#define YGGDRASILL_ICON_COUCH                            "\ue0fc"
#define YGGDRASILL_ICON_CREDIT_CARD                      "\ue0fd"
#define YGGDRASILL_ICON_CROP_ALT                         "\ue0fe"
#define YGGDRASILL_ICON_CROP                             "\ue0ff"
#define YGGDRASILL_ICON_CROSS                            "\ue100"
#define YGGDRASILL_ICON_CROSSHAIRS                       "\ue101"
#define YGGDRASILL_ICON_CROW                             "\ue102"
#define YGGDRASILL_ICON_CROWN                            "\ue103"
#define YGGDRASILL_ICON_CRUTCH                           "\ue104"
#define YGGDRASILL_ICON_CUBE                             "\ue105"
#define YGGDRASILL_ICON_CUBES                            "\ue106"
#define YGGDRASILL_ICON_CUT                              "\ue107"
#define YGGDRASILL_ICON_DATABASE                         "\ue108"
#define YGGDRASILL_ICON_DEAF                             "\ue109"
#define YGGDRASILL_ICON_DEMOCRAT                         "\ue10a"
#define YGGDRASILL_ICON_DESKTOP                          "\ue10b"
#define YGGDRASILL_ICON_DHARMACHAKRA                     "\ue10c"
#define YGGDRASILL_ICON_DIAGNOSES                        "\ue10d"
#define YGGDRASILL_ICON_DICE_D20                         "\ue10e"
#define YGGDRASILL_ICON_DICE_D6                          "\ue10f"
#define YGGDRASILL_ICON_DICE_FIVE                        "\ue110"
#define YGGDRASILL_ICON_DICE_FOUR                        "\ue111"
#define YGGDRASILL_ICON_DICE_ONE                         "\ue112"
#define YGGDRASILL_ICON_DICE_SIX                         "\ue113"
#define YGGDRASILL_ICON_DICE_THREE                       "\ue114"
#define YGGDRASILL_ICON_DICE_TWO                         "\ue115"
#define YGGDRASILL_ICON_DICE                             "\ue116"
#define YGGDRASILL_ICON_DIGITAL_TACHOGRAPH               "\ue117"
#define YGGDRASILL_ICON_DIRECTIONS                       "\ue118"
#define YGGDRASILL_ICON_DISEASE                          "\ue119"
#define YGGDRASILL_ICON_DIVIDE                           "\ue11a"
#define YGGDRASILL_ICON_DIZZY                            "\ue11b"
#define YGGDRASILL_ICON_DNA                              "\ue11c"
#define YGGDRASILL_ICON_DOG                              "\ue11d"
#define YGGDRASILL_ICON_DOLLAR_SIGN                      "\ue11e"
#define YGGDRASILL_ICON_DOLLY_FLATBED                    "\ue11f"
#define YGGDRASILL_ICON_DOLLY                            "\ue120"
#define YGGDRASILL_ICON_DONATE                           "\ue121"
#define YGGDRASILL_ICON_DOOR_CLOSED                      "\ue122"
#define YGGDRASILL_ICON_DOOR_OPEN                        "\ue123"
#define YGGDRASILL_ICON_DOT_CIRCLE                       "\ue124"
#define YGGDRASILL_ICON_DOVE                             "\ue125"
#define YGGDRASILL_ICON_DOWNLOAD                         "\ue126"
#define YGGDRASILL_ICON_DRAFTING_COMPASS                 "\ue127"
#define YGGDRASILL_ICON_DRAGON                           "\ue128"
#define YGGDRASILL_ICON_DRAW_POLYGON                     "\ue129"
#define YGGDRASILL_ICON_DRUM_STEELPAN                    "\ue12a"
#define YGGDRASILL_ICON_DRUM                             "\ue12b"
#define YGGDRASILL_ICON_DRUMSTICK_BITE                   "\ue12c"
#define YGGDRASILL_ICON_DUMBBELL                         "\ue12d"
#define YGGDRASILL_ICON_DUMPSTER_FIRE                    "\ue12e"
#define YGGDRASILL_ICON_DUMPSTER                         "\ue12f"
#define YGGDRASILL_ICON_DUNGEON                          "\ue130"
#define YGGDRASILL_ICON_EDIT                             "\ue131"
#define YGGDRASILL_ICON_EGG                              "\ue132"
#define YGGDRASILL_ICON_EJECT                            "\ue133"
#define YGGDRASILL_ICON_ELLIPSIS_H                       "\ue134"
#define YGGDRASILL_ICON_ELLIPSIS_V                       "\ue135"
#define YGGDRASILL_ICON_ENVELOPE_OPEN_TEXT               "\ue136"
#define YGGDRASILL_ICON_ENVELOPE_OPEN                    "\ue137"
#define YGGDRASILL_ICON_ENVELOPE_SQUARE                  "\ue138"
#define YGGDRASILL_ICON_ENVELOPE                         "\ue139"
#define YGGDRASILL_ICON_EQUALS                           "\ue13a"
#define YGGDRASILL_ICON_ERASER                           "\ue13b"
#define YGGDRASILL_ICON_ETHERNET                         "\ue13c"
#define YGGDRASILL_ICON_EURO_SIGN                        "\ue13d"
#define YGGDRASILL_ICON_EXCHANGE_ALT                     "\ue13e"
#define YGGDRASILL_ICON_EXCLAMATION_CIRCLE               "\ue13f"
#define YGGDRASILL_ICON_EXCLAMATION_TRIANGLE             "\ue140"
#define YGGDRASILL_ICON_EXCLAMATION                      "\ue141"
#define YGGDRASILL_ICON_EXPAND_ALT                       "\ue142"
#define YGGDRASILL_ICON_EXPAND_ARROWS_ALT                "\ue143"
#define YGGDRASILL_ICON_EXPAND                           "\ue144"
#define YGGDRASILL_ICON_EXTERNAL_LINK_ALT                "\ue145"
#define YGGDRASILL_ICON_EXTERNAL_LINK_SQUARE_ALT         "\ue146"
#define YGGDRASILL_ICON_EYE_DROPPER                      "\ue147"
#define YGGDRASILL_ICON_EYE_SLASH                        "\ue148"
#define YGGDRASILL_ICON_EYE                              "\ue149"
#define YGGDRASILL_ICON_FAN                              "\ue14a"
#define YGGDRASILL_ICON_FAST_BACKWARD                    "\ue14b"
#define YGGDRASILL_ICON_FAST_FORWARD                     "\ue14c"
#define YGGDRASILL_ICON_FAUCET                           "\ue14d"
#define YGGDRASILL_ICON_FAX                              "\ue14e"
#define YGGDRASILL_ICON_FEATHER_ALT                      "\ue14f"
#define YGGDRASILL_ICON_FEATHER                          "\ue150"
#define YGGDRASILL_ICON_FEMALE                           "\ue151"
#define YGGDRASILL_ICON_FIGHTER_JET                      "\ue152"
#define YGGDRASILL_ICON_FILE_ALT                         "\ue153"
#define YGGDRASILL_ICON_FILE_ARCHIVE                     "\ue154"
#define YGGDRASILL_ICON_FILE_AUDIO                       "\ue155"
#define YGGDRASILL_ICON_FILE_CODE                        "\ue156"
#define YGGDRASILL_ICON_FILE_CONTRACT                    "\ue157"
#define YGGDRASILL_ICON_FILE_CSV                         "\ue158"
#define YGGDRASILL_ICON_FILE_DOWNLOAD                    "\ue159"
#define YGGDRASILL_ICON_FILE_EXCEL                       "\ue15a"
#define YGGDRASILL_ICON_FILE_EXPORT                      "\ue15b"
#define YGGDRASILL_ICON_FILE_IMAGE                       "\ue15c"
#define YGGDRASILL_ICON_FILE_IMPORT                      "\ue15d"
#define YGGDRASILL_ICON_FILE_INVOICE_DOLLAR              "\ue15e"
#define YGGDRASILL_ICON_FILE_INVOICE                     "\ue15f"
#define YGGDRASILL_ICON_FILE_MEDICAL_ALT                 "\ue160"
#define YGGDRASILL_ICON_FILE_MEDICAL                     "\ue161"
#define YGGDRASILL_ICON_FILE_PDF                         "\ue162"
#define YGGDRASILL_ICON_FILE_POWERPOINT                  "\ue163"
#define YGGDRASILL_ICON_FILE_PRESCRIPTION                "\ue164"
#define YGGDRASILL_ICON_FILE_SIGNATURE                   "\ue165"
#define YGGDRASILL_ICON_FILE_UPLOAD                      "\ue166"
#define YGGDRASILL_ICON_FILE_VIDEO                       "\ue167"
#define YGGDRASILL_ICON_FILE_WORD                        "\ue168"
#define YGGDRASILL_ICON_FILE                             "\ue169"
#define YGGDRASILL_ICON_FILL_DRIP                        "\ue16a"
#define YGGDRASILL_ICON_FILL                             "\ue16b"
#define YGGDRASILL_ICON_FILM                             "\ue16c"
#define YGGDRASILL_ICON_FILTER                           "\ue16d"
#define YGGDRASILL_ICON_FINGERPRINT                      "\ue16e"
#define YGGDRASILL_ICON_FIRE_ALT                         "\ue16f"
#define YGGDRASILL_ICON_FIRE_EXTINGUISHER                "\ue170"
#define YGGDRASILL_ICON_FIRE                             "\ue171"
#define YGGDRASILL_ICON_FIRST_AID                        "\ue172"
#define YGGDRASILL_ICON_FISH                             "\ue173"
#define YGGDRASILL_ICON_FIST_RAISED                      "\ue174"
#define YGGDRASILL_ICON_FLAG_CHECKERED                   "\ue175"
#define YGGDRASILL_ICON_FLAG_USA                         "\ue176"
#define YGGDRASILL_ICON_FLAG                             "\ue177"
#define YGGDRASILL_ICON_FLASK                            "\ue178"
#define YGGDRASILL_ICON_FLUSHED                          "\ue179"
#define YGGDRASILL_ICON_FOLDER_MINUS                     "\ue17a"
#define YGGDRASILL_ICON_FOLDER_OPEN                      "\ue17b"
#define YGGDRASILL_ICON_FOLDER_PLUS                      "\ue17c"
#define YGGDRASILL_ICON_FOLDER                           "\ue17d"
#define YGGDRASILL_ICON_FONT_AWESOME_LOGO_FULL           "\ue17e"
#define YGGDRASILL_ICON_FONT                             "\ue17f"
#define YGGDRASILL_ICON_FOOTBALL_BALL                    "\ue180"
#define YGGDRASILL_ICON_FORWARD                          "\ue181"
#define YGGDRASILL_ICON_FROG                             "\ue182"
#define YGGDRASILL_ICON_FROWN_OPEN                       "\ue183"
#define YGGDRASILL_ICON_FROWN                            "\ue184"
#define YGGDRASILL_ICON_FUNNEL_DOLLAR                    "\ue185"
#define YGGDRASILL_ICON_FUTBOL                           "\ue186"
#define YGGDRASILL_ICON_GAMEPAD                          "\ue187"
#define YGGDRASILL_ICON_GAS_PUMP                         "\ue188"
#define YGGDRASILL_ICON_GAVEL                            "\ue189"
#define YGGDRASILL_ICON_GEM                              "\ue18a"
#define YGGDRASILL_ICON_GENDERLESS                       "\ue18b"
#define YGGDRASILL_ICON_GHOST                            "\ue18c"
#define YGGDRASILL_ICON_GIFT                             "\ue18d"
#define YGGDRASILL_ICON_GIFTS                            "\ue18e"
#define YGGDRASILL_ICON_GLASS_CHEERS                     "\ue18f"
#define YGGDRASILL_ICON_GLASS_MARTINI_ALT                "\ue190"
#define YGGDRASILL_ICON_GLASS_MARTINI                    "\ue191"
#define YGGDRASILL_ICON_GLASS_WHISKEY                    "\ue192"
#define YGGDRASILL_ICON_GLASSES                          "\ue193"
#define YGGDRASILL_ICON_GLOBE_AFRICA                     "\ue194"
#define YGGDRASILL_ICON_GLOBE_AMERICAS                   "\ue195"
#define YGGDRASILL_ICON_GLOBE_ASIA                       "\ue196"
#define YGGDRASILL_ICON_GLOBE_EUROPE                     "\ue197"
#define YGGDRASILL_ICON_GLOBE                            "\ue198"
#define YGGDRASILL_ICON_GOLF_BALL                        "\ue199"
#define YGGDRASILL_ICON_GOPURAM                          "\ue19a"
#define YGGDRASILL_ICON_GRADUATION_CAP                   "\ue19b"
#define YGGDRASILL_ICON_GREATER_THAN_EQUAL               "\ue19c"
#define YGGDRASILL_ICON_GREATER_THAN                     "\ue19d"
#define YGGDRASILL_ICON_GRIMACE                          "\ue19e"
#define YGGDRASILL_ICON_GRIN_ALT                         "\ue19f"
#define YGGDRASILL_ICON_GRIN_BEAM_SWEAT                  "\ue1a0"
#define YGGDRASILL_ICON_GRIN_BEAM                        "\ue1a1"
#define YGGDRASILL_ICON_GRIN_HEARTS                      "\ue1a2"
#define YGGDRASILL_ICON_GRIN_SQUINT_TEARS                "\ue1a3"
#define YGGDRASILL_ICON_GRIN_SQUINT                      "\ue1a4"
#define YGGDRASILL_ICON_GRIN_STARS                       "\ue1a5"
#define YGGDRASILL_ICON_GRIN_TEARS                       "\ue1a6"
#define YGGDRASILL_ICON_GRIN_TONGUE_SQUINT               "\ue1a7"
#define YGGDRASILL_ICON_GRIN_TONGUE_WINK                 "\ue1a8"
#define YGGDRASILL_ICON_GRIN_TONGUE                      "\ue1a9"
#define YGGDRASILL_ICON_GRIN_WINK                        "\ue1aa"
#define YGGDRASILL_ICON_GRIN                             "\ue1ab"
#define YGGDRASILL_ICON_GRIP_HORIZONTAL                  "\ue1ac"
#define YGGDRASILL_ICON_GRIP_LINES_VERTICAL              "\ue1ad"
#define YGGDRASILL_ICON_GRIP_LINES                       "\ue1ae"
#define YGGDRASILL_ICON_GRIP_VERTICAL                    "\ue1af"
#define YGGDRASILL_ICON_GUITAR                           "\ue1b0"
#define YGGDRASILL_ICON_H_SQUARE                         "\ue1b1"
#define YGGDRASILL_ICON_HAMBURGER                        "\ue1b2"
#define YGGDRASILL_ICON_HAMMER                           "\ue1b3"
#define YGGDRASILL_ICON_HAMSA                            "\ue1b4"
#define YGGDRASILL_ICON_HAND_HOLDING_HEART               "\ue1b5"
#define YGGDRASILL_ICON_HAND_HOLDING_MEDICAL             "\ue1b6"
#define YGGDRASILL_ICON_HAND_HOLDING_USD                 "\ue1b7"
#define YGGDRASILL_ICON_HAND_HOLDING_WATER               "\ue1b8"
#define YGGDRASILL_ICON_HAND_HOLDING                     "\ue1b9"
#define YGGDRASILL_ICON_HAND_LIZARD                      "\ue1ba"
#define YGGDRASILL_ICON_HAND_MIDDLE_FINGER               "\ue1bb"
#define YGGDRASILL_ICON_HAND_PAPER                       "\ue1bc"
#define YGGDRASILL_ICON_HAND_PEACE                       "\ue1bd"
#define YGGDRASILL_ICON_HAND_POINT_DOWN                  "\ue1be"
#define YGGDRASILL_ICON_HAND_POINT_LEFT                  "\ue1bf"
#define YGGDRASILL_ICON_HAND_POINT_RIGHT                 "\ue1c0"
#define YGGDRASILL_ICON_HAND_POINT_UP                    "\ue1c1"
#define YGGDRASILL_ICON_HAND_POINTER                     "\ue1c2"
#define YGGDRASILL_ICON_HAND_ROCK                        "\ue1c3"
#define YGGDRASILL_ICON_HAND_SCISSORS                    "\ue1c4"
#define YGGDRASILL_ICON_HAND_SPARKLES                    "\ue1c5"
#define YGGDRASILL_ICON_HAND_SPOCK                       "\ue1c6"
#define YGGDRASILL_ICON_HANDS_HELPING                    "\ue1c7"
#define YGGDRASILL_ICON_HANDS_WASH                       "\ue1c8"
#define YGGDRASILL_ICON_HANDS                            "\ue1c9"
#define YGGDRASILL_ICON_HANDSHAKE_ALT_SLASH              "\ue1ca"
#define YGGDRASILL_ICON_HANDSHAKE_SLASH                  "\ue1cb"
#define YGGDRASILL_ICON_HANDSHAKE                        "\ue1cc"
#define YGGDRASILL_ICON_HANUKIAH                         "\ue1cd"
#define YGGDRASILL_ICON_HARD_HAT                         "\ue1ce"
#define YGGDRASILL_ICON_HASHTAG                          "\ue1cf"
#define YGGDRASILL_ICON_HAT_COWBOY_SIDE                  "\ue1d0"
#define YGGDRASILL_ICON_HAT_COWBOY                       "\ue1d1"
#define YGGDRASILL_ICON_HAT_WIZARD                       "\ue1d2"
#define YGGDRASILL_ICON_HDD                              "\ue1d3"
#define YGGDRASILL_ICON_HEAD_SIDE_COUGH_SLASH            "\ue1d4"
#define YGGDRASILL_ICON_HEAD_SIDE_COUGH                  "\ue1d5"
#define YGGDRASILL_ICON_HEAD_SIDE_MASK                   "\ue1d6"
#define YGGDRASILL_ICON_HEAD_SIDE_VIRUS                  "\ue1d7"
#define YGGDRASILL_ICON_HEADING                          "\ue1d8"
#define YGGDRASILL_ICON_HEADPHONES_ALT                   "\ue1d9"
#define YGGDRASILL_ICON_HEADPHONES                       "\ue1da"
#define YGGDRASILL_ICON_HEADSET                          "\ue1db"
#define YGGDRASILL_ICON_HEART_BROKEN                     "\ue1dc"
#define YGGDRASILL_ICON_HEART                            "\ue1dd"
#define YGGDRASILL_ICON_HEARTBEAT                        "\ue1de"
#define YGGDRASILL_ICON_HELICOPTER                       "\ue1df"
#define YGGDRASILL_ICON_HIGHLIGHTER                      "\ue1e0"
#define YGGDRASILL_ICON_HIKING                           "\ue1e1"
#define YGGDRASILL_ICON_HIPPO                            "\ue1e2"
#define YGGDRASILL_ICON_HISTORY                          "\ue1e3"
#define YGGDRASILL_ICON_HOCKEY_PUCK                      "\ue1e4"
#define YGGDRASILL_ICON_HOLLY_BERRY                      "\ue1e5"
#define YGGDRASILL_ICON_HOME                             "\ue1e6"
#define YGGDRASILL_ICON_HORSE_HEAD                       "\ue1e7"
#define YGGDRASILL_ICON_HORSE                            "\ue1e8"
#define YGGDRASILL_ICON_HOSPITAL_ALT                     "\ue1e9"
#define YGGDRASILL_ICON_HOSPITAL_SYMBOL                  "\ue1ea"
#define YGGDRASILL_ICON_HOSPITAL_USER                    "\ue1eb"
#define YGGDRASILL_ICON_HOSPITAL                         "\ue1ec"
#define YGGDRASILL_ICON_HOT_TUB                          "\ue1ed"
#define YGGDRASILL_ICON_HOTDOG                           "\ue1ee"
#define YGGDRASILL_ICON_HOTEL                            "\ue1ef"
#define YGGDRASILL_ICON_HOURGLASS_END                    "\ue1f0"
#define YGGDRASILL_ICON_HOURGLASS_HALF                   "\ue1f1"
#define YGGDRASILL_ICON_HOURGLASS_START                  "\ue1f2"
#define YGGDRASILL_ICON_HOURGLASS                        "\ue1f3"
#define YGGDRASILL_ICON_HOUSE_DAMAGE                     "\ue1f4"
#define YGGDRASILL_ICON_HOUSE_USER                       "\ue1f5"
#define YGGDRASILL_ICON_HRYVNIA                          "\ue1f6"
#define YGGDRASILL_ICON_I_CURSOR                         "\ue1f7"
#define YGGDRASILL_ICON_ICE_CREAM                        "\ue1f8"
#define YGGDRASILL_ICON_ICICLES                          "\ue1f9"
#define YGGDRASILL_ICON_ICONS                            "\ue1fa"
#define YGGDRASILL_ICON_ID_BADGE                         "\ue1fb"
#define YGGDRASILL_ICON_ID_CARD_ALT                      "\ue1fc"
#define YGGDRASILL_ICON_ID_CARD                          "\ue1fd"
#define YGGDRASILL_ICON_IGLOO                            "\ue1fe"
#define YGGDRASILL_ICON_IMAGE                            "\ue1ff"
#define YGGDRASILL_ICON_IMAGES                           "\ue200"
#define YGGDRASILL_ICON_INBOX                            "\ue201"
#define YGGDRASILL_ICON_INDENT                           "\ue202"
#define YGGDRASILL_ICON_INDUSTRY                         "\ue203"
#define YGGDRASILL_ICON_INFINITY                         "\ue204"
#define YGGDRASILL_ICON_INFO_CIRCLE                      "\ue205"
#define YGGDRASILL_ICON_INFO                             "\ue206"
#define YGGDRASILL_ICON_ITALIC                           "\ue207"
#define YGGDRASILL_ICON_JEDI                             "\ue208"
#define YGGDRASILL_ICON_JOINT                            "\ue209"
#define YGGDRASILL_ICON_JOURNAL_WHILLS                   "\ue20a"
#define YGGDRASILL_ICON_KAABA                            "\ue20b"
#define YGGDRASILL_ICON_KEY                              "\ue20c"
#define YGGDRASILL_ICON_KEYBOARD                         "\ue20d"
#define YGGDRASILL_ICON_KHANDA                           "\ue20e"
#define YGGDRASILL_ICON_KISS_BEAM                        "\ue20f"
#define YGGDRASILL_ICON_KISS_WINK_HEART                  "\ue210"
#define YGGDRASILL_ICON_KISS                             "\ue211"
#define YGGDRASILL_ICON_KIWI_BIRD                        "\ue212"
#define YGGDRASILL_ICON_LANDMARK                         "\ue213"
#define YGGDRASILL_ICON_LANGUAGE                         "\ue214"
#define YGGDRASILL_ICON_LAPTOP_CODE                      "\ue215"
#define YGGDRASILL_ICON_LAPTOP_HOUSE                     "\ue216"
#define YGGDRASILL_ICON_LAPTOP_MEDICAL                   "\ue217"
#define YGGDRASILL_ICON_LAPTOP                           "\ue218"
#define YGGDRASILL_ICON_LAUGH_BEAM                       "\ue219"
#define YGGDRASILL_ICON_LAUGH_SQUINT                     "\ue21a"
#define YGGDRASILL_ICON_LAUGH_WINK                       "\ue21b"
#define YGGDRASILL_ICON_LAUGH                            "\ue21c"
#define YGGDRASILL_ICON_LAYER_GROUP                      "\ue21d"
#define YGGDRASILL_ICON_LEAF                             "\ue21e"
#define YGGDRASILL_ICON_LEMON                            "\ue21f"
#define YGGDRASILL_ICON_LESS_THAN_EQUAL                  "\ue220"
#define YGGDRASILL_ICON_LESS_THAN                        "\ue221"
#define YGGDRASILL_ICON_LEVEL_DOWN_ALT                   "\ue222"
#define YGGDRASILL_ICON_LEVEL_UP_ALT                     "\ue223"
#define YGGDRASILL_ICON_LIFE_RING                        "\ue224"
#define YGGDRASILL_ICON_LIGHTBULB                        "\ue225"
#define YGGDRASILL_ICON_LINK                             "\ue226"
#define YGGDRASILL_ICON_LIRA_SIGN                        "\ue227"
#define YGGDRASILL_ICON_LIST_ALT                         "\ue228"
#define YGGDRASILL_ICON_LIST_OL                          "\ue229"
#define YGGDRASILL_ICON_LIST_UL                          "\ue22a"
#define YGGDRASILL_ICON_LIST                             "\ue22b"
#define YGGDRASILL_ICON_LOCATION_ARROW                   "\ue22c"
#define YGGDRASILL_ICON_LOCK_OPEN                        "\ue22d"
#define YGGDRASILL_ICON_LOCK                             "\ue22e"
#define YGGDRASILL_ICON_LONG_ARROW_ALT_DOWN              "\ue22f"
#define YGGDRASILL_ICON_LONG_ARROW_ALT_LEFT              "\ue230"
#define YGGDRASILL_ICON_LONG_ARROW_ALT_RIGHT             "\ue231"
#define YGGDRASILL_ICON_LONG_ARROW_ALT_UP                "\ue232"
#define YGGDRASILL_ICON_LOW_VISION                       "\ue233"
#define YGGDRASILL_ICON_LUGGAGE_CART                     "\ue234"
#define YGGDRASILL_ICON_LUNGS_VIRUS                      "\ue235"
#define YGGDRASILL_ICON_LUNGS                            "\ue236"
#define YGGDRASILL_ICON_MAGIC                            "\ue237"
#define YGGDRASILL_ICON_MAGNET                           "\ue238"
#define YGGDRASILL_ICON_MAIL_BULK                        "\ue239"
#define YGGDRASILL_ICON_MALE                             "\ue23a"
#define YGGDRASILL_ICON_MAP_MARKED_ALT                   "\ue23b"
#define YGGDRASILL_ICON_MAP_MARKED                       "\ue23c"
#define YGGDRASILL_ICON_MAP_MARKER_ALT                   "\ue23d"
#define YGGDRASILL_ICON_MAP_MARKER                       "\ue23e"
#define YGGDRASILL_ICON_MAP_PIN                          "\ue23f"
#define YGGDRASILL_ICON_MAP_SIGNS                        "\ue240"
#define YGGDRASILL_ICON_MAP                              "\ue241"
#define YGGDRASILL_ICON_MARKER                           "\ue242"
#define YGGDRASILL_ICON_MARS_DOUBLE                      "\ue243"
#define YGGDRASILL_ICON_MARS_STROKE_H                    "\ue244"
#define YGGDRASILL_ICON_MARS_STROKE_V                    "\ue245"
#define YGGDRASILL_ICON_MARS_STROKE                      "\ue246"
#define YGGDRASILL_ICON_MARS                             "\ue247"
#define YGGDRASILL_ICON_MASK                             "\ue248"
#define YGGDRASILL_ICON_MEDAL                            "\ue249"
#define YGGDRASILL_ICON_MEDKIT                           "\ue24a"
#define YGGDRASILL_ICON_MEH_BLANK                        "\ue24b"
#define YGGDRASILL_ICON_MEH_ROLLING_EYES                 "\ue24c"
#define YGGDRASILL_ICON_MEH                              "\ue24d"
#define YGGDRASILL_ICON_MEMORY                           "\ue24e"
#define YGGDRASILL_ICON_MENORAH                          "\ue24f"
#define YGGDRASILL_ICON_MERCURY                          "\ue250"
#define YGGDRASILL_ICON_METEOR                           "\ue251"
#define YGGDRASILL_ICON_MICROCHIP                        "\ue252"
#define YGGDRASILL_ICON_MICROPHONE_ALT_SLASH             "\ue253"
#define YGGDRASILL_ICON_MICROPHONE_ALT                   "\ue254"
#define YGGDRASILL_ICON_MICROPHONE_SLASH                 "\ue255"
#define YGGDRASILL_ICON_MICROPHONE                       "\ue256"
#define YGGDRASILL_ICON_MICROSCOPE                       "\ue257"
#define YGGDRASILL_ICON_MINUS_CIRCLE                     "\ue258"
#define YGGDRASILL_ICON_MINUS_SQUARE                     "\ue259"
#define YGGDRASILL_ICON_MINUS                            "\ue25a"
#define YGGDRASILL_ICON_MITTEN                           "\ue25b"
#define YGGDRASILL_ICON_MOBILE_ALT                       "\ue25c"
#define YGGDRASILL_ICON_MOBILE                           "\ue25d"
#define YGGDRASILL_ICON_MONEY_BILL_ALT                   "\ue25e"
#define YGGDRASILL_ICON_MONEY_BILL_WAVE_ALT              "\ue25f"
#define YGGDRASILL_ICON_MONEY_BILL_WAVE                  "\ue260"
#define YGGDRASILL_ICON_MONEY_BILL                       "\ue261"
#define YGGDRASILL_ICON_MONEY_CHECK_ALT                  "\ue262"
#define YGGDRASILL_ICON_MONEY_CHECK                      "\ue263"
#define YGGDRASILL_ICON_MONUMENT                         "\ue264"
#define YGGDRASILL_ICON_MOON                             "\ue265"
#define YGGDRASILL_ICON_MORTAR_PESTLE                    "\ue266"
#define YGGDRASILL_ICON_MOSQUE                           "\ue267"
#define YGGDRASILL_ICON_MOTORCYCLE                       "\ue268"
#define YGGDRASILL_ICON_MOUNTAIN                         "\ue269"
#define YGGDRASILL_ICON_MOUSE_POINTER                    "\ue26a"
#define YGGDRASILL_ICON_MOUSE                            "\ue26b"
#define YGGDRASILL_ICON_MUG_HOT                          "\ue26c"
#define YGGDRASILL_ICON_MUSIC                            "\ue26d"
#define YGGDRASILL_ICON_NETWORK_WIRED                    "\ue26e"
#define YGGDRASILL_ICON_NEUTER                           "\ue26f"
#define YGGDRASILL_ICON_NEWSPAPER                        "\ue270"
#define YGGDRASILL_ICON_NOT_EQUAL                        "\ue271"
#define YGGDRASILL_ICON_NOTES_MEDICAL                    "\ue272"
#define YGGDRASILL_ICON_OBJECT_GROUP                     "\ue273"
#define YGGDRASILL_ICON_OBJECT_UNGROUP                   "\ue274"
#define YGGDRASILL_ICON_OIL_CAN                          "\ue275"
#define YGGDRASILL_ICON_OM                               "\ue276"
#define YGGDRASILL_ICON_OTTER                            "\ue277"
#define YGGDRASILL_ICON_OUTDENT                          "\ue278"
#define YGGDRASILL_ICON_PAGER                            "\ue279"
#define YGGDRASILL_ICON_PAINT_BRUSH                      "\ue27a"
#define YGGDRASILL_ICON_PAINT_ROLLER                     "\ue27b"
#define YGGDRASILL_ICON_PALETTE                          "\ue27c"
#define YGGDRASILL_ICON_PALLET                           "\ue27d"
#define YGGDRASILL_ICON_PAPER_PLANE                      "\ue27e"
#define YGGDRASILL_ICON_PAPERCLIP                        "\ue27f"
#define YGGDRASILL_ICON_PARACHUTE_BOX                    "\ue280"
#define YGGDRASILL_ICON_PARAGRAPH                        "\ue281"
#define YGGDRASILL_ICON_PARKING                          "\ue282"
#define YGGDRASILL_ICON_PASSPORT                         "\ue283"
#define YGGDRASILL_ICON_PASTAFARIANISM                   "\ue284"
#define YGGDRASILL_ICON_PASTE                            "\ue285"
#define YGGDRASILL_ICON_PAUSE_CIRCLE                     "\ue286"
#define YGGDRASILL_ICON_PAUSE                            "\ue287"
#define YGGDRASILL_ICON_PAW                              "\ue288"
#define YGGDRASILL_ICON_PEACE                            "\ue289"
#define YGGDRASILL_ICON_PEN_ALT                          "\ue28a"
#define YGGDRASILL_ICON_PEN_FANCY                        "\ue28b"
#define YGGDRASILL_ICON_PEN_NIB                          "\ue28c"
#define YGGDRASILL_ICON_PEN_SQUARE                       "\ue28d"
#define YGGDRASILL_ICON_PEN                              "\ue28e"
#define YGGDRASILL_ICON_PENCIL_ALT                       "\ue28f"
#define YGGDRASILL_ICON_PENCIL_RULER                     "\ue290"
#define YGGDRASILL_ICON_PEOPLE_ARROWS                    "\ue291"
#define YGGDRASILL_ICON_PEOPLE_CARRY                     "\ue292"
#define YGGDRASILL_ICON_PEPPER_HOT                       "\ue293"
#define YGGDRASILL_ICON_PERCENT                          "\ue294"
#define YGGDRASILL_ICON_PERCENTAGE                       "\ue295"
#define YGGDRASILL_ICON_PERSON_BOOTH                     "\ue296"
#define YGGDRASILL_ICON_PHONE_ALT                        "\ue297"
#define YGGDRASILL_ICON_PHONE_SLASH                      "\ue298"
#define YGGDRASILL_ICON_PHONE_SQUARE_ALT                 "\ue299"
#define YGGDRASILL_ICON_PHONE_SQUARE                     "\ue29a"
#define YGGDRASILL_ICON_PHONE_VOLUME                     "\ue29b"
#define YGGDRASILL_ICON_PHONE                            "\ue29c"
#define YGGDRASILL_ICON_PHOTO_VIDEO                      "\ue29d"
#define YGGDRASILL_ICON_PIGGY_BANK                       "\ue29e"
#define YGGDRASILL_ICON_PILLS                            "\ue29f"
#define YGGDRASILL_ICON_PIZZA_SLICE                      "\ue2a0"
#define YGGDRASILL_ICON_PLACE_OF_WORSHIP                 "\ue2a1"
#define YGGDRASILL_ICON_PLANE_ARRIVAL                    "\ue2a2"
#define YGGDRASILL_ICON_PLANE_DEPARTURE                  "\ue2a3"
#define YGGDRASILL_ICON_PLANE_SLASH                      "\ue2a4"
#define YGGDRASILL_ICON_PLANE                            "\ue2a5"
#define YGGDRASILL_ICON_PLAY_CIRCLE                      "\ue2a6"
#define YGGDRASILL_ICON_PLAY                             "\ue2a7"
#define YGGDRASILL_ICON_PLUG                             "\ue2a8"
#define YGGDRASILL_ICON_PLUS_CIRCLE                      "\ue2a9"
#define YGGDRASILL_ICON_PLUS_SQUARE                      "\ue2aa"
#define YGGDRASILL_ICON_PLUS                             "\ue2ab"
#define YGGDRASILL_ICON_PODCAST                          "\ue2ac"
#define YGGDRASILL_ICON_POLL_H                           "\ue2ad"
#define YGGDRASILL_ICON_POLL                             "\ue2ae"
#define YGGDRASILL_ICON_POO_STORM                        "\ue2af"
#define YGGDRASILL_ICON_POO                              "\ue2b0"
#define YGGDRASILL_ICON_POOP                             "\ue2b1"
#define YGGDRASILL_ICON_PORTRAIT                         "\ue2b2"
#define YGGDRASILL_ICON_POUND_SIGN                       "\ue2b3"
#define YGGDRASILL_ICON_POWER_OFF                        "\ue2b4"
#define YGGDRASILL_ICON_PRAY                             "\ue2b5"
#define YGGDRASILL_ICON_PRAYING_HANDS                    "\ue2b6"
#define YGGDRASILL_ICON_PRESCRIPTION_BOTTLE_ALT          "\ue2b7"
#define YGGDRASILL_ICON_PRESCRIPTION_BOTTLE              "\ue2b8"
#define YGGDRASILL_ICON_PRESCRIPTION                     "\ue2b9"
#define YGGDRASILL_ICON_PRINT                            "\ue2ba"
#define YGGDRASILL_ICON_PROCEDURES                       "\ue2bb"
#define YGGDRASILL_ICON_PROJECT_DIAGRAM                  "\ue2bc"
#define YGGDRASILL_ICON_PUMP_MEDICAL                     "\ue2bd"
#define YGGDRASILL_ICON_PUMP_SOAP                        "\ue2be"
#define YGGDRASILL_ICON_PUZZLE_PIECE                     "\ue2bf"
#define YGGDRASILL_ICON_QRCODE                           "\ue2c0"
#define YGGDRASILL_ICON_QUESTION_CIRCLE                  "\ue2c1"
#define YGGDRASILL_ICON_QUESTION                         "\ue2c2"
#define YGGDRASILL_ICON_QUIDDITCH                        "\ue2c3"
#define YGGDRASILL_ICON_QUOTE_LEFT                       "\ue2c4"
#define YGGDRASILL_ICON_QUOTE_RIGHT                      "\ue2c5"
#define YGGDRASILL_ICON_QURAN                            "\ue2c6"
#define YGGDRASILL_ICON_RADIATION_ALT                    "\ue2c7"
#define YGGDRASILL_ICON_RADIATION                        "\ue2c8"
#define YGGDRASILL_ICON_RAINBOW                          "\ue2c9"
#define YGGDRASILL_ICON_RANDOM                           "\ue2ca"
#define YGGDRASILL_ICON_RECEIPT                          "\ue2cb"
#define YGGDRASILL_ICON_RECORD_VINYL                     "\ue2cc"
#define YGGDRASILL_ICON_RECYCLE                          "\ue2cd"
#define YGGDRASILL_ICON_REDO_ALT                         "\ue2ce"
#define YGGDRASILL_ICON_REDO                             "\ue2cf"
#define YGGDRASILL_ICON_REGISTERED                       "\ue2d0"
#define YGGDRASILL_ICON_REMOVE_FORMAT                    "\ue2d1"
#define YGGDRASILL_ICON_REPLY_ALL                        "\ue2d2"
#define YGGDRASILL_ICON_REPLY                            "\ue2d3"
#define YGGDRASILL_ICON_REPUBLICAN                       "\ue2d4"
#define YGGDRASILL_ICON_RESTROOM                         "\ue2d5"
#define YGGDRASILL_ICON_RETWEET                          "\ue2d6"
#define YGGDRASILL_ICON_RIBBON                           "\ue2d7"
#define YGGDRASILL_ICON_RING                             "\ue2d8"
#define YGGDRASILL_ICON_ROAD                             "\ue2d9"
#define YGGDRASILL_ICON_ROBOT                            "\ue2da"
#define YGGDRASILL_ICON_ROCKET                           "\ue2db"
#define YGGDRASILL_ICON_ROUTE                            "\ue2dc"
#define YGGDRASILL_ICON_RSS_SQUARE                       "\ue2dd"
#define YGGDRASILL_ICON_RSS                              "\ue2de"
#define YGGDRASILL_ICON_RUBLE_SIGN                       "\ue2df"
#define YGGDRASILL_ICON_RULER_COMBINED                   "\ue2e0"
#define YGGDRASILL_ICON_RULER_HORIZONTAL                 "\ue2e1"
#define YGGDRASILL_ICON_RULER_VERTICAL                   "\ue2e2"
#define YGGDRASILL_ICON_RULER                            "\ue2e3"
#define YGGDRASILL_ICON_RUNNING                          "\ue2e4"
#define YGGDRASILL_ICON_RUPEE_SIGN                       "\ue2e5"
#define YGGDRASILL_ICON_SAD_CRY                          "\ue2e6"
#define YGGDRASILL_ICON_SAD_TEAR                         "\ue2e7"
#define YGGDRASILL_ICON_SATELLITE_DISH                   "\ue2e8"
#define YGGDRASILL_ICON_SATELLITE                        "\ue2e9"
#define YGGDRASILL_ICON_SAVE                             "\ue2ea"
#define YGGDRASILL_ICON_SCHOOL                           "\ue2eb"
#define YGGDRASILL_ICON_SCREWDRIVER                      "\ue2ec"
#define YGGDRASILL_ICON_SCROLL                           "\ue2ed"
#define YGGDRASILL_ICON_SD_CARD                          "\ue2ee"
#define YGGDRASILL_ICON_SEARCH_DOLLAR                    "\ue2ef"
#define YGGDRASILL_ICON_SEARCH_LOCATION                  "\ue2f0"
#define YGGDRASILL_ICON_SEARCH_MINUS                     "\ue2f1"
#define YGGDRASILL_ICON_SEARCH_PLUS                      "\ue2f2"
#define YGGDRASILL_ICON_SEARCH                           "\ue2f3"
#define YGGDRASILL_ICON_SEEDLING                         "\ue2f4"
#define YGGDRASILL_ICON_SERVER                           "\ue2f5"
#define YGGDRASILL_ICON_SHAPES                           "\ue2f6"
#define YGGDRASILL_ICON_SHARE_ALT_SQUARE                 "\ue2f7"
#define YGGDRASILL_ICON_SHARE_ALT                        "\ue2f8"
#define YGGDRASILL_ICON_SHARE_SQUARE                     "\ue2f9"
#define YGGDRASILL_ICON_SHARE                            "\ue2fa"
#define YGGDRASILL_ICON_SHEKEL_SIGN                      "\ue2fb"
#define YGGDRASILL_ICON_SHIELD_ALT                       "\ue2fc"
#define YGGDRASILL_ICON_SHIELD_VIRUS                     "\ue2fd"
#define YGGDRASILL_ICON_SHIP                             "\ue2fe"
#define YGGDRASILL_ICON_SHIPPING_FAST                    "\ue2ff"
#define YGGDRASILL_ICON_SHOE_PRINTS                      "\ue300"
#define YGGDRASILL_ICON_SHOPPING_BAG                     "\ue301"
#define YGGDRASILL_ICON_SHOPPING_BASKET                  "\ue302"
#define YGGDRASILL_ICON_SHOPPING_CART                    "\ue303"
#define YGGDRASILL_ICON_SHOWER                           "\ue304"
#define YGGDRASILL_ICON_SHUTTLE_VAN                      "\ue305"
#define YGGDRASILL_ICON_SIGN_IN_ALT                      "\ue306"
#define YGGDRASILL_ICON_SIGN_LANGUAGE                    "\ue307"
#define YGGDRASILL_ICON_SIGN_OUT_ALT                     "\ue308"
#define YGGDRASILL_ICON_SIGN                             "\ue309"
#define YGGDRASILL_ICON_SIGNAL                           "\ue30a"
#define YGGDRASILL_ICON_SIGNATURE                        "\ue30b"
#define YGGDRASILL_ICON_SIM_CARD                         "\ue30c"
#define YGGDRASILL_ICON_SINK                             "\ue30d"
#define YGGDRASILL_ICON_SITEMAP                          "\ue30e"
#define YGGDRASILL_ICON_SKATING                          "\ue30f"
#define YGGDRASILL_ICON_SKIING_NORDIC                    "\ue310"
#define YGGDRASILL_ICON_SKIING                           "\ue311"
#define YGGDRASILL_ICON_SKULL_CROSSBONES                 "\ue312"
#define YGGDRASILL_ICON_SKULL                            "\ue313"
#define YGGDRASILL_ICON_SLASH                            "\ue314"
#define YGGDRASILL_ICON_SLEIGH                           "\ue315"
#define YGGDRASILL_ICON_SLIDERS_H                        "\ue316"
#define YGGDRASILL_ICON_SMILE_BEAM                       "\ue317"
#define YGGDRASILL_ICON_SMILE_WINK                       "\ue318"
#define YGGDRASILL_ICON_SMILE                            "\ue319"
#define YGGDRASILL_ICON_SMOG                             "\ue31a"
#define YGGDRASILL_ICON_SMOKING_BAN                      "\ue31b"
#define YGGDRASILL_ICON_SMOKING                          "\ue31c"
#define YGGDRASILL_ICON_SMS                              "\ue31d"
#define YGGDRASILL_ICON_SNOWBOARDING                     "\ue31e"
#define YGGDRASILL_ICON_SNOWFLAKE                        "\ue31f"
#define YGGDRASILL_ICON_SNOWMAN                          "\ue320"
#define YGGDRASILL_ICON_SNOWPLOW                         "\ue321"
#define YGGDRASILL_ICON_SOAP                             "\ue322"
#define YGGDRASILL_ICON_SOCKS                            "\ue323"
#define YGGDRASILL_ICON_SOLAR_PANEL                      "\ue324"
#define YGGDRASILL_ICON_SORT_ALPHA_DOWN_ALT              "\ue325"
#define YGGDRASILL_ICON_SORT_ALPHA_DOWN                  "\ue326"
#define YGGDRASILL_ICON_SORT_ALPHA_UP_ALT                "\ue327"
#define YGGDRASILL_ICON_SORT_ALPHA_UP                    "\ue328"
#define YGGDRASILL_ICON_SORT_AMOUNT_DOWN_ALT             "\ue329"
#define YGGDRASILL_ICON_SORT_AMOUNT_DOWN                 "\ue32a"
#define YGGDRASILL_ICON_SORT_AMOUNT_UP_ALT               "\ue32b"
#define YGGDRASILL_ICON_SORT_AMOUNT_UP                   "\ue32c"
#define YGGDRASILL_ICON_SORT_DOWN                        "\ue32d"
#define YGGDRASILL_ICON_SORT_NUMERIC_DOWN_ALT            "\ue32e"
#define YGGDRASILL_ICON_SORT_NUMERIC_DOWN                "\ue32f"
#define YGGDRASILL_ICON_SORT_NUMERIC_UP_ALT              "\ue330"
#define YGGDRASILL_ICON_SORT_NUMERIC_UP                  "\ue331"
#define YGGDRASILL_ICON_SORT_UP                          "\ue332"
#define YGGDRASILL_ICON_SORT                             "\ue333"
#define YGGDRASILL_ICON_SPA                              "\ue334"
#define YGGDRASILL_ICON_SPACE_SHUTTLE                    "\ue335"
#define YGGDRASILL_ICON_SPELL_CHECK                      "\ue336"
#define YGGDRASILL_ICON_SPIDER                           "\ue337"
#define YGGDRASILL_ICON_SPINNER                          "\ue338"
#define YGGDRASILL_ICON_SPLOTCH                          "\ue339"
#define YGGDRASILL_ICON_SPRAY_CAN                        "\ue33a"
#define YGGDRASILL_ICON_SQUARE_FULL                      "\ue33b"
#define YGGDRASILL_ICON_SQUARE_ROOT_ALT                  "\ue33c"
#define YGGDRASILL_ICON_SQUARE                           "\ue33d"
#define YGGDRASILL_ICON_STAMP                            "\ue33e"
#define YGGDRASILL_ICON_STAR_AND_CRESCENT                "\ue33f"
#define YGGDRASILL_ICON_STAR_HALF_ALT                    "\ue340"
#define YGGDRASILL_ICON_STAR_HALF                        "\ue341"
#define YGGDRASILL_ICON_STAR_OF_DAVID                    "\ue342"
#define YGGDRASILL_ICON_STAR_OF_LIFE                     "\ue343"
#define YGGDRASILL_ICON_STAR                             "\ue344"
#define YGGDRASILL_ICON_STEP_BACKWARD                    "\ue345"
#define YGGDRASILL_ICON_STEP_FORWARD                     "\ue346"
#define YGGDRASILL_ICON_STETHOSCOPE                      "\ue347"
#define YGGDRASILL_ICON_STICKY_NOTE                      "\ue348"
#define YGGDRASILL_ICON_STOP_CIRCLE                      "\ue349"
#define YGGDRASILL_ICON_STOP                             "\ue34a"
#define YGGDRASILL_ICON_STOPWATCH_20                     "\ue34b"
#define YGGDRASILL_ICON_STOPWATCH                        "\ue34c"
#define YGGDRASILL_ICON_STORE_ALT_SLASH                  "\ue34d"
#define YGGDRASILL_ICON_STORE_ALT                        "\ue34e"
#define YGGDRASILL_ICON_STORE_SLASH                      "\ue34f"
#define YGGDRASILL_ICON_STORE                            "\ue350"
#define YGGDRASILL_ICON_STREAM                           "\ue351"
#define YGGDRASILL_ICON_STREET_VIEW                      "\ue352"
#define YGGDRASILL_ICON_STRIKETHROUGH                    "\ue353"
#define YGGDRASILL_ICON_STROOPWAFEL                      "\ue354"
#define YGGDRASILL_ICON_SUBSCRIPT                        "\ue355"
#define YGGDRASILL_ICON_SUBWAY                           "\ue356"
#define YGGDRASILL_ICON_SUITCASE_ROLLING                 "\ue357"
#define YGGDRASILL_ICON_SUITCASE                         "\ue358"
#define YGGDRASILL_ICON_SUN                              "\ue359"
#define YGGDRASILL_ICON_SUPERSCRIPT                      "\ue35a"
#define YGGDRASILL_ICON_SURPRISE                         "\ue35b"
#define YGGDRASILL_ICON_SWATCHBOOK                       "\ue35c"
#define YGGDRASILL_ICON_SWIMMER                          "\ue35d"
#define YGGDRASILL_ICON_SWIMMING_POOL                    "\ue35e"
#define YGGDRASILL_ICON_SYNAGOGUE                        "\ue35f"
#define YGGDRASILL_ICON_SYNC_ALT                         "\ue360"
#define YGGDRASILL_ICON_SYNC                             "\ue361"
#define YGGDRASILL_ICON_SYRINGE                          "\ue362"
#define YGGDRASILL_ICON_TABLE_TENNIS                     "\ue363"
#define YGGDRASILL_ICON_TABLE                            "\ue364"
#define YGGDRASILL_ICON_TABLET_ALT                       "\ue365"
#define YGGDRASILL_ICON_TABLET                           "\ue366"
#define YGGDRASILL_ICON_TABLETS                          "\ue367"
#define YGGDRASILL_ICON_TACHOMETER_ALT                   "\ue368"
#define YGGDRASILL_ICON_TAG                              "\ue369"
#define YGGDRASILL_ICON_TAGS                             "\ue36a"
#define YGGDRASILL_ICON_TAPE                             "\ue36b"
#define YGGDRASILL_ICON_TASKS                            "\ue36c"
#define YGGDRASILL_ICON_TAXI                             "\ue36d"
#define YGGDRASILL_ICON_TEETH_OPEN                       "\ue36e"
#define YGGDRASILL_ICON_TEETH                            "\ue36f"
#define YGGDRASILL_ICON_TEMPERATURE_HIGH                 "\ue370"
#define YGGDRASILL_ICON_TEMPERATURE_LOW                  "\ue371"
#define YGGDRASILL_ICON_TENGE                            "\ue372"
#define YGGDRASILL_ICON_TERMINAL                         "\ue373"
#define YGGDRASILL_ICON_TEXT_HEIGHT                      "\ue374"
#define YGGDRASILL_ICON_TEXT_WIDTH                       "\ue375"
#define YGGDRASILL_ICON_TH_LARGE                         "\ue376"
#define YGGDRASILL_ICON_TH_LIST                          "\ue377"
#define YGGDRASILL_ICON_TH                               "\ue378"
#define YGGDRASILL_ICON_THEATER_MASKS                    "\ue379"
#define YGGDRASILL_ICON_THERMOMETER_EMPTY                "\ue37a"
#define YGGDRASILL_ICON_THERMOMETER_FULL                 "\ue37b"
#define YGGDRASILL_ICON_THERMOMETER_HALF                 "\ue37c"
#define YGGDRASILL_ICON_THERMOMETER_QUARTER              "\ue37d"
#define YGGDRASILL_ICON_THERMOMETER_THREE_QUARTERS       "\ue37e"
#define YGGDRASILL_ICON_THERMOMETER                      "\ue37f"
#define YGGDRASILL_ICON_THUMBS_DOWN                      "\ue380"
#define YGGDRASILL_ICON_THUMBS_UP                        "\ue381"
#define YGGDRASILL_ICON_THUMBTACK                        "\ue382"
#define YGGDRASILL_ICON_TICKET_ALT                       "\ue383"
#define YGGDRASILL_ICON_TIMES_CIRCLE                     "\ue384"
#define YGGDRASILL_ICON_TIMES                            "\ue385"
#define YGGDRASILL_ICON_TINT_SLASH                       "\ue386"
#define YGGDRASILL_ICON_TINT                             "\ue387"
#define YGGDRASILL_ICON_TIRED                            "\ue388"
#define YGGDRASILL_ICON_TOGGLE_OFF                       "\ue389"
#define YGGDRASILL_ICON_TOGGLE_ON                        "\ue38a"
#define YGGDRASILL_ICON_TOILET_PAPER_SLASH               "\ue38b"
#define YGGDRASILL_ICON_TOILET_PAPER                     "\ue38c"
#define YGGDRASILL_ICON_TOILET                           "\ue38d"
#define YGGDRASILL_ICON_TOOLBOX                          "\ue38e"
#define YGGDRASILL_ICON_TOOLS                            "\ue38f"
#define YGGDRASILL_ICON_TOOTH                            "\ue390"
#define YGGDRASILL_ICON_TORAH                            "\ue391"
#define YGGDRASILL_ICON_TORII_GATE                       "\ue392"
#define YGGDRASILL_ICON_TRACTOR                          "\ue393"
#define YGGDRASILL_ICON_TRADEMARK                        "\ue394"
#define YGGDRASILL_ICON_TRAFFIC_LIGHT                    "\ue395"
#define YGGDRASILL_ICON_TRAILER                          "\ue396"
#define YGGDRASILL_ICON_TRAIN                            "\ue397"
#define YGGDRASILL_ICON_TRAM                             "\ue398"
#define YGGDRASILL_ICON_TRANSGENDER_ALT                  "\ue399"
#define YGGDRASILL_ICON_TRANSGENDER                      "\ue39a"
#define YGGDRASILL_ICON_TRASH_ALT                        "\ue39b"
#define YGGDRASILL_ICON_TRASH_RESTORE_ALT                "\ue39c"
#define YGGDRASILL_ICON_TRASH_RESTORE                    "\ue39d"
#define YGGDRASILL_ICON_TRASH                            "\ue39e"
#define YGGDRASILL_ICON_TREE                             "\ue39f"
#define YGGDRASILL_ICON_TROPHY                           "\ue3a0"
#define YGGDRASILL_ICON_TRUCK_LOADING                    "\ue3a1"
#define YGGDRASILL_ICON_TRUCK_MONSTER                    "\ue3a2"
#define YGGDRASILL_ICON_TRUCK_MOVING                     "\ue3a3"
#define YGGDRASILL_ICON_TRUCK_PICKUP                     "\ue3a4"
#define YGGDRASILL_ICON_TRUCK                            "\ue3a5"
#define YGGDRASILL_ICON_TSHIRT                           "\ue3a6"
#define YGGDRASILL_ICON_TTY                              "\ue3a7"
#define YGGDRASILL_ICON_TV                               "\ue3a8"
#define YGGDRASILL_ICON_UMBRELLA_BEACH                   "\ue3a9"
#define YGGDRASILL_ICON_UMBRELLA                         "\ue3aa"
#define YGGDRASILL_ICON_UNDERLINE                        "\ue3ab"
#define YGGDRASILL_ICON_UNDO_ALT                         "\ue3ac"
#define YGGDRASILL_ICON_UNDO                             "\ue3ad"
#define YGGDRASILL_ICON_UNIVERSAL_ACCESS                 "\ue3ae"
#define YGGDRASILL_ICON_UNIVERSITY                       "\ue3af"
#define YGGDRASILL_ICON_UNLINK                           "\ue3b0"
#define YGGDRASILL_ICON_UNLOCK_ALT                       "\ue3b1"
#define YGGDRASILL_ICON_UNLOCK                           "\ue3b2"
#define YGGDRASILL_ICON_UPLOAD                           "\ue3b3"
#define YGGDRASILL_ICON_USER_ALT_SLASH                   "\ue3b4"
#define YGGDRASILL_ICON_USER_ALT                         "\ue3b5"
#define YGGDRASILL_ICON_USER_ASTRONAUT                   "\ue3b6"
#define YGGDRASILL_ICON_USER_CHECK                       "\ue3b7"
#define YGGDRASILL_ICON_USER_CIRCLE                      "\ue3b8"
#define YGGDRASILL_ICON_USER_CLOCK                       "\ue3b9"
#define YGGDRASILL_ICON_USER_COG                         "\ue3ba"
#define YGGDRASILL_ICON_USER_EDIT                        "\ue3bb"
#define YGGDRASILL_ICON_USER_FRIENDS                     "\ue3bc"
#define YGGDRASILL_ICON_USER_GRADUATE                    "\ue3bd"
#define YGGDRASILL_ICON_USER_INJURED                     "\ue3be"
#define YGGDRASILL_ICON_USER_LOCK                        "\ue3bf"
#define YGGDRASILL_ICON_USER_MD                          "\ue3c0"
#define YGGDRASILL_ICON_USER_MINUS                       "\ue3c1"
#define YGGDRASILL_ICON_USER_NINJA                       "\ue3c2"
#define YGGDRASILL_ICON_USER_NURSE                       "\ue3c3"
#define YGGDRASILL_ICON_USER_PLUS                        "\ue3c4"
#define YGGDRASILL_ICON_USER_SECRET                      "\ue3c5"
#define YGGDRASILL_ICON_USER_SHIELD                      "\ue3c6"
#define YGGDRASILL_ICON_USER_SLASH                       "\ue3c7"
#define YGGDRASILL_ICON_USER_TAG                         "\ue3c8"
#define YGGDRASILL_ICON_USER_TIE                         "\ue3c9"
#define YGGDRASILL_ICON_USER_TIMES                       "\ue3ca"
#define YGGDRASILL_ICON_USER                             "\ue3cb"
#define YGGDRASILL_ICON_USERS_COG                        "\ue3cc"
#define YGGDRASILL_ICON_USERS_SLASH                      "\ue3cd"
#define YGGDRASILL_ICON_USERS                            "\ue3ce"
#define YGGDRASILL_ICON_UTENSIL_SPOON                    "\ue3cf"
#define YGGDRASILL_ICON_UTENSILS                         "\ue3d0"
#define YGGDRASILL_ICON_VECTOR_SQUARE                    "\ue3d1"
#define YGGDRASILL_ICON_VENUS_DOUBLE                     "\ue3d2"
#define YGGDRASILL_ICON_VENUS_MARS                       "\ue3d3"
#define YGGDRASILL_ICON_VENUS                            "\ue3d4"
#define YGGDRASILL_ICON_VEST_PATCHES                     "\ue3d5"
#define YGGDRASILL_ICON_VEST                             "\ue3d6"
#define YGGDRASILL_ICON_VIAL                             "\ue3d7"
#define YGGDRASILL_ICON_VIALS                            "\ue3d8"
#define YGGDRASILL_ICON_VIDEO_SLASH                      "\ue3d9"
#define YGGDRASILL_ICON_VIDEO                            "\ue3da"
#define YGGDRASILL_ICON_VIHARA                           "\ue3db"
#define YGGDRASILL_ICON_VIRUS_SLASH                      "\ue3dc"
#define YGGDRASILL_ICON_VIRUS                            "\ue3dd"
#define YGGDRASILL_ICON_VIRUSES                          "\ue3de"
#define YGGDRASILL_ICON_VOICEMAIL                        "\ue3df"
#define YGGDRASILL_ICON_VOLLEYBALL_BALL                  "\ue3e0"
#define YGGDRASILL_ICON_VOLUME_DOWN                      "\ue3e1"
#define YGGDRASILL_ICON_VOLUME_MUTE                      "\ue3e2"
#define YGGDRASILL_ICON_VOLUME_OFF                       "\ue3e3"
#define YGGDRASILL_ICON_VOLUME_UP                        "\ue3e4"
#define YGGDRASILL_ICON_VOTE_YEA                         "\ue3e5"
#define YGGDRASILL_ICON_VR_CARDBOARD                     "\ue3e6"
#define YGGDRASILL_ICON_WALKING                          "\ue3e7"
#define YGGDRASILL_ICON_WALLET                           "\ue3e8"
#define YGGDRASILL_ICON_WAREHOUSE                        "\ue3e9"
#define YGGDRASILL_ICON_WATER                            "\ue3ea"
#define YGGDRASILL_ICON_WAVE_SQUARE                      "\ue3eb"
#define YGGDRASILL_ICON_WEIGHT_HANGING                   "\ue3ec"
#define YGGDRASILL_ICON_WEIGHT                           "\ue3ed"
#define YGGDRASILL_ICON_WHEELCHAIR                       "\ue3ee"
#define YGGDRASILL_ICON_WIFI                             "\ue3ef"
#define YGGDRASILL_ICON_WIND                             "\ue3f0"
#define YGGDRASILL_ICON_WINDOW_CLOSE                     "\ue3f1"
#define YGGDRASILL_ICON_WINDOW_MAXIMIZE                  "\ue3f2"
#define YGGDRASILL_ICON_WINDOW_MINIMIZE                  "\ue3f3"
#define YGGDRASILL_ICON_WINDOW_RESTORE                   "\ue3f4"
#define YGGDRASILL_ICON_WINE_BOTTLE                      "\ue3f5"
#define YGGDRASILL_ICON_WINE_GLASS_ALT                   "\ue3f6"
#define YGGDRASILL_ICON_WINE_GLASS                       "\ue3f7"
#define YGGDRASILL_ICON_WON_SIGN                         "\ue3f8"
#define YGGDRASILL_ICON_WRENCH                           "\ue3f9"
#define YGGDRASILL_ICON_X_RAY                            "\ue3fa"
#define YGGDRASILL_ICON_YEN_SIGN                         "\ue3fb"
#define YGGDRASILL_ICON_YIN_YANG                         "\ue3fc"

namespace yggdrasill::icons
{
    constexpr auto font_family                      = "yggdrasill-icons";
    constexpr auto font_start_code                  = 0xe000;
    constexpr auto font_end_code                    = 0xe3fc;
    constexpr auto branching_copy                   = "\ue000";
    constexpr auto branching_copy_once              = "\ue001";
    constexpr auto branching_empty                  = "\ue002";
    constexpr auto branching_empty_once             = "\ue003";
    constexpr auto branching_linear                 = "\ue004";
    constexpr auto delete_branch                    = "\ue005";
    constexpr auto goto_last_change                 = "\ue006";
    constexpr auto play_from_start                  = "\ue007";
    constexpr auto tool_elevation                   = "\ue008";
    constexpr auto tool_elevation_hill              = "\ue009";
    constexpr auto tool_elevation_manual            = "\ue00a";
    constexpr auto tool_elevation_mountain          = "\ue00b";
    constexpr auto tool_elevation_ocean             = "\ue00c";
    constexpr auto tool_elevation_water             = "\ue00d";
    constexpr auto tool_measure                     = "\ue00e";
    constexpr auto tool_mesh_subdivision            = "\ue00f";
    constexpr auto tool_plate_assignment            = "\ue010";
    constexpr auto tool_plate_velocity              = "\ue011";
    constexpr auto tool_select                      = "\ue012";
    constexpr auto ad                               = "\ue013";
    constexpr auto address_book                     = "\ue014";
    constexpr auto address_card                     = "\ue015";
    constexpr auto adjust                           = "\ue016";
    constexpr auto air_freshener                    = "\ue017";
    constexpr auto align_center                     = "\ue018";
    constexpr auto align_justify                    = "\ue019";
    constexpr auto align_left                       = "\ue01a";
    constexpr auto align_right                      = "\ue01b";
    constexpr auto allergies                        = "\ue01c";
    constexpr auto ambulance                        = "\ue01d";
    constexpr auto american_sign_language_interpreting = "\ue01e";
    constexpr auto anchor                           = "\ue01f";
    constexpr auto angle_double_down                = "\ue020";
    constexpr auto angle_double_left                = "\ue021";
    constexpr auto angle_double_right               = "\ue022";
    constexpr auto angle_double_up                  = "\ue023";
    constexpr auto angle_down                       = "\ue024";
    constexpr auto angle_left                       = "\ue025";
    constexpr auto angle_right                      = "\ue026";
    constexpr auto angle_up                         = "\ue027";
    constexpr auto angry                            = "\ue028";
    constexpr auto ankh                             = "\ue029";
    constexpr auto apple_alt                        = "\ue02a";
    constexpr auto archive                          = "\ue02b";
    constexpr auto archway                          = "\ue02c";
    constexpr auto arrow_alt_circle_down            = "\ue02d";
    constexpr auto arrow_alt_circle_left            = "\ue02e";
    constexpr auto arrow_alt_circle_right           = "\ue02f";
    constexpr auto arrow_alt_circle_up              = "\ue030";
    constexpr auto arrow_circle_down                = "\ue031";
    constexpr auto arrow_circle_left                = "\ue032";
    constexpr auto arrow_circle_right               = "\ue033";
    constexpr auto arrow_circle_up                  = "\ue034";
    constexpr auto arrow_down                       = "\ue035";
    constexpr auto arrow_left                       = "\ue036";
    constexpr auto arrow_right                      = "\ue037";
    constexpr auto arrow_up                         = "\ue038";
    constexpr auto arrows_alt_h                     = "\ue039";
    constexpr auto arrows_alt_v                     = "\ue03a";
    constexpr auto arrows_alt                       = "\ue03b";
    constexpr auto assistive_listening_systems      = "\ue03c";
    constexpr auto asterisk                         = "\ue03d";
    constexpr auto at                               = "\ue03e";
    constexpr auto atlas                            = "\ue03f";
    constexpr auto atom                             = "\ue040";
    constexpr auto audio_description                = "\ue041";
    constexpr auto award                            = "\ue042";
    constexpr auto baby_carriage                    = "\ue043";
    constexpr auto baby                             = "\ue044";
    constexpr auto backspace                        = "\ue045";
    constexpr auto backward                         = "\ue046";
    constexpr auto bacon                            = "\ue047";
    constexpr auto bacteria                         = "\ue048";
    constexpr auto bacterium                        = "\ue049";
    constexpr auto bahai                            = "\ue04a";
    constexpr auto balance_scale_left               = "\ue04b";
    constexpr auto balance_scale_right              = "\ue04c";
    constexpr auto balance_scale                    = "\ue04d";
    constexpr auto ban                              = "\ue04e";
    constexpr auto band_aid                         = "\ue04f";
    constexpr auto barcode                          = "\ue050";
    constexpr auto bars                             = "\ue051";
    constexpr auto baseball_ball                    = "\ue052";
    constexpr auto basketball_ball                  = "\ue053";
    constexpr auto bath                             = "\ue054";
    constexpr auto battery_empty                    = "\ue055";
    constexpr auto battery_full                     = "\ue056";
    constexpr auto battery_half                     = "\ue057";
    constexpr auto battery_quarter                  = "\ue058";
    constexpr auto battery_three_quarters           = "\ue059";
    constexpr auto bed                              = "\ue05a";
    constexpr auto beer                             = "\ue05b";
    constexpr auto bell_slash                       = "\ue05c";
    constexpr auto bell                             = "\ue05d";
    constexpr auto bezier_curve                     = "\ue05e";
    constexpr auto bible                            = "\ue05f";
    constexpr auto bicycle                          = "\ue060";
    constexpr auto biking                           = "\ue061";
    constexpr auto binoculars                       = "\ue062";
    constexpr auto biohazard                        = "\ue063";
    constexpr auto birthday_cake                    = "\ue064";
    constexpr auto blender_phone                    = "\ue065";
    constexpr auto blender                          = "\ue066";
    constexpr auto blind                            = "\ue067";
    constexpr auto blog                             = "\ue068";
    constexpr auto bold                             = "\ue069";
    constexpr auto bolt                             = "\ue06a";
    constexpr auto bomb                             = "\ue06b";
    constexpr auto bone                             = "\ue06c";
    constexpr auto bong                             = "\ue06d";
    constexpr auto book_dead                        = "\ue06e";
    constexpr auto book_medical                     = "\ue06f";
    constexpr auto book_open                        = "\ue070";
    constexpr auto book_reader                      = "\ue071";
    constexpr auto book                             = "\ue072";
    constexpr auto bookmark                         = "\ue073";
    constexpr auto border_all                       = "\ue074";
    constexpr auto border_none                      = "\ue075";
    constexpr auto border_style                     = "\ue076";
    constexpr auto bowling_ball                     = "\ue077";
    constexpr auto box_open                         = "\ue078";
    constexpr auto box_tissue                       = "\ue079";
    constexpr auto box                              = "\ue07a";
    constexpr auto boxes                            = "\ue07b";
    constexpr auto braille                          = "\ue07c";
    constexpr auto brain                            = "\ue07d";
    constexpr auto bread_slice                      = "\ue07e";
    constexpr auto briefcase_medical                = "\ue07f";
    constexpr auto briefcase                        = "\ue080";
    constexpr auto broadcast_tower                  = "\ue081";
    constexpr auto broom                            = "\ue082";
    constexpr auto brush                            = "\ue083";
    constexpr auto bug                              = "\ue084";
    constexpr auto building                         = "\ue085";
    constexpr auto bullhorn                         = "\ue086";
    constexpr auto bullseye                         = "\ue087";
    constexpr auto burn                             = "\ue088";
    constexpr auto bus_alt                          = "\ue089";
    constexpr auto bus                              = "\ue08a";
    constexpr auto business_time                    = "\ue08b";
    constexpr auto calculator                       = "\ue08c";
    constexpr auto calendar_alt                     = "\ue08d";
    constexpr auto calendar_check                   = "\ue08e";
    constexpr auto calendar_day                     = "\ue08f";
    constexpr auto calendar_minus                   = "\ue090";
    constexpr auto calendar_plus                    = "\ue091";
    constexpr auto calendar_times                   = "\ue092";
    constexpr auto calendar_week                    = "\ue093";
    constexpr auto calendar                         = "\ue094";
    constexpr auto camera_retro                     = "\ue095";
    constexpr auto camera                           = "\ue096";
    constexpr auto campground                       = "\ue097";
    constexpr auto candy_cane                       = "\ue098";
    constexpr auto cannabis                         = "\ue099";
    constexpr auto capsules                         = "\ue09a";
    constexpr auto car_alt                          = "\ue09b";
    constexpr auto car_battery                      = "\ue09c";
    constexpr auto car_crash                        = "\ue09d";
    constexpr auto car_side                         = "\ue09e";
    constexpr auto car                              = "\ue09f";
    constexpr auto caravan                          = "\ue0a0";
    constexpr auto caret_down                       = "\ue0a1";
    constexpr auto caret_left                       = "\ue0a2";
    constexpr auto caret_right                      = "\ue0a3";
    constexpr auto caret_square_down                = "\ue0a4";
    constexpr auto caret_square_left                = "\ue0a5";
    constexpr auto caret_square_right               = "\ue0a6";
    constexpr auto caret_square_up                  = "\ue0a7";
    constexpr auto caret_up                         = "\ue0a8";
    constexpr auto carrot                           = "\ue0a9";
    constexpr auto cart_arrow_down                  = "\ue0aa";
    constexpr auto cart_plus                        = "\ue0ab";
    constexpr auto cash_register                    = "\ue0ac";
    constexpr auto cat                              = "\ue0ad";
    constexpr auto certificate                      = "\ue0ae";
    constexpr auto chair                            = "\ue0af";
    constexpr auto chalkboard_teacher               = "\ue0b0";
    constexpr auto chalkboard                       = "\ue0b1";
    constexpr auto charging_station                 = "\ue0b2";
    constexpr auto chart_area                       = "\ue0b3";
    constexpr auto chart_bar                        = "\ue0b4";
    constexpr auto chart_line                       = "\ue0b5";
    constexpr auto chart_pie                        = "\ue0b6";
    constexpr auto check_circle                     = "\ue0b7";
    constexpr auto check_double                     = "\ue0b8";
    constexpr auto check_square                     = "\ue0b9";
    constexpr auto check                            = "\ue0ba";
    constexpr auto cheese                           = "\ue0bb";
    constexpr auto chess_bishop                     = "\ue0bc";
    constexpr auto chess_board                      = "\ue0bd";
    constexpr auto chess_king                       = "\ue0be";
    constexpr auto chess_knight                     = "\ue0bf";
    constexpr auto chess_pawn                       = "\ue0c0";
    constexpr auto chess_queen                      = "\ue0c1";
    constexpr auto chess_rook                       = "\ue0c2";
    constexpr auto chess                            = "\ue0c3";
    constexpr auto chevron_circle_down              = "\ue0c4";
    constexpr auto chevron_circle_left              = "\ue0c5";
    constexpr auto chevron_circle_right             = "\ue0c6";
    constexpr auto chevron_circle_up                = "\ue0c7";
    constexpr auto chevron_down                     = "\ue0c8";
    constexpr auto chevron_left                     = "\ue0c9";
    constexpr auto chevron_right                    = "\ue0ca";
    constexpr auto chevron_up                       = "\ue0cb";
    constexpr auto child                            = "\ue0cc";
    constexpr auto church                           = "\ue0cd";
    constexpr auto circle_notch                     = "\ue0ce";
    constexpr auto circle                           = "\ue0cf";
    constexpr auto city                             = "\ue0d0";
    constexpr auto clinic_medical                   = "\ue0d1";
    constexpr auto clipboard_check                  = "\ue0d2";
    constexpr auto clipboard_list                   = "\ue0d3";
    constexpr auto clipboard                        = "\ue0d4";
    constexpr auto clock                            = "\ue0d5";
    constexpr auto clone                            = "\ue0d6";
    constexpr auto closed_captioning                = "\ue0d7";
    constexpr auto cloud_download_alt               = "\ue0d8";
    constexpr auto cloud_meatball                   = "\ue0d9";
    constexpr auto cloud_moon_rain                  = "\ue0da";
    constexpr auto cloud_moon                       = "\ue0db";
    constexpr auto cloud_rain                       = "\ue0dc";
    constexpr auto cloud_showers_heavy              = "\ue0dd";
    constexpr auto cloud_sun_rain                   = "\ue0de";
    constexpr auto cloud_sun                        = "\ue0df";
    constexpr auto cloud_upload_alt                 = "\ue0e0";
    constexpr auto cloud                            = "\ue0e1";
    constexpr auto cocktail                         = "\ue0e2";
    constexpr auto code_branch                      = "\ue0e3";
    constexpr auto code                             = "\ue0e4";
    constexpr auto coffee                           = "\ue0e5";
    constexpr auto cog                              = "\ue0e6";
    constexpr auto cogs                             = "\ue0e7";
    constexpr auto coins                            = "\ue0e8";
    constexpr auto columns                          = "\ue0e9";
    constexpr auto comment_alt                      = "\ue0ea";
    constexpr auto comment_dollar                   = "\ue0eb";
    constexpr auto comment_dots                     = "\ue0ec";
    constexpr auto comment_medical                  = "\ue0ed";
    constexpr auto comment_slash                    = "\ue0ee";
    constexpr auto comment                          = "\ue0ef";
    constexpr auto comments_dollar                  = "\ue0f0";
    constexpr auto comments                         = "\ue0f1";
    constexpr auto compact_disc                     = "\ue0f2";
    constexpr auto compass                          = "\ue0f3";
    constexpr auto compress_alt                     = "\ue0f4";
    constexpr auto compress_arrows_alt              = "\ue0f5";
    constexpr auto compress                         = "\ue0f6";
    constexpr auto concierge_bell                   = "\ue0f7";
    constexpr auto cookie_bite                      = "\ue0f8";
    constexpr auto cookie                           = "\ue0f9";
    constexpr auto copy                             = "\ue0fa";
    constexpr auto copyright                        = "\ue0fb";
    constexpr auto couch                            = "\ue0fc";
    constexpr auto credit_card                      = "\ue0fd";
    constexpr auto crop_alt                         = "\ue0fe";
    constexpr auto crop                             = "\ue0ff";
    constexpr auto cross                            = "\ue100";
    constexpr auto crosshairs                       = "\ue101";
    constexpr auto crow                             = "\ue102";
    constexpr auto crown                            = "\ue103";
    constexpr auto crutch                           = "\ue104";
    constexpr auto cube                             = "\ue105";
    constexpr auto cubes                            = "\ue106";
    constexpr auto cut                              = "\ue107";
    constexpr auto database                         = "\ue108";
    constexpr auto deaf                             = "\ue109";
    constexpr auto democrat                         = "\ue10a";
    constexpr auto desktop                          = "\ue10b";
    constexpr auto dharmachakra                     = "\ue10c";
    constexpr auto diagnoses                        = "\ue10d";
    constexpr auto dice_d20                         = "\ue10e";
    constexpr auto dice_d6                          = "\ue10f";
    constexpr auto dice_five                        = "\ue110";
    constexpr auto dice_four                        = "\ue111";
    constexpr auto dice_one                         = "\ue112";
    constexpr auto dice_six                         = "\ue113";
    constexpr auto dice_three                       = "\ue114";
    constexpr auto dice_two                         = "\ue115";
    constexpr auto dice                             = "\ue116";
    constexpr auto digital_tachograph               = "\ue117";
    constexpr auto directions                       = "\ue118";
    constexpr auto disease                          = "\ue119";
    constexpr auto divide                           = "\ue11a";
    constexpr auto dizzy                            = "\ue11b";
    constexpr auto dna                              = "\ue11c";
    constexpr auto dog                              = "\ue11d";
    constexpr auto dollar_sign                      = "\ue11e";
    constexpr auto dolly_flatbed                    = "\ue11f";
    constexpr auto dolly                            = "\ue120";
    constexpr auto donate                           = "\ue121";
    constexpr auto door_closed                      = "\ue122";
    constexpr auto door_open                        = "\ue123";
    constexpr auto dot_circle                       = "\ue124";
    constexpr auto dove                             = "\ue125";
    constexpr auto download                         = "\ue126";
    constexpr auto drafting_compass                 = "\ue127";
    constexpr auto dragon                           = "\ue128";
    constexpr auto draw_polygon                     = "\ue129";
    constexpr auto drum_steelpan                    = "\ue12a";
    constexpr auto drum                             = "\ue12b";
    constexpr auto drumstick_bite                   = "\ue12c";
    constexpr auto dumbbell                         = "\ue12d";
    constexpr auto dumpster_fire                    = "\ue12e";
    constexpr auto dumpster                         = "\ue12f";
    constexpr auto dungeon                          = "\ue130";
    constexpr auto edit                             = "\ue131";
    constexpr auto egg                              = "\ue132";
    constexpr auto eject                            = "\ue133";
    constexpr auto ellipsis_h                       = "\ue134";
    constexpr auto ellipsis_v                       = "\ue135";
    constexpr auto envelope_open_text               = "\ue136";
    constexpr auto envelope_open                    = "\ue137";
    constexpr auto envelope_square                  = "\ue138";
    constexpr auto envelope                         = "\ue139";
    constexpr auto equals                           = "\ue13a";
    constexpr auto eraser                           = "\ue13b";
    constexpr auto ethernet                         = "\ue13c";
    constexpr auto euro_sign                        = "\ue13d";
    constexpr auto exchange_alt                     = "\ue13e";
    constexpr auto exclamation_circle               = "\ue13f";
    constexpr auto exclamation_triangle             = "\ue140";
    constexpr auto exclamation                      = "\ue141";
    constexpr auto expand_alt                       = "\ue142";
    constexpr auto expand_arrows_alt                = "\ue143";
    constexpr auto expand                           = "\ue144";
    constexpr auto external_link_alt                = "\ue145";
    constexpr auto external_link_square_alt         = "\ue146";
    constexpr auto eye_dropper                      = "\ue147";
    constexpr auto eye_slash                        = "\ue148";
    constexpr auto eye                              = "\ue149";
    constexpr auto fan                              = "\ue14a";
    constexpr auto fast_backward                    = "\ue14b";
    constexpr auto fast_forward                     = "\ue14c";
    constexpr auto faucet                           = "\ue14d";
    constexpr auto fax                              = "\ue14e";
    constexpr auto feather_alt                      = "\ue14f";
    constexpr auto feather                          = "\ue150";
    constexpr auto female                           = "\ue151";
    constexpr auto fighter_jet                      = "\ue152";
    constexpr auto file_alt                         = "\ue153";
    constexpr auto file_archive                     = "\ue154";
    constexpr auto file_audio                       = "\ue155";
    constexpr auto file_code                        = "\ue156";
    constexpr auto file_contract                    = "\ue157";
    constexpr auto file_csv                         = "\ue158";
    constexpr auto file_download                    = "\ue159";
    constexpr auto file_excel                       = "\ue15a";
    constexpr auto file_export                      = "\ue15b";
    constexpr auto file_image                       = "\ue15c";
    constexpr auto file_import                      = "\ue15d";
    constexpr auto file_invoice_dollar              = "\ue15e";
    constexpr auto file_invoice                     = "\ue15f";
    constexpr auto file_medical_alt                 = "\ue160";
    constexpr auto file_medical                     = "\ue161";
    constexpr auto file_pdf                         = "\ue162";
    constexpr auto file_powerpoint                  = "\ue163";
    constexpr auto file_prescription                = "\ue164";
    constexpr auto file_signature                   = "\ue165";
    constexpr auto file_upload                      = "\ue166";
    constexpr auto file_video                       = "\ue167";
    constexpr auto file_word                        = "\ue168";
    constexpr auto file                             = "\ue169";
    constexpr auto fill_drip                        = "\ue16a";
    constexpr auto fill                             = "\ue16b";
    constexpr auto film                             = "\ue16c";
    constexpr auto filter                           = "\ue16d";
    constexpr auto fingerprint                      = "\ue16e";
    constexpr auto fire_alt                         = "\ue16f";
    constexpr auto fire_extinguisher                = "\ue170";
    constexpr auto fire                             = "\ue171";
    constexpr auto first_aid                        = "\ue172";
    constexpr auto fish                             = "\ue173";
    constexpr auto fist_raised                      = "\ue174";
    constexpr auto flag_checkered                   = "\ue175";
    constexpr auto flag_usa                         = "\ue176";
    constexpr auto flag                             = "\ue177";
    constexpr auto flask                            = "\ue178";
    constexpr auto flushed                          = "\ue179";
    constexpr auto folder_minus                     = "\ue17a";
    constexpr auto folder_open                      = "\ue17b";
    constexpr auto folder_plus                      = "\ue17c";
    constexpr auto folder                           = "\ue17d";
    constexpr auto font_awesome_logo_full           = "\ue17e";
    constexpr auto font                             = "\ue17f";
    constexpr auto football_ball                    = "\ue180";
    constexpr auto forward                          = "\ue181";
    constexpr auto frog                             = "\ue182";
    constexpr auto frown_open                       = "\ue183";
    constexpr auto frown                            = "\ue184";
    constexpr auto funnel_dollar                    = "\ue185";
    constexpr auto futbol                           = "\ue186";
    constexpr auto gamepad                          = "\ue187";
    constexpr auto gas_pump                         = "\ue188";
    constexpr auto gavel                            = "\ue189";
    constexpr auto gem                              = "\ue18a";
    constexpr auto genderless                       = "\ue18b";
    constexpr auto ghost                            = "\ue18c";
    constexpr auto gift                             = "\ue18d";
    constexpr auto gifts                            = "\ue18e";
    constexpr auto glass_cheers                     = "\ue18f";
    constexpr auto glass_martini_alt                = "\ue190";
    constexpr auto glass_martini                    = "\ue191";
    constexpr auto glass_whiskey                    = "\ue192";
    constexpr auto glasses                          = "\ue193";
    constexpr auto globe_africa                     = "\ue194";
    constexpr auto globe_americas                   = "\ue195";
    constexpr auto globe_asia                       = "\ue196";
    constexpr auto globe_europe                     = "\ue197";
    constexpr auto globe                            = "\ue198";
    constexpr auto golf_ball                        = "\ue199";
    constexpr auto gopuram                          = "\ue19a";
    constexpr auto graduation_cap                   = "\ue19b";
    constexpr auto greater_than_equal               = "\ue19c";
    constexpr auto greater_than                     = "\ue19d";
    constexpr auto grimace                          = "\ue19e";
    constexpr auto grin_alt                         = "\ue19f";
    constexpr auto grin_beam_sweat                  = "\ue1a0";
    constexpr auto grin_beam                        = "\ue1a1";
    constexpr auto grin_hearts                      = "\ue1a2";
    constexpr auto grin_squint_tears                = "\ue1a3";
    constexpr auto grin_squint                      = "\ue1a4";
    constexpr auto grin_stars                       = "\ue1a5";
    constexpr auto grin_tears                       = "\ue1a6";
    constexpr auto grin_tongue_squint               = "\ue1a7";
    constexpr auto grin_tongue_wink                 = "\ue1a8";
    constexpr auto grin_tongue                      = "\ue1a9";
    constexpr auto grin_wink                        = "\ue1aa";
    constexpr auto grin                             = "\ue1ab";
    constexpr auto grip_horizontal                  = "\ue1ac";
    constexpr auto grip_lines_vertical              = "\ue1ad";
    constexpr auto grip_lines                       = "\ue1ae";
    constexpr auto grip_vertical                    = "\ue1af";
    constexpr auto guitar                           = "\ue1b0";
    constexpr auto h_square                         = "\ue1b1";
    constexpr auto hamburger                        = "\ue1b2";
    constexpr auto hammer                           = "\ue1b3";
    constexpr auto hamsa                            = "\ue1b4";
    constexpr auto hand_holding_heart               = "\ue1b5";
    constexpr auto hand_holding_medical             = "\ue1b6";
    constexpr auto hand_holding_usd                 = "\ue1b7";
    constexpr auto hand_holding_water               = "\ue1b8";
    constexpr auto hand_holding                     = "\ue1b9";
    constexpr auto hand_lizard                      = "\ue1ba";
    constexpr auto hand_middle_finger               = "\ue1bb";
    constexpr auto hand_paper                       = "\ue1bc";
    constexpr auto hand_peace                       = "\ue1bd";
    constexpr auto hand_point_down                  = "\ue1be";
    constexpr auto hand_point_left                  = "\ue1bf";
    constexpr auto hand_point_right                 = "\ue1c0";
    constexpr auto hand_point_up                    = "\ue1c1";
    constexpr auto hand_pointer                     = "\ue1c2";
    constexpr auto hand_rock                        = "\ue1c3";
    constexpr auto hand_scissors                    = "\ue1c4";
    constexpr auto hand_sparkles                    = "\ue1c5";
    constexpr auto hand_spock                       = "\ue1c6";
    constexpr auto hands_helping                    = "\ue1c7";
    constexpr auto hands_wash                       = "\ue1c8";
    constexpr auto hands                            = "\ue1c9";
    constexpr auto handshake_alt_slash              = "\ue1ca";
    constexpr auto handshake_slash                  = "\ue1cb";
    constexpr auto handshake                        = "\ue1cc";
    constexpr auto hanukiah                         = "\ue1cd";
    constexpr auto hard_hat                         = "\ue1ce";
    constexpr auto hashtag                          = "\ue1cf";
    constexpr auto hat_cowboy_side                  = "\ue1d0";
    constexpr auto hat_cowboy                       = "\ue1d1";
    constexpr auto hat_wizard                       = "\ue1d2";
    constexpr auto hdd                              = "\ue1d3";
    constexpr auto head_side_cough_slash            = "\ue1d4";
    constexpr auto head_side_cough                  = "\ue1d5";
    constexpr auto head_side_mask                   = "\ue1d6";
    constexpr auto head_side_virus                  = "\ue1d7";
    constexpr auto heading                          = "\ue1d8";
    constexpr auto headphones_alt                   = "\ue1d9";
    constexpr auto headphones                       = "\ue1da";
    constexpr auto headset                          = "\ue1db";
    constexpr auto heart_broken                     = "\ue1dc";
    constexpr auto heart                            = "\ue1dd";
    constexpr auto heartbeat                        = "\ue1de";
    constexpr auto helicopter                       = "\ue1df";
    constexpr auto highlighter                      = "\ue1e0";
    constexpr auto hiking                           = "\ue1e1";
    constexpr auto hippo                            = "\ue1e2";
    constexpr auto history                          = "\ue1e3";
    constexpr auto hockey_puck                      = "\ue1e4";
    constexpr auto holly_berry                      = "\ue1e5";
    constexpr auto home                             = "\ue1e6";
    constexpr auto horse_head                       = "\ue1e7";
    constexpr auto horse                            = "\ue1e8";
    constexpr auto hospital_alt                     = "\ue1e9";
    constexpr auto hospital_symbol                  = "\ue1ea";
    constexpr auto hospital_user                    = "\ue1eb";
    constexpr auto hospital                         = "\ue1ec";
    constexpr auto hot_tub                          = "\ue1ed";
    constexpr auto hotdog                           = "\ue1ee";
    constexpr auto hotel                            = "\ue1ef";
    constexpr auto hourglass_end                    = "\ue1f0";
    constexpr auto hourglass_half                   = "\ue1f1";
    constexpr auto hourglass_start                  = "\ue1f2";
    constexpr auto hourglass                        = "\ue1f3";
    constexpr auto house_damage                     = "\ue1f4";
    constexpr auto house_user                       = "\ue1f5";
    constexpr auto hryvnia                          = "\ue1f6";
    constexpr auto i_cursor                         = "\ue1f7";
    constexpr auto ice_cream                        = "\ue1f8";
    constexpr auto icicles                          = "\ue1f9";
    constexpr auto icons                            = "\ue1fa";
    constexpr auto id_badge                         = "\ue1fb";
    constexpr auto id_card_alt                      = "\ue1fc";
    constexpr auto id_card                          = "\ue1fd";
    constexpr auto igloo                            = "\ue1fe";
    constexpr auto image                            = "\ue1ff";
    constexpr auto images                           = "\ue200";
    constexpr auto inbox                            = "\ue201";
    constexpr auto indent                           = "\ue202";
    constexpr auto industry                         = "\ue203";
    constexpr auto infinity                         = "\ue204";
    constexpr auto info_circle                      = "\ue205";
    constexpr auto info                             = "\ue206";
    constexpr auto italic                           = "\ue207";
    constexpr auto jedi                             = "\ue208";
    constexpr auto joint                            = "\ue209";
    constexpr auto journal_whills                   = "\ue20a";
    constexpr auto kaaba                            = "\ue20b";
    constexpr auto key                              = "\ue20c";
    constexpr auto keyboard                         = "\ue20d";
    constexpr auto khanda                           = "\ue20e";
    constexpr auto kiss_beam                        = "\ue20f";
    constexpr auto kiss_wink_heart                  = "\ue210";
    constexpr auto kiss                             = "\ue211";
    constexpr auto kiwi_bird                        = "\ue212";
    constexpr auto landmark                         = "\ue213";
    constexpr auto language                         = "\ue214";
    constexpr auto laptop_code                      = "\ue215";
    constexpr auto laptop_house                     = "\ue216";
    constexpr auto laptop_medical                   = "\ue217";
    constexpr auto laptop                           = "\ue218";
    constexpr auto laugh_beam                       = "\ue219";
    constexpr auto laugh_squint                     = "\ue21a";
    constexpr auto laugh_wink                       = "\ue21b";
    constexpr auto laugh                            = "\ue21c";
    constexpr auto layer_group                      = "\ue21d";
    constexpr auto leaf                             = "\ue21e";
    constexpr auto lemon                            = "\ue21f";
    constexpr auto less_than_equal                  = "\ue220";
    constexpr auto less_than                        = "\ue221";
    constexpr auto level_down_alt                   = "\ue222";
    constexpr auto level_up_alt                     = "\ue223";
    constexpr auto life_ring                        = "\ue224";
    constexpr auto lightbulb                        = "\ue225";
    constexpr auto link                             = "\ue226";
    constexpr auto lira_sign                        = "\ue227";
    constexpr auto list_alt                         = "\ue228";
    constexpr auto list_ol                          = "\ue229";
    constexpr auto list_ul                          = "\ue22a";
    constexpr auto list                             = "\ue22b";
    constexpr auto location_arrow                   = "\ue22c";
    constexpr auto lock_open                        = "\ue22d";
    constexpr auto lock                             = "\ue22e";
    constexpr auto long_arrow_alt_down              = "\ue22f";
    constexpr auto long_arrow_alt_left              = "\ue230";
    constexpr auto long_arrow_alt_right             = "\ue231";
    constexpr auto long_arrow_alt_up                = "\ue232";
    constexpr auto low_vision                       = "\ue233";
    constexpr auto luggage_cart                     = "\ue234";
    constexpr auto lungs_virus                      = "\ue235";
    constexpr auto lungs                            = "\ue236";
    constexpr auto magic                            = "\ue237";
    constexpr auto magnet                           = "\ue238";
    constexpr auto mail_bulk                        = "\ue239";
    constexpr auto male                             = "\ue23a";
    constexpr auto map_marked_alt                   = "\ue23b";
    constexpr auto map_marked                       = "\ue23c";
    constexpr auto map_marker_alt                   = "\ue23d";
    constexpr auto map_marker                       = "\ue23e";
    constexpr auto map_pin                          = "\ue23f";
    constexpr auto map_signs                        = "\ue240";
    constexpr auto map                              = "\ue241";
    constexpr auto marker                           = "\ue242";
    constexpr auto mars_double                      = "\ue243";
    constexpr auto mars_stroke_h                    = "\ue244";
    constexpr auto mars_stroke_v                    = "\ue245";
    constexpr auto mars_stroke                      = "\ue246";
    constexpr auto mars                             = "\ue247";
    constexpr auto mask                             = "\ue248";
    constexpr auto medal                            = "\ue249";
    constexpr auto medkit                           = "\ue24a";
    constexpr auto meh_blank                        = "\ue24b";
    constexpr auto meh_rolling_eyes                 = "\ue24c";
    constexpr auto meh                              = "\ue24d";
    constexpr auto memory                           = "\ue24e";
    constexpr auto menorah                          = "\ue24f";
    constexpr auto mercury                          = "\ue250";
    constexpr auto meteor                           = "\ue251";
    constexpr auto microchip                        = "\ue252";
    constexpr auto microphone_alt_slash             = "\ue253";
    constexpr auto microphone_alt                   = "\ue254";
    constexpr auto microphone_slash                 = "\ue255";
    constexpr auto microphone                       = "\ue256";
    constexpr auto microscope                       = "\ue257";
    constexpr auto minus_circle                     = "\ue258";
    constexpr auto minus_square                     = "\ue259";
    constexpr auto minus                            = "\ue25a";
    constexpr auto mitten                           = "\ue25b";
    constexpr auto mobile_alt                       = "\ue25c";
    constexpr auto mobile                           = "\ue25d";
    constexpr auto money_bill_alt                   = "\ue25e";
    constexpr auto money_bill_wave_alt              = "\ue25f";
    constexpr auto money_bill_wave                  = "\ue260";
    constexpr auto money_bill                       = "\ue261";
    constexpr auto money_check_alt                  = "\ue262";
    constexpr auto money_check                      = "\ue263";
    constexpr auto monument                         = "\ue264";
    constexpr auto moon                             = "\ue265";
    constexpr auto mortar_pestle                    = "\ue266";
    constexpr auto mosque                           = "\ue267";
    constexpr auto motorcycle                       = "\ue268";
    constexpr auto mountain                         = "\ue269";
    constexpr auto mouse_pointer                    = "\ue26a";
    constexpr auto mouse                            = "\ue26b";
    constexpr auto mug_hot                          = "\ue26c";
    constexpr auto music                            = "\ue26d";
    constexpr auto network_wired                    = "\ue26e";
    constexpr auto neuter                           = "\ue26f";
    constexpr auto newspaper                        = "\ue270";
    constexpr auto not_equal                        = "\ue271";
    constexpr auto notes_medical                    = "\ue272";
    constexpr auto object_group                     = "\ue273";
    constexpr auto object_ungroup                   = "\ue274";
    constexpr auto oil_can                          = "\ue275";
    constexpr auto om                               = "\ue276";
    constexpr auto otter                            = "\ue277";
    constexpr auto outdent                          = "\ue278";
    constexpr auto pager                            = "\ue279";
    constexpr auto paint_brush                      = "\ue27a";
    constexpr auto paint_roller                     = "\ue27b";
    constexpr auto palette                          = "\ue27c";
    constexpr auto pallet                           = "\ue27d";
    constexpr auto paper_plane                      = "\ue27e";
    constexpr auto paperclip                        = "\ue27f";
    constexpr auto parachute_box                    = "\ue280";
    constexpr auto paragraph                        = "\ue281";
    constexpr auto parking                          = "\ue282";
    constexpr auto passport                         = "\ue283";
    constexpr auto pastafarianism                   = "\ue284";
    constexpr auto paste                            = "\ue285";
    constexpr auto pause_circle                     = "\ue286";
    constexpr auto pause                            = "\ue287";
    constexpr auto paw                              = "\ue288";
    constexpr auto peace                            = "\ue289";
    constexpr auto pen_alt                          = "\ue28a";
    constexpr auto pen_fancy                        = "\ue28b";
    constexpr auto pen_nib                          = "\ue28c";
    constexpr auto pen_square                       = "\ue28d";
    constexpr auto pen                              = "\ue28e";
    constexpr auto pencil_alt                       = "\ue28f";
    constexpr auto pencil_ruler                     = "\ue290";
    constexpr auto people_arrows                    = "\ue291";
    constexpr auto people_carry                     = "\ue292";
    constexpr auto pepper_hot                       = "\ue293";
    constexpr auto percent                          = "\ue294";
    constexpr auto percentage                       = "\ue295";
    constexpr auto person_booth                     = "\ue296";
    constexpr auto phone_alt                        = "\ue297";
    constexpr auto phone_slash                      = "\ue298";
    constexpr auto phone_square_alt                 = "\ue299";
    constexpr auto phone_square                     = "\ue29a";
    constexpr auto phone_volume                     = "\ue29b";
    constexpr auto phone                            = "\ue29c";
    constexpr auto photo_video                      = "\ue29d";
    constexpr auto piggy_bank                       = "\ue29e";
    constexpr auto pills                            = "\ue29f";
    constexpr auto pizza_slice                      = "\ue2a0";
    constexpr auto place_of_worship                 = "\ue2a1";
    constexpr auto plane_arrival                    = "\ue2a2";
    constexpr auto plane_departure                  = "\ue2a3";
    constexpr auto plane_slash                      = "\ue2a4";
    constexpr auto plane                            = "\ue2a5";
    constexpr auto play_circle                      = "\ue2a6";
    constexpr auto play                             = "\ue2a7";
    constexpr auto plug                             = "\ue2a8";
    constexpr auto plus_circle                      = "\ue2a9";
    constexpr auto plus_square                      = "\ue2aa";
    constexpr auto plus                             = "\ue2ab";
    constexpr auto podcast                          = "\ue2ac";
    constexpr auto poll_h                           = "\ue2ad";
    constexpr auto poll                             = "\ue2ae";
    constexpr auto poo_storm                        = "\ue2af";
    constexpr auto poo                              = "\ue2b0";
    constexpr auto poop                             = "\ue2b1";
    constexpr auto portrait                         = "\ue2b2";
    constexpr auto pound_sign                       = "\ue2b3";
    constexpr auto power_off                        = "\ue2b4";
    constexpr auto pray                             = "\ue2b5";
    constexpr auto praying_hands                    = "\ue2b6";
    constexpr auto prescription_bottle_alt          = "\ue2b7";
    constexpr auto prescription_bottle              = "\ue2b8";
    constexpr auto prescription                     = "\ue2b9";
    constexpr auto print                            = "\ue2ba";
    constexpr auto procedures                       = "\ue2bb";
    constexpr auto project_diagram                  = "\ue2bc";
    constexpr auto pump_medical                     = "\ue2bd";
    constexpr auto pump_soap                        = "\ue2be";
    constexpr auto puzzle_piece                     = "\ue2bf";
    constexpr auto qrcode                           = "\ue2c0";
    constexpr auto question_circle                  = "\ue2c1";
    constexpr auto question                         = "\ue2c2";
    constexpr auto quidditch                        = "\ue2c3";
    constexpr auto quote_left                       = "\ue2c4";
    constexpr auto quote_right                      = "\ue2c5";
    constexpr auto quran                            = "\ue2c6";
    constexpr auto radiation_alt                    = "\ue2c7";
    constexpr auto radiation                        = "\ue2c8";
    constexpr auto rainbow                          = "\ue2c9";
    constexpr auto random                           = "\ue2ca";
    constexpr auto receipt                          = "\ue2cb";
    constexpr auto record_vinyl                     = "\ue2cc";
    constexpr auto recycle                          = "\ue2cd";
    constexpr auto redo_alt                         = "\ue2ce";
    constexpr auto redo                             = "\ue2cf";
    constexpr auto registered                       = "\ue2d0";
    constexpr auto remove_format                    = "\ue2d1";
    constexpr auto reply_all                        = "\ue2d2";
    constexpr auto reply                            = "\ue2d3";
    constexpr auto republican                       = "\ue2d4";
    constexpr auto restroom                         = "\ue2d5";
    constexpr auto retweet                          = "\ue2d6";
    constexpr auto ribbon                           = "\ue2d7";
    constexpr auto ring                             = "\ue2d8";
    constexpr auto road                             = "\ue2d9";
    constexpr auto robot                            = "\ue2da";
    constexpr auto rocket                           = "\ue2db";
    constexpr auto route                            = "\ue2dc";
    constexpr auto rss_square                       = "\ue2dd";
    constexpr auto rss                              = "\ue2de";
    constexpr auto ruble_sign                       = "\ue2df";
    constexpr auto ruler_combined                   = "\ue2e0";
    constexpr auto ruler_horizontal                 = "\ue2e1";
    constexpr auto ruler_vertical                   = "\ue2e2";
    constexpr auto ruler                            = "\ue2e3";
    constexpr auto running                          = "\ue2e4";
    constexpr auto rupee_sign                       = "\ue2e5";
    constexpr auto sad_cry                          = "\ue2e6";
    constexpr auto sad_tear                         = "\ue2e7";
    constexpr auto satellite_dish                   = "\ue2e8";
    constexpr auto satellite                        = "\ue2e9";
    constexpr auto save                             = "\ue2ea";
    constexpr auto school                           = "\ue2eb";
    constexpr auto screwdriver                      = "\ue2ec";
    constexpr auto scroll                           = "\ue2ed";
    constexpr auto sd_card                          = "\ue2ee";
    constexpr auto search_dollar                    = "\ue2ef";
    constexpr auto search_location                  = "\ue2f0";
    constexpr auto search_minus                     = "\ue2f1";
    constexpr auto search_plus                      = "\ue2f2";
    constexpr auto search                           = "\ue2f3";
    constexpr auto seedling                         = "\ue2f4";
    constexpr auto server                           = "\ue2f5";
    constexpr auto shapes                           = "\ue2f6";
    constexpr auto share_alt_square                 = "\ue2f7";
    constexpr auto share_alt                        = "\ue2f8";
    constexpr auto share_square                     = "\ue2f9";
    constexpr auto share                            = "\ue2fa";
    constexpr auto shekel_sign                      = "\ue2fb";
    constexpr auto shield_alt                       = "\ue2fc";
    constexpr auto shield_virus                     = "\ue2fd";
    constexpr auto ship                             = "\ue2fe";
    constexpr auto shipping_fast                    = "\ue2ff";
    constexpr auto shoe_prints                      = "\ue300";
    constexpr auto shopping_bag                     = "\ue301";
    constexpr auto shopping_basket                  = "\ue302";
    constexpr auto shopping_cart                    = "\ue303";
    constexpr auto shower                           = "\ue304";
    constexpr auto shuttle_van                      = "\ue305";
    constexpr auto sign_in_alt                      = "\ue306";
    constexpr auto sign_language                    = "\ue307";
    constexpr auto sign_out_alt                     = "\ue308";
    constexpr auto sign                             = "\ue309";
    constexpr auto signal                           = "\ue30a";
    constexpr auto signature                        = "\ue30b";
    constexpr auto sim_card                         = "\ue30c";
    constexpr auto sink                             = "\ue30d";
    constexpr auto sitemap                          = "\ue30e";
    constexpr auto skating                          = "\ue30f";
    constexpr auto skiing_nordic                    = "\ue310";
    constexpr auto skiing                           = "\ue311";
    constexpr auto skull_crossbones                 = "\ue312";
    constexpr auto skull                            = "\ue313";
    constexpr auto slash                            = "\ue314";
    constexpr auto sleigh                           = "\ue315";
    constexpr auto sliders_h                        = "\ue316";
    constexpr auto smile_beam                       = "\ue317";
    constexpr auto smile_wink                       = "\ue318";
    constexpr auto smile                            = "\ue319";
    constexpr auto smog                             = "\ue31a";
    constexpr auto smoking_ban                      = "\ue31b";
    constexpr auto smoking                          = "\ue31c";
    constexpr auto sms                              = "\ue31d";
    constexpr auto snowboarding                     = "\ue31e";
    constexpr auto snowflake                        = "\ue31f";
    constexpr auto snowman                          = "\ue320";
    constexpr auto snowplow                         = "\ue321";
    constexpr auto soap                             = "\ue322";
    constexpr auto socks                            = "\ue323";
    constexpr auto solar_panel                      = "\ue324";
    constexpr auto sort_alpha_down_alt              = "\ue325";
    constexpr auto sort_alpha_down                  = "\ue326";
    constexpr auto sort_alpha_up_alt                = "\ue327";
    constexpr auto sort_alpha_up                    = "\ue328";
    constexpr auto sort_amount_down_alt             = "\ue329";
    constexpr auto sort_amount_down                 = "\ue32a";
    constexpr auto sort_amount_up_alt               = "\ue32b";
    constexpr auto sort_amount_up                   = "\ue32c";
    constexpr auto sort_down                        = "\ue32d";
    constexpr auto sort_numeric_down_alt            = "\ue32e";
    constexpr auto sort_numeric_down                = "\ue32f";
    constexpr auto sort_numeric_up_alt              = "\ue330";
    constexpr auto sort_numeric_up                  = "\ue331";
    constexpr auto sort_up                          = "\ue332";
    constexpr auto sort                             = "\ue333";
    constexpr auto spa                              = "\ue334";
    constexpr auto space_shuttle                    = "\ue335";
    constexpr auto spell_check                      = "\ue336";
    constexpr auto spider                           = "\ue337";
    constexpr auto spinner                          = "\ue338";
    constexpr auto splotch                          = "\ue339";
    constexpr auto spray_can                        = "\ue33a";
    constexpr auto square_full                      = "\ue33b";
    constexpr auto square_root_alt                  = "\ue33c";
    constexpr auto square                           = "\ue33d";
    constexpr auto stamp                            = "\ue33e";
    constexpr auto star_and_crescent                = "\ue33f";
    constexpr auto star_half_alt                    = "\ue340";
    constexpr auto star_half                        = "\ue341";
    constexpr auto star_of_david                    = "\ue342";
    constexpr auto star_of_life                     = "\ue343";
    constexpr auto star                             = "\ue344";
    constexpr auto step_backward                    = "\ue345";
    constexpr auto step_forward                     = "\ue346";
    constexpr auto stethoscope                      = "\ue347";
    constexpr auto sticky_note                      = "\ue348";
    constexpr auto stop_circle                      = "\ue349";
    constexpr auto stop                             = "\ue34a";
    constexpr auto stopwatch_20                     = "\ue34b";
    constexpr auto stopwatch                        = "\ue34c";
    constexpr auto store_alt_slash                  = "\ue34d";
    constexpr auto store_alt                        = "\ue34e";
    constexpr auto store_slash                      = "\ue34f";
    constexpr auto store                            = "\ue350";
    constexpr auto stream                           = "\ue351";
    constexpr auto street_view                      = "\ue352";
    constexpr auto strikethrough                    = "\ue353";
    constexpr auto stroopwafel                      = "\ue354";
    constexpr auto subscript                        = "\ue355";
    constexpr auto subway                           = "\ue356";
    constexpr auto suitcase_rolling                 = "\ue357";
    constexpr auto suitcase                         = "\ue358";
    constexpr auto sun                              = "\ue359";
    constexpr auto superscript                      = "\ue35a";
    constexpr auto surprise                         = "\ue35b";
    constexpr auto swatchbook                       = "\ue35c";
    constexpr auto swimmer                          = "\ue35d";
    constexpr auto swimming_pool                    = "\ue35e";
    constexpr auto synagogue                        = "\ue35f";
    constexpr auto sync_alt                         = "\ue360";
    constexpr auto sync                             = "\ue361";
    constexpr auto syringe                          = "\ue362";
    constexpr auto table_tennis                     = "\ue363";
    constexpr auto table                            = "\ue364";
    constexpr auto tablet_alt                       = "\ue365";
    constexpr auto tablet                           = "\ue366";
    constexpr auto tablets                          = "\ue367";
    constexpr auto tachometer_alt                   = "\ue368";
    constexpr auto tag                              = "\ue369";
    constexpr auto tags                             = "\ue36a";
    constexpr auto tape                             = "\ue36b";
    constexpr auto tasks                            = "\ue36c";
    constexpr auto taxi                             = "\ue36d";
    constexpr auto teeth_open                       = "\ue36e";
    constexpr auto teeth                            = "\ue36f";
    constexpr auto temperature_high                 = "\ue370";
    constexpr auto temperature_low                  = "\ue371";
    constexpr auto tenge                            = "\ue372";
    constexpr auto terminal                         = "\ue373";
    constexpr auto text_height                      = "\ue374";
    constexpr auto text_width                       = "\ue375";
    constexpr auto th_large                         = "\ue376";
    constexpr auto th_list                          = "\ue377";
    constexpr auto th                               = "\ue378";
    constexpr auto theater_masks                    = "\ue379";
    constexpr auto thermometer_empty                = "\ue37a";
    constexpr auto thermometer_full                 = "\ue37b";
    constexpr auto thermometer_half                 = "\ue37c";
    constexpr auto thermometer_quarter              = "\ue37d";
    constexpr auto thermometer_three_quarters       = "\ue37e";
    constexpr auto thermometer                      = "\ue37f";
    constexpr auto thumbs_down                      = "\ue380";
    constexpr auto thumbs_up                        = "\ue381";
    constexpr auto thumbtack                        = "\ue382";
    constexpr auto ticket_alt                       = "\ue383";
    constexpr auto times_circle                     = "\ue384";
    constexpr auto times                            = "\ue385";
    constexpr auto tint_slash                       = "\ue386";
    constexpr auto tint                             = "\ue387";
    constexpr auto tired                            = "\ue388";
    constexpr auto toggle_off                       = "\ue389";
    constexpr auto toggle_on                        = "\ue38a";
    constexpr auto toilet_paper_slash               = "\ue38b";
    constexpr auto toilet_paper                     = "\ue38c";
    constexpr auto toilet                           = "\ue38d";
    constexpr auto toolbox                          = "\ue38e";
    constexpr auto tools                            = "\ue38f";
    constexpr auto tooth                            = "\ue390";
    constexpr auto torah                            = "\ue391";
    constexpr auto torii_gate                       = "\ue392";
    constexpr auto tractor                          = "\ue393";
    constexpr auto trademark                        = "\ue394";
    constexpr auto traffic_light                    = "\ue395";
    constexpr auto trailer                          = "\ue396";
    constexpr auto train                            = "\ue397";
    constexpr auto tram                             = "\ue398";
    constexpr auto transgender_alt                  = "\ue399";
    constexpr auto transgender                      = "\ue39a";
    constexpr auto trash_alt                        = "\ue39b";
    constexpr auto trash_restore_alt                = "\ue39c";
    constexpr auto trash_restore                    = "\ue39d";
    constexpr auto trash                            = "\ue39e";
    constexpr auto tree                             = "\ue39f";
    constexpr auto trophy                           = "\ue3a0";
    constexpr auto truck_loading                    = "\ue3a1";
    constexpr auto truck_monster                    = "\ue3a2";
    constexpr auto truck_moving                     = "\ue3a3";
    constexpr auto truck_pickup                     = "\ue3a4";
    constexpr auto truck                            = "\ue3a5";
    constexpr auto tshirt                           = "\ue3a6";
    constexpr auto tty                              = "\ue3a7";
    constexpr auto tv                               = "\ue3a8";
    constexpr auto umbrella_beach                   = "\ue3a9";
    constexpr auto umbrella                         = "\ue3aa";
    constexpr auto underline                        = "\ue3ab";
    constexpr auto undo_alt                         = "\ue3ac";
    constexpr auto undo                             = "\ue3ad";
    constexpr auto universal_access                 = "\ue3ae";
    constexpr auto university                       = "\ue3af";
    constexpr auto unlink                           = "\ue3b0";
    constexpr auto unlock_alt                       = "\ue3b1";
    constexpr auto unlock                           = "\ue3b2";
    constexpr auto upload                           = "\ue3b3";
    constexpr auto user_alt_slash                   = "\ue3b4";
    constexpr auto user_alt                         = "\ue3b5";
    constexpr auto user_astronaut                   = "\ue3b6";
    constexpr auto user_check                       = "\ue3b7";
    constexpr auto user_circle                      = "\ue3b8";
    constexpr auto user_clock                       = "\ue3b9";
    constexpr auto user_cog                         = "\ue3ba";
    constexpr auto user_edit                        = "\ue3bb";
    constexpr auto user_friends                     = "\ue3bc";
    constexpr auto user_graduate                    = "\ue3bd";
    constexpr auto user_injured                     = "\ue3be";
    constexpr auto user_lock                        = "\ue3bf";
    constexpr auto user_md                          = "\ue3c0";
    constexpr auto user_minus                       = "\ue3c1";
    constexpr auto user_ninja                       = "\ue3c2";
    constexpr auto user_nurse                       = "\ue3c3";
    constexpr auto user_plus                        = "\ue3c4";
    constexpr auto user_secret                      = "\ue3c5";
    constexpr auto user_shield                      = "\ue3c6";
    constexpr auto user_slash                       = "\ue3c7";
    constexpr auto user_tag                         = "\ue3c8";
    constexpr auto user_tie                         = "\ue3c9";
    constexpr auto user_times                       = "\ue3ca";
    constexpr auto user                             = "\ue3cb";
    constexpr auto users_cog                        = "\ue3cc";
    constexpr auto users_slash                      = "\ue3cd";
    constexpr auto users                            = "\ue3ce";
    constexpr auto utensil_spoon                    = "\ue3cf";
    constexpr auto utensils                         = "\ue3d0";
    constexpr auto vector_square                    = "\ue3d1";
    constexpr auto venus_double                     = "\ue3d2";
    constexpr auto venus_mars                       = "\ue3d3";
    constexpr auto venus                            = "\ue3d4";
    constexpr auto vest_patches                     = "\ue3d5";
    constexpr auto vest                             = "\ue3d6";
    constexpr auto vial                             = "\ue3d7";
    constexpr auto vials                            = "\ue3d8";
    constexpr auto video_slash                      = "\ue3d9";
    constexpr auto video                            = "\ue3da";
    constexpr auto vihara                           = "\ue3db";
    constexpr auto virus_slash                      = "\ue3dc";
    constexpr auto virus                            = "\ue3dd";
    constexpr auto viruses                          = "\ue3de";
    constexpr auto voicemail                        = "\ue3df";
    constexpr auto volleyball_ball                  = "\ue3e0";
    constexpr auto volume_down                      = "\ue3e1";
    constexpr auto volume_mute                      = "\ue3e2";
    constexpr auto volume_off                       = "\ue3e3";
    constexpr auto volume_up                        = "\ue3e4";
    constexpr auto vote_yea                         = "\ue3e5";
    constexpr auto vr_cardboard                     = "\ue3e6";
    constexpr auto walking                          = "\ue3e7";
    constexpr auto wallet                           = "\ue3e8";
    constexpr auto warehouse                        = "\ue3e9";
    constexpr auto water                            = "\ue3ea";
    constexpr auto wave_square                      = "\ue3eb";
    constexpr auto weight_hanging                   = "\ue3ec";
    constexpr auto weight                           = "\ue3ed";
    constexpr auto wheelchair                       = "\ue3ee";
    constexpr auto wifi                             = "\ue3ef";
    constexpr auto wind                             = "\ue3f0";
    constexpr auto window_close                     = "\ue3f1";
    constexpr auto window_maximize                  = "\ue3f2";
    constexpr auto window_minimize                  = "\ue3f3";
    constexpr auto window_restore                   = "\ue3f4";
    constexpr auto wine_bottle                      = "\ue3f5";
    constexpr auto wine_glass_alt                   = "\ue3f6";
    constexpr auto wine_glass                       = "\ue3f7";
    constexpr auto won_sign                         = "\ue3f8";
    constexpr auto wrench                           = "\ue3f9";
    constexpr auto x_ray                            = "\ue3fa";
    constexpr auto yen_sign                         = "\ue3fb";
    constexpr auto yin_yang                         = "\ue3fc";
}
