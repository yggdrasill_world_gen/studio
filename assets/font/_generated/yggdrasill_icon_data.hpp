#pragma once

namespace yggdrasill::icons {
 namespace data {
  extern const int size;
  extern const void* bytes;
 }
}
