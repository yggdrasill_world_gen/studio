#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp isampler2D;

in uint index;
in vec2 position;

out vec4 frag_color;

uniform mat4 transform;

uniform usampler2D data;
uniform vec3 rgb_true;
uniform vec3 rgb_false;
uniform float alpha;

vec4 calc_color(uint value) {
	return vec4(value==0 ? rgb_true : rgb_false, alpha);
}

void main() {
	gl_Position = transform * vec4(position.x, position.y, 0.0, 1.0);
	
	frag_color = calc_color(texelFetch(data, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).r);
}
