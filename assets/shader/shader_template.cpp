
#include <util/string.hpp>

namespace yggdrasill::shaders {

	extern const yggdrasill::util::String_literal ${CONSTANT_NAME} = R"xxx(${SHADER_CONTENT})xxx"_str;

}
