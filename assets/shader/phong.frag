#version 300 es

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in vec3 frag_pos;

out vec4 color;

uniform vec3 model_space_light;
uniform vec3 model_space_eye;
uniform int lighting;

void main() {
	const float shininess = 256.0;
	const vec3  light_color = vec3(255, 252, 236)/255.0;
	const vec3  model_color = vec3(200, 180, 160)/255.0;
	
	if(lighting==0) {
		color = vec4(light_color*model_color, 1.0);
	
	} else {
		vec3 N = normalize(frag_pos);
		
		vec3 light_dir = model_space_light;
		vec3 view_dir  = normalize(model_space_eye - frag_pos);
		vec3 H         = normalize(light_dir + view_dir);
		
		float diff = clamp(dot(N, light_dir), 0.0, 1.0);
		float spec = clamp(pow(max(dot(N, H), 0.0), shininess), 0.0, 1.0);
		diff -= spec;
		
		color = vec4((0.2+diff*light_color)*model_color + spec*light_color, 1.0);
	}
}
