#version 300 es

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in vec3 frag_weights;

out vec4 color;

uniform float alpha;

void main() {
	vec3 deltas = abs(dFdx(frag_weights)) + abs(dFdy(frag_weights));
	
	vec3 cell_border = smoothstep(deltas*0.1, deltas*3.0, vec3(
		abs(frag_weights[0]-max(frag_weights[1], frag_weights[2])),
		abs(frag_weights[1]-max(frag_weights[0], frag_weights[2])),
		abs(frag_weights[2]-max(frag_weights[1], frag_weights[0]))
	));
	
	color = vec4(vec3(0.0), alpha * (1.0-min(cell_border[0], min(cell_border[1], cell_border[2]))));
}
