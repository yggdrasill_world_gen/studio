#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in vec3 frag_weights;
flat in uvec3 frag_indices;

out vec4 color;

uniform usampler2D data;
uniform vec3 rgb_true;
uniform vec3 rgb_false;
uniform float alpha;

vec3 calc_color(uint value) {
	return value==0 ? rgb_true : rgb_false;
}

void main() {
	vec3 vertex_colors[3] = vec3[](
		calc_color(texelFetch(data, ivec2(frag_indices[0]%MAX_TEX_SIZE, frag_indices[0]/MAX_TEX_SIZE), 0).r),
		calc_color(texelFetch(data, ivec2(frag_indices[1]%MAX_TEX_SIZE, frag_indices[1]/MAX_TEX_SIZE), 0).r),
		calc_color(texelFetch(data, ivec2(frag_indices[2]%MAX_TEX_SIZE, frag_indices[2]/MAX_TEX_SIZE), 0).r)
	);
	
	float max_weight  = max(frag_weights[0], max(frag_weights[1], frag_weights[2]));
	vec3  weights     = normalize(smoothstep(vec3(max_weight*0.99), vec3(max_weight), frag_weights));
	vec3  model_color = vertex_colors[0]*weights.r + vertex_colors[1]*weights.g + vertex_colors[2]*weights.b;
	
	color = vec4(model_color, alpha);
}
