#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in uint index;
in vec2 position;

out vec3 frag_pos;

uniform mat4 transform;
uniform sampler2D  positions;
uniform float radius;

void main() {
	vec3 pos = texelFetch(positions, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).xyz * radius;
	frag_pos = pos;
	gl_Position = transform * vec4(position.x, position.y, 0.0, 1.0);
}
