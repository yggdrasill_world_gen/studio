#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in uint index;
in vec2 position;

out vec3 frag_pos;
out vec3 frag_weights;
out float frag_elevation;
flat out uvec3 frag_indices;

uniform sampler2D plate_elevation;

uniform sampler2D  positions;
uniform usampler2D indices;
uniform float radius;
uniform mat4 projection;
uniform float elevation_scale;
uniform int flattened;

void main() {
	vec3 pos = texelFetch(positions, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).xyz * radius;
	frag_pos = normalize(pos);
	
	frag_elevation = texelFetch(plate_elevation, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).x;
	
	int local = gl_VertexID % 3;
	frag_weights = local==0 ? vec3(1, 0, 0) : (local==1 ? vec3(0, 1, 0) : vec3(0, 0, 1));
	
	uint global = uint(gl_VertexID - local);
	uvec3 indices = uvec3(
		texelFetch(indices, ivec2(global%MAX_TEX_SIZE, global/MAX_TEX_SIZE), 0).r,
		texelFetch(indices, ivec2((global+uint(1))%MAX_TEX_SIZE, (global+uint(1))/MAX_TEX_SIZE), 0).r,
		texelFetch(indices, ivec2((global+uint(2))%MAX_TEX_SIZE, (global+uint(2))/MAX_TEX_SIZE), 0).r
	);
	frag_indices = indices;
	
	gl_Position = projection * vec4(position.x, position.y, 0.0, 1.0);
}
