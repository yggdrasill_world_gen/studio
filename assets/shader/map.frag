#version 300 es

#define MAX_TEX_SIZE uint(4096)

#define M_PI 3.1415926535897932384626433832795f


precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp isampler2D;

in vec3 frag_pos;
in vec3 frag_weights;
in float frag_elevation;
flat in uvec3 frag_indices;

out vec4 color;

uniform isampler2D plate_id;
uniform isampler2D plate_type;
uniform sampler2D plate_elevation;

uniform vec3 model_space_light;
uniform vec3 model_space_eye;

uniform float noise_modulation;


vec3 hsv2rgb(vec3 c)
{
	c /= vec3(360.0, 100.0, 100.0);
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

// adapted from: https://www.shadertoy.com/view/4dS3Wd
// By Morgan McGuire @morgan3d, http://graphicscodex.com
// Reuse permitted under the BSD license.
float hash(float p) { p = fract(p * 0.011); p *= p + 7.5; p *= p + p; return fract(p); }
float hash(vec2 p) { vec3 p3 = fract(vec3(p.xyx) * 0.13); p3 += dot(p3, p3.yzx + 3.333); return fract((p3.x + p3.y) * p3.z); }
float noise(vec3 x) {
    const vec3 step = vec3(110, 241, 171);

    vec3 i = floor(x);
    vec3 f = fract(x);

    // For performance, compute the base input to a 1D hash from the integer part of the argument and the
    // incremental change to the 1D based on the 3D -> 1D wrapping
    float n = dot(i, step);

    vec3 u = f * f * (3.0 - 2.0 * f);
    return mix(mix(mix( hash(n + dot(step, vec3(0, 0, 0))), hash(n + dot(step, vec3(1, 0, 0))), u.x),
                   mix( hash(n + dot(step, vec3(0, 1, 0))), hash(n + dot(step, vec3(1, 1, 0))), u.x), u.y),
               mix(mix( hash(n + dot(step, vec3(0, 0, 1))), hash(n + dot(step, vec3(1, 0, 1))), u.x),
                   mix( hash(n + dot(step, vec3(0, 1, 1))), hash(n + dot(step, vec3(1, 1, 1))), u.x), u.y), u.z);
}

float snoise(vec3 x) {
	return noise(x)*2.0 - 1.0;
}

#define NUM_NOISE_OCTAVES_HIGH 18
#define NUM_NOISE_OCTAVES_LOW 10

float fbm_high(vec3 x) {
	float v = 0.0;
	float a = 0.5;
	vec3 shift = vec3(100);
	for (int i = 0; i < NUM_NOISE_OCTAVES_HIGH; ++i) {
		v += a * noise(x);
		x = x * 2.0 + shift;
		a *= 0.5;
	}
	return v;
}
float fbm_low(vec3 x) {
	float v = 0.0;
	float a = 0.5;
	vec3 shift = vec3(100);
	for (int i = 0; i < NUM_NOISE_OCTAVES_LOW; ++i) {
		v += a * noise(x);
		x = x * 2.0 + shift;
		a *= 0.5;
	}
	return v;
}
float turbulence(vec3 x) {
	float v = 0.0;
	float a = 0.5;
	vec3 shift = vec3(100);
	for (int i = 0; i < NUM_NOISE_OCTAVES_HIGH; ++i) {
		v += a * abs(snoise(x));
		x = x * 2.0 + shift;
		a *= 0.5;
	}

	v = 1.0-v;
	return v*v*v;
}

// returns [0-1]; 1 for elevation==target_elevation
float dist_step(float elevation, float target_elevation, float deviation) {
	float dist = abs(elevation-target_elevation);
	return 1.0 - smoothstep(0.0, deviation, dist);
}
float elevation_noise_high(vec3 seed, float freq, float factor) {
	return noise_modulation * (fbm_high(seed*freq)*2.0-1.0) * factor;
}
float elevation_noise_high_positive(vec3 seed, float freq, float factor) {
	return noise_modulation * (fbm_high(seed*freq)) * factor;
}
float elevation_noise_low(vec3 seed, float freq, float factor) {
	return noise_modulation * (fbm_low(seed*freq)*2.0-1.0) * factor;
}
float elevation_tnoise(vec3 seed, float freq, float factor) {
	return noise_modulation * (turbulence(seed*freq)*2.0-1.0) * factor;
}
float elevation_tnoise_positive(vec3 seed, float freq, float factor) {
	return noise_modulation * (turbulence(seed*freq)) * factor;
}

void main() {
	float elevation = frag_elevation;

	vec3 N = normalize(frag_pos);

	if(noise_modulation>0.0) {
		// modulate elevation with noise
		// global/general noise
		elevation += elevation_noise_low(N+vec3(7, 7, 7), 17.3, 50.0);
		
		float base_elevation = elevation;

		if(base_elevation>=-6000.0 && base_elevation<=1000.0) {
			float xx = elevation_tnoise(N, 7.3, dist_step(base_elevation, -2000.0, 1000.0));
			elevation += sign(xx) * xx*xx*2000.0;
			elevation += (elevation_noise_high(N, 26.73, dist_step(elevation, -500.0, 600.0))*700.0);
		} 
		
		if(base_elevation>=-4000.0 && base_elevation<1000.0) {
			// shallow ocean floor and coast
			elevation += elevation_noise_high(N, 16.2, dist_step(base_elevation, -1500.0, 1500.0)*2000.0);
			elevation += elevation_tnoise_positive(N, 6.3, dist_step(base_elevation, -1500.0, 1500.0)*2000.0);

			// valley
			elevation += elevation_noise_low(N, 12.1, dist_step(base_elevation, 400.0, 800.0)*400.0);
		}
		
		if(base_elevation>=500.0) {
			// (small) mountains
			elevation += elevation_noise_low(N, 13.0, (smoothstep(500.0, 1500.0, base_elevation)*0.75+0.25)*600.0);

			if(base_elevation>=1000.0) {
				// (tall) mountains
				elevation -= max(0.0, elevation_tnoise(N+vec3(3, 3, 3), 8.1, smoothstep(1000.0, 3000.0, base_elevation)*base_elevation/1.5))
						   + elevation_tnoise(N, 7.3, smoothstep(1500.0, 3000.0, base_elevation)*1800.0);
			}
		}
	}

	const float min_elevation = -10000.0;
	const float max_elevation =  10000.0;
	const float abysal_plane  = -6000.0;
	const float shallow_ocean = -500.0;

	if(frag_elevation < min_elevation*1.1f || frag_elevation > max_elevation*1.1f) {
		color = vec4(1,0,0,1);

	} else if(elevation < abysal_plane) {
		float t = 1.0 - (elevation - abysal_plane)/(min_elevation - abysal_plane);
		color = vec4(hsv2rgb(vec3(
		mix(230.0, 220.0, t),
		mix(120.0, 90.0, t),
		mix(20.0, 45.0, t)
		)), 1.0);

	} else if(elevation < shallow_ocean) {
		float t = 1.0 - (elevation - shallow_ocean)/(abysal_plane - shallow_ocean);
		color = vec4(hsv2rgb(vec3(
		mix(220.0, 210.0, t),
		mix(90.0, 70.0, t),
		mix(45.0, 55.0, t)
		)), 1.0);

	} else if(elevation < 0.0) {
		float t = 1.0 - elevation / shallow_ocean;
		color = vec4(hsv2rgb(vec3(
		mix(210.0, 190.0, t),
		mix(70.0, 50.0, t),
		mix(55.0, 85.0, t)
		)), 1.0);

	} else if(elevation < 500.0) {  // grün
		float t = (elevation-0.0)/(500.0-0.0);
		color = vec4(hsv2rgb(vec3(
		mix(145.0, 100.0, t),
		mix(100.0, 50.0, t),
		mix(40.0, 65.0, t)
		)), 1.0);

	} else if(elevation < 2000.0) { // gelb
		float t = (elevation-500.0)/(2000.0-500.0);
		color = vec4(hsv2rgb(vec3(
		mix(100.0, 35.0, t),
		mix(50.0, 62.0, t),
		mix(65.0, 80.0, t)
		)), 1.0);

	} else if(elevation < 5000.0) { // orange
		float t = (elevation-2000.0)/(5000.0-2000.0);
		color = vec4(hsv2rgb(vec3(
		mix(35.0, 22.0, t),
		mix(62.0, 90.0, t),
		mix(80.0, 60.0, t)
		)), 1.0);

	} else if(elevation < 8000.0) { // braun
		float t = min(1.0, (elevation-5000.0)/(8000.0-5000.0));
		color = vec4(hsv2rgb(vec3(
		mix(22.0, 8.0, t),
		mix(90.0, 85.0, t),
		mix(60.0, 35.0, t)
		)), 1.0);
	} else { // weiß
		float t = min(1.0, (elevation-8000.0)/(10000.0-8000.0));
		color = vec4(hsv2rgb(vec3(
		mix(10.0, 10.0, t),
		mix(0.0, 0.0, t),
		mix(70.0, 100.0, t)
		)), 1.0);
	}
}
