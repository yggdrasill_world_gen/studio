#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in uint index;
in vec3 position;

out vec3 frag_pos;
out vec3 frag_weights;
flat out uvec3 frag_indices;

uniform usampler2D indices;
uniform mat4 transform;

void main() {
	frag_pos = position;
	gl_Position = transform * vec4(position, 1.0);
	
	int local = gl_VertexID % 3;
	frag_weights = local==0 ? vec3(1, 0, 0) : (local==1 ? vec3(0, 1, 0) : vec3(0, 0, 1));
	
	uint global = uint(gl_VertexID - local);
	uvec3 indices = uvec3(
		texelFetch(indices, ivec2(global%MAX_TEX_SIZE, global/MAX_TEX_SIZE), 0).r,
		texelFetch(indices, ivec2((global+uint(1))%MAX_TEX_SIZE, (global+uint(1))/MAX_TEX_SIZE), 0).r,
		texelFetch(indices, ivec2((global+uint(2))%MAX_TEX_SIZE, (global+uint(2))/MAX_TEX_SIZE), 0).r
	);
	frag_indices = indices;
}
