#version 300 es

precision highp float;

in vec3 position;
in float elevation;
in vec3 direction;
in vec3 offset;
in vec3 normal;

out vec3 frag_pos;
out vec3 frag_normal;
out vec3 frag_color;

uniform mat4 view;
uniform mat4 projection;
uniform vec4 scale_info;// min/max len(direction); min/max length of final arrow
uniform int mode;
uniform float radius;
uniform float elevation_relief_scale;

#define PI 3.14159265359

vec3 equirectangular_projection(vec4 p)
{

	vec3 N    = normalize(p.xyz);
	float lat = asin(N.y);
	float lon =	abs(N.x) < 0.000001f ? (N.z < 0.f ? 0.f : PI) : atan(N.z, N.x);
	
	return vec3(
		-lon / PI / 2.0 + 0.5,
		(lat*0.98 / PI + 0.5),
		0.0
	);
}

mat3 build_rotation_matrix_sphere(vec3 N) {
	vec3 Y = normalize(direction);
	
	vec3 Z = -N;
	vec3 X = cross(Z, Y);
	float X_len = length(X);
	if (X_len<0.00001) { // Z and Y are parallel => try alternative up vectors
		Z = vec3(1, 0, 0);
		X = cross(Z, Y);
		X_len = length(X);
		if (X_len<0.00001) {
			Z = vec3(0, 1, 0);
			X = cross(Z, Y);
			X_len = length(X);
			if (X_len<0.00001) {
				Z = vec3(0, 0, 1);
				X = cross(Z, Y);
				X_len = length(X);
			}
		}
	}
	
	X /= X_len;
	Z = normalize(cross(X, Y));
	
	return mat3(X, Y, Z);
}

mat3 build_rotation_matrix_equirectangular(vec3 p) {
	vec3 diff = equirectangular_projection(view * vec4(normalize(position+normalize(direction)*0.2), 0.0)) - p;
	float a = abs(diff.x) <0.001 ? (diff.y<0.0 ? PI : 0.0) : atan(-diff.y, diff.x) + PI/2.0;
	
	return mat3(
		cos(a), -sin(a), 0.0,
		sin(a),  cos(a), 0.0,
		   0.0,     0.0, 1.0
	);
}

void main() {
	if(length(direction)<=0.0) {
		frag_pos = vec3(0, 0, 0);
		frag_normal = vec3(0, 0, 0);
		gl_Position = vec4(0, 0, 0, 1);
		return;
	}

	float size = radius * 0.07;
	vec3 p = position * radius;
	vec3 N = normalize(position);
	
	mat3 rotation;
	
	if(mode==0) {
		p += N*elevation_relief_scale*elevation;
		rotation = build_rotation_matrix_sphere(N);
		
	} else {
		size = 0.01;
		p = equirectangular_projection(view * vec4(p, 0.0));
		rotation = build_rotation_matrix_equirectangular(p);
	}
	
	float Y_len = length(direction);
	
	Y_len = mix(scale_info[2], scale_info[3], clamp((Y_len-scale_info[0])/(scale_info[1]-scale_info[0]), 0.0, 1.0));
	float scale = min(Y_len, scale_info[3]/2.0);
	
	rotation[0] *= scale;
	rotation[1] *= Y_len;
	rotation[2] *= scale;
	
	p += N*(size*scale*0.25);
	p += rotation*offset*size;
	
	frag_pos = p;
	frag_normal = rotation*normal;
	frag_color = scale_info[0]>scale_info[1] && Y_len > scale_info[1] ? vec3(1,0,0) : vec3(1,1,1);
	
	if(mode==0) {
		gl_Position = projection * view * vec4(p.xyz, 1.0);
	} else {
		gl_Position = projection * vec4(p.xyz, 1.0);
	}
}
