#version 300 es

precision highp float;

in vec4 frag_color;

out vec4 color_out;

void main() {
	color_out = frag_color;
}
