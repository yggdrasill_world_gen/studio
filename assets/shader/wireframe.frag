#version 300 es

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in vec3 frag_weights;

out vec4 color;

uniform float alpha;

void main() {
	vec3 deltas = abs(dFdx(frag_weights)) + abs(dFdy(frag_weights));
	
	vec3 triangle_border = smoothstep(deltas*0.01, deltas*2.0, frag_weights);
	
	color = vec4(vec3(0.1), alpha * (1.0-min(triangle_border[0], min(triangle_border[1], triangle_border[2]))));
}
