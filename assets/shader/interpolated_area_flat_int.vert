#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp isampler2D;

in uint index;
in vec2 position;

out vec4 frag_color;

uniform mat4 transform;

uniform isampler2D data;
uniform int min_value;
uniform int max_value;
uniform vec3 hsv_min;
uniform vec3 hsv_max;
uniform float alpha;

vec3 hsv2rgb(vec3 c) {
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec4 calc_color(int value) {
	float t = float(value-min_value)/float(max_value-min_value);
	return vec4(hsv2rgb(mix(hsv_min, hsv_max, t)), alpha);
}

void main() {
	gl_Position = transform * vec4(position.x, position.y, 0.0, 1.0);
	
	frag_color = calc_color(texelFetch(data, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).r);
}
