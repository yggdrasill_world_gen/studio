#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in uint index;
in vec2 position;

out vec3 frag_weights;
out vec3 frag_pos;

uniform mat4 transform;
uniform mat3 rotation;
uniform sampler2D  positions;
uniform float radius;

void main() {
	vec3 pos = texelFetch(positions, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).xyz * radius;
	frag_pos = rotation * normalize(pos);

	gl_Position = transform * vec4(position.x, position.y, 0.0, 1.0);
	
	int local = gl_VertexID % 3;
	frag_weights = local==0 ? vec3(1, 0, 0) : (local==1 ? vec3(0, 1, 0) : vec3(0, 0, 1));
}
