#version 300 es

#define M_PI 3.1415926535897932384626433832795f

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp isampler2D;

in vec3 frag_pos;
in vec3 frag_weights;

out vec4 color;

uniform float alpha;
uniform float line_weight;
uniform float density;

vec2 my_fwidth(vec2 c) {
	return abs(dFdx(c)) + abs(dFdy(c));
}

void main() {
	vec3 N = normalize(frag_pos);

	// draw lat/long lines
	vec2 lat_long = vec2(asin(N.y)*2.0, -atan(N.z, N.x)) / M_PI / vec2(2.0, 2.0) + 0.5;

	float steps = (64.0-8.0);
	float density_stepped = floor(max(1.0, density*steps))/steps;
	vec2 line_count = mix(vec2(8.0, 16.0), vec2(64.0,128.0), density_stepped);

	vec2 coord = lat_long*line_count;
	vec2 grid = abs(fract(coord - 0.5) - 0.5) / my_fwidth(coord) / line_weight;

	color.rgb = vec3(0);
	float a = min(grid.x,grid.y);
	color.a = mix(0.0, 1.0 - a*a, alpha) * smoothstep(0.01, 0.05, 1.0 - abs(lat_long.x*2.0-1.0));
}
