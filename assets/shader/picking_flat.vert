#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in vec2 position;

out vec3 frag_weights;
out vec3 frag_position;
flat out uint frag_index_global;

uniform mat4 transform;

uniform usampler2D indices;
uniform sampler2D positions;
uniform float radius;

void main() {
	gl_Position = transform * vec4(position.x, position.y, 0.0, 1.0);
	
	int local = gl_VertexID % 3;
	frag_weights = local==0 ? vec3(1, 0, 0) : (local==1 ? vec3(0, 1, 0) : vec3(0, 0, 1));
	frag_index_global = uint(gl_VertexID - local);

	uint vertex_id = uint(gl_VertexID);
	uint index = texelFetch(indices, ivec2(vertex_id%MAX_TEX_SIZE, vertex_id/MAX_TEX_SIZE), 0).x;
	frag_position = texelFetch(positions, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).xyz * radius;
}
