#version 300 es

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in uint index;
in vec3 position;

out vec3 frag_weights;

uniform mat4 transform;

void main() {
	gl_Position = transform * vec4(position, 1.0);
	
	int local = gl_VertexID % 3;
	frag_weights = local==0 ? vec3(1, 0, 0) : (local==1 ? vec3(0, 1, 0) : vec3(0, 0, 1));
}
