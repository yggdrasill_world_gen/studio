#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in vec3 frag_weights;
flat in uvec3 frag_indices;

out vec4 color;

uniform sampler2D data;
uniform vec3 hsv_min;
uniform vec3 hsv_max;
uniform float min_value;
uniform float max_value;
uniform float alpha;

vec3 hsv2rgb(vec3 c)
{
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
	return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 calc_color(float value) {
	return hsv2rgb(mix(hsv_min, hsv_max, (value-min_value) / (max_value-min_value)));
}

void main() {
	vec3 vertex_colors[3] = vec3[](
		calc_color(texelFetch(data, ivec2(frag_indices[0]%MAX_TEX_SIZE, frag_indices[0]/MAX_TEX_SIZE), 0).r),
		calc_color(texelFetch(data, ivec2(frag_indices[1]%MAX_TEX_SIZE, frag_indices[1]/MAX_TEX_SIZE), 0).r),
		calc_color(texelFetch(data, ivec2(frag_indices[2]%MAX_TEX_SIZE, frag_indices[2]/MAX_TEX_SIZE), 0).r)
	);
	
	float max_weight  = max(frag_weights[0], max(frag_weights[1], frag_weights[2]));
	vec3  weights     = normalize(smoothstep(vec3(max_weight*0.99), vec3(max_weight), frag_weights));
	vec3  model_color = vertex_colors[0]*weights.r + vertex_colors[1]*weights.g + vertex_colors[2]*weights.b;
	
	color = vec4(model_color, alpha);
}
