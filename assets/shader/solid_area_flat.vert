#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in uint index;
in vec2 position;

out vec3 frag_pos;
out vec3 frag_weights;
out float frag_elevation;
flat out uvec3 frag_indices;

uniform sampler2D plate_elevation;

uniform sampler2D  positions;
uniform usampler2D indices;
uniform mat4 transform;
uniform float elevation_scale;

vec3 calc_position(uint index) {
	vec3 pos = texelFetch(positions, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).xyz;
	float elevation = texelFetch(plate_elevation, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).x;
	return pos + normalize(pos)*elevation*elevation_scale;
}

void main() {
	vec3 pos = texelFetch(positions, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).xyz;
	float elevation = texelFetch(plate_elevation, ivec2(index%MAX_TEX_SIZE, index/MAX_TEX_SIZE), 0).x;
	pos += normalize(pos)*elevation*elevation_scale;
	frag_pos = pos;
	gl_Position = transform * vec4(position.x, position.y, 0.0, 1.0);
	
	frag_elevation = elevation;
	
	int local = gl_VertexID % 3;
	frag_weights = local==0 ? vec3(1, 0, 0) : (local==1 ? vec3(0, 1, 0) : vec3(0, 0, 1));
	
	uint global = uint(gl_VertexID - local);
	uvec3 indices = uvec3(
		texelFetch(indices, ivec2(global%MAX_TEX_SIZE, global/MAX_TEX_SIZE), 0).r,
		texelFetch(indices, ivec2((global+uint(1))%MAX_TEX_SIZE, (global+uint(1))/MAX_TEX_SIZE), 0).r,
		texelFetch(indices, ivec2((global+uint(2))%MAX_TEX_SIZE, (global+uint(2))/MAX_TEX_SIZE), 0).r
	);
	frag_indices = indices;
	
	if (frag_indices[0] > frag_indices[1]) {
		frag_indices.xy = frag_indices.yx;
		frag_weights.xy = frag_weights.yx;
	}
	if (frag_indices[1] > frag_indices[2]) {
		frag_indices.yz = frag_indices.zy;
		frag_weights.yz = frag_weights.zy;
	}
	if (frag_indices[0] > frag_indices[1]) {
		frag_indices.xy = frag_indices.yx;
		frag_weights.xy = frag_weights.yx;
	}
}
