#version 300 es

precision highp float;
precision highp int;

in vec3 frag_pos;
in vec3 frag_normal;
in vec3 frag_color;

out vec4 out_color;

uniform vec3 model_space_light;
uniform vec3 model_space_eye;
uniform vec3 color;
uniform int mode;

void main() {
	const float shininess = 1024.0;
	const vec3  light_color = vec3(1.0, 1.0, 1.0);
		
	
	vec3 N = normalize(frag_normal);
	
	vec3 light_dir = model_space_light;
	vec3 view_dir  = normalize(model_space_eye - frag_pos);
	vec3 H         = normalize(light_dir + view_dir);
	
	float diff = clamp(dot(N, light_dir), 0.0, 1.0);
	float spec = clamp(pow(max(dot(N, H), 0.0), shininess), 0.0, 1.0);
	
	out_color = vec4((0.2+diff*light_color)*color*frag_color + spec*light_color, 1.0);
}
