#version 300 es

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;
precision highp isampler2D;

in vec3 frag_pos;
in vec3 frag_weights;

out vec4 color;

uniform float alpha;
uniform float line_weight;
uniform float density;

const float SQRT3 = sqrt(3.0);
const float SQRT3_INV = 1.0/sqrt(3.0);
const float SQRT3_2 = 0.5 * SQRT3;

vec3 getTriangleBarycentric(vec3 p, vec3 p0, vec3 p1, vec3 p2) {
    vec3 v0 = p2 - p0;
    vec3 v1 = p1 - p0;
    vec3 v2 = p - p0;

    float dot00 = dot(v0, v0);
    float dot01 = dot(v0, v1);
    float dot02 = dot(v0, v2);
    float dot11 = dot(v1, v1);
    float dot12 = dot(v1, v2);

    float invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01);

    float s = (dot11 * dot02 - dot01 * dot12) * invDenom;
    float t = (dot00 * dot12 - dot01 * dot02) * invDenom;
    float q = 1.0 - s - t;
    return vec3(s, t, q);
}

float distance_to_hex_line(mat3 icoFace, vec3 pos)
{
    // normalize position to ico face surface
    vec3 normToTriangle = normalize(cross(icoFace[2] - icoFace[0], icoFace[1] - icoFace[0]));
    float triangleToPointDist = length( dot((pos - icoFace[0]), normToTriangle) * normToTriangle );
    pos = pos * (1.0 - triangleToPointDist);

    // calculate 2D coordinates on ico face
    vec3 baryP = getTriangleBarycentric(pos, icoFace[0], icoFace[1], icoFace[2]);
    float d = 1.0 + floor(density*20.0);
    vec2 planarPoint = vec2(
        (baryP.z*SQRT3_2)*d - SQRT3*0.25,
        (baryP.z*0.5+baryP.y)*d
    );

    // transform 2D coordinates to distance from hex-cell border
    planarPoint.x *= SQRT3_INV * 2.0;
    planarPoint.y += mod(floor(planarPoint.x), 2.0)*0.5;
    planarPoint = abs((mod(planarPoint, 1.0) - 0.5));
    return abs(max(planarPoint.x*1.5 + planarPoint.y, planarPoint.y*2.0) - 1.0);
}

bool is_in_triangle(vec3 p, mat3 face)
{
    float s1 = sign( dot(cross(face[0], face[1]), face[2]) );
    float s2 = sign( dot(cross(p,       face[1]), face[2]) );
    float s3 = sign( dot(cross(face[0], p),       face[2]) );
    float s4 = sign( dot(cross(face[0], face[1]), p ) );
    return all(equal(vec3(s2, s3, s4), vec3(s1)));
}

// arbitrary icosahedron, used as a basis for the coordinates to generate a truncated icosahedron
const mat3[] icosahedron_faces = mat3[20] (
    mat3(  -0.52573111211913, 0.0, 0.85065080835204,
           0.0, 0.85065080835204, 0.52573111211913,
           0.52573111211913, 0.0, 0.85065080835204),

    mat3(  -0.52573111211913, 0.0, 0.85065080835204,
           -0.85065080835204, 0.52573111211913, 0.0,
           0.0, 0.85065080835204, 0.52573111211913),

    mat3(  -0.85065080835204, 0.52573111211913, 0.0,
           0.0, 0.85065080835204, -0.52573111211913,
           0.0, 0.85065080835204, 0.52573111211913),

    mat3(  0.0, 0.85065080835204, 0.52573111211913,
           0.0, 0.85065080835204, -0.52573111211913,
           0.85065080835204, 0.52573111211913, 0.0),

    mat3(  0.0, 0.85065080835204, 0.52573111211913,
           0.85065080835204, 0.52573111211913, 0.0,
           0.52573111211913, 0.0, 0.85065080835204),

    mat3(  0.85065080835204, 0.52573111211913, 0.0,
           0.85065080835204, -0.52573111211913, 0.0,
           0.52573111211913, 0.0, 0.85065080835204),

    mat3(  0.85065080835204, 0.52573111211913, 0.0,
           0.52573111211913, 0.0, -0.85065080835204,
           0.85065080835204, -0.52573111211913, 0.0),

    mat3(  0.0, 0.85065080835204, -0.52573111211913,
           0.52573111211913, 0.0, -0.85065080835204,
           0.85065080835204, 0.52573111211913, 0.0),

    mat3(  0.0, 0.85065080835204, -0.52573111211913,
           -0.52573111211913, 0.0, -0.85065080835204,
           0.52573111211913, 0.0, -0.85065080835204),

    mat3( -0.52573111211913, 0.0, -0.85065080835204,
           0.0, -0.85065080835204, -0.52573111211913,
           0.52573111211913, 0.0, -0.85065080835204),

    mat3(  0.0, -0.85065080835204, -0.52573111211913,
           0.85065080835204, -0.52573111211913, 0.0,
           0.52573111211913, 0.0, -0.85065080835204),

    mat3( 0.0, -0.85065080835204, -0.52573111211913,
           0.0, -0.85065080835204, 0.52573111211913,
           0.85065080835204, -0.52573111211913, 0.0),

    mat3( 0.0, -0.85065080835204, -0.52573111211913,
           -0.85065080835204, -0.52573111211913, 0.0,
           0.0, -0.85065080835204, 0.52573111211913),

    mat3( -0.85065080835204, -0.52573111211913, 0.0,
           -0.52573111211913, 0.0, 0.85065080835204,
           0.0, -0.85065080835204, 0.52573111211913),
    mat3( -0.52573111211913, 0.0, 0.85065080835204,
           0.52573111211913, 0.0, 0.85065080835204,
           0.0, -0.85065080835204, 0.52573111211913),

    mat3( 0.0, -0.85065080835204, 0.52573111211913,
           0.52573111211913, 0.0, 0.85065080835204,
           0.85065080835204, -0.52573111211913, 0.0),

    mat3( -0.85065080835204, 0.52573111211913, 0.0,
           -0.52573111211913, 0.0, 0.85065080835204,
           -0.85065080835204, -0.52573111211913, 0.0),
    mat3( -0.85065080835204, 0.52573111211913, 0.0,
           -0.85065080835204, -0.52573111211913, 0.0,
           -0.52573111211913, 0.0, -0.85065080835204),

    mat3( -0.85065080835204, 0.52573111211913, 0.0,
           -0.52573111211913, 0.0, -0.85065080835204,
           0.0, 0.85065080835204, -0.52573111211913),

    mat3( 0.0, -0.85065080835204, -0.52573111211913,
           -0.52573111211913, 0.0, -0.85065080835204,
           -0.85065080835204, -0.52573111211913, 0.0)
);

void main() {
	color = vec4(0);

	vec3 N = normalize(frag_pos);

    // calculate normalization factor, so lines have a constant thickness, independent of zoom level or line density
	float density_stepped = floor(density*20.0)/20.0;
	vec3 deltas = (abs(dFdx(N)) + abs(dFdy(N)));
	float delta = min(max(max(deltas.x, deltas.y), deltas.z), 0.01)*mix(1.5, 40.0, density_stepped);

    // To draw the grid, we need to tile the spheres surface with the desired shape, which is impossible for just hexagons.
    // So we'll also need to introduce some pentagons. The easiest way to achive such a tiling is to generate a truncated icosahedron.
    // Which brings us to the following algorithm:
    //   - Find the triangle face of the icosahedron, that the current point would belong to, i.e. the face that contains it.
    //   - Calculate the barycentric coordinates of the point on this face
    //   - Rotate and overlay the coordinates in such a way, that their local maxima lies in the center of the nearest hexagon/pentagon
    //   - Draw lines at the minima of these coordinates, which corresponds to the borders of the shapes
    //
    // The code is based on this implementation: https://shaderfrog.com/app/view/2977
    //   as well as this (and the linked) post: https://math.stackexchange.com/questions/2810168/complete-tesselation-of-sphere-with-hexagons
    for (int i = 0; i <= 20; ++i) {
        mat3 icoFace = icosahedron_faces[i];
        if (is_in_triangle(N, icoFace)) {
            float dist = distance_to_hex_line(icoFace, N);
            float line = dist/delta/line_weight;
	        color.a = mix(0.0, 1.0 - line*line, alpha);
            break;
        }
    }
}
