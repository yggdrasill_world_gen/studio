#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp int;
precision highp sampler2D;
precision highp usampler2D;

in uint index;
in vec3 position;

out vec3 frag_pos;

uniform mat4 transform;

void main() {
	frag_pos = position;
	gl_Position = transform * vec4(position, 1.0);
}
