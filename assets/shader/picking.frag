#version 300 es

#define MAX_TEX_SIZE uint(4096)

precision highp float;
precision highp usampler2D;
precision highp int;

in vec3 frag_weights;
in vec3 frag_position;
flat in uint frag_index_global;

layout(location = 0) out uvec4 out_indices;
layout(location = 1) out uvec4 out_position;

uniform usampler2D indices;


void main() {
	uvec3 indices = uvec3(
		texelFetch(indices, ivec2(frag_index_global%MAX_TEX_SIZE, frag_index_global/MAX_TEX_SIZE), 0).r,
		texelFetch(indices, ivec2((frag_index_global+uint(1))%MAX_TEX_SIZE, (frag_index_global+uint(1))/MAX_TEX_SIZE), 0).r,
		texelFetch(indices, ivec2((frag_index_global+uint(2))%MAX_TEX_SIZE, (frag_index_global+uint(2))/MAX_TEX_SIZE), 0).r
	);
	
	// write indices to out_indices.rgb, sorted by frag_weights
	if(frag_weights[0] >= frag_weights[1] && frag_weights[0] >= frag_weights[2]) {
		out_indices[0] = indices[0];
		if(frag_weights[1] >= frag_weights[2]) {
			out_indices[1] = indices[1];
			out_indices[2] = indices[2];
		} else {
			out_indices[1] = indices[2];
			out_indices[2] = indices[1];
		}
		
	} else if(frag_weights[1] >= frag_weights[0] && frag_weights[1] >= frag_weights[2]) {
		out_indices[0] = indices[1];
		if(frag_weights[0] >= frag_weights[2]) {
			out_indices[1] = indices[0];
			out_indices[2] = indices[2];
		} else {
			out_indices[1] = indices[2];
			out_indices[2] = indices[0];
		}
		
	} else {
		out_indices[0] = indices[2];
		if(frag_weights[0] >= frag_weights[1]) {
			out_indices[1] = indices[0];
			out_indices[2] = indices[1];
		} else {
			out_indices[1] = indices[1];
			out_indices[2] = indices[0];
		}
	}

	out_position[0] = floatBitsToUint(frag_position[0]);
	out_position[1] = floatBitsToUint(frag_position[1]);
	out_position[2] = floatBitsToUint(frag_position[2]);

	// calculate local resolution in meter per pixel
	out_indices[3] = floatBitsToUint(max(length(dFdx(frag_position)), length(dFdy(frag_position))));
}
