#pragma once

#include <glm/vec2.hpp>

#define IM_VEC2_CLASS_EXTRA      \
	ImVec2(const glm::vec2& rhs) \
	{                            \
		x = rhs.x;               \
		y = rhs.y;               \
	}                            \
	operator glm::vec2() const   \
	{                            \
		return glm::vec2(x, y);  \
	}
