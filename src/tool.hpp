#pragma once

#include "selection.hpp"

#include "simulation/cursor.hpp"
#include "simulation/graph.hpp"
#include "util/string.hpp"

#include <yggdrasill/mesh_utils.hpp>

#include <memory>
#include <unordered_set>

namespace yggdrasill::tools {

	class Tool {
	  public:
		Tool() = default;
		virtual ~Tool();

		virtual util::String_literal button_label() const = 0;
		virtual util::String_literal tooltip() const      = 0;
		virtual auto used_buttons() const -> ImGuiButtonFlags { return ImGuiButtonFlags_MouseButtonLeft; }
		virtual auto readonly() const -> bool { return false; }

		/// called every frame while the tool is selected, before on_use is called
		/// can e.g. be used to display settings
		virtual void update(simulation::Cursor&, Selection&) {}

		/// called while the tool is selected, if the user has selected part of the map and one of the mouse buttons is held down or has just been released
		virtual void on_use(simulation::Cursor&, Selection&, ImGuiButtonFlags pressed_buttons) = 0;

		virtual void draw_status_active(simulation::Cursor&, Selection&) {}
		virtual void draw_status_always(simulation::Cursor&, Selection&) {}

	  protected:
		Tool(const Tool&)            = default;
		Tool(Tool&&)                 = default;
		Tool& operator=(const Tool&) = default;
		Tool& operator=(Tool&&)      = default;

		template <typename F>
		void foreach_vertex_in_range(const simulation::Cursor& state, const Selection& s, float radius, F&& f)
		{
			foreach_vertex_in_range(state, s.vertex(), s.world_position(), radius, std::forward<F>(f));
		}
		template <typename F>
		void foreach_vertex_in_range(
		        const simulation::Cursor& state, Vertex nearest_vertex, const Vec3 p, float radius, F&& f)
		{
			static constexpr auto layer_position = Layer_definition<Vec3, Ref_type::vertex>("position");

			// start from selected vertex and collect all affected vertices
			auto world = state.world();
			if(!world)
				return;

			const auto& mesh         = world->mesh();
			const auto& positions    = world->required_layer(layer_position);
			const auto  world_radius = world->required_unstructured_layer("meta")["radius"].get<float>();
			const auto  radius_2     = radius * radius;

			if(!nearest_vertex.valid(mesh))
				return;

			tmp_seen_vertices_.clear();
			flood_fill(mesh, {nearest_vertex}, Neighbor_type::with_dual_edges, [&](auto v) {
				if(!tmp_seen_vertices_.insert(v).second)
					return false;

				const auto dd = dist2(p, positions[v] * world_radius);

				if(dd >= radius_2 && v != nearest_vertex)
					return false;

				f(v, dd);
				return true;
			});
		}

	  private:
		std::unordered_set<Vertex> tmp_seen_vertices_;
	};

	template <typename Command>
	class Cmd_based_tool : public Tool {
	  public:
		explicit Cmd_based_tool(simulation::Graph& graph) { graph.register_command<Command>(); }

	  protected:
		Cmd_based_tool(const Cmd_based_tool&)            = default;
		Cmd_based_tool(Cmd_based_tool&&)                 = default;
		Cmd_based_tool& operator=(const Cmd_based_tool&) = default;
		Cmd_based_tool& operator=(Cmd_based_tool&&)      = default;

		template <typename... Args>
		auto command(Args&&... args) -> Command&
		{
			if(!command_active()) {
				next_command_ = std::make_unique<Command>(std::forward<Args>(args)...);
			}
			return next_command_ ? *next_command_ : *active_command_;
		}

		bool command_active() const { return active_command_ || next_command_; }

	  private:
		std::unique_ptr<Command> next_command_;
		Command*                 active_command_ = nullptr;
		const simulation::Node*  last_node_      = nullptr;

		virtual bool do_on_use(simulation::Cursor&, Selection&, ImGuiButtonFlags pressed_buttons) = 0;

		void on_use(simulation::Cursor& state, Selection& selection, ImGuiButtonFlags pressed_buttons) override final
		{
			if(*last_node_ != state) {
				active_command_ = nullptr;
				last_node_      = nullptr;
			}

			const auto active = do_on_use(state, selection, pressed_buttons);
			if(active) {
				if(next_command_ && !next_command_->noop()) {
					// new command => execute
					active_command_ = next_command_.get();
					last_node_      = &state.execute(std::move(next_command_));
				} else if(last_node_) {
					// command changed => invalidate node
					state.graph().invalidate(*last_node_);
				}
			} else {
				next_command_.reset();
				active_command_ = nullptr;
				last_node_      = nullptr;
			}
		}
	};

} // namespace yggdrasill::tools
