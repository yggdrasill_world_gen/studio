#pragma once

#include "gui/config.hpp"
#include "util/string.hpp"

#include <yggdrasill/world.hpp>


namespace yggdrasill::graphic {
	class Layer_cache;
}
namespace yggdrasill::renderer {
	struct Compiled_camera;
}

namespace yggdrasill {

	/// A single data-layer visualization. Displayed in the UI as a reorderable layer
	class Layer_renderer {
	  public:
		Layer_renderer() = default;
		virtual ~Layer_renderer();

		/// Name of the layer
		/// Used both in the UI and as a unique id to store config values like position in the layer-list, alpha, ...
		[[nodiscard]] virtual util::String_literal name() const = 0;

		/// Draw on the world-mesh
		/// The OpenGL-State is already setup (see view_renderer.cpp) and the vertex layout for the current
		///   view-mode, its buffers, as well as the index-texture (index 0) are already bound when this function is called.
		/// The depth-buffer is already pre-populated and depth-writes are disabled for calls to this function.
		///
		/// If the function modifies any OpenGL state (besides the bound shader and uniform buffers/textures >=1)
		///   it has to reset it before returning.
		///
		/// This means that this function is limited to drawing on the planets surfaces. Use draw_girmos() for
		///   more complex drawing operations (executed after all draw_planet calls and no requirements/guarantees for OpenGL state).
		///
		/// \param indices The index count of the bound buffers, to be passed to glDrawArrays(GL_TRIANGLES, 0, indices);
		virtual void draw_planet(Const_world_view, const renderer::Compiled_camera&, int indices) {}

		/// General draw function for things like arrows and other geometry not bound to the planets surface.
		/// These functions are called after all draw_planet operations.
		/// Contrary to draw_planet, there are no pre-bound vertex layouts and depth-writes are enabled.
		virtual void draw_gizmos(Const_world_view, const renderer::Compiled_camera&) {}

		/// Function to draw UI elements that allows changing view specific settings
		virtual void draw_settings(gui::Config::Properties& cfg) {}
		virtual bool has_settings() const { return false; }

		/// getter/setter used to toggle drawing of this layer
		void               draw(bool d) { draw_ = d; }
		[[nodiscard]] bool draw() const { return draw_; }

		/// optional alpha value, that can be used by draw_planet() to blend with previous layers
		[[nodiscard]] virtual bool uses_alpha() const { return false; }
		void                       alpha(float a) { alpha_ = a; }
		[[nodiscard]] float        alpha() const { return alpha_; }

		[[nodiscard]] std::uint32_t settings_version() const { return settings_version_; }

	  protected:
		Layer_renderer(const Layer_renderer&)            = default;
		Layer_renderer(Layer_renderer&&)                 = default;
		Layer_renderer& operator=(const Layer_renderer&) = default;
		Layer_renderer& operator=(Layer_renderer&&)      = default;

		void on_settings_changed() { settings_version_++; }

	  private:
		bool          draw_             = true;
		float         alpha_            = 1.f;
		std::uint32_t settings_version_ = 0;
	};

} // namespace yggdrasill
