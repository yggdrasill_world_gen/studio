#pragma once

#include "gui/config.hpp"

namespace yggdrasill {

	class Settings {
	  public:
		explicit Settings(gui::Config::Properties& cfg);

		auto render_scale_interactive() const { return render_scale_interactive_; }
		auto render_scale_static() const { return render_scale_static_; }

		void render_scale_interactive(float);
		void render_scale_static(float);

	  private:
		gui::Config::Properties& cfg_;

		float render_scale_interactive_ = 0.75f;
		float render_scale_static_      = 1.f;
	};

} // namespace yggdrasill