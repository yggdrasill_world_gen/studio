#pragma once

#include "gui/config.hpp"
#include "util/maybe.hpp"
#include "util/string.hpp"

namespace yggdrasill {

	using Dockspace = unsigned int;

	class Window {
	  public:
		Window(Dockspace* dock, gui::Config::Properties& cfg, bool visible)
		  : default_visible_(visible), visible_(cfg.get("visible", visible)), dock_(dock), cfg_(&cfg)
		{
		}
		virtual ~Window() = default;

		// TODO: change to std::string_view once imgui adds support for non-null-terminated strings
		//       see: https://github.com/ocornut/imgui/pull/3038
		virtual const char* id() const = 0;

		/// \returns true if no further messages should be printed
		virtual bool draw_status() { return false; }

		void update();

		void visible(bool v);
		bool visible() const { return visible_; }

		void reset_position()
		{
			reset_position_ = true;
			visible(default_visible_);
		}

	  protected:
		Window(const Window&)            = default;
		Window(Window&&)                 = default;
		Window& operator=(const Window&) = default;
		Window& operator=(Window&&)      = default;

		auto& window_cfg() { return *cfg_; }

	  private:
		bool                     default_visible_;
		bool                     visible_;
		bool                     reset_position_ = false;
		Dockspace*               dock_;
		gui::Config::Properties* cfg_;

		virtual bool do_update() = 0;
		virtual void on_visible_changed(bool v) {}
		virtual void do_always_update() {}
	};

} // namespace yggdrasill
