#include "../layer_renderer.hpp"
#include "../util/string.hpp"

#include <yggdrasill/layer.hpp>

#include <glm/vec3.hpp>

#include <memory>

namespace yggdrasill::views {

	extern std::unique_ptr<Layer_renderer> solid_area_view(util::String_literal view_name,
	                                                       graphic::Layer_cache&,
	                                                       const Layer_definition<YGDL_Bool, Ref_type::vertex>&,
	                                                       const glm::vec3& rgb_true,
	                                                       const glm::vec3& rgb_false);

	extern std::unique_ptr<Layer_renderer> solid_area_view(util::String_literal view_name,
	                                                       graphic::Layer_cache&,
	                                                       const Layer_definition<std::int8_t, Ref_type::vertex>&);

	extern std::unique_ptr<Layer_renderer> solid_area_view(
	        util::String_literal view_name,
	        graphic::Layer_cache&,
	        const Layer_definition<std::int32_t, Ref_type::vertex>&);

	extern std::unique_ptr<Layer_renderer> solid_area_view(util::String_literal view_name,
	                                                       graphic::Layer_cache&,
	                                                       const Layer_definition<float, Ref_type::vertex>&,
	                                                       float            min,
	                                                       float            max,
	                                                       const glm::vec3& hsv_min,
	                                                       const glm::vec3& hsv_max);

	inline std::unique_ptr<Layer_renderer> plates(graphic::Layer_cache& cache)
	{
		return solid_area_view(
		        "Tectonic Plates"_str, cache, Layer_definition<std::int32_t, Ref_type::vertex>("plate_id"));
	}

} // namespace yggdrasill::views
