#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "../layer_renderer.hpp"
#include "../graphic/layer_backed.hpp"
#include "../graphic/shader.hpp"
#include "../renderer/camera.hpp"

#include <yggdrasill/layer.hpp>

#include <yggdrasill_shaders.hpp>


namespace yggdrasill::views {

	namespace {
		constexpr auto layer_position = Layer_definition<Vec3, Ref_type::vertex>("position");

		struct Base_view final : public Layer_renderer {
			graphic::Shader shader_phong =
			        graphic::Shader_compiler("base_sphere")
			                .vertex_shader("shaders::simple_spherical_vert", shaders::simple_spherical_vert)
			                .fragment_shader("shaders::phong_frag", shaders::phong_frag)
			                .vertex_layout(renderer::vertex_layout_spherical)
			                .build();

			graphic::Shader shader_phong_flat =
			        graphic::Shader_compiler("base_flat")
			                .vertex_shader("shaders::simple_flat_vert", shaders::simple_flat_vert)
			                .fragment_shader("shaders::phong_frag", shaders::phong_frag)
			                .vertex_layout(renderer::vertex_layout_flat)
			                .build();

			graphic::Layer_backed<graphic::Texture> vertex_positions;

			Base_view(graphic::Layer_cache& cache) : vertex_positions(cache.texture(layer_position)) {}

			util::String_literal name() const override { return "Base"_str; }

			void draw_planet(Const_world_view world, const renderer::Compiled_camera& state, int indices) override
			{
				using graphic::transform;

				auto& shader =
				        state.view_mode == renderer::View_mode::spherical ? shader_phong : shader_phong_flat;
				shader.bind();

				if(state.view_mode == renderer::View_mode::spherical) {
					shader.set_uniform("transform", state.view_proj);
					shader.set_uniform("lighting", 1);

				} else {
					shader.set_uniform("transform", state.proj);
					shader.set_uniform("lighting", 0);
				}

				shader.set_uniform("model_space_light",
				                   normalize(transform(state.inv_view, {0.3f, 0.5f, 0.5f, 0.f})));
				shader.set_uniform("model_space_eye",
				                   transform(state.inv_view, {0.f, 0.f, state.view_distance, 1.f}));
				shader.set_uniform("radius", world.required_unstructured_layer("meta")["radius"].get<float>());
				shader.set_uniform("indices", 0);
				shader.set_uniform("positions", 1);
				vertex_positions->bind(1);
				glDrawArrays(GL_TRIANGLES, 0, indices);
			}
		};
	} // namespace

	std::unique_ptr<Layer_renderer> base_view(graphic::Layer_cache& cache)
	{
		return std::make_unique<Base_view>(cache);
	}

} // namespace yggdrasill::views
