#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "map.hpp"

#include "../layer_renderer.hpp"
#include "../graphic/layer_backed.hpp"
#include "../graphic/shader.hpp"
#include "../gui/ui_elements.hpp"
#include "../renderer/camera.hpp"

#include <yggdrasill/layer.hpp>
#include <yggdrasill/world.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <yggdrasill_shaders.hpp>


namespace yggdrasill::views {

	namespace {
		constexpr auto layer_id        = Layer_definition<std::int32_t, Ref_type::vertex>("plate_id");
		constexpr auto layer_type      = Layer_definition<int8_t, Ref_type::vertex>("crust_type");
		constexpr auto layer_elevation = Layer_definition<float, Ref_type::vertex>("elevation");
		constexpr auto layer_position  = Layer_definition<Vec3, Ref_type::vertex>("position");

		struct Map_view final : public Layer_renderer {
			graphic::Shader sphere_shader =
			        graphic::Shader_compiler("map (sphere)")
			                .vertex_shader("shaders::map_sphere_vert", shaders::map_sphere_vert)
			                .fragment_shader("shaders::map_frag", shaders::map_frag)
			                .vertex_layout(renderer::vertex_layout_spherical)
			                .build();

			graphic::Shader flat_shader = graphic::Shader_compiler("map (flat)")
			                                      .vertex_shader("shaders::map_flat_vert", shaders::map_flat_vert)
			                                      .fragment_shader("shaders::map_frag", shaders::map_frag)
			                                      .vertex_layout(renderer::vertex_layout_flat)
			                                      .build();

			graphic::Layer_backed<graphic::Texture> vertex_positions;
			graphic::Layer_backed<graphic::Texture> vertex_plate_ids;
			graphic::Layer_backed<graphic::Texture> vertex_plate_types;
			graphic::Layer_backed<graphic::Texture> vertex_elevations;

			float noise_modulation = -0.1f;

			Map_view(graphic::Layer_cache& cache)
			  : vertex_positions(cache.texture(layer_position))
			  , vertex_plate_ids(cache.texture(layer_id))
			  , vertex_plate_types(cache.texture(layer_type))
			  , vertex_elevations(cache.texture(layer_elevation))
			{
			}

			util::String_literal name() const override { return "Map"_str; }

			bool has_settings() const override { return true; }
			void draw_settings(gui::Config::Properties& cfg) override
			{
				if(noise_modulation < 0) {
					noise_modulation = cfg.get("map_noise", 1.f);
				}

				gui::label("Noise");
				if(ImGui::SliderFloat("##Noise", &noise_modulation, 0.f, 4.f)) {
					cfg.set("map_noise", noise_modulation);
					on_settings_changed();
				}
			}

			void draw_planet(Const_world_view world, const renderer::Compiled_camera& state, int indices) override
			{
				using graphic::transform;

				const auto model_space_light = normalize(transform(state.inv_view, {0.6f, 0.4f, 0.6f, 0.f}));
				const auto model_space_eye = transform(state.inv_view, {0.f, 0.f, state.view_distance, 1.f});

				auto& shader = state.view_mode == renderer::View_mode::spherical ? sphere_shader : flat_shader;

				shader.bind();
				shader.set_uniform("projection", state.proj);
				shader.set_uniform("radius", world.required_unstructured_layer("meta")["radius"].get<float>());
				shader.set_uniform("indices", 0);
				shader.set_uniform("positions", 1);
				shader.set_uniform("plate_id", 2);
				shader.set_uniform("plate_type", 3);
				shader.set_uniform("plate_elevation", 4);
				shader.set_uniform("model_space_light", model_space_light);
				shader.set_uniform("model_space_eye", model_space_eye);
				shader.set_uniform("noise_modulation", noise_modulation);

				if(state.view_mode == renderer::View_mode::spherical) {
					sphere_shader.set_uniform("view", state.view);
				}

				vertex_positions->bind(1);
				vertex_plate_ids->bind(2);
				vertex_plate_types->bind(3);
				vertex_elevations->bind(4);

				glDrawArrays(GL_TRIANGLES, 0, indices);
			}
		};
	} // namespace

	std::unique_ptr<Layer_renderer> map(graphic::Layer_cache& cache)
	{
		return std::make_unique<Map_view>(cache);
	}

} // namespace yggdrasill::views
