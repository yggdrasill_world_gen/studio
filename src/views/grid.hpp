#include "../layer_renderer.hpp"

#include <memory>

namespace yggdrasill::views {

	extern std::unique_ptr<Layer_renderer> grid_cells(graphic::Layer_cache&);
	extern std::unique_ptr<Layer_renderer> grid_hex(graphic::Layer_cache&);
	extern std::unique_ptr<Layer_renderer> grid_lat_long(graphic::Layer_cache&);

} // namespace yggdrasill::views
