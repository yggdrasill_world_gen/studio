#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "../layer_renderer.hpp"
#include "../graphic/layer_backed.hpp"
#include "../graphic/shader.hpp"
#include "../graphic/texture.hpp"
#include "../gui/ui_elements.hpp"
#include "../renderer/camera.hpp"

#include <yggdrasill/layer.hpp>

#include <glm/gtx/euler_angles.hpp>
#include <yggdrasill_shaders.hpp>


namespace yggdrasill::views {

	namespace {
		constexpr auto layer_position = Layer_definition<Vec3, Ref_type::vertex>("position");

		struct Grid_view final : public Layer_renderer {
			const util::String_literal              name_;
			graphic::Shader                         shader_spherical;
			graphic::Shader                         shader_flat;
			graphic::Layer_backed<graphic::Texture> vertex_positions;

			bool  enable_settings;
			bool  load_settings      = true;
			float line_weight        = 1.5f;
			float density            = 0.3f;
			float rotation_elevation = 0.f;
			float rotation_azimuth   = 0.f;

			Grid_view(graphic::Layer_cache& cache,
			          util::String_literal  name,
			          std::string_view      frag_shader,
			          bool                  enable_settings)
			  : name_(name)
			  , shader_spherical(graphic::Shader_compiler("cell_sphere")
			                             .vertex_shader("shaders::wireframe_spherical_vert",
			                                            shaders::grid_spherical_vert)
			                             .fragment_shader("shaders::cell_frag", frag_shader)
			                             .vertex_layout(renderer::vertex_layout_spherical)
			                             .build())
			  , shader_flat(graphic::Shader_compiler("cell_flat")
			                        .vertex_shader("shaders::wireframe_flat_vert", shaders::grid_flat_vert)
			                        .fragment_shader("shaders::cell_frag", frag_shader)
			                        .vertex_layout(renderer::vertex_layout_flat)
			                        .build())
			  , vertex_positions(cache.texture(layer_position))
			  , enable_settings(enable_settings)
			{
			}

			util::String_literal name() const override { return name_; }
			bool                 uses_alpha() const override { return true; }

			bool has_settings() const override { return enable_settings; }
			void draw_settings(gui::Config::Properties& cfg) override
			{
				if(!enable_settings)
					return;

				if(load_settings) {
					load_settings      = false;
					line_weight        = cfg.get(name_.str() + "_line_weight", line_weight);
					density            = cfg.get(name_.str() + "_density", density);
					rotation_elevation = cfg.get(name_.str() + "_elevation", rotation_elevation);
					rotation_azimuth   = cfg.get(name_.str() + "_azimuth", rotation_azimuth);
				}

				gui::label("Line weight");
				if(ImGui::SliderFloat("##line_width", &line_weight, 1.f, 5.f, "%.1f")) {
					cfg.set(name_.str() + "_line_weight", line_weight);
					on_settings_changed();
				}

				gui::label("Density");
				if(ImGui::SliderFloat("##density", &density, 0.f, 1.f, "%.2f")) {
					cfg.set(name_.str() + "_density", density);
					on_settings_changed();
				}

				ImGui::TextUnformatted("Rotation");
				gui::label("Elevation");
				if(ImGui::DragFloat("##rotation_elevation", &rotation_elevation, 0.1f, -90.f, 90.f, "%.3f°")) {
					cfg.set(name_.str() + "_elevation", rotation_elevation);
					on_settings_changed();
				}

				gui::label("Azimuth");
				if(ImGui::DragFloat("##rotation_azimuth", &rotation_azimuth, 0.1f, -45.f, 45.f, "%.3f°")) {
					cfg.set(name_.str() + "_azimuth", rotation_azimuth);
					on_settings_changed();
				}
			}

			void draw_planet(Const_world_view world, const renderer::Compiled_camera& state, int indices) override
			{
				const auto sphere = state.view_mode == renderer::View_mode::spherical;

				auto& shader = sphere ? shader_spherical : shader_flat;

				shader.bind();
				shader.set_uniform("transform", sphere ? state.view_proj : state.proj);
				shader.set_uniform("radius", world.required_unstructured_layer("meta")["radius"].get<float>());
				shader.set_uniform("alpha", alpha());
				shader.set_uniform("positions", 1);
				shader.set_uniform("line_weight", line_weight);
				shader.set_uniform("density", density);
				shader.set_uniform("rotation",
				                   glm::mat3(glm::eulerAngleYX(glm::radians(rotation_azimuth),
				                                               glm::radians(rotation_elevation))));

				vertex_positions->bind(1);

				glDrawArrays(GL_TRIANGLES, 0, indices);
			}
		};
	} // namespace

	std::unique_ptr<Layer_renderer> grid_cells(graphic::Layer_cache& cache)
	{
		return std::make_unique<Grid_view>(cache, "Grid: Cells"_str, shaders::grid_cell_frag, false);
	}
	std::unique_ptr<Layer_renderer> grid_hex(graphic::Layer_cache& cache)
	{
		return std::make_unique<Grid_view>(cache, "Grid: Hex"_str, shaders::grid_hex_frag, true);
	}
	std::unique_ptr<Layer_renderer> grid_lat_long(graphic::Layer_cache& cache)
	{
		return std::make_unique<Grid_view>(cache, "Grid: Lat/Long"_str, shaders::grid_lat_long_frag, true);
	}

} // namespace yggdrasill::views
