#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "wireframe.hpp"

#include "../layer_renderer.hpp"
#include "../graphic/shader.hpp"
#include "../renderer/camera.hpp"

#include <yggdrasill/layer.hpp>

#include <yggdrasill_shaders.hpp>


namespace yggdrasill::views {

	namespace {
		struct Wireframe_view final : public Layer_renderer {
			graphic::Shader shader_wireframe =
			        graphic::Shader_compiler("wireframe_sphere")
			                .vertex_shader("shaders::wireframe_spherical_vert", shaders::wireframe_spherical_vert)
			                .fragment_shader("shaders::wireframe_frag", shaders::wireframe_frag)
			                .vertex_layout(renderer::vertex_layout_spherical)
			                .build();

			graphic::Shader shader_wireframe_flat =
			        graphic::Shader_compiler("wireframe_flat")
			                .vertex_shader("shaders::wireframe_flat_vert", shaders::wireframe_flat_vert)
			                .fragment_shader("shaders::wireframe_frag", shaders::wireframe_frag)
			                .vertex_layout(renderer::vertex_layout_flat)
			                .build();

			util::String_literal name() const override { return "Grid: Wireframe"_str; }
			bool                 uses_alpha() const override { return true; }

			void draw_planet(Const_world_view, const renderer::Compiled_camera& state, int indices) override
			{
				const auto sphere = state.view_mode == renderer::View_mode::spherical;
				auto&      shader = sphere ? shader_wireframe : shader_wireframe_flat;

				shader.bind();
				shader.set_uniform("transform", sphere ? state.view_proj : state.proj);
				shader.set_uniform("alpha", alpha());
				glDrawArrays(GL_TRIANGLES, 0, indices);
			}
		};
	} // namespace

	std::unique_ptr<Layer_renderer> wireframe(graphic::Layer_cache&)
	{
		return std::make_unique<Wireframe_view>();
	}

} // namespace yggdrasill::views
