#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "solid_area_views.hpp"

#include "../layer_renderer.hpp"
#include "../graphic/layer_backed.hpp"
#include "../graphic/shader.hpp"
#include "../graphic/vertex_object.hpp"
#include "../gui/ui_elements.hpp"
#include "../renderer/camera.hpp"

#include <yggdrasill/layer.hpp>
#include <yggdrasill/world.hpp>

#include <yggdrasill_shaders.hpp>

namespace yggdrasill::views {

	namespace {
		struct Arrow_vertex {
			Vec3 offset;
			Vec3 normal;
		};
		const auto arrow_layout =
		        graphic::Vertex_layout{graphic::Vertex_layout::Mode::triangles,
		                               graphic::vertex<Vec3>("position", 0, uint8_t(1)),
		                               graphic::vertex<float>("elevation", 1, uint8_t(1)),
		                               graphic::vertex<Vec3>("direction", 2, uint8_t(1)),
		                               graphic::vertex("offset", &Arrow_vertex::offset, 3, uint8_t(0)),
		                               graphic::vertex("normal", &Arrow_vertex::normal, 3, uint8_t(0))};

		constexpr auto layer_position  = Layer_definition<Vec3, Ref_type::vertex>("position");
		constexpr auto layer_elevation = Layer_definition<float, Ref_type::vertex>("elevation");

		constexpr auto arrow_vertices = [] {
			std::array<Arrow_vertex, 4 * 3> v;

			constexpr auto tip   = Vec3{0, 0.5f, 0.f};
			constexpr auto left  = Vec3{-0.2f, -0.5f, -0.2f};
			constexpr auto right = Vec3{+0.2f, -0.5f, -0.2f};
			constexpr auto top   = Vec3{0, -0.5f, +0.2f};

			auto i        = std::size_t(0);
			v[i++].offset = tip;
			v[i++].offset = left;
			v[i++].offset = right;
			for(int j = 1; j <= 3; j++)
				v[i - j].normal = Vec3(0.f, 0.0f, -1.f);

			v[i++].offset = tip;
			v[i++].offset = left;
			v[i++].offset = top;
			for(int j = 1; j <= 3; j++)
				v[i - j].normal = Vec3(-0.88f, 0.176f, 0.44f);

			v[i++].offset = top;
			v[i++].offset = right;
			v[i++].offset = tip;
			for(int j = 1; j <= 3; j++)
				v[i - j].normal = Vec3(0.88f, 0.176f, 0.44f);

			v[i++].offset = right;
			v[i++].offset = top;
			v[i++].offset = left;
			for(int j = 1; j <= 3; j++)
				v[i - j].normal = Vec3(0.f, -1.0f, 0.f);

			return v;
		}();

		glm::vec3 transform(const glm::mat4& m, const glm::vec4& v)
		{
			auto r = m * v;
			return {r.x, r.y, r.z};
		}

		struct Vec3_view final : public Layer_renderer {
			graphic::Shader shader_arrow =
			        graphic::Shader_compiler("vec3 view")
			                .vertex_shader("shaders::vector_view_vertex_vec3_vert",
			                               shaders::vector_view_vertex_vec3_vert)
			                .fragment_shader("shaders::vector_view_frag", shaders::vector_view_frag)
			                .vertex_layout(arrow_layout)
			                .build();

			util::String_literal view_name;

			graphic::Buffer                        arrow_buffer = graphic::create_buffer(arrow_vertices);
			graphic::Layer_backed<graphic::Buffer> position_buffer;
			graphic::Layer_backed<graphic::Buffer> elevation_buffer;
			graphic::Layer_backed<graphic::Buffer> data_buffer;

			const float     min_length;
			const float     max_length;
			const glm::vec3 color;

			float vec_draw_length = -1;

			Vec3_view(util::String_literal                            view_name,
			          graphic::Layer_cache&                           cache,
			          const Layer_definition<Vec3, Ref_type::vertex>& layer,
			          float                                           min_length,
			          float                                           max_length,
			          const glm::vec3&                                color)
			  : view_name(view_name)
			  , position_buffer(cache.buffer(layer_position))
			  , elevation_buffer(cache.buffer(layer_elevation))
			  , data_buffer(cache.buffer(layer))
			  , min_length(min_length)
			  , max_length(max_length)
			  , color(color)
			{
			}

			util::String_literal name() const override { return view_name; }
			bool                 has_settings() const override { return true; }

			void draw_gizmos(Const_world_view world, const renderer::Compiled_camera& state) override
			{
				if(!position_buffer || !data_buffer)
					return;

				const auto model_space_light =
				        state.view_mode == renderer::View_mode::spherical
				                ? normalize(transform(state.inv_view, {0.6f, 0.4f, 0.6f, 0.f}))
				                : normalize(glm::vec3{0.4f, 0.4f, 0.8f});
				const auto model_space_eye = transform(state.inv_view, {0.f, 0.f, state.view_distance, 1.f});

				arrow_layout.build({&*position_buffer, &*elevation_buffer, &*data_buffer, &arrow_buffer});

				shader_arrow.bind();
				shader_arrow.set_uniform("view", state.view);
				shader_arrow.set_uniform("projection", state.proj);
				shader_arrow.set_uniform("radius",
				                         world.required_unstructured_layer("meta")["radius"].get<float>());
				shader_arrow.set_uniform("model_space_light", model_space_light);
				shader_arrow.set_uniform("model_space_eye", model_space_eye);
				shader_arrow.set_uniform(
				        "scale_info", glm::vec4(min_length, max_length, vec_draw_length * 0.1f, vec_draw_length));
				shader_arrow.set_uniform("color", color);
				shader_arrow.set_uniform("mode", static_cast<int>(state.view_mode));
				shader_arrow.set_uniform("elevation_relief_scale", state.elevation_relief_scale);

				glDrawArraysInstanced(GL_TRIANGLES, 0, int(arrow_vertices.size()), int(position_buffer->size()));
			}
			void draw_settings(gui::Config::Properties& cfg) override
			{
				if(vec_draw_length < 0) {
					vec_draw_length = cfg.get(view_name.str() + ".vec_draw_length", 1.f);
				}

				gui::label("Vector Length");
				if(ImGui::SliderFloat("##vec_draw_length", &vec_draw_length, 0.f, 4.f)) {
					cfg.set(view_name.str() + ".vec_draw_length", vec_draw_length);
					on_settings_changed();
				}
			}
		};
	} // namespace

	std::unique_ptr<Layer_renderer> vector_view(util::String_literal                            view_name,
	                                            graphic::Layer_cache&                           cache,
	                                            const Layer_definition<Vec3, Ref_type::vertex>& layer,
	                                            float                                           min_length,
	                                            float                                           max_length,
	                                            const glm::vec3&                                color)
	{
		return std::make_unique<Vec3_view>(view_name, cache, layer, min_length, max_length, color);
	}

} // namespace yggdrasill::views
