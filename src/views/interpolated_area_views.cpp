#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "interpolated_area_views.hpp"

#include "../layer_renderer.hpp"
#include "../graphic/layer_backed.hpp"
#include "../graphic/shader.hpp"
#include "../renderer/camera.hpp"

#include <yggdrasill/layer.hpp>

#include <yggdrasill_shaders.hpp>

namespace yggdrasill::views {

	namespace {
		struct No_uniforms {
			void operator()(Const_world_view, graphic::Shader&) {}
		};

		template <typename AdditionalUniforms = No_uniforms>
		struct Interpolated_area_view : public Layer_renderer {
			util::String_literal view_name;

			graphic::Shader shader_plates;
			graphic::Shader shader_plates_flat;

			graphic::Layer_backed<graphic::Texture> data;

			AdditionalUniforms additional_uniforms;

			Interpolated_area_view(util::String_literal                    view_name,
			                       graphic::Layer_backed<graphic::Texture> data,
			                       std::string_view                        vertex_shader_flat_name,
			                       std::string_view                        vertex_shader_flat,
			                       std::string_view                        vertex_shader_sphere_name,
			                       std::string_view                        vertex_shader_sphere,
			                       AdditionalUniforms&&                    additional_uniforms = {})
			  : view_name(view_name)
			  , shader_plates(graphic::Shader_compiler("interpolated_area_sphere")
			                          .vertex_shader(vertex_shader_sphere_name, vertex_shader_sphere)
			                          .fragment_shader("shaders::simple_frag", shaders::simple_frag)
			                          .vertex_layout(renderer::vertex_layout_spherical)
			                          .build())
			  , shader_plates_flat(graphic::Shader_compiler("interpolated_area_flat")
			                               .vertex_shader(vertex_shader_flat_name, vertex_shader_flat)
			                               .fragment_shader("shaders::simple_frag", shaders::simple_frag)
			                               .vertex_layout(renderer::vertex_layout_flat)
			                               .build())
			  , data(std::move(data))
			  , additional_uniforms(std::move(additional_uniforms))
			{
			}

			[[nodiscard]] util::String_literal name() const override { return view_name; }
			[[nodiscard]] bool                 uses_alpha() const override { return true; }

			virtual void set_uniforms(Const_world_view world, graphic::Shader& shader)
			{
				additional_uniforms(world, shader);
			}

			void draw_planet(Const_world_view world, const renderer::Compiled_camera& state, int indices) override
			{
				if(!data)
					return;

				const auto sphere = state.view_mode == renderer::View_mode::spherical;
				auto&      shader = sphere ? shader_plates : shader_plates_flat;

				shader.bind();
				shader.set_uniform("transform", sphere ? state.view_proj : state.proj);
				shader.set_uniform("indices", 0);
				shader.set_uniform("data", 1);
				shader.set_uniform("alpha", alpha());
				set_uniforms(world, shader);

				data->bind(1);

				glDrawArrays(GL_TRIANGLES, 0, indices);
			}
		};
	} // namespace

	std::unique_ptr<Layer_renderer> interpolated_area_view(util::String_literal  view_name,
	                                                       graphic::Layer_cache& cache,
	                                                       const Layer_definition<YGDL_Bool, Ref_type::vertex>& layer,
	                                                       const glm::vec3& rgb_true,
	                                                       const glm::vec3& rgb_false)
	{
		auto uniforms = [=](Const_world_view, graphic::Shader& shader) {
			shader.set_uniform("rgb_true", rgb_true);
			shader.set_uniform("rgb_false", rgb_false);
		};
		return std::make_unique<Interpolated_area_view<decltype(uniforms)>>(
		        view_name,
		        cache.texture(layer),
		        "shaders::interpolated_area_flat_bool_vert",
		        shaders::interpolated_area_flat_bool_vert,
		        "shaders::interpolated_area_spherical_bool_vert",
		        shaders::interpolated_area_spherical_bool_vert,
		        std::move(uniforms));
	}

	std::unique_ptr<Layer_renderer> interpolated_area_view(
	        util::String_literal                                   view_name,
	        graphic::Layer_cache&                                  cache,
	        const Layer_definition<std::int8_t, Ref_type::vertex>& layer,
	        std::int8_t                                            min,
	        std::int8_t                                            max,
	        const glm::vec3&                                       hsv_min,
	        const glm::vec3&                                       hsv_max)
	{
		auto uniforms = [=](Const_world_view, graphic::Shader& shader) {
			shader.set_uniform("min_value", min);
			shader.set_uniform("max_value", max);
			shader.set_uniform("hsv_min", hsv_min);
			shader.set_uniform("hsv_max", hsv_max);
		};
		return std::make_unique<Interpolated_area_view<decltype(uniforms)>>(
		        view_name,
		        cache.texture(layer),
		        "shaders::interpolated_area_flat_int_vert",
		        shaders::interpolated_area_flat_int_vert,
		        "shaders::interpolated_area_spherical_int_vert",
		        shaders::interpolated_area_spherical_int_vert,
		        std::move(uniforms));
	}

	std::unique_ptr<Layer_renderer> interpolated_area_view(
	        util::String_literal                                    view_name,
	        graphic::Layer_cache&                                   cache,
	        const Layer_definition<std::int32_t, Ref_type::vertex>& layer,
	        std::int32_t                                            min,
	        std::int32_t                                            max,
	        const glm::vec3&                                        hsv_min,
	        const glm::vec3&                                        hsv_max)
	{
		auto uniforms = [=](Const_world_view, graphic::Shader& shader) {
			shader.set_uniform("min_value", min);
			shader.set_uniform("max_value", max);
			shader.set_uniform("hsv_min", hsv_min);
			shader.set_uniform("hsv_max", hsv_max);
		};
		return std::make_unique<Interpolated_area_view<decltype(uniforms)>>(
		        view_name,
		        cache.texture(layer),
		        "shaders::interpolated_area_flat_int_vert",
		        shaders::interpolated_area_flat_int_vert,
		        "shaders::interpolated_area_spherical_int_vert",
		        shaders::interpolated_area_spherical_int_vert,
		        std::move(uniforms));
	}

	std::unique_ptr<Layer_renderer> interpolated_area_view(util::String_literal  view_name,
	                                                       graphic::Layer_cache& cache,
	                                                       const Layer_definition<float, Ref_type::vertex>& layer,
	                                                       float            min,
	                                                       float            max,
	                                                       const glm::vec3& hsv_min,
	                                                       const glm::vec3& hsv_max)
	{
		auto uniforms = [=](Const_world_view, graphic::Shader& shader) {
			shader.set_uniform("min_value", min);
			shader.set_uniform("max_value", max);
			shader.set_uniform("hsv_min", hsv_min);
			shader.set_uniform("hsv_max", hsv_max);
		};
		return std::make_unique<Interpolated_area_view<decltype(uniforms)>>(
		        view_name,
		        cache.texture(layer),
		        "shaders::interpolated_area_flat_float_vert",
		        shaders::interpolated_area_flat_float_vert,
		        "shaders::interpolated_area_spherical_float_vert",
		        shaders::interpolated_area_spherical_float_vert,
		        std::move(uniforms));
	}

	std::unique_ptr<Layer_renderer> age(graphic::Layer_cache& cache)
	{
		auto uniforms = [=](Const_world_view world, graphic::Shader& shader) {
			const auto& meta = world.unstructured_layer("meta");
			const auto  now  = meta ? (*meta)["age"].get(0.f) : 0.f;

			shader.set_uniform("min_value", now - 200'000'000.f);
			shader.set_uniform("max_value", now);
			shader.set_uniform("hsv_min", glm::vec3{0, 0, 1});
			shader.set_uniform("hsv_max", glm::vec3{0, 0, 0});
		};

		return std::make_unique<Interpolated_area_view<decltype(uniforms)>>(
		        "Plate Age"_str,
		        cache.texture(Layer_definition<float, Ref_type::vertex>("crust_created")),
		        "shaders::interpolated_area_flat_float_vert",
		        shaders::interpolated_area_flat_float_vert,
		        "shaders::interpolated_area_spherical_float_vert",
		        shaders::interpolated_area_spherical_float_vert,
		        std::move(uniforms));
	}

} // namespace yggdrasill::views
