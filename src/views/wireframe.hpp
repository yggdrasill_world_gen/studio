#include "../layer_renderer.hpp"

#include <memory>

namespace yggdrasill::views {

	extern std::unique_ptr<Layer_renderer> wireframe(graphic::Layer_cache&);

}
