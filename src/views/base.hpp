#include "../layer_renderer.hpp"

#include <memory>

namespace yggdrasill::views {

	extern std::unique_ptr<Layer_renderer> base_view(graphic::Layer_cache&);

}
