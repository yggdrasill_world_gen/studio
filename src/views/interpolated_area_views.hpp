#include "../layer_renderer.hpp"
#include "../util/string.hpp"

#include <yggdrasill/layer.hpp>

#include <glm/vec3.hpp>

#include <memory>

namespace yggdrasill::views {

	extern std::unique_ptr<Layer_renderer> interpolated_area_view(
	        util::String_literal view_name,
	        graphic::Layer_cache&,
	        const Layer_definition<YGDL_Bool, Ref_type::vertex>&,
	        const glm::vec3& rgb_true,
	        const glm::vec3& rgb_false);

	extern std::unique_ptr<Layer_renderer> interpolated_area_view(
	        util::String_literal view_name,
	        graphic::Layer_cache&,
	        const Layer_definition<std::int8_t, Ref_type::vertex>&,
	        std::int8_t      min,
	        std::int8_t      max,
	        const glm::vec3& hsv_min,
	        const glm::vec3& hsv_max);

	extern std::unique_ptr<Layer_renderer> interpolated_area_view(
	        util::String_literal view_name,
	        graphic::Layer_cache&,
	        const Layer_definition<std::int32_t, Ref_type::vertex>&,
	        std::int32_t     min,
	        std::int32_t     max,
	        const glm::vec3& hsv_min,
	        const glm::vec3& hsv_max);

	extern std::unique_ptr<Layer_renderer> interpolated_area_view(util::String_literal view_name,
	                                                              graphic::Layer_cache&,
	                                                              const Layer_definition<float, Ref_type::vertex>&,
	                                                              float            min,
	                                                              float            max,
	                                                              const glm::vec3& hsv_min,
	                                                              const glm::vec3& hsv_max);

	extern std::unique_ptr<Layer_renderer> age(graphic::Layer_cache& cache);

	inline std::unique_ptr<Layer_renderer> elevation(graphic::Layer_cache& cache)
	{
		return interpolated_area_view("Elevation"_str,
		                              cache,
		                              Layer_definition<float, Ref_type::vertex>("elevation"),
		                              -10'000.f,
		                              +10'000.f,
		                              {0, 0, 0},
		                              {0, 0, 1});
	}

	inline std::unique_ptr<Layer_renderer> target_density(graphic::Layer_cache& cache)
	{
		return interpolated_area_view("Target Density"_str,
		                              cache,
		                              Layer_definition<float, Ref_type::vertex>("refine_target_density"),
		                              0.f,
		                              1.f,
		                              {0, 0, 0},
		                              {0, 0, 1});
	}

} // namespace yggdrasill::views
