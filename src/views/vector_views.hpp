#include "../layer_renderer.hpp"
#include "../util/string.hpp"

#include <yggdrasill/layer.hpp>

#include <glm/vec3.hpp>

#include <memory>

namespace yggdrasill::views {

	extern std::unique_ptr<Layer_renderer> vector_view(util::String_literal view_name,
	                                                   graphic::Layer_cache&,
	                                                   const Layer_definition<Vec3, Ref_type::vertex>&,
	                                                   float            min_length,
	                                                   float            max_length,
	                                                   const glm::vec3& color);

	inline std::unique_ptr<Layer_renderer> plate_velocities(graphic::Layer_cache& cache)
	{
		return vector_view("Plate Velocities"_str,
		                   cache,
		                   Layer_definition<Vec3, Ref_type::vertex>("velocity"),
		                   0.01f,
		                   0.1f,
		                   glm::vec3(200.f, 180.f, 160.f) / 255.f);
	}

} // namespace yggdrasill::views
