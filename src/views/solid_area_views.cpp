#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "solid_area_views.hpp"

#include "../layer_renderer.hpp"
#include "../graphic/layer_backed.hpp"
#include "../graphic/shader.hpp"
#include "../renderer/camera.hpp"

#include <yggdrasill/layer.hpp>

#include <yggdrasill_shaders.hpp>

namespace yggdrasill::views {

	namespace {
		struct No_uniforms {
			constexpr void operator()(graphic::Shader&) {}
		};

		template <typename AdditionalUniforms = No_uniforms>
		struct Solid_area_view final : public Layer_renderer {
			util::String_literal view_name;

			graphic::Shader shader_plates;
			graphic::Shader shader_plates_flat;

			graphic::Layer_backed<graphic::Texture> data;

			AdditionalUniforms additional_uniforms;

			Solid_area_view(util::String_literal                    view_name,
			                graphic::Layer_backed<graphic::Texture> data,
			                std::string_view                        fragment_shader_name,
			                std::string_view                        fragment_shader,
			                AdditionalUniforms&&                    additional_uniforms = {})
			  : view_name(view_name)
			  , shader_plates(graphic::Shader_compiler("solid_area_sphere")
			                          .vertex_shader("shaders::solid_area_vert", shaders::solid_area_vert)
			                          .fragment_shader(fragment_shader_name, fragment_shader)
			                          .vertex_layout(renderer::vertex_layout_spherical)
			                          .build())
			  , shader_plates_flat(
			            graphic::Shader_compiler("solid_area_flat")
			                    .vertex_shader("shaders::solid_area_flat_vert", shaders::solid_area_flat_vert)
			                    .fragment_shader(fragment_shader_name, fragment_shader)
			                    .vertex_layout(renderer::vertex_layout_flat)
			                    .build())
			  , data(std::move(data))
			  , additional_uniforms(std::move(additional_uniforms))
			{
			}

			[[nodiscard]] util::String_literal name() const override { return view_name; }
			[[nodiscard]] bool                 uses_alpha() const override { return true; }

			void draw_planet(Const_world_view, const renderer::Compiled_camera& state, int indices) override
			{
				if(!data)
					return;

				const auto sphere = state.view_mode == renderer::View_mode::spherical;
				auto&      shader = sphere ? shader_plates : shader_plates_flat;

				shader.bind();
				shader.set_uniform("transform", sphere ? state.view_proj : state.proj);
				shader.set_uniform("indices", 0);
				shader.set_uniform("data", 1);
				shader.set_uniform("alpha", alpha());
				additional_uniforms(shader);

				data->bind(1);

				glDrawArrays(GL_TRIANGLES, 0, indices);
			}
		};
	} // namespace

	std::unique_ptr<Layer_renderer> solid_area_view(util::String_literal  view_name,
	                                                graphic::Layer_cache& cache,
	                                                const Layer_definition<YGDL_Bool, Ref_type::vertex>& layer,
	                                                const glm::vec3& rgb_true,
	                                                const glm::vec3& rgb_false)
	{
		auto uniforms = [=](graphic::Shader& shader) {
			shader.set_uniform("rgb_true", rgb_true);
			shader.set_uniform("rgb_false", rgb_false);
		};
		return std::make_unique<Solid_area_view<decltype(uniforms)>>(view_name,
		                                                             cache.texture(layer),
		                                                             "shaders::solid_area_bool_frag",
		                                                             shaders::solid_area_bool_frag,
		                                                             std::move(uniforms));
	}

	std::unique_ptr<Layer_renderer> solid_area_view(util::String_literal  view_name,
	                                                graphic::Layer_cache& cache,
	                                                const Layer_definition<std::int8_t, Ref_type::vertex>& layer)
	{
		return std::make_unique<Solid_area_view<>>(
		        view_name, cache.texture(layer), "shaders::solid_area_int_frag", shaders::solid_area_int_frag);
	}

	std::unique_ptr<Layer_renderer> solid_area_view(util::String_literal  view_name,
	                                                graphic::Layer_cache& cache,
	                                                const Layer_definition<std::int32_t, Ref_type::vertex>& layer)
	{
		return std::make_unique<Solid_area_view<>>(
		        view_name, cache.texture(layer), "shaders::solid_area_int_frag", shaders::solid_area_int_frag);
	}

	std::unique_ptr<Layer_renderer> solid_area_view(util::String_literal  view_name,
	                                                graphic::Layer_cache& cache,
	                                                const Layer_definition<float, Ref_type::vertex>& layer,
	                                                float                                            min,
	                                                float                                            max,
	                                                const glm::vec3&                                 hsv_min,
	                                                const glm::vec3&                                 hsv_max)
	{
		auto uniforms = [=](graphic::Shader& shader) {
			shader.set_uniform("min_value", min);
			shader.set_uniform("max_value", max);
			shader.set_uniform("hsv_min", hsv_min);
			shader.set_uniform("hsv_max", hsv_max);
		};
		return std::make_unique<Solid_area_view<decltype(uniforms)>>(view_name,
		                                                             cache.texture(layer),
		                                                             "shaders::solid_area_float_frag",
		                                                             shaders::solid_area_float_frag,
		                                                             std::move(uniforms));
	}

} // namespace yggdrasill::views
