#include "ui_elements.hpp"

#include "../util/func_traits.hpp"

#include <imgui.h>
#include <imgui_internal.h>

#include <array>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

namespace ImGui {
	extern template float ScaleRatioFromValueT<float, float, float>(
	        ImGuiDataType, float, float, float, bool, float, float);
	extern template float RoundScalarWithFormatT<float>(char const*, int, float);
	extern template float ScaleRatioFromValueT<int, int, float>(ImGuiDataType, int, int, int, bool, float, float);
} // namespace ImGui

namespace yggdrasill::gui {

	bool begin_context_popup_area(const char* str_id, ImVec2 begin, std::optional<ImVec2> end)
	{
		if(!end) {
			// estimate bottom-right of the last line
			end = ImGui::GetCursorScreenPos();
			ImGui::SameLine();
			end->x = ImGui::GetCursorScreenPos().x;
			ImGui::NewLine();
		}

		ImGuiContext& g      = *GImGui;
		ImGuiWindow*  window = g.CurrentWindow;
		if(window->SkipItems)
			return false;
		ImGuiID id = window->GetID(str_id);
		if(ImGui::IsMouseReleased(ImGuiMouseButton_Right)
		   && ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByPopup)
		   && ImGui::IsMouseHoveringRect(begin, *end))
			ImGui::OpenPopupEx(id, 1);
		return ImGui::BeginPopupEx(id,
		                           ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoTitleBar
		                                   | ImGuiWindowFlags_NoSavedSettings);
	}
	void end_context_popup_area()
	{
		ImGui::EndPopup();
	}

	bool checkbox(const char* label, bool& value, bool enabled, const char* tooltip)
	{
		return enabled_if(enabled, [&] {
			auto ret = ImGui::Checkbox(label, &value);

			if(tooltip) {
				gui::ui_help_tooltip(tooltip);
			}

			return ret;
		});
	}

	bool dropdown(const char* label, int64_t& value, std::span<const char* const> value_labels)
	{
		auto v = std::clamp<std::int64_t>(value, 0, static_cast<int64_t>(value_labels.size()));
		if(ImGui::BeginCombo(label, v != value ? "<invalid>" : value_labels[v])) {
			for(int i = 0; i < int64_t(value_labels.size()); i++) {
				if(ImGui::Selectable(value_labels[i], i == v)) {
					value = i;
					ImGui::EndCombo();
					return true;
				}
				if(i == v)
					ImGui::SetItemDefaultFocus();
			}
			ImGui::EndCombo();
		}
		return false;
	}

	bool button(const char* label, bool enabled, const char* tooltip, ImVec2 size)
	{
		if(!enabled) {
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}

		auto ret = ImGui::Button(label, size);

		if(!enabled) {
			ImGui::PopItemFlag();
			ImGui::PopStyleVar();
		}

		if(tooltip) {
			gui::ui_help_tooltip(tooltip);
		}

		return ret;
	}
	bool right_aligned_button(const char* label, bool enabled)
	{
		ImGui::SameLine(ImGui::GetWindowContentRegionMax().x - ImGui::CalcTextSize(label).x - 10);

		return button(label, enabled);
	}

	bool input_int_with_hint(
	        const char* label, const char* hint, int& v, int empty_value, int step, int step_fast, ImGuiInputTextFlags flags)
	{
		using namespace ImGui;

		const char* format    = (flags & ImGuiInputTextFlags_CharsHexadecimal) ? "%08X" : "%d";
		const auto  data_type = ImGuiDataType_S32;

		ImGuiWindow* window = GetCurrentWindow();
		if(window->SkipItems)
			return false;

		ImGuiContext& g     = *GImGui;
		ImGuiStyle&   style = g.Style;

		if(!format)
			format = DataTypeGetInfo(data_type)->PrintFmt;

		char buf[64];
		if(v == empty_value)
			buf[0] = 0;
		else
			DataTypeFormatString(buf, IM_ARRAYSIZE(buf), data_type, &v, format);

		bool value_changed = false;
		if((flags & (ImGuiInputTextFlags_CharsHexadecimal | ImGuiInputTextFlags_CharsScientific)) == 0)
			flags |= ImGuiInputTextFlags_CharsDecimal;
		flags |= ImGuiInputTextFlags_AutoSelectAll;
		flags |= ImGuiInputTextFlags_NoMarkEdited; // We call MarkItemEdited() ourselves by comparing the actual data rather than the string.

		if(step > 0) {
			const float button_size = GetFrameHeight();

			BeginGroup(); // The only purpose of the group here is to allow the caller to query item data e.g. IsItemActive()
			PushID(label);
			SetNextItemWidth(ImMax(1.0f, CalcItemWidth() - (button_size + style.ItemInnerSpacing.x) * 2));
			if(InputTextWithHint("",
			                     hint,
			                     buf,
			                     IM_ARRAYSIZE(buf),
			                     flags)) // PushId(label) + "" gives us the expected ID from outside point of view
				value_changed = DataTypeApplyFromText(buf, data_type, &v, format);

			// Step buttons
			const ImVec2 backup_frame_padding = style.FramePadding;
			style.FramePadding.x              = style.FramePadding.y;
			ImGuiButtonFlags button_flags     = ImGuiButtonFlags_Repeat | ImGuiButtonFlags_DontClosePopups;
			if(flags & ImGuiInputTextFlags_ReadOnly)
				BeginDisabled();
			SameLine(0, style.ItemInnerSpacing.x);
			if(ButtonEx("-", ImVec2(button_size, button_size), button_flags)) {
				DataTypeApplyOp(data_type, '-', &v, &v, g.IO.KeyCtrl && step_fast > 0 ? &step_fast : &step);
				value_changed = true;
			}
			SameLine(0, style.ItemInnerSpacing.x);
			if(ButtonEx("+", ImVec2(button_size, button_size), button_flags)) {
				DataTypeApplyOp(data_type, '+', &v, &v, g.IO.KeyCtrl && step_fast > 0 ? &step_fast : &step);
				value_changed = true;
			}
			if(flags & ImGuiInputTextFlags_ReadOnly)
				EndDisabled();

			const char* label_end = FindRenderedTextEnd(label);
			if(label != label_end) {
				SameLine(0, style.ItemInnerSpacing.x);
				TextEx(label, label_end);
			}
			style.FramePadding = backup_frame_padding;

			PopID();
			EndGroup();
		} else {
			if(InputTextWithHint(label, hint, buf, IM_ARRAYSIZE(buf), flags))
				value_changed = DataTypeApplyFromText(buf, data_type, &v, format);
		}
		if(value_changed)
			MarkItemEdited(g.LastItemData.ID);

		return value_changed;
	}

	void ui_help_tooltip(const char* str)
	{
		if(str && ImGui::IsItemHovered(ImGuiHoveredFlags_AllowWhenDisabled)) {
			ImGui::PushFont(nullptr);
			ImGui::BeginTooltip();
			ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
			ImGui::TextUnformatted(str);
			ImGui::PopTextWrapPos();
			ImGui::EndTooltip();
			ImGui::PopFont();
		}
	}

	bool begin_button_dropdown(const char* label, bool enabled, const char* tooltip, bool right_align)
	{
		if(!enabled) {
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}

		const auto& style = ImGui::GetStyle();

		const auto pos = ImGui::GetCursorScreenPos();

		const auto pressed = ImGui::Button(reinterpret_cast<const char*>(label));
		const auto size    = ImGui::GetItemRectSize();

		if(tooltip) {
			gui::ui_help_tooltip(tooltip);
		}
		if(!enabled) {
			ImGui::PopItemFlag();
			ImGui::PopStyleVar();
		}

		const auto popup_pos  = ImVec2{pos.x + (right_align ? size.x : 0.f), pos.y + size.y};
		const auto max_height = ImGui::GetMainViewport()->Size.y - popup_pos.y - 20.f;

		ImGui::SetNextWindowPos(popup_pos, ImGuiCond_Appearing, right_align ? ImVec2{1, 0} : ImVec2{0, 0});
		ImGui::SetNextWindowSizeConstraints(ImVec2{10.f, 10.f}, ImVec2{999.f, max_height});
		if(pressed) {
			ImGui::OpenPopup(reinterpret_cast<const char*>(label));
		}

		if(ImGui::BeginPopup(reinterpret_cast<const char*>(label))) {
			ImGui::PushStyleColor(ImGuiCol_FrameBg, style.Colors[ImGuiCol_Button]);
			ImGui::PushStyleColor(ImGuiCol_WindowBg, style.Colors[ImGuiCol_Button]);
			ImGui::PushStyleColor(ImGuiCol_ChildBg, style.Colors[ImGuiCol_Button]);
			return true;
		}

		return false;
	}
	void end_button_dropdown()
	{
		ImGui::PopStyleColor(3);
		ImGui::EndPopup();
	}

	static int input_text_callback_ygdl_str(ImGuiInputTextCallbackData* data)
	{
		auto* str = static_cast<String*>(data->UserData);
		if(data->EventFlag == ImGuiInputTextFlags_CallbackResize) {
			IM_ASSERT(data->Buf == str->data());
			str->resize(static_cast<std::size_t>(data->BufTextLen));
			data->Buf = str->data();
		}

		return 0;
	}
	static int input_text_callback_std_str(ImGuiInputTextCallbackData* data)
	{
		auto* str = static_cast<std::string*>(data->UserData);
		if(data->EventFlag == ImGuiInputTextFlags_CallbackResize) {
			IM_ASSERT(data->Buf == str->data());
			str->resize(static_cast<std::size_t>(data->BufTextLen));
			data->Buf = str->data();
		}

		return 0;
	}

	bool input_text(const char* label, String& str, ImGuiInputTextFlags flags)
	{
		return ImGui::InputText(label,
		                        str.data(),
		                        str.capacity() + 1,
		                        flags | ImGuiInputTextFlags_CallbackResize | ImGuiInputTextFlags_EnterReturnsTrue,
		                        input_text_callback_ygdl_str,
		                        &str);
	}

	bool input_text(const char* label, std::string& str, ImGuiInputTextFlags flags)
	{
		return ImGui::InputText(label,
		                        str.data(),
		                        str.capacity() + 1,
		                        flags | ImGuiInputTextFlags_CallbackResize | ImGuiInputTextFlags_EnterReturnsTrue,
		                        input_text_callback_std_str,
		                        &str);
	}

	auto input_text_lazy(const char* label, std::string_view input, ImGuiInputTextFlags flags)
	        -> std::optional<std::string>
	{

		auto callback = [](ImGuiInputTextCallbackData* data) -> int {
			auto* opt = static_cast<std::optional<std::string>*>(data->UserData);
			if(data->EventFlag == ImGuiInputTextFlags_CallbackResize) {
				auto& str = opt->emplace();
				str.resize(static_cast<std::size_t>(data->BufTextLen));
				data->Buf = str.data();
			}

			return 0;
		};

		auto output = std::optional<std::string>();

		if(ImGui::InputText(label,
		                    const_cast<char*>(input.data()), // not modified inside, replaced with output though callback, before modification takes place
		                    input.size() + 1,
		                    flags | ImGuiInputTextFlags_CallbackResize | ImGuiInputTextFlags_EnterReturnsTrue,
		                    callback,
		                    &output)) {
			return output;

		} else {
			return std::nullopt;
		}
	}

	std::string pretty_print_position(const Vec3& p)
	{
		auto pos = normalized(p);
		auto lat = glm::degrees(std::asin(pos.y));
		auto lon = std::abs(pos.x) < 1e-5f ? (pos.z < 0 ? -180.f : 180.f)
		                                   : glm::degrees(-std::atan2(pos.z, pos.x));

		auto ss = std::ostringstream();
		ss << std::fixed << std::setprecision(4);
		ss << (lat > 0 ? 'N' : 'S') << std::abs(lat) << "° ";
		ss << (lon <= 0 ? 'W' : 'E') << std::abs(lon) << "°";
		return std::move(ss).str();
	}

	static ImVector<ImRect> group_panel_label_stack;

	void begin_group_panel(std::string_view name, const ImVec2& size)
	{
		ImGui::BeginGroup();

		auto item_spacing = ImGui::GetStyle().ItemSpacing;
		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.0f, 0.0f));
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 0.0f));

		auto frame_height = ImGui::GetFrameHeight();
		ImGui::BeginGroup();

		ImGui::Dummy(ImVec2(frame_height, 0.0f));
		ImGui::SameLine(0.0f, 0.0f);
		ImGui::BeginGroup();
		ImGui::Dummy(ImVec2(frame_height, 0.0f));
		ImGui::SameLine(0.0f, 0.0f);

		auto label_min = ImVec2(0, 0);
		auto label_max = ImVec2(0, 0);
		if(!name.empty()) {
			ImGui::TextUnformatted(name.data(), name.data() + name.size());
			label_min = ImGui::GetItemRectMin();
			label_max = ImGui::GetItemRectMax();
		}
		ImGui::Dummy(ImVec2(label_max.x - label_min.x, 0));
		ImGui::SameLine(0.0f, 0.0f);
		ImGui::Dummy(ImVec2(0.0, frame_height + item_spacing.y));
		ImGui::BeginGroup();

		auto   org_pos        = ImGui::GetCursorScreenPos();
		ImVec2 effective_size = size;
		if(size.x < 0.0f)
			effective_size.x = ImGui::GetContentRegionAvail().x;
		if(size.y < 0.0f)
			effective_size.x = ImGui::GetContentRegionAvail().y;
		ImGui::Dummy(effective_size);
		ImGui::SetCursorScreenPos(org_pos);

		ImGui::PopStyleVar(2);

		ImGui::GetCurrentWindow()->ContentRegionRect.Max.x -= frame_height * 0.5f;
		ImGui::GetCurrentWindow()->WorkRect.Max.x -= frame_height * 0.5f;
		ImGui::GetCurrentWindow()->InnerRect.Max.x -= frame_height * 0.5f;
		ImGui::GetCurrentWindow()->Size.x -= frame_height;

		auto item_width = ImGui::CalcItemWidth();
		ImGui::PushItemWidth(ImMax(0.0f, item_width - frame_height));

		group_panel_label_stack.push_back(ImRect(label_min, label_max));
	}

	ImRect end_group_panel()
	{
		ImGui::PopItemWidth();

		auto item_spacing = ImGui::GetStyle().ItemSpacing;

		ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0.0f, 0.0f));
		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 0.0f));

		auto frame_height = ImGui::GetFrameHeight();

		ImGui::EndGroup();

		ImGui::EndGroup();

		ImGui::SameLine(0.0f, 0.0f);
		ImGui::Dummy(ImVec2(frame_height * 0.5f, 0.0f));
		ImGui::Dummy(ImVec2(0.0, frame_height - item_spacing.y));

		ImGui::EndGroup();

		auto item_min = ImGui::GetItemRectMin();
		auto item_max = ImGui::GetItemRectMax();

		auto label_rect = group_panel_label_stack.back();
		group_panel_label_stack.pop_back();

		ImVec2 half_frame = ImVec2(frame_height * 0.25f, frame_height) * 0.5f;
		ImRect frame_rect = ImRect(item_min + half_frame, item_max - ImVec2(half_frame.x, 0.0f));
		if(label_rect.Min.x < label_rect.Max.x) {
			label_rect.Min.x -= item_spacing.x;
			label_rect.Max.x += item_spacing.x;
		}
		for(int i = 0; i < 4; ++i) {
			switch(i) {
				// left half-plane
				case 0:
					ImGui::PushClipRect(ImVec2(-FLT_MAX, -FLT_MAX), ImVec2(label_rect.Min.x, FLT_MAX), true);
					break;
				// right half-plane
				case 1:
					ImGui::PushClipRect(ImVec2(label_rect.Max.x, -FLT_MAX), ImVec2(FLT_MAX, FLT_MAX), true);
					break;
				// top
				case 2:
					ImGui::PushClipRect(ImVec2(label_rect.Min.x, -FLT_MAX),
					                    ImVec2(label_rect.Max.x, label_rect.Min.y),
					                    true);
					break;
				// bottom
				case 3:
					ImGui::PushClipRect(ImVec2(label_rect.Min.x, label_rect.Max.y),
					                    ImVec2(label_rect.Max.x, FLT_MAX),
					                    true);
					break;
			}

			ImGui::GetWindowDrawList()->AddRect(frame_rect.Min,
			                                    frame_rect.Max,
			                                    ImColor(ImGui::GetStyleColorVec4(ImGuiCol_Border)),
			                                    half_frame.x);

			ImGui::PopClipRect();
		}

		ImGui::PopStyleVar(2);

		ImGui::GetCurrentWindow()->ContentRegionRect.Max.x += frame_height * 0.5f;
		ImGui::GetCurrentWindow()->WorkRect.Max.x += frame_height * 0.5f;
		ImGui::GetCurrentWindow()->InnerRect.Max.x += frame_height * 0.5f;
		ImGui::GetCurrentWindow()->Size.x += frame_height;

		ImGui::Dummy(ImVec2(0.0f, 0.0f));

		ImGui::EndGroup();

		return frame_rect;
	}


	void spinner(ImDrawList& draw_list, ImVec2 centre, float radius, float thickness, const ImU32& color, bool culling)
	{
		if(culling && !ImGui::IsRectVisible(centre - ImVec2{radius, radius}, centre + ImVec2{radius, radius}))
			return;

		auto& g = *GImGui;

		draw_list.PathClear();

		constexpr auto num_segments = 30;
		const auto     start        = std::abs(std::sin(g.Time * 1.8f) * (num_segments - 5));

		const auto a_min = IM_PI * 2.0f * start / static_cast<float>(num_segments);
		const auto a_max = IM_PI * 2.0f * (num_segments - 3) / static_cast<float>(num_segments);

		for(int i = 0; i < num_segments; i++) {
			const auto alpha = static_cast<float>(g.Time * 8 + a_min
			                                      + (i / static_cast<float>(num_segments)) * (a_max - a_min));
			draw_list.PathLineTo(ImVec2(centre.x + std::cos(alpha) * radius, centre.y + std::sin(alpha) * radius));
		}

		draw_list.PathStroke(color, false, thickness);

		ImGui::SetMaxWaitBeforeNextFrame(0.1f);
	}

	bool spinner(const char* label, float radius, float thickness, const ImU32& color)
	{
		auto window = ImGui::GetCurrentWindow();
		if(window->SkipItems)
			return false;

		const auto& g     = *GImGui;
		const auto& style = g.Style;
		const auto  id    = window->GetID(label);

		const auto pos  = window->DC.CursorPos;
		const auto size = ImVec2{radius * 2, (radius + style.FramePadding.y) * 2};

		const auto bb = ImRect{pos, pos + size};
		ImGui::ItemSize(bb, style.FramePadding.y);
		if(!ImGui::ItemAdd(bb, id))
			return false;

		const auto centre = ImVec2(pos.x + radius, pos.y + radius + style.FramePadding.y);
		spinner(*window->DrawList, centre, radius, thickness, color);
		return true;
	}

	// ~80% common code with ImGui::SliderBehavior
	template <ImGuiDataType DataType, typename T>
	static bool range_slider_behavior(const ImRect&    frame_bb,
	                                  ImGuiID          id,
	                                  T*               v1,
	                                  T*               v2,
	                                  T                v_min,
	                                  T                v_max,
	                                  float            power,
	                                  int              decimal_precision,
	                                  ImGuiSliderFlags flags)
	{
		ImGuiContext&     g      = *GImGui;
		ImGuiWindow*      window = ImGui::GetCurrentWindow();
		const ImGuiStyle& style  = g.Style;

		const bool is_non_linear = (power < 1.0f - 0.00001f) || (power > 1.0f + 0.00001f);
		const bool is_horizontal = (flags & ImGuiSliderFlags_Vertical) == 0;

		const float grab_padding = 2.0f;
		const float slider_sz    = is_horizontal ? (frame_bb.GetWidth() - grab_padding * 2.0f)
		                                         : (frame_bb.GetHeight() - grab_padding * 2.0f);
		float       grab_sz;
		if(decimal_precision > 0)
			grab_sz = ImMin(style.GrabMinSize, slider_sz);
		else
			grab_sz = ImMin(ImMax(1.0f * (slider_sz / ((v_min < v_max ? v_max - v_min : v_min - v_max) + 1.0f)),
			                      style.GrabMinSize),
			                slider_sz); // Integer sliders, if possible have the grab size represent 1 unit
		const float slider_usable_sz = slider_sz - grab_sz;
		const float slider_usable_pos_min =
		        (is_horizontal ? frame_bb.Min.x : frame_bb.Min.y) + grab_padding + grab_sz * 0.5f;
		const float slider_usable_pos_max =
		        (is_horizontal ? frame_bb.Max.x : frame_bb.Max.y) - grab_padding - grab_sz * 0.5f;

		// For logarithmic sliders that cross over sign boundary we want the exponential increase to be symmetric around 0.0f
		float linear_zero_pos = 0.0f; // 0.0->1.0f
		if(v_min * v_max < 0) {
			// Different sign
			const float linear_dist_min_to_0 = std::pow(std::abs(0.0f - v_min), 1.0f / power);
			const float linear_dist_max_to_0 = std::pow(std::abs(v_max - 0.0f), 1.0f / power);
			linear_zero_pos = linear_dist_min_to_0 / (linear_dist_min_to_0 + linear_dist_max_to_0);
		} else {
			// Same sign
			linear_zero_pos = v_min < 0 ? 1.0f : 0.0f;
		}

		// Process clicking on the slider
		bool value_changed = false;
		if(g.ActiveId == id) {
			if(g.IO.MouseDown[0]) {
				const float mouse_abs_pos = is_horizontal ? g.IO.MousePos.x : g.IO.MousePos.y;
				float       clicked_t =
                        (slider_usable_sz > 0.0f)
				                      ? ImClamp((mouse_abs_pos - slider_usable_pos_min) / slider_usable_sz, 0.0f, 1.0f)
				                      : 0.0f;
				if(!is_horizontal)
					clicked_t = 1.0f - clicked_t;

				T new_value;
				if(is_non_linear) {
					// Account for logarithmic scale on both sides of the zero
					if(clicked_t < linear_zero_pos) {
						// Negative: rescale to the negative range before powering
						float a   = 1.0f - (clicked_t / linear_zero_pos);
						a         = std::pow(a, power);
						new_value = ImLerp(ImMin(v_max, T(0)), v_min, a);
					} else {
						// Positive: rescale to the positive range before powering
						float a;
						if(std::abs(linear_zero_pos - 1.0f) > 1.e-6f)
							a = (clicked_t - linear_zero_pos) / (1.0f - linear_zero_pos);
						else
							a = clicked_t;
						a         = powf(a, power);
						new_value = ImLerp(ImMax(v_min, T(0)), v_max, a);
					}
				} else {
					// Linear slider
					new_value = ImLerp(v_min, v_max, clicked_t);
				}

				if constexpr(std::is_floating_point_v<T>) {
					char fmt[64];
					snprintf(fmt, 64, "%%.%df", decimal_precision);

					// Round past decimal precision
					new_value = ImGui::RoundScalarWithFormatT(fmt, DataType, new_value);
				}

				if(*v1 != new_value || *v2 != new_value) {
					if(std::abs(*v1 - new_value) < std::abs(*v2 - new_value)) {
						*v1 = new_value;
					} else {
						*v2 = new_value;
					}
					value_changed = true;
				}
			} else {
				ImGui::ClearActiveID();
			}
		}

		// Calculate slider grab positioning
		float grab_t = ImGui::ScaleRatioFromValueT<T, T, float>(
		        DataType, *v1, v_min, v_max, false, power, linear_zero_pos);

		// Draw
		if(!is_horizontal)
			grab_t = 1.0f - grab_t;
		float  grab_pos = ImLerp(slider_usable_pos_min, slider_usable_pos_max, grab_t);
		ImRect grab_bb1;
		if(is_horizontal)
			grab_bb1 = ImRect(ImVec2(grab_pos - grab_sz * 0.5f, frame_bb.Min.y + grab_padding),
			                  ImVec2(grab_pos + grab_sz * 0.5f, frame_bb.Max.y - grab_padding));
		else
			grab_bb1 = ImRect(ImVec2(frame_bb.Min.x + grab_padding, grab_pos - grab_sz * 0.5f),
			                  ImVec2(frame_bb.Max.x - grab_padding, grab_pos + grab_sz * 0.5f));
		window->DrawList->AddRectFilled(
		        grab_bb1.Min,
		        grab_bb1.Max,
		        ImGui::GetColorU32(g.ActiveId == id ? ImGuiCol_SliderGrabActive : ImGuiCol_SliderGrab),
		        style.GrabRounding);

		// Calculate slider grab positioning
		grab_t = ImGui::ScaleRatioFromValueT<T, T, float>(
		        DataType, *v2, v_min, v_max, false, power, linear_zero_pos);

		// Draw
		if(!is_horizontal)
			grab_t = 1.0f - grab_t;
		grab_pos = ImLerp(slider_usable_pos_min, slider_usable_pos_max, grab_t);
		ImRect grab_bb2;
		if(is_horizontal)
			grab_bb2 = ImRect(ImVec2(grab_pos - grab_sz * 0.5f, frame_bb.Min.y + grab_padding),
			                  ImVec2(grab_pos + grab_sz * 0.5f, frame_bb.Max.y - grab_padding));
		else
			grab_bb2 = ImRect(ImVec2(frame_bb.Min.x + grab_padding, grab_pos - grab_sz * 0.5f),
			                  ImVec2(frame_bb.Max.x - grab_padding, grab_pos + grab_sz * 0.5f));
		window->DrawList->AddRectFilled(
		        grab_bb2.Min,
		        grab_bb2.Max,
		        ImGui::GetColorU32(g.ActiveId == id ? ImGuiCol_SliderGrabActive : ImGuiCol_SliderGrab),
		        style.GrabRounding);

		ImRect connector(grab_bb1.Min, grab_bb2.Max);
		connector.Min.x += grab_sz;
		connector.Min.y += grab_sz * 0.3f;
		connector.Max.x -= grab_sz;
		connector.Max.y -= grab_sz * 0.3f;

		window->DrawList->AddRectFilled(
		        connector.Min, connector.Max, ImGui::GetColorU32(ImGuiCol_SliderGrab), style.GrabRounding);

		return value_changed;
	}

	template <typename T>
	static bool range_slider(
	        const char* label, T* v1, T* v2, T v_min, T v_max, const char* display_format, float power)
	{
		ImGuiWindow* window = ImGui::GetCurrentWindow();
		if(window->SkipItems)
			return false;

		ImGuiContext&     g     = *GImGui;
		const ImGuiStyle& style = g.Style;
		const ImGuiID     id    = window->GetID(label);
		const float       w     = ImGui::CalcItemWidth();

		const ImVec2 label_size = ImGui::CalcTextSize(label, nullptr, true);
		const ImRect frame_bb(window->DC.CursorPos,
		                      window->DC.CursorPos + ImVec2(w, label_size.y + style.FramePadding.y * 2.0f));
		const ImRect total_bb(
		        frame_bb.Min,
		        frame_bb.Max + ImVec2(label_size.x > 0.0f ? style.ItemInnerSpacing.x + label_size.x : 0.0f, 0.0f));

		// NB- we don't call ItemSize() yet because we may turn into a text edit box below
		ImGui::ItemSize(total_bb, style.FramePadding.y);
		if(!ImGui::ItemAdd(total_bb, id, &frame_bb, ImGuiItemFlags_Inputable))
			return false;

		const bool hovered = ImGui::ItemHoverable(frame_bb, id);
		if(hovered)
			ImGui::SetHoveredID(id);

		if(!display_format)
			display_format = "(%.3f, %.3f)";
		int decimal_precision = ImParseFormatPrecision(display_format, 3);

		// Tabbing or CTRL-clicking on Slider turns it into an input box
		bool       start_text_input = false;
		const bool tab_focus_requested =
		        (ImGui::GetItemStatusFlags() & ImGuiItemStatusFlags_FocusedByTabbing) != 0;
		if(tab_focus_requested || (hovered && g.IO.MouseClicked[0])) {
			ImGui::SetActiveID(id, window);
			ImGui::FocusWindow(window);

			if(tab_focus_requested || g.IO.KeyCtrl) {
				start_text_input = true;
				g.TempInputId    = 0;
			}
		}

		if(start_text_input || (g.ActiveId == id && g.TempInputId == id)) {
			char fmt[64];
			snprintf(fmt, 64, "%%.%df", decimal_precision);
			return ImGui::TempInputScalar(frame_bb, id, label, ImGuiDataType_Float, v1, fmt);
		}

		// Draw frame
		const ImU32 frame_col = ImGui::GetColorU32(g.ActiveId == id ? ImGuiCol_FrameBgActive
		                                           : hovered        ? ImGuiCol_FrameBgHovered
		                                                            : ImGuiCol_FrameBg);
		ImGui::RenderNavHighlight(frame_bb, id);
		ImGui::RenderFrame(frame_bb.Min, frame_bb.Max, frame_col, true, g.Style.FrameRounding);

		// Actual slider behavior + render grab
		const bool value_changed = range_slider_behavior<ImGuiDataType_Float>(
		        frame_bb, id, v1, v2, v_min, v_max, power, decimal_precision, 0);

		// Display value using user-provided display format so user can add prefix/suffix/decorations to the value.
		char        value_buf[64];
		const char* value_buf_end =
		        value_buf + ImFormatString(value_buf, IM_ARRAYSIZE(value_buf), display_format, *v1, *v2);
		ImGui::RenderTextClipped(frame_bb.Min, frame_bb.Max, value_buf, value_buf_end, nullptr, ImVec2(0.5f, 0.5f));

		if(label_size.x > 0.0f)
			ImGui::RenderText(
			        ImVec2(frame_bb.Max.x + style.ItemInnerSpacing.x, frame_bb.Min.y + style.FramePadding.y),
			        label);

		IMGUI_TEST_ENGINE_ITEM_INFO(id, label, g.LastItemData.StatusFlags);
		return value_changed;
	}

	bool range_slider_float(
	        const char* label, float* v1, float* v2, float v_min, float v_max, const char* display_format, float power)
	{
		return range_slider(label, v1, v2, v_min, v_max, display_format, power);
	}
	bool range_slider_int(
	        const char* label, int32_t* v1, int32_t* v2, int32_t v_min, int32_t v_max, const char* display_format)
	{
		return range_slider(label, v1, v2, v_min, v_max, display_format, 1.f);
	}

} // namespace yggdrasill::gui

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
