#include "yggdrasill_type_widgets.hpp"

#include "ui_elements.hpp"

#include <imgui.h>
#include <imgui_internal.h>
#include <yggdrasill_icons.hpp>

#include <array>
#include <cinttypes>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

namespace yggdrasill {
	extern float current_time();
} // namespace yggdrasill

namespace yggdrasill::gui {

	static constexpr std::array<const char*, 12> type_dropdown_values{"None",
	                                                                  "Bool",
	                                                                  "Int 8",
	                                                                  "Int 32",
	                                                                  "Int 64",
	                                                                  "Float",
	                                                                  "Double",
	                                                                  "Vec 2D",
	                                                                  "Vec 3D",
	                                                                  "String",
	                                                                  "Array",
	                                                                  "Dictionary"};

	void Edit_state::highlight_current_row(float duration, ImU32 color, std::string tooltip)
	{
		highlighting_ = Highlight{duration,
		                          duration + current_time(),
		                          ImGui::GetID(""),
		                          ImGui::ColorConvertU32ToFloat4(color),
		                          std::move(tooltip)};
	}
	auto Edit_state::handle_highlighting()
	{
		struct Hl_handler {
			bool        highlighted;
			const char* tooltip;

			~Hl_handler()
			{
				operator()();
				if(highlighted)
					ImGui::PopStyleColor();
			}
			void operator()() { ui_help_tooltip(tooltip); }
		};

		const auto highlighted = highlighting_ && highlighting_->id == ImGui::GetID("");
		if(highlighted) {
			const auto t = (current_time() - highlighting_->fade_end_time) / highlighting_->duration;
			ImGui::PushStyleColor(ImGuiCol_Text,
			                      lerp_color(highlighting_->color, ImGui::GetStyleColorVec4(ImGuiCol_Text), t));
		}

		return Hl_handler{highlighted,
		                  highlighted && !highlighting_->tooltip.empty() ? highlighting_->tooltip.c_str()
		                                                                 : nullptr};
	}


	void dictionary_edit(const char* id, Const_dictionary_view dict, Edit_state& edit_state)
	{
		if(edit_state.highlighting_ && current_time() > edit_state.highlighting_->fade_end_time) {
			edit_state.highlighting_.reset();
		}

		ImGui::PushID(id);

		ImGui::SetNextItemWidth(100);
		auto add = input_text("##add_entry", edit_state.new_key_);
		ImGui::SameLine();
		add |= button(YGGDRASILL_ICON_PLUS_SQUARE " Add Entry");

		if(ImGui::BeginTable("rows", 2, ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg)) {
			ImGui::TableSetupColumn("key", ImGuiTableColumnFlags_WidthFixed);
			ImGui::TableSetupColumn("value", ImGuiTableColumnFlags_WidthStretch);

			if(add) {
				ImGui::PushID(edit_state.new_key_.data(),
				              edit_state.new_key_.data() + edit_state.new_key_.size());
				edit_state.highlight_current_row(1.f, "#6f6"_color);
				edit_state.dict_set(edit_state.new_key_, 0);
				ImGui::PopID();
				edit_state.new_key_.clear();
			}

			for(auto&& key : dict.keys()) {
				auto       value   = dict[key];
				const auto type_id = static_cast<std::size_t>(value.type());

				ImGui::PushID(key.data(), key.data() + key.size());
				ON_EXIT(ImGui::PopID());

				ImGui::TableNextColumn();
				auto hl = edit_state.handle_highlighting();

				ImGui::TextUnformatted(key.data(), key.data() + key.size());
				hl();
				ImGui::TableNextColumn();

				ImGui::SetNextItemWidth(80);
				auto new_type = std::optional<Type>();
				if(ImGui::BeginCombo("##type", type_dropdown_values[type_id])) {
					for(std::size_t i = 1; i < type_dropdown_values.size(); i++) {
						if(ImGui::Selectable(type_dropdown_values[i], i == type_id)) {
							new_type = static_cast<Type>(i);
						}
						if(!value.convertible(static_cast<Type>(i))) {
							ImGui::SameLine();
							ImGui::TextUnformatted("*");
						}

						if(i == type_id)
							ImGui::SetItemDefaultFocus();
					}

					ImGui::EndCombo();
				}
				if(new_type) {
					edit_state.dict_set_type(key, *new_type);
				}

				hl();

				ImGui::SameLine();

				ImGui::SetNextItemWidth(100);
				value.visit([&] { ImGui::TextUnformatted("-"); },
				            [&](const YGDL_Bool& v) {
					            bool bv = v == YGDL_TRUE;
					            if(ImGui::Checkbox("##value", &bv)) {
						            edit_state.dict_set(key, bv);
					            }
				            },
				            [&](const std::int8_t& v) {
					            int i = static_cast<int>(v); // NOLINT(bugprone-signed-char-misuse)
					            ImGui::InputInt("##value", &i, 1, 100);
					            if(ImGui::IsItemDeactivatedAfterEdit()) {
						            edit_state.dict_set(key, static_cast<int8_t>(i));
					            }
				            },
				            [&](const std::int32_t& v) {
					            auto vc = v;
					            ImGui::InputInt("##value", &vc, 1, 100);
					            if(ImGui::IsItemDeactivatedAfterEdit()) {
						            edit_state.dict_set(key, vc);
					            }
				            },
				            [&](const std::int64_t& v) {
					            auto vc        = v;
					            auto step      = 1;
					            auto step_fast = 100;
					            ImGui::InputScalar(
					                    "##value", ImGuiDataType_S64, &vc, &step, &step_fast, "%" PRId64);
					            if(ImGui::IsItemDeactivatedAfterEdit()) {
						            edit_state.dict_set(key, vc);
					            }
				            },
				            [&](const float& v) {
					            auto vc = v;
					            ImGui::InputFloat("##value", &vc, 0.001f, 0.1f, "%.6f");
					            if(ImGui::IsItemDeactivatedAfterEdit()) {
						            edit_state.dict_set(key, vc);
					            }
				            },
				            [&](const double& v) {
					            auto vc        = v;
					            auto step      = 0.001f;
					            auto step_fast = 0.1f;
					            ImGui::InputScalar(
					                    "##value", ImGuiDataType_Double, &vc, &step, &step_fast, "%.6f");
					            if(ImGui::IsItemDeactivatedAfterEdit()) {
						            edit_state.dict_set(key, vc);
					            }
				            },
				            [&](const Vec2& v) {
					            auto vc = v;
					            ImGui::InputFloat2("##value", &vc.x, "%.3f");
					            if(ImGui::IsItemDeactivatedAfterEdit()) {
						            edit_state.dict_set(key, vc);
					            }
				            },
				            [&](const Vec3& v) {
					            auto vc = v;
					            ImGui::InputFloat3("##value", &vc.x, "%.3f");
					            if(ImGui::IsItemDeactivatedAfterEdit()) {
						            edit_state.dict_set(key, vc);
					            }
				            },
				            [&](const String& v) {
					            if(auto nv = input_text_lazy("##value", v)) {
						            edit_state.dict_set(key, *nv);
					            }
				            },
				            [&](Const_array_view<void> v) {
					            ImGui::SetNextItemWidth(80);
					            const auto type_id = static_cast<std::size_t>(value.type());
					            if(ImGui::BeginCombo("##sub_type", type_dropdown_values[type_id])) {
						            for(std::size_t i = 0; i < type_dropdown_values.size(); i++) {
							            if(ImGui::Selectable(type_dropdown_values[i], i == type_id)) {
								            edit_state.dict_set_type(key, Type::array_t, static_cast<Type>(i));
								            // cancel drawing the remaining structure, because we might have invalidated it
								            ImGui::EndCombo();
								            return;
							            }
							            if(i == type_id)
								            ImGui::SetItemDefaultFocus();
						            }

						            ImGui::EndCombo();
					            }

					            auto _ = edit_state.push_path(key, Edit_state::Path_element_type::array);
					            array_edit("value", v, edit_state);
				            },
				            [&](Const_dictionary_view v) {
					            auto _ = edit_state.push_path(key, Edit_state::Path_element_type::dict);
					            dictionary_edit("value", v, edit_state);
				            });
			}
			ImGui::EndTable();
		}
		ImGui::PopID();
	}

	void dictionary_view(const char* id, Const_dictionary_view dict)
	{
		ImGui::PushID(id);

		if(ImGui::BeginTable("rows", 2, ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg)) {
			ImGui::TableSetupColumn("key", ImGuiTableColumnFlags_WidthFixed);
			ImGui::TableSetupColumn("value", ImGuiTableColumnFlags_WidthStretch);

			for(auto&& key : dict.keys()) {
				ImGui::TableNextColumn();

				ImGui::PushID(key.data(), key.data() + key.size());
				ImGui::TextUnformatted(key.data(), key.data() + key.size());

				ImGui::TableNextColumn();

				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);

				auto value = dict[key];

				ImGui::SetNextItemWidth(180);
				value.visit([&] { ImGui::TextUnformatted("-"); },
				            [&](const YGDL_Bool& v) {
					            bool bv = v == YGDL_TRUE;
					            ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);

					            ImGui::Checkbox("##value", &bv);

					            ImGui::PopItemFlag();
				            },
				            [&](std::int8_t v) {
					            int i = static_cast<int>(v); // NOLINT(bugprone-signed-char-misuse)
					            ImGui::InputInt("##value", &i, 1, 100, ImGuiInputTextFlags_ReadOnly);
				            },
				            [&](std::int32_t v) {
					            ImGui::InputInt("##value", &v, 1, 100, ImGuiInputTextFlags_ReadOnly);
				            },
				            [&](std::int64_t v) {
					            ImGui::InputScalar("##value",
					                               ImGuiDataType_S64,
					                               &v,
					                               nullptr,
					                               nullptr,
					                               "%" PRId64,
					                               ImGuiInputTextFlags_ReadOnly);
				            },
				            [&](float v) {
					            ImGui::InputFloat("##value", &v, 0.1f, 0.001f, "%.6f", ImGuiInputTextFlags_ReadOnly);
				            },
				            [&](double v) {
					            ImGui::InputScalar("##value",
					                               ImGuiDataType_Double,
					                               &v,
					                               nullptr,
					                               nullptr,
					                               "%.6f",
					                               ImGuiInputTextFlags_ReadOnly);
				            },
				            [&](Vec2 v) {
					            ImGui::InputFloat2("##value", &v.x, "%.3f", ImGuiInputTextFlags_ReadOnly);
				            },
				            [&](Vec3 v) {
					            ImGui::InputFloat3("##value", &v.x, "%.3f", ImGuiInputTextFlags_ReadOnly);
				            },
				            [&](const String& v) {
					            // NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast) non-const required by ImGui API but the value won't be modified
					            input_text("##value", const_cast<String&>(v), ImGuiInputTextFlags_ReadOnly);
				            },
				            [&](Const_array_view<void> v) {
					            ImGui::PopStyleVar();

					            array_view("value", v);

					            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
				            },
				            [&](Const_dictionary_view v) {
					            ImGui::PopStyleVar();

					            dictionary_view("value", v);

					            ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
				            });

				ImGui::PopStyleVar();

				ImGui::PopID();
			}
			ImGui::EndTable();
		}
		ImGui::PopID();
	}

	void array_edit(const char* id, Const_array_view<void> array, Edit_state& edit_state)
	{
		ImGui::PushID(id);

		if(ImGui::BeginTable(
		           "rows", 2, ImGuiTableFlags_SizingFixedFit | ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg)) {

			auto print_rows = [&](auto& values, auto&& val_renderer) {
				for(yggdrasill::index_t i = 0; i < values.size(); i++) {
					ImGui::TableNextColumn();
					ImGui::Text("[%i]", i);
					ImGui::TableNextColumn();
					ImGui::PushID(i);
					auto hl = edit_state.handle_highlighting();
					ImGui::SetNextItemWidth(200);
					val_renderer(i, values[i]);
					hl();

					ImGui::SameLine();
					if(button(YGGDRASILL_ICON_TIMES_CIRCLE, true)) {
						edit_state.array_erase(i);
						i--;
					}

					ImGui::PopID();
				}
			};

			array.visit_value_types(
			        [&] { ImGui::TextUnformatted("-"); },
			        [&](Const_array_view<YGDL_Bool> values) {
				        print_rows(values, [&](index_t i, const auto& v) {
					        bool bv = v == YGDL_TRUE;
					        if(ImGui::Checkbox("##value", &bv)) {
						        edit_state.array_set(i, bv ? YGDL_TRUE : YGDL_FALSE);
					        }
				        });
			        },
			        [&](Const_array_view<std::int8_t> values) {
				        print_rows(values, [&](index_t i, const auto& v) {
					        // NOLINTNEXTLINE(bugprone-signed-char-misuse)
					        auto vc = static_cast<int>(v);
					        ImGui::InputInt("##value", &i, 1, 100);
					        if(ImGui::IsItemDeactivatedAfterEdit()) {
						        edit_state.array_set(i, static_cast<int8_t>(vc));
					        }
				        });
			        },
			        [&](Const_array_view<std::int32_t> values) {
				        print_rows(values, [&](index_t i, auto v) {
					        ImGui::InputInt("##value", &v, 1, 100);
					        if(ImGui::IsItemDeactivatedAfterEdit()) {
						        edit_state.array_set(i, v);
					        }
				        });
			        },
			        [&](Const_array_view<std::int64_t> values) {
				        print_rows(values, [&](index_t i, auto v) {
					        auto step      = 1;
					        auto step_fast = 100;
					        ImGui::InputScalar(
					                "##value", ImGuiDataType_S64, &v, &step, &step_fast, "%" PRId64);
					        if(ImGui::IsItemDeactivatedAfterEdit()) {
						        edit_state.array_set(i, v);
					        }
				        });
			        },
			        [&](Const_array_view<float> values) {
				        print_rows(values, [&](index_t i, auto v) {
					        ImGui::InputFloat("##value", &v, 0.1f, 0.001f, "%.6f");
					        if(ImGui::IsItemDeactivatedAfterEdit()) {
						        edit_state.array_set(i, v);
					        }
				        });
			        },
			        [&](Const_array_view<double> values) {
				        print_rows(values, [&](index_t i, auto v) {
					        auto step      = 0.001f;
					        auto step_fast = 0.1f;
					        ImGui::InputScalar("##value", ImGuiDataType_Double, &v, &step, &step_fast, "%.6f");
					        if(ImGui::IsItemDeactivatedAfterEdit()) {
						        edit_state.array_set(i, v);
					        }
				        });
			        },
			        [&](Const_array_view<Vec2> values) {
				        print_rows(values, [&](index_t i, const auto& v) {
					        auto vc = v;
					        ImGui::InputFloat2("##value", &vc.x, "%.3f");
					        if(ImGui::IsItemDeactivatedAfterEdit()) {
						        edit_state.array_set(i, vc);
					        }
				        });
			        },
			        [&](Const_array_view<Vec3> values) {
				        print_rows(values, [&](index_t i, const auto& v) {
					        auto vc = v;
					        ImGui::InputFloat3("##value", &vc.x, "%.3f");
					        if(ImGui::IsItemDeactivatedAfterEdit()) {
						        edit_state.array_set(i, vc);
					        }
				        });
			        },
			        [&](Const_array_view<String> values) {
				        print_rows(values, [&](index_t i, const auto& v) {
					        if(auto nv = input_text_lazy("##value", v, ImGuiInputTextFlags_EnterReturnsTrue)) {
						        edit_state.array_set(i, *nv);
					        }
				        });
			        },
			        [&](Const_array_view<Const_array_view<void>> values) {
				        print_rows(values, [&](index_t i, const auto& v) {
					        auto _ = edit_state.push_path(i, Edit_state::Path_element_type::array);
					        array_edit("value", v, edit_state);
				        });
			        },
			        [&](Const_array_view<Const_dictionary_view> values) {
				        print_rows(values, [&](index_t i, const auto& v) {
					        auto _ = edit_state.push_path(i, Edit_state::Path_element_type::dict);
					        dictionary_edit("value", v, edit_state);
				        });
			        });

			ImGui::EndTable();

			if(button(YGGDRASILL_ICON_PLUS_SQUARE " Append", true)) {
				ImGui::PushID(array.size());
				edit_state.array_append();
				ImGui::PopID();
			}
		}

		ImGui::PopID();
	}

	void array_view(const char* id, Const_array_view<void> array)
	{
		ImGui::PushID(id);

		if(ImGui::BeginTable(
		           "rows", 2, ImGuiTableFlags_SizingFixedFit | ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg)) {
			// ImGui::SetColumnWidth(0, 20);

			auto print_rows = [&](auto& values, auto&& val_renderer, bool disable = true) {
				for(yggdrasill::index_t i = 0; i < values.size(); i++) {
					ImGui::TableNextColumn();
					ImGui::Text("[%i]", i);
					ImGui::TableNextColumn();
					ImGui::PushID(i);
					ImGui::SetNextItemWidth(200);
					if(disable)
						ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);

					val_renderer(values[i]);

					if(disable)
						ImGui::PopStyleVar();
					ImGui::PopID();
				}
			};

			array.visit_value_types(
			        [&] { ImGui::TextUnformatted("-"); },
			        [&](Const_array_view<YGDL_Bool> values) {
				        print_rows(values, [&](auto v) {
					        bool bv = v == YGDL_TRUE;
					        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
					        ImGui::Checkbox("##value", &bv);
					        ImGui::PopItemFlag();
				        });
			        },
			        [&](Const_array_view<std::int8_t> values) {
				        print_rows(values, [&](auto v) {
					        int i = v; // NOLINT(bugprone-signed-char-misuse)
					        ImGui::InputInt("##value", &i, 1, 100, ImGuiInputTextFlags_ReadOnly);
					        v = static_cast<int8_t>(i);
				        });
			        },
			        [&](Const_array_view<std::int32_t> values) {
				        print_rows(values, [&](auto v) {
					        ImGui::InputInt("##value", &v, 1, 100, ImGuiInputTextFlags_ReadOnly);
				        });
			        },
			        [&](Const_array_view<std::int64_t> values) {
				        print_rows(values, [&](auto v) {
					        ImGui::InputScalar("##value", ImGuiDataType_S64, &v, nullptr, nullptr, "%" PRId64);
				        });
			        },
			        [&](Const_array_view<float> values) {
				        print_rows(values, [&](auto v) {
					        ImGui::InputFloat("##value", &v, 0.1f, 0.001f, "%.6f", ImGuiInputTextFlags_ReadOnly);
				        });
			        },
			        [&](Const_array_view<double> values) {
				        print_rows(values, [&](auto v) {
					        ImGui::InputScalar("##value", ImGuiDataType_Double, &v, nullptr, nullptr, "%.6f");
				        });
			        },
			        [&](Const_array_view<Vec2> values) {
				        print_rows(values, [&](auto v) {
					        ImGui::InputFloat2("##value", &v.x, "%.3f", ImGuiInputTextFlags_ReadOnly);
				        });
			        },
			        [&](Const_array_view<Vec3> values) {
				        print_rows(values, [&](auto v) {
					        ImGui::InputFloat3("##value", &v.x, "%.3f", ImGuiInputTextFlags_ReadOnly);
				        });
			        },
			        [&](Const_array_view<String> values) {
				        print_rows(values, [&](const auto& v) {
					        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast) non-const required by ImGui API but the value won't be modified
					        input_text("##value", const_cast<String&>(v), ImGuiInputTextFlags_ReadOnly);
				        });
			        },
			        [&](Const_array_view<Const_array_view<void>> values) {
				        print_rows(
				                values, [&](const auto& v) { array_view("value", v); }, false);
			        },
			        [&](Const_array_view<Const_dictionary_view> values) {
				        print_rows(
				                values, [&](const auto& v) { dictionary_view("value", v); }, false);
			        });

			ImGui::EndTable();
		}

		ImGui::PopID();
	}


	void Direct_edit_state::on_push_path(std::string_view key, Path_element_type type)
	{
		switch(type) {
			case Path_element_type::array:
				path_stack_.emplace_back(dict()[key].get<Array_view<void>>());
				break;
			case Path_element_type::dict: path_stack_.emplace_back(dict()[key].get<Dictionary_view>()); break;
		}
	}
	void Direct_edit_state::on_push_path(index_t index, Path_element_type type)
	{
		switch(type) {
			case Path_element_type::array:
				path_stack_.emplace_back(array().cast_to<Array_view<void>>()[index]);
				break;
			case Path_element_type::dict:
				path_stack_.emplace_back(array().cast_to<Dictionary_view>()[index]);
				break;
		}
	}
	void Direct_edit_state::on_pop_path()
	{
		path_stack_.pop_back();
	}

	void Direct_edit_state::dict_set_type(std::string_view key, Type new_type, Type element_type)
	{
		dict()[key].type(new_type, element_type);
	}
	void Direct_edit_state::dict_set(std::string_view key, bool new_value)
	{
		dict()[key] = new_value;
	}
	void Direct_edit_state::dict_set(std::string_view key, std::int8_t new_value)
	{
		dict()[key] = new_value;
	}
	void Direct_edit_state::dict_set(std::string_view key, std::int32_t new_value)
	{
		dict()[key] = new_value;
	}
	void Direct_edit_state::dict_set(std::string_view key, std::int64_t new_value)
	{
		dict()[key] = new_value;
	}
	void Direct_edit_state::dict_set(std::string_view key, float new_value)
	{
		dict()[key] = new_value;
	}
	void Direct_edit_state::dict_set(std::string_view key, double new_value)
	{
		dict()[key] = new_value;
	}
	void Direct_edit_state::dict_set(std::string_view key, Vec2 new_value)
	{
		dict()[key] = new_value;
	}
	void Direct_edit_state::dict_set(std::string_view key, Vec3 new_value)
	{
		dict()[key] = new_value;
	}
	void Direct_edit_state::dict_set(std::string_view key, std::string_view new_value)
	{
		dict()[key] = new_value;
	}

	void Direct_edit_state::array_erase(yggdrasill::index_t index)
	{
		array().erase_ordered(index);
	}
	void Direct_edit_state::array_append()
	{
		array().push_back();
	}
	void Direct_edit_state::array_set(index_t index, bool new_value)
	{
		array().cast_to<YGDL_Bool>()[index] = new_value;
	}
	void Direct_edit_state::array_set(index_t index, std::int8_t new_value)
	{
		array().cast_to<int8_t>()[index] = new_value;
	}
	void Direct_edit_state::array_set(index_t index, std::int32_t new_value)
	{
		array().cast_to<int32_t>()[index] = new_value;
	}
	void Direct_edit_state::array_set(index_t index, std::int64_t new_value)
	{
		array().cast_to<int64_t>()[index] = new_value;
	}
	void Direct_edit_state::array_set(index_t index, float new_value)
	{
		array().cast_to<float>()[index] = new_value;
	}
	void Direct_edit_state::array_set(index_t index, double new_value)
	{
		array().cast_to<double>()[index] = new_value;
	}
	void Direct_edit_state::array_set(index_t index, Vec2 new_value)
	{
		array().cast_to<Vec2>()[index] = new_value;
	}
	void Direct_edit_state::array_set(index_t index, Vec3 new_value)
	{
		array().cast_to<Vec3>()[index] = new_value;
	}
	void Direct_edit_state::array_set(index_t index, std::string_view new_value)
	{
		array().cast_to<String>()[index] = new_value;
	}

	static auto traverse_path_impl(const auto& path, std::variant<Dictionary_view, Array_view<void>> root)
	{
		for(auto& n : path) {
			visit(
			        n.key,
			        [&](const std::string_view& key) {
				        auto&& v = (std::get<0>(root))[key];
				        switch(n.type) {
					        case Edit_state::Path_element_type::array:
						        root = v.template get<Array_view<void>>();
						        break;
					        case Edit_state::Path_element_type::dict:
						        root = v.template get<Dictionary_view>();
						        break;
				        }
			        },
			        [&](index_t index) {
				        auto& arr = std::get<1>(root);
				        switch(n.type) {
					        case Edit_state::Path_element_type::array:
						        root = arr.template cast_to<Array_view<void>>()[index];
						        break;
					        case Edit_state::Path_element_type::dict:
						        root = arr.template cast_to<Dictionary_view>()[index];
						        break;
				        }
			        });
		}

		return root;
	}
	static auto traverse_path_impl(const auto&                                                 path,
	                               std::variant<Const_dictionary_view, Const_array_view<void>> root)
	{
		for(auto& n : path) {
			visit(
			        n.key,
			        [&](std::string_view key) {
				        auto&& v = (std::get<0>(root))[key];
				        switch(n.type) {
					        case Edit_state::Path_element_type::array:
						        root = v.template get<Const_array_view<void>>();
						        break;
					        case Edit_state::Path_element_type::dict:
						        root = v.template get<Const_dictionary_view>();
						        break;
				        }
			        },
			        [&](index_t index) {
				        auto& arr = std::get<1>(root);
				        switch(n.type) {
					        case Edit_state::Path_element_type::array:
						        root = arr.template cast_to<Const_array_view<void>>()[index];
						        break;
					        case Edit_state::Path_element_type::dict:
						        root = arr.template cast_to<Const_dictionary_view>()[index];
						        break;
				        }
			        });
		}

		return root;
	}

	class Command_based_edit_state::Dict_edit_command final : public simulation::Command_base<Dict_edit_command> {
	  public:
		struct Path_node {
			std::variant<std::string, index_t> key;
			Edit_state::Path_element_type      type;

			Path_node(std::variant<std::string, index_t>&& key, Edit_state::Path_element_type type)
			  : key(std::move(key)), type(type)
			{
			}

			bool operator==(const Path_node&) const = default;
		};

		template <typename Operation>
		Dict_edit_command(std::vector<Path_node>          path,
		                  std::string                     id,
		                  std::string                     action_description,
		                  simulation::Command_type        type,
		                  Command_based_edit_state::Apply apply,
		                  Operation&&                     op)
		  : path_(std::move(path)), id_(std::move(id)), type_(type), apply_(std::move(apply))
		{
			replace_action(std::move(action_description), std::forward<Operation>(op));
		}

		[[nodiscard]] bool                     force_synchronous() const override { return true; }
		[[nodiscard]] bool                     repeatable() const override { return false; }
		[[nodiscard]] std::optional<float>     estimated_delta_time() const override { return 0.f; }
		[[nodiscard]] std::string_view         description() const override { return description_; }
		[[nodiscard]] simulation::Command_type type() const override { return type_; }
		void execute(simulation::State& state) override { apply_(state, action_); }

		/// not serialized
		std::string_view serialize(Dictionary_view) override { return {}; }

		bool is_same_path(std::string_view id, const std::vector<Path_node>& path)
		{
			if(id.empty() || id_.empty())
				return false; // empty ID == never merge
			return id == id_ && path == path_;
		}
		template <typename Operation>
		void replace_action(std::string description, Operation&& op)
		{
			description_ = std::move(description);
			action_      = [this, op = std::forward<Operation>(op)](
                              std::variant<Dictionary_view, Array_view<void>> arg) { op(traverse_path(arg)); };
		}

	  private:
		std::vector<Path_node>           path_;
		std::string                      id_;
		std::string                      description_;
		simulation::Command_type         type_;
		Command_based_edit_state::Apply  apply_;
		Command_based_edit_state::Action action_;

		auto traverse_path(std::variant<Dictionary_view, Array_view<void>> root)
		        -> std::variant<Dictionary_view, Array_view<void>>
		{
			return traverse_path_impl(path_, root);
		}
	};

	template <typename Cmd, typename Check>
	void Command_based_edit_state::execute_command(std::string id, std::string description, Cmd&& cmd, Check&& check)
	{
		using T = typename util::nth_func_arg_t<std::decay_t<Cmd>, 0>;

		auto path_copy = std::vector<Dict_edit_command::Path_node>();
		path_copy.reserve(path_.size());
		for(const auto& n : path_) {
			path_copy.emplace_back(
			        visit(
			                n.key,
			                [&](const std::string_view& key) -> std::variant<std::string, index_t> {
				                return std::string(key);
			                },
			                [&](const index_t& index) -> std::variant<std::string, index_t> { return index; }),
			        n.type);
		}

		auto final_description = operation_name_ + ": " + description;

		try {
			auto action = [cmd = std::forward<Cmd>(cmd)](std::variant<Dictionary_view, Array_view<void>> arg) {
				if constexpr(std::is_same_v<Dictionary_view, T>) {
					cmd(std::get<Dictionary_view>(arg));
				} else {
					cmd(std::get<Array_view<void>>(arg));
				}
			};

			// if the cursor_ still points to our last node, both it and its command are still alive and we can reuse them
			if(last_added_node_ && cursor_ == *last_added_node_
			   && last_added_command_->is_same_path(id, path_copy)) {
				last_added_command_->replace_action(std::move(final_description), std::move(action));
				cursor_.graph().unsafe_rerun_inplace(*last_added_node_);
			} else {
				auto command = std::make_unique<Dict_edit_command>(std::move(path_copy),
				                                                   std::move(id),
				                                                   std::move(final_description),
				                                                   type_,
				                                                   apply_,
				                                                   std::move(action));

				// weak-reference is ok, because end-of-lifetime is checked above using cursor_
				last_added_command_ = command.get();
				last_added_node_    = &cursor_.execute(std::move(command));

				if(post_execute_) {
					post_execute_(*last_added_node_);
				}
			}

			const auto success = [&] {
				auto new_value = traverse_path_impl(path_, get_());
				if constexpr(std::is_same_v<Dictionary_view, T>) {
					return check(std::get<Const_dictionary_view>(new_value));
				} else {
					return check(std::get<Const_array_view<void>>(new_value));
				}
			}();

			if(success)
				highlight_current_row(2.f, "#6f6"_color);
			else
				highlight_current_row(4.f, "#fa0"_color, "Change was overriden by validation");

		} catch(const std::exception& e) {
			std::cout << "Edit operation failed with " << e.what() << '\n';
			highlight_current_row(60.f, "#f66"_color, e.what());
		}
	}

	template <class T, typename = decltype(T::key), typename = decltype(T::type)>
	static std::ostream& operator<<(std::ostream& os, const std::vector<T>& path)
	{
		for(auto& n : path) {
			visit(
			        n.key,
			        [&](const std::string_view& key) { os << '.' << key; },
			        [&](const index_t& index) { os << '[' << index << ']'; });
		}

		return os;
	}

	void Command_based_edit_state::on_push_path(std::string_view key, Path_element_type type)
	{
		path_.emplace_back(key, type);
	}
	void Command_based_edit_state::on_push_path(index_t key, Path_element_type type)
	{
		path_.emplace_back(key, type);
	}
	void Command_based_edit_state::on_pop_path()
	{
		path_.pop_back();
	}

	void Command_based_edit_state::dict_set_type(std::string_view key, Type type, Type element_type)
	{
		auto array_type = std::string();
		if(type == Type::array_t && element_type != Type::none) {
			array_type = util::concat(" of '", type_name(element_type), '\'');
		}
		execute_command(
		        util::concat("Cast ", path_, ".", key),
		        util::concat("Cast ", path_, ".", key, " to '", type_name(type), '\'', array_type),
		        [key = std::string(key), type, element_type](Dictionary_view dict) {
			        dict[key].type(type, element_type);
		        },
		        [key = std::string(key), type](Const_dictionary_view dict) { return dict[key].type() == type; });
	}
	void Command_based_edit_state::dict_set(std::string_view key, bool new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", new_value, "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}
	void Command_based_edit_state::dict_set(std::string_view key, std::int8_t new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", int(new_value), "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}
	void Command_based_edit_state::dict_set(std::string_view key, std::int32_t new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", new_value, "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}
	void Command_based_edit_state::dict_set(std::string_view key, std::int64_t new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", new_value, "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}
	void Command_based_edit_state::dict_set(std::string_view key, float new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", new_value, "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}
	void Command_based_edit_state::dict_set(std::string_view key, double new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", new_value, "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}
	void Command_based_edit_state::dict_set(std::string_view key, Vec2 new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", new_value, "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}
	void Command_based_edit_state::dict_set(std::string_view key, Vec3 new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", new_value, "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}
	void Command_based_edit_state::dict_set(std::string_view key, std::string_view new_value)
	{
		execute_command(
		        util::concat("Set ", path_, ".", key),
		        util::concat("Set ", path_, ".", key, " to '", new_value, "'"),
		        [key = std::string(key), new_value](Dictionary_view dict) { dict[key] = new_value; },
		        [key = std::string(key), new_value](Const_dictionary_view dict) {
			        return dict[key] == new_value;
		        });
	}

	void Command_based_edit_state::array_erase(yggdrasill::index_t index)
	{
		execute_command(
		        util::concat("Erase index ", index, " from ", path_),
		        util::concat("Erase index ", index, " from ", path_),
		        [index](Array_view<void> array) { array.erase_ordered(index); },
		        [](Const_array_view<void> array) { return true; });
	}
	void Command_based_edit_state::array_append()
	{
		execute_command(
		        std::string{},
		        util::concat("Append element to ", path_),
		        [](Array_view<void> array) { array.push_back(); },
		        [](Const_array_view<void> array) { return true; });
	}
	void Command_based_edit_state::array_set(index_t index, bool new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<YGDL_Bool>()[index] = new_value; },
		        [=](Const_array_view<void> array) {
			        return (array.cast_to<YGDL_Bool>()[index] == YGDL_TRUE) == new_value;
		        });
	}
	void Command_based_edit_state::array_set(index_t index, std::int8_t new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<int8_t>()[index] = new_value; },
		        [=](Const_array_view<void> array) { return array.cast_to<int8_t>()[index] == new_value; });
	}
	void Command_based_edit_state::array_set(index_t index, std::int32_t new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<int32_t>()[index] = new_value; },
		        [=](Const_array_view<void> array) { return array.cast_to<int32_t>()[index] == new_value; });
	}
	void Command_based_edit_state::array_set(index_t index, std::int64_t new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<int64_t>()[index] = new_value; },
		        [=](Const_array_view<void> array) { return array.cast_to<int64_t>()[index] == new_value; });
	}
	void Command_based_edit_state::array_set(index_t index, float new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<float>()[index] = new_value; },
		        [=](Const_array_view<void> array) { return array.cast_to<float>()[index] == new_value; });
	}
	void Command_based_edit_state::array_set(index_t index, double new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<double>()[index] = new_value; },
		        [=](Const_array_view<void> array) { return array.cast_to<double>()[index] == new_value; });
	}
	void Command_based_edit_state::array_set(index_t index, Vec2 new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<Vec2>()[index] = new_value; },
		        [=](Const_array_view<void> array) { return array.cast_to<Vec2>()[index] == new_value; });
	}
	void Command_based_edit_state::array_set(index_t index, Vec3 new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<Vec3>()[index] = new_value; },
		        [=](Const_array_view<void> array) { return array.cast_to<Vec3>()[index] == new_value; });
	}
	void Command_based_edit_state::array_set(index_t index, std::string_view new_value)
	{
		execute_command(
		        util::concat("Set ", path_, "[", index, "]"),
		        util::concat("Set ", path_, "[", index, "] to '", new_value, '\''),
		        [=](Array_view<void> array) { array.cast_to<String>()[index] = new_value; },
		        [=](Const_array_view<void> array) { return array.cast_to<String>()[index] == new_value; });
	}

} // namespace yggdrasill::gui

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
