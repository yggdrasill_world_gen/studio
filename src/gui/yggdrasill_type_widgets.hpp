#pragma once

#include "../simulation/cursor.hpp"
#include "../simulation/graph.hpp"

#include <yggdrasill/array.hpp>
#include <yggdrasill/dictionary.hpp>

#include <imgui.h>
#include <imgui_internal.h>

#include <iomanip>
#include <locale>
#include <sstream>
#include <string>

namespace yggdrasill::gui {

	class Edit_state;
	void dictionary_edit(const char* id, Const_dictionary_view, Edit_state& edit_state);
	void dictionary_view(const char* id, Const_dictionary_view);

	void array_edit(const char* id, Const_array_view<void>, Edit_state& edit_state);
	void array_view(const char* id, Const_array_view<void>);


	class Edit_state {
	  public:
		class [[nodiscard]] Path_raii {
		  public:
			Path_raii(const Path_raii&)            = delete;
			Path_raii& operator=(const Path_raii&) = delete;

			Path_raii(Path_raii&& rhs) noexcept : state_(std::exchange(rhs.state_, nullptr)) {}
			Path_raii& operator=(Path_raii&& rhs) noexcept
			{
				state_ = std::exchange(rhs.state_, nullptr);
				return *this;
			}

			~Path_raii()
			{
				if(state_)
					state_->on_pop_path();
			}

		  private:
			friend class Edit_state;
			Edit_state* state_;
			explicit Path_raii(Edit_state& s) : state_(&s) {}
		};
		enum class Path_element_type { dict, array };


		Edit_state()                  = default;
		Edit_state(Edit_state&&)      = default;
		Edit_state(const Edit_state&) = default;
		virtual ~Edit_state()         = default;

		Edit_state& operator=(Edit_state&&)      = default;
		Edit_state& operator=(const Edit_state&) = default;

		auto push_path(std::string_view key, Path_element_type type)
		{
			on_push_path(key, type);
			return Path_raii{*this};
		}
		auto push_path(index_t index, Path_element_type type)
		{
			on_push_path(index, type);
			return Path_raii{*this};
		}

		virtual void dict_set_type(std::string_view, Type new_type, Type element_type = Type::none) = 0;
		virtual void dict_set(std::string_view, bool new_value)                                     = 0;
		virtual void dict_set(std::string_view, std::int8_t new_value)                              = 0;
		virtual void dict_set(std::string_view, std::int32_t new_value)                             = 0;
		virtual void dict_set(std::string_view, std::int64_t new_value)                             = 0;
		virtual void dict_set(std::string_view, float new_value)                                    = 0;
		virtual void dict_set(std::string_view, double new_value)                                   = 0;
		virtual void dict_set(std::string_view, Vec2 new_value)                                     = 0;
		virtual void dict_set(std::string_view, Vec3 new_value)                                     = 0;
		virtual void dict_set(std::string_view, std::string_view new_value)                         = 0;

		virtual void array_erase(yggdrasill::index_t)               = 0;
		virtual void array_append()                                 = 0;
		virtual void array_set(index_t, bool new_value)             = 0;
		virtual void array_set(index_t, std::int8_t new_value)      = 0;
		virtual void array_set(index_t, std::int32_t new_value)     = 0;
		virtual void array_set(index_t, std::int64_t new_value)     = 0;
		virtual void array_set(index_t, float new_value)            = 0;
		virtual void array_set(index_t, double new_value)           = 0;
		virtual void array_set(index_t, Vec2 new_value)             = 0;
		virtual void array_set(index_t, Vec3 new_value)             = 0;
		virtual void array_set(index_t, std::string_view new_value) = 0;

		void highlight_current_row(float duration, ImU32 color, std::string tooltip = {});

	  private:
		friend void dictionary_edit(const char* id, Const_dictionary_view, Edit_state& edit_state);
		friend void array_edit(const char* id, Const_array_view<void>, Edit_state& edit_state);

		struct Highlight {
			float       duration;
			float       fade_end_time;
			ImGuiID     id;
			ImVec4      color;
			std::string tooltip;
		};

		std::string              new_key_;
		std::optional<Highlight> highlighting_;

		auto handle_highlighting();

		virtual void on_push_path(std::string_view, Path_element_type) = 0;
		virtual void on_push_path(index_t, Path_element_type)          = 0;
		virtual void on_pop_path()                                     = 0;
	};

	class Direct_edit_state : public Edit_state {
	  public:
		explicit Direct_edit_state(Dictionary_view root) { path_stack_.emplace_back(root); }
		explicit Direct_edit_state(Array_view<void> root) { path_stack_.emplace_back(root); }

		[[nodiscard]] auto root_dict() const { return get<Dictionary_view>(path_stack_.front()); }
		[[nodiscard]] auto root_array() const { return get<Array_view<void>>(path_stack_.front()); }

	  private:
		std::vector<std::variant<Dictionary_view, Array_view<void>>> path_stack_;

		void on_push_path(std::string_view, Path_element_type) override;
		void on_push_path(index_t, Path_element_type) override;
		void on_pop_path() override;

		auto dict() { return get<Dictionary_view>(path_stack_.back()); }
		auto array() { return get<Array_view<void>>(path_stack_.back()); }

		void dict_set_type(std::string_view, Type new_type, Type element_type = Type::none) override;
		void dict_set(std::string_view, bool new_value) override;
		void dict_set(std::string_view, std::int8_t new_value) override;
		void dict_set(std::string_view, std::int32_t new_value) override;
		void dict_set(std::string_view, std::int64_t new_value) override;
		void dict_set(std::string_view, float new_value) override;
		void dict_set(std::string_view, double new_value) override;
		void dict_set(std::string_view, Vec2 new_value) override;
		void dict_set(std::string_view, Vec3 new_value) override;
		void dict_set(std::string_view, std::string_view new_value) override;

		void array_erase(yggdrasill::index_t) override;
		void array_append() override;
		void array_set(index_t, bool new_value) override;
		void array_set(index_t, std::int8_t new_value) override;
		void array_set(index_t, std::int32_t new_value) override;
		void array_set(index_t, std::int64_t new_value) override;
		void array_set(index_t, float new_value) override;
		void array_set(index_t, double new_value) override;
		void array_set(index_t, Vec2 new_value) override;
		void array_set(index_t, Vec3 new_value) override;
		void array_set(index_t, std::string_view new_value) override;
	};


	class Command_based_edit_state : public Edit_state {
	  public:
		using Action       = std::function<void(std::variant<Dictionary_view, Array_view<void>>)>;
		using Apply        = std::function<void(simulation::State&, Action)>;
		using Get          = std::function<std::variant<Const_dictionary_view, Const_array_view<void>>()>;
		using Post_execute = std::function<void(const simulation::Node&)>;

		template <typename GenericGet>
		explicit Command_based_edit_state(std::string              operation_name,
		                                  simulation::Command_type type,
		                                  Apply                    apply,
		                                  GenericGet               get,
		                                  simulation::Cursor&      simulation,
		                                  Post_execute             post_execute = {})
		  : operation_name_(std::move(operation_name))
		  , type_(type)
		  , apply_(std::move(apply))
		  , get_([get = std::move(get)]() -> std::variant<Const_dictionary_view, Const_array_view<void>> {
			  return std::invoke(get);
		  })
		  , cursor_(simulation)
		  , post_execute_(post_execute)
		{
		}

	  private:
		class Dict_edit_command;

		struct Path_node {
			std::variant<std::string_view, index_t> key;
			Path_element_type                       type;

			Path_node(std::variant<std::string_view, index_t> key, Path_element_type type)
			  : key(key), type(type)
			{
			}
		};
		std::vector<Path_node>   path_;
		std::string              operation_name_;
		simulation::Command_type type_;
		Apply                    apply_;
		Get                      get_;
		simulation::Cursor&      cursor_;
		Post_execute             post_execute_;

		const simulation::Node* last_added_node_    = nullptr;
		Dict_edit_command*      last_added_command_ = nullptr;

		template <typename Cmd, typename Check>
		void execute_command(std::string id, std::string description, Cmd&& cmd, Check&& check);

		void on_push_path(std::string_view, Path_element_type) override;
		void on_push_path(index_t, Path_element_type) override;
		void on_pop_path() override;

		void dict_set_type(std::string_view, Type new_type, Type element_type = Type::none) override;
		void dict_set(std::string_view, bool new_value) override;
		void dict_set(std::string_view, std::int8_t new_value) override;
		void dict_set(std::string_view, std::int32_t new_value) override;
		void dict_set(std::string_view, std::int64_t new_value) override;
		void dict_set(std::string_view, float new_value) override;
		void dict_set(std::string_view, double new_value) override;
		void dict_set(std::string_view, Vec2 new_value) override;
		void dict_set(std::string_view, Vec3 new_value) override;
		void dict_set(std::string_view, std::string_view new_value) override;

		void array_erase(yggdrasill::index_t) override;
		void array_append() override;
		void array_set(index_t, bool new_value) override;
		void array_set(index_t, std::int8_t new_value) override;
		void array_set(index_t, std::int32_t new_value) override;
		void array_set(index_t, std::int64_t new_value) override;
		void array_set(index_t, float new_value) override;
		void array_set(index_t, double new_value) override;
		void array_set(index_t, Vec2 new_value) override;
		void array_set(index_t, Vec3 new_value) override;
		void array_set(index_t, std::string_view new_value) override;
	};

} // namespace yggdrasill::gui
