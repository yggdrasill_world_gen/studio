#pragma once

#include "../util/string.hpp"

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <memory>
#include <string>

struct ImGuiContext;

namespace yggdrasill::gui {

	extern void enable_persistent_imgui_storage();

	enum class Config_clear { values, all };

	class Config {
	  public:
		/// move-only to prevent user-errors (copies wouldn't reflect later changes)
		class Properties {
		  public:
			Properties()                             = default;
			Properties(Properties&&)                 = default;
			Properties(const Properties&)            = delete;
			Properties& operator=(Properties&&)      = default;
			Properties& operator=(const Properties&) = delete;

			std::string_view operator[](std::string_view key) const
			{
				if(auto iter = data_.find(key); iter != data_.end())
					return iter->second;
				else
					return {};
			}

			std::string_view get(std::string_view key, std::string_view default_value) const;
			int              get(std::string_view key, int default_value) const;
			bool             get(std::string_view key, bool default_value) const;
			float            get(std::string_view key, float default_value) const;
			glm::vec2        get(std::string_view key, glm::vec2 default_value) const;
			glm::vec3        get(std::string_view key, glm::vec3 default_value) const;
			template <typename Enum, typename = std::enable_if_t<std::is_enum_v<Enum>>>
			Enum get(std::string_view key, Enum default_value) const
			{
				return static_cast<Enum>(get(key, static_cast<int>(default_value)));
			}

			void set(std::string_view key, const void* value) = delete;
			void set(std::string_view key, const char* value) { set(key, std::string(value)); }
			void set(std::string_view key, std::string value);
			template <typename T,
			          typename = decltype(std::to_string(std::declval<T>())),
			          typename = std::enable_if_t<!std::is_enum_v<T>>>
			void set(std::string_view key, const T& v)
			{
				set(key, std::to_string(v));
			}
			void set(std::string_view key, const bool& v) { set(key, v ? "true" : "false"); }
			void set(std::string_view key, glm::vec2 value);
			void set(std::string_view key, glm::vec3 value);

			template <typename T, typename = std::enable_if_t<std::is_enum_v<T>>>
			void set(std::string_view key, const T& v)
			{
				set(key, static_cast<int>(v));
			}

			template <typename F>
			void foreach(F&& f)
			{
				for(auto&& [key, value] : data_) {
					f(key, value);
				}
			}

			void clear();

		  private:
			friend class Config;
			util::String_map<std::string, std::string> data_;
		};

		/// Creates a new config object and attaches it to the given context.
		/// The lifetime of the object is managed by ImGui (destroyed on shutdown),
		///   because early destruction could corrupt the ini file
		static Config& create(ImGuiContext&, std::string name);

		Properties& operator[](std::string_view entry)
		{
			// risk cost of two lookup (if the key is missing), to save the cost of constructing a temporary std::string
			if(auto iter = entries_.find(entry); iter != entries_.end())
				return iter->second;
			else
				return entries_[std::string(entry)];
		}

		void erase(std::string_view entry);

		template <typename F>
		void foreach_entry(F&& f)
		{
			for(auto&& [key, value] : entries_) {
				f(key);
			}
		}

		auto begin() const { return entries_.begin(); }
		auto end() const { return entries_.end(); }

		void clear(Config_clear mode);


		Config(Config&&)                 = delete;
		Config(const Config&)            = delete;
		Config& operator=(Config&&)      = delete;
		Config& operator=(const Config&) = delete;
		~Config()                        = default;

	  private:
		std::string                               name_;
		util::String_map<std::string, Properties> entries_;
		bool                                      loaded_ = false;

		Config(ImGuiContext&, std::string name);
	};

} // namespace yggdrasill::gui
