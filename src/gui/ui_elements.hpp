#pragma once

#include "../util/func_traits.hpp"

#include <yggdrasill/string.hpp>

#include <imgui.h>
#include <imgui_internal.h>

#include <algorithm>
#include <iomanip>
#include <locale>
#include <optional>
#include <sstream>
#include <string>

namespace yggdrasill::gui {

	inline void label(const char* str, float width = 140.f)
	{
		ImGui::AlignTextToFramePadding();
		ImGui::TextUnformatted(str);
		ImGui::SameLine(width);
		ImGui::SetNextItemWidth(-FLT_MIN);
	}

	template <typename F>
	requires(!std::is_void_v<std::invoke_result_t<F>>)
	auto enabled_if(bool enabled, F&& body) -> std::invoke_result_t<F>
	{
		if(!enabled) {
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}

		auto&& ret = std::invoke(body);

		if(!enabled) {
			ImGui::PopItemFlag();
			ImGui::PopStyleVar();
		}

		return std::forward<decltype(ret)>(ret);
	}
	template <typename F>
	requires std::is_void_v<std::invoke_result_t<F>>
	void enabled_if(bool enabled, F&& body)
	{
		if(!enabled) {
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}

		std::invoke(body);

		if(!enabled) {
			ImGui::PopItemFlag();
			ImGui::PopStyleVar();
		}
	}

	bool begin_context_popup_area(const char* id, ImVec2 begin, std::optional<ImVec2> end = std::nullopt);
	void end_context_popup_area();

	bool checkbox(const char* label, bool& value, bool enabled = true, const char* tooltip = nullptr);

	bool dropdown(const char* label, int64_t& value, std::span<const char* const> value_labels);
	template <typename T>
	bool dropdown(const char* label, T& value, std::span<const char* const> value_labels)
	{
		auto tmp = static_cast<int64_t>(value);
		auto r   = dropdown(label, tmp, value_labels);
		value    = static_cast<T>(tmp);
		return r;
	}

	bool button(const char* label, bool enabled = true, const char* tooltip = nullptr, ImVec2 size = {0, 0});
	bool right_aligned_button(const char* label, bool enabled = true);

	template <typename F>
	void radio_buttons(F&& body)
	{
		const auto button_size  = ImGui::GetFrameHeight();
		const auto window_width = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;
		const auto padding      = ImGui::GetStyle().ItemSpacing.x + button_size;
		auto       first        = true;

		body([&](bool active, const char* label, const char* tooltip) {
			const auto next_x = ImGui::GetItemRectMax().x + padding;
			if(!first && next_x < window_width)
				ImGui::SameLine();

			first = false;

			if(active)
				ImGui::PushStyleColor(ImGuiCol_Button, ImGui::GetColorU32(ImGuiCol_ButtonActive));

			ON_EXIT_IF(active, ImGui::PopStyleColor());

			return gui::button(label, true, tooltip, ImVec2(button_size, button_size));
		});
	}

	class Same_line_right {
	  public:
		void clear()
		{
			i_      = 0;
			offset_ = ImGui::GetStyle().ItemSpacing.x;
		}

		template <typename Body>
		auto add(Body&& body)
		{
			if(i_ >= widths_.size())
				widths_.resize(i_ + 1, 50.f);

			offset_ += widths_[i_];
			ImGui::SameLine(ImGui::GetWindowWidth() - offset_);
			auto r      = body();
			widths_[i_] = ImGui::GetItemRectSize().x + ImGui::GetStyle().ItemSpacing.x;
			i_++;
			return r;
		}

		template <typename... Args>
		bool add_button(Args&&... args)
		{
			return add([&] { return button(std::forward<Args>(args)...); });
		}

	  private:
		std::vector<float> widths_;
		std::size_t        i_      = 0;
		float              offset_ = 0;
	};

	bool input_int_with_hint(const char*         label,
	                         const char*         hint,
	                         int&                v,
	                         int                 empty_value,
	                         int                 step      = 1,
	                         int                 step_fast = 100,
	                         ImGuiInputTextFlags flags     = 0);

	void ui_help_tooltip(const char* str);

	bool begin_button_dropdown(
	        const char* label, bool enabled = true, const char* tooltip = nullptr, bool right_align = false);
	void end_button_dropdown();

	bool input_text(const char* label, String&, ImGuiInputTextFlags flags = 0);
	bool input_text(const char* label, std::string&, ImGuiInputTextFlags flags = 0);
	auto input_text_lazy(const char* label, std::string_view, ImGuiInputTextFlags flags = 0)
	        -> std::optional<std::string>;

	void   begin_group_panel(std::string_view name = {}, const ImVec2& size = ImVec2(0, 0));
	ImRect end_group_panel();

	void spinner(
	        ImDrawList& draw_list, ImVec2 pos, float radius, float thickness, const ImU32& color, bool culling = true);
	bool spinner(const char* label, float radius, float thickness, const ImU32& color);

	bool range_slider_float(const char* label,
	                        float*      v1,
	                        float*      v2,
	                        float       v_min,
	                        float       v_max,
	                        const char* display_format = "(%.3f, %.3f)",
	                        float       power          = 1.0f);

	bool range_slider_int(const char* label,
	                      int32_t*    v1,
	                      int32_t*    v2,
	                      int32_t     v_min,
	                      int32_t     v_max,
	                      const char* display_format = "(%i, %i)");

	inline std::string pretty_print(std::int8_t value)
	{
		return std::to_string(static_cast<int>(value));
	}
	inline std::string pretty_print(std::uint8_t value)
	{
		if(value == YGDL_TRUE)
			return "true";
		else if(value == YGDL_FALSE)
			return "false";
		else
			return std::to_string(static_cast<int>(value));
	}
	template <typename T>
	std::string pretty_print(const T& value)
	{
		struct facet : std::numpunct<char> {
			char        do_thousands_sep() const { return '\''; }
			std::string do_grouping() const { return "\03"; }
		};

		static const auto loc = std::locale(std::locale::classic(), new facet);

		auto ss = std::ostringstream();
		ss.imbue(loc);
		ss << std::boolalpha << std::setprecision(8);
		ss << value;
		return std::move(ss).str();
	}

	std::string pretty_print_position(const Vec3& p);


	inline ImU32 darken(float factor, ImU32 color)
	{
		auto fc = ImGui::ColorConvertU32ToFloat4(color);
		fc.x *= factor;
		fc.y *= factor;
		fc.z *= factor;
		return ImGui::ColorConvertFloat4ToU32(fc);
	}

	constexpr ImU32 apply_alpha(ImU32 in, float alpha)
	{
		auto a = (in & IM_COL32_A_MASK) >> IM_COL32_A_SHIFT;
		a      = static_cast<ImU32>(std::clamp(a * alpha, 0.f, 255.f));
		return (in & ~IM_COL32_A_MASK) | (a << IM_COL32_A_SHIFT);
	}
	inline ImU32 apply_style_alpha(ImU32 in)
	{
		return apply_alpha(in, ImGui::GetStyle().Alpha);
	}

	inline ImVec4 lerp_color(const ImVec4& a, const ImVec4& b, float t)
	{
		return ImVec4{a.x * (1.f - t) + b.x * t,
		              a.y * (1.f - t) + b.y * t,
		              a.z * (1.f - t) + b.z * t,
		              a.w * (1.f - t) + b.w * t};
	}

	inline float elastic_out(float x)
	{
		constexpr auto epsilon = 1e-5f;
		constexpr auto c       = (2 * 3.14159265359f) / 3;

		if(x < epsilon)
			return 0.f;
		else if(x > 1.f - epsilon)
			return 1.f;
		else
			return std::pow(2.f, -10.f * x) * std::sin((x * 10.f - 0.75f) * c) + 1.f;
	}
	inline float bounce_out(float x)
	{
		constexpr auto n1 = 7.5625f;
		constexpr auto d1 = 2.75f;

		if(x < 1 / d1) {
			return n1 * x * x;
		} else if(x < 2 / d1) {
			x -= 1.5f / d1;
			return n1 * x * x + 0.75f;
		} else if(x < 2.5 / d1) {
			x -= 2.25f / d1;
			return n1 * x * x + 0.9375f;
		} else {
			x -= 2.625f / d1;
			return n1 * x * x + 0.984375f;
		}
	}

} // namespace yggdrasill::gui

// TODO: change to consteval once clang fully supports it (or we drop clang support)
#ifdef __cpp_consteval
consteval
#else
constexpr
#endif
        ImU32
        operator"" _color(const char* bytes, size_t length)
{
	if(bytes[0] != '#')
		throw std::invalid_argument("Color literal have to start with #");

	for(size_t i = 1; i < length; i++) {
		if(bytes[i] >= 'a' && bytes[i] <= 'f')
			continue;
		if(bytes[i] >= 'A' && bytes[i] <= 'F')
			continue;
		if(bytes[i] >= '0' && bytes[i] <= '9')
			continue;
		throw std::invalid_argument("Invalid character " + std::to_string(bytes[i]) + " in color literal");
	}

	auto byte_val = [&](size_t i) {
		auto v = bytes[i];
		return v <= '9' ? v - '0' : 9 + (v & 0b111);
	};
	auto byte_single_val = [&](size_t i) {
		auto v = byte_val(i);
		return v << 4 | v;
	};
	auto byte_pair_val = [&](size_t i) { return byte_val(i) << 4 | byte_val(i + 1); };

	switch(length) {
		case 4: // #RGB
			return byte_single_val(1) << IM_COL32_R_SHIFT | byte_single_val(2) << IM_COL32_G_SHIFT
			       | byte_single_val(3) << IM_COL32_B_SHIFT | 255 << IM_COL32_A_SHIFT;
		case 5: // #RGBA
			return byte_single_val(1) << IM_COL32_R_SHIFT | byte_single_val(2) << IM_COL32_G_SHIFT
			       | byte_single_val(3) << IM_COL32_B_SHIFT | byte_single_val(4) << IM_COL32_A_SHIFT;

		case 7: // #RRGGBB
			return byte_pair_val(1) << IM_COL32_R_SHIFT | byte_pair_val(3) << IM_COL32_G_SHIFT
			       | byte_pair_val(5) << IM_COL32_B_SHIFT | 255 << IM_COL32_A_SHIFT;

		case 9: // #RRGGBBAA
			return byte_pair_val(1) << IM_COL32_R_SHIFT | byte_pair_val(3) << IM_COL32_G_SHIFT
			       | byte_pair_val(5) << IM_COL32_B_SHIFT | byte_pair_val(7) << IM_COL32_A_SHIFT;

		default:
			throw std::invalid_argument(
			        "Color literal have to follow one of the following formats: RGB, RGBA, RRGGBB, "
			        "RRGGBBAA");
	}
}
