#include "file_dialogs.hpp"

#include "../util/func_traits.hpp"
#include "../util/string.hpp"

#include <yggdrasill/error.hpp>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>

namespace {
	using Load_dialog_callback = void (*)(const char*);
}

EM_JS(void, call_offer_download, (const char* filename, int l), { offer_download(UTF8ToString(filename, l)); })
EM_JS(void, call_request_upload, (const char* accept, int l, Load_dialog_callback cb), {
	request_upload(UTF8ToString(accept, l), cb);
})
EM_JS(void, call_delete_file, (const char* filename, int l), { delete_file(UTF8ToString(filename, l)); })

extern "C" EMSCRIPTEN_KEEPALIVE int call_load_dialog_callback(char const* path, Load_dialog_callback callback)
{
	callback(path);
	return 1;
}

#else
#ifdef APIENTRY
#undef APIENTRY
#endif
#include <portable-file-dialogs.h>
#endif

#include <imgui.h>

#include <fstream>
#include <unordered_set>


namespace yggdrasill::gui {

#ifdef __EMSCRIPTEN__
	void update_file_dialogs() {}

	void save_dialog(std::string,
	                 std::string default_filename,
	                 File_extension_filter,
	                 std::function<void(std::string_view)> writer)
	{
		writer(default_filename);
		call_offer_download(default_filename.data(), int(default_filename.size()));
	}
	void save_dialog(std::string                                                          title,
	                 std::string                                                          default_filename,
	                 File_extension_filter                                                file_extensions,
	                 std::function<void(std::string_view, std::function<void()> on_done)> writer)
	{
		writer(default_filename, [default_filename] {
			call_offer_download(default_filename.data(), int(default_filename.size()));
		});
	}

	void load_dialog(std::string                           title,
	                 std::string                           default_filename,
	                 File_extension_filter                 file_extensions,
	                 std::function<void(std::string_view)> reader)
	{
		load_dialog(std::move(title),
		            std::move(default_filename),
		            std::move(file_extensions),
		            [reader = std::move(reader)](std::string_view path, std::function<void()> on_done) {
			            reader(path);
			            on_done();
		            });
	}
	void load_dialog(std::string,
	                 std::string,
	                 File_extension_filter                                                file_extensions,
	                 std::function<void(std::string_view, std::function<void()> on_done)> reader)
	{
		// There is no way to determine if the user closed the upload dialog, so we can't manage the
		//   callback's lifetime based on that. Instead a shared copy is held and cleared when a file
		//   was selected.
		// This works suffiecently for us because there are never more than one upload at once.
		static std::function<void(std::string_view, std::function<void()> on_done)> saved_reader;

		saved_reader  = std::move(reader);
		auto reader_c = static_cast<Load_dialog_callback>([](const char* file) {
			saved_reader(std::string_view(file), [file_str = std::string(file)] {
				call_delete_file(file_str.data(), int(file_str.size()));
			});
			saved_reader = nullptr;
		});

		// build a filter string that is compatible withthe uploads accept,
		//   see https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/accept
		auto file_filter = std::string();
		for(std::size_t i = 1; i < file_extensions.size(); i += 2) {
			if(!file_filter.empty())
				file_filter += ',';
			file_filter += file_extensions[i];
		}
		call_request_upload(file_filter.c_str(), int(file_filter.size()), reader_c);
	}

#else
	namespace {
		static std::string last_directory = pfd::path::home();

		std::optional<std::string> single_path(std::string&& str)
		{
			return str.empty() ? std::nullopt : std::optional<std::string>{std::move(str)};
		}
		std::optional<std::string> single_path(std::vector<std::string>&& strs)
		{
			if(strs.empty())
				return std::nullopt;
			return single_path(std::move(strs[0]));
		}


		struct Dialog_data {
			std::variant<pfd::save_file, pfd::open_file> dialog;
			std::function<void(const std::string&)>      writer_callback;

			std::optional<std::string> extract_path()
			{
				return visit(dialog, [&](auto& d) -> std::optional<std::string> {
					if(d.ready(0))
						return single_path(d.result());
					else
						return std::nullopt;
				});
			}
		};

		std::vector<Dialog_data> open_dialogs;
	} // namespace

	void update_file_dialogs()
	{
		std::erase_if(open_dialogs, [&](Dialog_data& d) {
			if(auto path = d.extract_path()) {
				auto generic_path = util::replace_all(*path, "\\", "/");
				last_directory    = generic_path.substr(0, generic_path.find_last_of('/'));
				d.writer_callback(generic_path);
				return true;
			}
			return false;
		});
	}

	void save_load_done(std::string_view filename)
	{
		// ignored; only relevant to Emscripten based builds
	}

	void save_dialog(std::string                           title,
	                 std::string                           default_filename,
	                 File_extension_filter                 file_extensions,
	                 std::function<void(std::string_view)> writer)
	{
		auto path = util::contains(default_filename, '/') || util::contains(default_filename, '\\')
		                    ? default_filename
		                    : last_directory + "/" + default_filename;
		open_dialogs.emplace_back(Dialog_data{
		        pfd::save_file{title, std::move(path), file_extensions, pfd::opt::none},
                std::move(writer)
        });
	}
	void save_dialog(std::string                                                          title,
	                 std::string                                                          default_filename,
	                 File_extension_filter                                                file_extensions,
	                 std::function<void(std::string_view, std::function<void()> on_done)> writer)
	{
		save_dialog(std::move(title),
		            std::move(default_filename),
		            std::move(file_extensions),
		            [writer = std::move(writer)](std::string_view path) { writer(path, [] {}); });
	}

	void load_dialog(std::string                           title,
	                 std::string                           default_filename,
	                 File_extension_filter                 file_extensions,
	                 std::function<void(std::string_view)> reader)
	{
		auto path = util::contains(default_filename, '/') || util::contains(default_filename, '\\')
		                    ? default_filename
		                    : last_directory + "/" + default_filename;
		open_dialogs.emplace_back(Dialog_data{
		        pfd::open_file{title, std::move(path), file_extensions, pfd::opt::none},
                std::move(reader)
        });
	}
	void load_dialog(std::string                                                          title,
	                 std::string                                                          default_filename,
	                 File_extension_filter                                                file_extensions,
	                 std::function<void(std::string_view, std::function<void()> on_done)> reader)
	{
		load_dialog(std::move(title),
		            std::move(default_filename),
		            std::move(file_extensions),
		            [reader = std::move(reader)](std::string_view path) { reader(path, [] {}); });
	}

#endif

} // namespace yggdrasill::gui
