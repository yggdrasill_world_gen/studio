#include "config.hpp"

#include "../util/string.hpp"

#include <imgui_internal.h>

#include <iostream>
#include <sstream>

namespace yggdrasill::gui {

	static Config& get_this(const ImGuiSettingsHandler* h)
	{
		return *reinterpret_cast<Config*>(h->UserData);
	}

	static void write_escaped(ImGuiTextBuffer* out_buf, std::string_view str)
	{
		for(auto c : str) {
			switch(c) {
				case '\n': out_buf->append("\\n"); break;
				case '\\': out_buf->append("\\\\"); break;
				case '=': out_buf->append("\\="); break;
				default: out_buf->append(&c, (&c) + 1); break;
			}
		}
	}
	static std::string read_escaped(std::string_view str)
	{
		auto out = std::string();
		out.reserve(str.size());

		for(auto iter = str.begin(); iter != str.end(); ++iter) {
			switch(*iter) {
				case '\\':
					++iter;
					out += *iter;
					break;
				default: out += *iter; break;
			}
		}

		return out;
	}

	static std::size_t rfind_last_not_of(std::string_view str, char c, std::size_t index)
	{
		while(str[index] == c) {
			if(index == 0)
				return std::string_view ::npos;
			else
				--index;
		}

		return index;
	}


	void enable_persistent_imgui_storage()
	{
		static auto ignore_writes = true;

		auto& ctx = *ImGui::GetCurrentContext();

		ImGuiSettingsHandler h;
		h.TypeName   = "persistent_storage";
		h.TypeHash   = ImHashStr(h.TypeName);
		h.UserData   = nullptr;
		h.ClearAllFn = +[](ImGuiContext* ctx, ImGuiSettingsHandler* h) {};
		h.ReadInitFn = h.ClearAllFn;
		h.ReadOpenFn = +[](ImGuiContext* ctx, ImGuiSettingsHandler* h, const char* name) -> void* {
			const auto name_str = std::string_view(name);

			for(int i = 0; i != ctx->Windows.Size; i++) {
				ImGuiWindow* window = ctx->Windows[i];
				if(name_str == window->Name)
					return window;
			}
			return nullptr;
		};
		h.ReadLineFn = +[](ImGuiContext* ctx, ImGuiSettingsHandler* h, void* entry, const char* line_cstr) {
			if(entry == nullptr)
				return;

			auto* window = static_cast<ImGuiWindow*>(entry);

			auto key = ImGuiID(0);
			auto mem = std::uintptr_t(0);
			std::sscanf(line_cstr, "%X = %tX", &key, &mem);

			window->StateStorage.SetVoidPtr(key, reinterpret_cast<void*>(mem));
		};
		h.WriteAllFn = +[](ImGuiContext* ctx, ImGuiSettingsHandler* h, ImGuiTextBuffer* out_buf) {
			if(ignore_writes)
				return;

			for(int i = 0; i != ctx->Windows.Size; i++) {
				ImGuiWindow* window = ctx->Windows[i];
				if(window->StateStorage.Data.Size == 0)
					continue;

				out_buf->appendf("[%s][%s]\n", h->TypeName, window->Name);
				for(auto& data : window->StateStorage.Data) {
					auto mem = std::uintptr_t(0);
					std::memcpy(&mem, &data.val_p, sizeof(mem));
					out_buf->appendf("%X = %tX\n", data.key, mem);
				}
				out_buf->append("\n");
			}
		};
		ctx.SettingsHandlers.push_back(h);

		// At this point the windows won't have been created, so the configuration would be ignored.
		// So, we register a hook to wait until after the first frame to load the configuration and ignore all WriteAllFn until then
		auto hook     = ImGuiContextHook{};
		hook.Type     = ImGuiContextHookType_EndFramePost;
		hook.Callback = [](ImGuiContext* ctx, ImGuiContextHook* hook) {
			ImGui::RemoveContextHook(ctx, hook->HookId);

			if(ImGui::GetIO().IniFilename)
				ImGui::LoadIniSettingsFromDisk(ImGui::GetIO().IniFilename);

			ignore_writes = false;
		};
		ImGui::AddContextHook(&ctx, &hook);
	}


	Config::Config(ImGuiContext& ctx, std::string name) : name_("_EXT_" + std::move(name))
	{
		ImGuiSettingsHandler h;
		h.TypeName   = name_.c_str();
		h.TypeHash   = ImHashStr(h.TypeName);
		h.UserData   = this;
		h.ClearAllFn = +[](ImGuiContext* ctx, ImGuiSettingsHandler* h) {
			for(auto& [e, p] : get_this(h).entries_) {
				p.data_.clear();
			}
		};
		h.ReadInitFn = h.ClearAllFn;
		h.ReadOpenFn = +[](ImGuiContext*, ImGuiSettingsHandler* h, const char* name) -> void* {
			return &get_this(h).entries_[name];
		};
		h.ReadLineFn = +[](ImGuiContext* ctx, ImGuiSettingsHandler* h, void* entry, const char* line_cstr) {
			auto& props = *reinterpret_cast<Properties*>(entry);

			auto line = std::string_view(line_cstr);

			if(auto sep = line.find('='); sep != std::string_view ::npos) {
				const auto key_last  = rfind_last_not_of(line, ' ', sep - 1);
				const auto val_begin = line.find_first_not_of(' ', sep + 1);

				if(key_last != std::string_view::npos && val_begin != std::string_view::npos) {
					props.data_[read_escaped(line.substr(0, key_last + 1))] =
					        read_escaped(line.substr(val_begin));
				}
			}
		};
		h.ApplyAllFn = +[](ImGuiContext* ctx, ImGuiSettingsHandler* h) { get_this(h).loaded_ = true; };
		h.WriteAllFn = +[](ImGuiContext* ctx, ImGuiSettingsHandler* h, ImGuiTextBuffer* out_buf) {
			std::cout << "Saved ini\n";
			auto& self = get_this(h);

			for(auto&& [entry, properties] : self.entries_) {
				if(!properties.data_.empty()) {
					out_buf->appendf("[%s][%s]\n", h->TypeName, entry.c_str());

					for(auto&& [key, value] : properties.data_) {
						write_escaped(out_buf, key);
						out_buf->append(" = ");
						write_escaped(out_buf, value);
						out_buf->append("\n");
					}
					out_buf->append("\n");
				}
			}
		};
		ctx.SettingsHandlers.push_back(h);
	}

	Config& Config::create(ImGuiContext& ctx, std::string name)
	{
		// uses new/delete, because the object has to be destroyed by an imgui shutdown-hook and non of
		//   the C-API calls between new and AddContextHook can throw
		auto cfg = new Config(ctx, name);

		auto hook     = ImGuiContextHook{};
		hook.Type     = ImGuiContextHookType_Shutdown;
		hook.UserData = cfg;
		hook.Callback = [](ImGuiContext*, ImGuiContextHook* hook) {
			delete reinterpret_cast<Config*>(hook->UserData);
		};
		ImGui::AddContextHook(&ctx, &hook);

		// force early loading of ini config
		if(ImGui::GetIO().IniFilename)
			ImGui::LoadIniSettingsFromDisk(ImGui::GetIO().IniFilename);

		return *cfg;
	}

	void Config::erase(std::string_view entry)
	{
		if(auto iter = entries_.find(entry); iter != entries_.end()) {
			entries_.erase(iter);
			ImGui::MarkIniSettingsDirty();
		}
	}

	std::string_view Config::Properties::get(std::string_view key, std::string_view default_value) const
	{
		if(auto iter = data_.find(key); iter != data_.end()) {
			return iter->second;
		}

		return default_value;
	}
	int Config::Properties::get(std::string_view key, int default_value) const
	{
		if(auto iter = data_.find(key); iter != data_.end()) {
			try {
				return std::stoi(iter->second);
			} catch(...) {
			} // ignored (invalid value in ini file)
		}

		return default_value;
	}
	bool Config::Properties::get(std::string_view key, bool default_value) const
	{
		if(auto iter = data_.find(key); iter != data_.end()) {
			if(iter->second.find("true") != std::string::npos) {
				return true;
			} else if(iter->second.find("false") != std::string::npos) {
				return false;
			}
		}

		return default_value;
	}
	float Config::Properties::get(std::string_view key, float default_value) const
	{
		if(auto iter = data_.find(key); iter != data_.end()) {
			try {
				return std::stof(iter->second);
			} catch(...) {
			} // ignored (invalid value in ini file)
		}

		return default_value;
	}
	glm::vec2 Config::Properties::get(std::string_view key, glm::vec2 default_value) const
	{
		if(auto iter = data_.find(key); iter != data_.end()) {
			try {
				auto s = std::istringstream(iter->second);
				s >> default_value.x;
				s >> default_value.y;
			} catch(...) {
			} // ignored (invalid value in ini file)
		}

		return default_value;
	}
	glm::vec3 Config::Properties::get(std::string_view key, glm::vec3 default_value) const
	{
		if(auto iter = data_.find(key); iter != data_.end()) {
			try {
				auto s = std::istringstream(iter->second);
				s >> default_value.x;
				s >> default_value.y;
				s >> default_value.z;
			} catch(...) {
			} // ignored (invalid value in ini file)
		}

		return default_value;
	}

	void Config::Properties::set(std::string_view key, std::string new_value)
	{
		if(auto iter = data_.find(key); iter != data_.end()) {
			if(iter->second == new_value)
				return; // value is unchanged

			iter->second = std::move(new_value);
		} else {
			data_.emplace(std::string(key), std::move(new_value));
		}

		ImGui::MarkIniSettingsDirty();
	}

	void Config::Properties::set(std::string_view key, glm::vec2 value)
	{
		auto s = std::ostringstream();
		s << value.x << " " << value.y;
		set(key, std::move(s).str());
	}
	void Config::Properties::set(std::string_view key, glm::vec3 value)
	{
		auto s = std::ostringstream();
		s << value.x << " " << value.y << " " << value.z;
		set(key, std::move(s).str());
	}

	void Config::Properties::clear()
	{
		data_.clear();
	}


	void Config::clear(Config_clear mode)
	{
		if(mode == Config_clear::all) {
			entries_.clear();
		} else {
			for(auto&& [key, value] : entries_) {
				value.clear();
			}
		}
		ImGui::MarkIniSettingsDirty();
	}

} // namespace yggdrasill::gui
