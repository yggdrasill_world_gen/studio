#pragma once

#include <imgui.h>
#include <yggdrasill_icons.hpp>

#include <functional>
#include <string>

namespace yggdrasill::gui {

	extern void draw_popups();

	extern void open_popup(std::string id, std::function<bool()> body);

	extern void open_modal_popup(std::string id, std::function<bool()> body);

	/// \returns true if any of the buttons has been pressed
	template <typename F>
	extern bool dialog_buttons(const char* ok, bool ok_enabled, F&& on_ok)
	{
		ImGui::Spacing();
		ImGui::SetItemDefaultFocus();

		if(!ok_enabled) {
			ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
			ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
		}
		const auto ok_pressed = (ImGui::Button(ok) || ImGui::IsKeyPressed(ImGuiKey_Enter, false)) && ok_enabled;
		if(!ok_enabled) {
			ImGui::PopItemFlag();
			ImGui::PopStyleVar();
		}
		if(ok_pressed) {
			on_ok();
			return true;
		}

		ImGui::SameLine();
		if(ImGui::Button(YGGDRASILL_ICON_TIMES " Cancel") || ImGui::IsKeyPressed(ImGuiKey_Escape, false)) {
			return true;
		}

		return false;
	}

	/// \returns true if any of the buttons has been pressed
	template <typename F>
	extern bool dialog_buttons(const char* ok, F&& on_ok)
	{
		return dialog_buttons(ok, true, std::forward<F>(on_ok));
	}

	extern void show_error_dialog(std::string title, std::string message);

} // namespace yggdrasill::gui
