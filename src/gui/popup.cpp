#include "popup.hpp"

#include <imgui.h>

#include <unordered_map>

namespace yggdrasill::gui {

	namespace {
		struct Popup_data {
			std::function<bool()> body;
			bool                  modal;
			bool                  first  = true;
			bool                  closed = false;
		};
		static auto popups = std::unordered_map<std::string, Popup_data>();
	} // namespace

	void draw_popups()
	{
		for(auto&& [id, data] : popups) {
			if(data.first) {
				data.first = false;
				ImGui::OpenPopup(id.c_str());
			}
			const auto size = ImGui::GetIO().DisplaySize;
			ImGui::SetNextWindowPos(ImVec2(size.x * 0.5f, size.y * 0.5f), ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));
			if(data.modal ? ImGui::BeginPopupModal(id.c_str(), nullptr, ImGuiWindowFlags_AlwaysAutoResize)
			              : ImGui::BeginPopup(id.c_str(), ImGuiWindowFlags_AlwaysAutoResize)) {
				const auto close = data.body();
				ImGui::EndPopup();

				data.closed |= close || !ImGui::IsPopupOpen(id.c_str());
			} else {
				data.closed = true;
			}
		}

		std::erase_if(popups, [&](auto& e) { return e.second.closed; });
	}

	void open_popup(std::string id, std::function<bool()> body)
	{
		popups.emplace(std::move(id), Popup_data{std::move(body), false});
		ImGui::SetMaxWaitBeforeNextFrame(0.f);
	}

	void open_modal_popup(std::string id, std::function<bool()> body)
	{
		popups.emplace(std::move(id), Popup_data{std::move(body), true});
		ImGui::SetMaxWaitBeforeNextFrame(0.f);
	}

	void show_error_dialog(std::string title, std::string message)
	{
		open_modal_popup(std::move(title), [message = std::move(message)]() -> bool {
			ImGui::Text("%s", message.c_str());

			if(ImGui::Button(YGGDRASILL_ICON_TIMES " OK") || ImGui::IsKeyPressed(ImGuiKey_Escape, false)
			   || ImGui::IsKeyPressed(ImGuiKey_Enter, false)) {
				return true;
			}

			return false;
		});
	}
} // namespace yggdrasill::gui
