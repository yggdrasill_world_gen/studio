#pragma once

#include <functional>
#include <iostream>

namespace yggdrasill::gui {

	using File_extension_filter = std::vector<std::string>;

	extern void update_file_dialogs();

	/// write callback might be called immediately or only after multiple frames
	extern void save_dialog(std::string                           title,
	                        std::string                           default_filename,
	                        File_extension_filter                 file_extensions,
	                        std::function<void(std::string_view)> writer);

	/// async-overload
	extern void save_dialog(std::string           title,
	                        std::string           default_filename,
	                        File_extension_filter file_extensions,
	                        std::function<void(std::string_view, std::function<void()> on_done)> writer);

	/// read callback might be called immediately or only after multiple frames
	extern void load_dialog(std::string                           title,
	                        std::string                           default_filename,
	                        File_extension_filter                 file_extensions,
	                        std::function<void(std::string_view)> reader);
	/// async-overload
	extern void load_dialog(std::string           title,
	                        std::string           default_filename,
	                        File_extension_filter file_extensions,
	                        std::function<void(std::string_view, std::function<void()> on_done)> reader);

} // namespace yggdrasill::gui
