#include "window.hpp"

#include <imgui.h>

namespace yggdrasill {

	void Window::visible(bool v)
	{
		visible_ = v;
		cfg_->set("visible", visible_);
		on_visible_changed(v);
	}

	void Window::update()
	{
		do_always_update();

		if(visible()) {
			if(dock_) {
				ImGui::SetNextWindowDockID(*dock_, reset_position_ ? ImGuiCond_Always : ImGuiCond_FirstUseEver);
			}
			if(reset_position_ && !dock_) {
				if(auto window = ImGui::FindWindowByName(id())) {
					window->AutoFitFramesX = window->AutoFitFramesY = 2;
					ImGui::SetNextWindowSize(ImGui::CalcWindowNextAutoFitSize(window), ImGuiCond_Always);

					const auto size = ImGui::GetIO().DisplaySize;
					ImGui::SetNextWindowPos(
					        ImVec2(size.x * 0.5f, size.y * 0.5f), ImGuiCond_Always, ImVec2(0.5f, 0.5f));
				}
			}
			reset_position_ = false;

			visible(do_update());
		}
	}

} // namespace yggdrasill
