#include "settings.hpp"


namespace yggdrasill {

	static float round_scale(float v)
	{
		if(std::abs(v - 1.f) < 0.1f)
			return 1.f;
		if(std::abs(v - 2.f) < 0.1f)
			return 2.f;
		if(std::abs(v - 0.5f) < 0.05f)
			return 0.5f;
		if(std::abs(v - 0.25f) < 0.05f)
			return 0.25f;
		return v;
	}

	Settings::Settings(gui::Config::Properties& cfg) : cfg_(cfg)
	{
		render_scale_interactive_ = cfg.get("render_scale_interactive", render_scale_interactive_);
		render_scale_static_      = cfg.get("render_scale_static", render_scale_static_);
	}

	void Settings::render_scale_interactive(float v)
	{
		v = round_scale(v);
		if(render_scale_interactive_ != v) {
			cfg_.set("render_scale_interactive", render_scale_interactive_);
			render_scale_interactive_ = v;
		}
	}
	void Settings::render_scale_static(float v)
	{
		v = round_scale(v);
		if(render_scale_static_ != v) {
			cfg_.set("render_scale_static", render_scale_static_);
			render_scale_static_ = v;
		}
	}

} // namespace yggdrasill