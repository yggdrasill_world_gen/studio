#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "editor.hpp"

#include "graphic/common.hpp"
#include "gui/file_dialogs.hpp"
#include "gui/font_cousine.hpp"
#include "gui/imgui_impl_glfw.h"
#include "gui/imgui_impl_opengl3.h"
#include "gui/popup.hpp"

#include <imgui.h>
#include <yggdrasill_icon_data.hpp>
#include <yggdrasill_icons.hpp>

#include <chrono>
#include <cstdint>
#include <cstdio>
#include <exception>
#include <iostream>
#include <memory>
#include <thread>

#ifdef __EMSCRIPTEN__
#include <emscripten/emscripten.h>
#include <emscripten/html5.h>
#endif

#ifdef __clang__
// silence warning caused by GLEW macros
#pragma clang diagnostic ignored "-Wold-style-cast"
#endif

namespace yggdrasill {
	extern float current_time();
	extern void  set_window_title_suffix(const std::string& str);
} // namespace yggdrasill

static std::uint8_t frame_counter = 0;

namespace yggdrasill {
	extern ImFont* font_small;
	ImFont*        font_small = nullptr;

	extern ImFont* font_larger;
	ImFont*        font_larger = nullptr;

	extern ImFont* font_largest;
	ImFont*        font_largest = nullptr;
} // namespace yggdrasill

namespace {
	GLFWwindow*                         window = nullptr;
	std::unique_ptr<yggdrasill::Editor> editor;
	float                               current_time_value = 0.f;

#ifndef __EMSCRIPTEN__
	void
#ifdef GLAPIENTRY
	        GLAPIENTRY
#endif
	        gl_debug_callback(GLenum        source,
	                          GLenum        type,
	                          GLuint        id,
	                          GLenum        severity,
	                          GLsizei       length,
	                          const GLchar* message,
	                          const void*)
	{
		if(severity != GL_DEBUG_SEVERITY_LOW && severity != GL_DEBUG_SEVERITY_MEDIUM
		   && severity != GL_DEBUG_SEVERITY_HIGH)
			return;

		switch(severity) {
			case GL_DEBUG_SEVERITY_MEDIUM: std::cerr << "[GL WARNING] "; break;

			case GL_DEBUG_SEVERITY_HIGH: std::cerr << "[GL ERROR] "; break;
			case GL_DEBUG_SEVERITY_LOW:
			default: std::cerr << "[GL] ";
		}


		switch(type) {
			case GL_DEBUG_TYPE_ERROR: std::cerr << "ERROR"; break;
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cerr << "DEPRECATED_BEHAVIOR"; break;
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR: std::cerr << "UNDEFINED_BEHAVIOR"; break;
			case GL_DEBUG_TYPE_PORTABILITY: std::cerr << "PORTABILITY"; break;
			case GL_DEBUG_TYPE_PERFORMANCE: std::cerr << "PERFORMANCE"; break;
			case GL_DEBUG_TYPE_OTHER: std::cerr << "OTHER"; break;
		}

		std::cerr << "(" << id << "): " << message;

		std::cerr << "\n";
	}
#endif

	int init(int argc, char** argv)
	{
		try {
			glfwSetErrorCallback(+[](int errc, const char* description) {
				std::cerr << "GLFW Error " << errc << ": " << description;
			});
			if(!glfwInit())
				return 1;

			const char* glsl_version = "#version 100";

			glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
			glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
			glfwWindowHint(GLFW_DEPTH_BITS, 32);
			glfwWindowHint(GLFW_SAMPLES, 4);

			auto width  = 1920.0;
			auto height = 1080.0;

#ifdef __EMSCRIPTEN__
			emscripten_get_element_css_size("canvas", &width, &height);
#endif

			window = glfwCreateWindow(
			        static_cast<int>(width), static_cast<int>(height), "Yggdrasill Studio", nullptr, nullptr);
			if(window == nullptr)
				return 1;

			glfwMakeContextCurrent(window);

			// Enable vsync
			if(glfwExtensionSupported("WGL_EXT_swap_control_tear")
			   || glfwExtensionSupported("GLX_EXT_swap_control_tear")) {
				glfwSwapInterval(-1);
			} else {
				glfwSwapInterval(1);
			}

			IMGUI_CHECKVERSION();
			ImGui::CreateContext();
			ImGuiIO& io = ImGui::GetIO();
			//io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;
			io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
			io.ConfigFlags |= ImGuiConfigFlags_EnablePowerSavingMode;
			//io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

			io.KeyRepeatDelay = 0.5f;
			io.KeyRepeatRate  = 0.1f;

			ImGui::StyleColorsDark();
			glewExperimental = GL_TRUE;
			glewInit();

			yggdrasill::graphic::enable_multisample();

#ifndef __EMSCRIPTEN__
			if(GLEW_KHR_debug) {
				glEnable(GL_DEBUG_OUTPUT);
				glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
				glDebugMessageCallback((GLDEBUGPROC) gl_debug_callback, stderr);
			} else {
				std::cerr << "No OpenGL debug log available.\n";
			}
#endif

			ImGui_ImplGlfw_InitForOpenGL(window, true);
			ImGui_ImplOpenGL3_Init(glsl_version);

			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);

			// add default font
			constexpr auto base_font_size       = 14.f;
			auto           main_font_config     = ImFontConfig();
			main_font_config.RasterizerMultiply = 1.2f;
			io.Fonts->AddFontFromMemoryCompressedTTF(
			        cousine_compressed_data, cousine_compressed_size, base_font_size, &main_font_config);

			// merge in icons from custom icon font
			const ImWchar icons_ranges[] = {
			        yggdrasill::icons::font_start_code, yggdrasill::icons::font_end_code, 0};
			ImFontConfig icons_config;
			icons_config.MergeMode            = true;
			icons_config.PixelSnapH           = true;
			icons_config.FontDataOwnedByAtlas = false;
			icons_config.GlyphMinAdvanceX     = base_font_size;
			// non-const required for imgui API, but not modified internally
			auto font_bytes = const_cast<void*>(yggdrasill::icons::data::bytes);
			io.Fonts->AddFontFromMemoryTTF(
			        font_bytes, int(yggdrasill::icons::data::size), base_font_size, &icons_config, icons_ranges);

			// add larger font
			const auto larger_font_size = base_font_size * 2.f;
			yggdrasill::font_larger     = io.Fonts->AddFontFromMemoryCompressedTTF(
                    cousine_compressed_data, cousine_compressed_size, larger_font_size, &main_font_config);
			icons_config.GlyphMinAdvanceX = larger_font_size;
			io.Fonts->AddFontFromMemoryTTF(
			        font_bytes, yggdrasill::icons::data::size, larger_font_size * 0.8f, &icons_config, icons_ranges);

			// add largest font
			const auto largest_font_size = base_font_size * 4.f;
			yggdrasill::font_largest     = io.Fonts->AddFontFromMemoryCompressedTTF(
                    cousine_compressed_data, cousine_compressed_size, largest_font_size, &main_font_config);
			icons_config.GlyphMinAdvanceX = largest_font_size;
			io.Fonts->AddFontFromMemoryTTF(
			        font_bytes, yggdrasill::icons::data::size, largest_font_size * 0.8f, &icons_config, icons_ranges);


			// add smallprint font
			yggdrasill::font_small = io.Fonts->AddFontFromMemoryCompressedTTF(
			        cousine_compressed_data, cousine_compressed_size, base_font_size * 0.8f, &main_font_config);
			icons_config.GlyphMinAdvanceX = base_font_size * 0.8f;
			io.Fonts->AddFontFromMemoryTTF(
			        font_bytes, yggdrasill::icons::data::size, base_font_size * 0.8f, &icons_config, icons_ranges);

			io.Fonts->Build();

			io.ConfigWindowsMoveFromTitleBarOnly = true;
			io.ConfigWindowsResizeFromEdges      = true;

			yggdrasill::gui::enable_persistent_imgui_storage();

			editor = std::make_unique<yggdrasill::Editor>(argc > 1 ? argv[1] : ".");

			return 0;

		} catch(const std::exception& e) {
			std::cerr << "Unhandled exception: " << e.what() << "\n";
			throw e;
		}
	}
	[[maybe_unused]] void shutdown()
	{
		try {
			editor.reset();

			ImGui_ImplOpenGL3_Shutdown();
			ImGui_ImplGlfw_Shutdown();
			ImGui::DestroyContext();

			glfwDestroyWindow(window);
			window = nullptr;
			glfwTerminate();

		} catch(const std::exception& e) {
			std::cerr << "Unhandled exception: " << e.what() << "\n";
			throw e;
		}
	}

	bool on_frame()
	{
#ifdef __EMSCRIPTEN__
		try {
#endif
			ImGui_ImplGlfw_WaitForEvent();
			glfwPollEvents();

			current_time_value = static_cast<float>(glfwGetTime());
			frame_counter++;

			ImGui_ImplOpenGL3_NewFrame();
			ImGui_ImplGlfw_NewFrame();
			ImGui::NewFrame();

			int display_w, display_h;
			glfwMakeContextCurrent(window);
			glfwGetFramebufferSize(window, &display_w, &display_h);
			glViewport(0, 0, display_w, display_h);
			glClearColor(0, 0, 0, 0);
			glClear(GL_COLOR_BUFFER_BIT);

			editor->update();

			yggdrasill::gui::draw_popups();
			yggdrasill::gui::update_file_dialogs();

			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

			// Update and Render additional Platform Windows
			ImGuiIO& io = ImGui::GetIO();
			if(io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable) {
				ImGui::UpdatePlatformWindows();
				ImGui::RenderPlatformWindowsDefault();
			}

			glfwMakeContextCurrent(window);
			glfwSwapBuffers(window);

			if(ImGui::IsMouseDown(ImGuiMouseButton_Left) || ImGui::IsMouseDown(ImGuiMouseButton_Right)
			   || ImGui::IsMouseDown(ImGuiMouseButton_Middle))
				ImGui::SetMaxWaitBeforeNextFrame(0.f);
			else
				ImGui::SetMaxWaitBeforeNextFrame(1.f);

			return !editor->quit();

#ifdef __EMSCRIPTEN__
		} catch(const std::exception& e) {
			std::cerr << "Unhandled exception: " << e.what() << "\n";
			throw e;
		}
#endif
	}
#ifdef __EMSCRIPTEN__
	void on_frame_void()
	{
		on_frame();
	}
#endif
} // namespace

namespace yggdrasill {
	extern float current_time()
	{
		return current_time_value;
	}
	extern void set_window_title_suffix(const std::string& str)
	{
		glfwSetWindowTitle(window, ("Yggdrasill - " + str).c_str());
	}
} // namespace yggdrasill

int main(int argc, char** argv)
{
	if(auto errc = init(argc, argv); errc != 0)
		return errc;

#ifdef __EMSCRIPTEN__
	emscripten_set_main_loop(on_frame_void, 0, 0);

#else
	auto running        = true;
	auto should_quit    = false;
	auto quit_requested = false;
	while(running) {
		if(glfwWindowShouldClose(window))
			should_quit = true;

		if(!on_frame())
			should_quit = true;

		if(should_quit && !quit_requested) {
			quit_requested = true;
			should_quit = quit_requested = false;
			editor->reset_quit();
			glfwSetWindowShouldClose(window, false);

			editor->safe_destructive_operation([&] { running = false; });
		}
	}

	shutdown();
#endif
}
