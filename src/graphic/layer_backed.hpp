#pragma once

#include "texture.hpp"
#include "vertex_object.hpp"

#include <yggdrasill/layer.hpp>
#include <yggdrasill/world.hpp>

#include <functional>
#include <memory>
#include <optional>
#include <unordered_map>

namespace yggdrasill::graphic {

	constexpr inline auto max_texture_size = 4096;

	namespace detail {
		template <typename T>
		constexpr Texture_format get_format()
		{
			switch(yggdrasill::detail::c_api_type<T>) {
				case Type::bool_t: return Texture_format::R_ubyte;
				case Type::int8_t: return Texture_format::R_byte;
				case Type::int32_t: return Texture_format::R_int;
				case Type::float_t: return Texture_format::R_float;
				case Type::vec2_t: return Texture_format::RG_32F;
				case Type::vec3_t: return Texture_format::RGB_32F;

				case Type::none:
					assert(!"Can't map layers of type [none] to an OpenGL texture");
					std::abort();
				case Type::int64_t:
					assert(!"Can't map layers of type [int64_t] to an OpenGL texture");
					std::abort();
				case Type::double_t:
					assert(!"Can't map layers of type [double_t] to an OpenGL texture");
					std::abort();
				case Type::string_t:
					assert(!"Can't map layers of type [string] to an OpenGL texture");
					std::abort();
				case Type::array_t:
					assert(!"Can't map layers of type [array] to an OpenGL texture");
					std::abort();
				case Type::dict_t:
					assert(!"Can't map layers of type [dict] to an OpenGL texture");
					std::abort();
			}

			assert(!"unreachable");
			std::abort();
		}
	} // namespace detail

	template <typename TargetType>
	class Layer_backed {
	  public:
		const TargetType& operator*() const { return **target_; }
		const TargetType* operator->() const { return &**target_; }
		explicit          operator bool() const { return target_->has_value(); }

	  private:
		friend class Layer_cache;

		std::shared_ptr<const std::optional<TargetType>> target_;

		Layer_backed(std::shared_ptr<const std::optional<TargetType>>&& target) : target_(std::move(target))
		{
		}
	};

	class Layer_cache {
	  public:
		template <typename T, Ref_type RefType>
		Layer_backed<Buffer> buffer(const Layer_definition<T, RefType>& layer)
		{
			auto [iter, _] = layer_buffers_.emplace(layer.id(), nullptr);
			if(!iter->second) {
				auto refresh = [layer, version = Data_version()](Const_world_view       world,
				                                                 std::optional<Buffer>& result) mutable {
					if(world.layer_modified(layer, version)) {
						if(auto layer_data = world.layer(layer)) {
							if(!result)
								result.emplace(sizeof(T), layer_data->size(), true, layer_data->data(), false);
							else
								result->set(layer_data->begin(), layer_data->end());

						} else {
							result.reset();
						}
						return true;
					}
					return false;
				};

				iter->second = std::make_shared<Value<Buffer>>(std::move(refresh));
			}

			return {
			        std::shared_ptr<const std::optional<Buffer>>{iter->second, &iter->second->target}
            };
		}

		template <typename T, Ref_type RefType>
		Layer_backed<Texture> texture(const Layer_definition<T, RefType>& layer)
		{
			auto [iter, _] = layer_textures_.emplace(layer.id(), nullptr);
			if(!iter->second) {
				auto refresh = [layer, version = Data_version()](Const_world_view        world,
				                                                 std::optional<Texture>& result) mutable {
					if(world.layer_modified(layer, version)) {
						if(auto layer_data = world.layer(layer)) {
							const auto width  = std::min(int(layer_data->size()), max_texture_size);
							const auto height = 1 + layer_data->size() / max_texture_size;

							if(!result)
								result.emplace(
								        width, height, detail::get_format<T>(), Texture_data{*layer_data}, false);
							else
								result->update_data(width, height, *layer_data);

						} else {
							result.reset();
						}
						return true;
					}
					return false;
				};

				iter->second = std::make_shared<Value<Texture>>(std::move(refresh));
			}

			return {
			        std::shared_ptr<const std::optional<Texture>>{iter->second, &iter->second->target}
            };
		}

		bool refresh(Const_world_view);

	  private:
		template <typename TargetType>
		using Refresh_function = std::function<bool(Const_world_view, std::optional<TargetType>&)>;

		template <typename TargetType>
		struct Value {
			std::optional<TargetType>    target;
			Refresh_function<TargetType> refresh;

			Value(Refresh_function<TargetType>&& refresh) : target{}, refresh(std::move(refresh)) {}
		};

		std::unordered_map<std::string_view, std::shared_ptr<Value<graphic::Buffer>>>  layer_buffers_;
		std::unordered_map<std::string_view, std::shared_ptr<Value<graphic::Texture>>> layer_textures_;
	};
	// usage: Layer_backed<graphic::Buffer> position_buffer = cache.buffer(position_layer);

} // namespace yggdrasill::graphic
