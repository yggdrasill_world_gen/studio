#include "layer_backed.hpp"


namespace yggdrasill::graphic {

	bool Layer_cache::refresh(Const_world_view world)
	{
		std::erase_if(layer_buffers_, [](auto& e) { return e.second.use_count() == 1; });
		std::erase_if(layer_textures_, [](auto& e) { return e.second.use_count() == 1; });

		auto any_changes = false;

		for(auto&& [key, value] : layer_buffers_) {
			any_changes |= value->refresh(world, value->target);
		}

		for(auto&& [key, value] : layer_textures_) {
			any_changes |= value->refresh(world, value->target);
		}

		return any_changes;
	}

} // namespace yggdrasill::graphic
