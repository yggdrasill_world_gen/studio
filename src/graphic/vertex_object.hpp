/** Wrappers for VAOs & VBOs *************************************************
 *                                                                           *
 * Copyright (c) 2014 Florian Oetke                                          *
 *  This file is distributed under the MIT License                           *
 *  See LICENSE file for details.                                            *
\*****************************************************************************/

#pragma once

#include "../util/func_traits.hpp"

#include <yggdrasill/commons.hpp>

#include <glm/glm.hpp>

#include <initializer_list>
#include <iterator>
#include <string>
#include <utility>
#include <vector>

/*
 * Examples:
	struct FancyData {
		glm::vec2 pos;
		bool isMagic;
	};

	std::vector<FancyData> data;

	auto buffer = createBuffer(data, true);
	Vertex_layout layout{
		Vertex_layout::Mode::triangles,
		vertex("position",  &FancyData::pos),
		vertex("magic",     &FancyData::isMagic)
	};

	auto shader = Shader_program{}
		.attachShader(assetMgr.load<Shader>("frag_shader:example"_aid))
		.attachShader(assetMgr.load<Shader>("vert_shader:example"_aid))
		.bindAllAttributeLocations(layout)
		.build();

	Object obj{layout, {buffer}};

	shader.set_uniform("liveMagic", 0.42f)
	      .bind();

	obj.draw();
	data.push_back({1,2,3,true}); //< update data
	obj.getBuffer().set(data);

	obj.draw(); //< draws updated data
 */
namespace yggdrasill::graphic {

	class Object;

	enum class Index_type { unsigned_byte, unsigned_short, unsigned_int };

	class Buffer {
		friend class Object;
		friend class Vertex_layout;

	  public:
		Buffer(std::size_t element_size,
		       std::size_t elements,
		       bool        dynamic      = true,
		       const void* data         = nullptr,
		       bool        index_buffer = false);
		Buffer(Buffer&& b) noexcept;
		~Buffer() noexcept;

		template <class T>
		void set(const std::vector<T>& container);

		template <class Iter>
		void set(Iter begin, Iter end);

		template <class T, class F>
		void set(std::size_t count, F&& f)
		{
			f(reinterpret_cast<T*>(map(sizeof(T), count)));
			unmap();
		}

		Buffer& operator=(Buffer&& b) noexcept;

		[[nodiscard]] std::size_t size() const noexcept { return elements_; }

		[[nodiscard]] auto index_buffer() const noexcept { return index_buffer_; }
		[[nodiscard]] auto index_buffer_type() const noexcept { return index_buffer_type_; }

		void bind() const;

	  private:
		unsigned int id_;
		std::size_t  element_size_;
		std::size_t  elements_;
		std::size_t  max_elements_;
		bool         dynamic_;
		bool         index_buffer_; //< GL_ELEMENT_ARRAY_BUFFER
		Index_type   index_buffer_type_ = Index_type::unsigned_byte;

		void  set_raw(std::size_t element_size, std::size_t size, const void* data);
		void* map(std::size_t element_size, std::size_t size);
		void  unmap();
	};
	template <class T>
	Buffer create_dynamic_buffer(std::size_t elements, bool index_buffer = false);

	template <class C>
	Buffer create_buffer(const C& container, bool dynamic = false, bool index_buffer = false);

	template <class I>
	Buffer create_buffer(I begin, I end, bool dynamic = false, bool index_buffer = false);


	class Vertex_layout {
		friend class Object;

	  public:
		enum class Mode { points, line_strip, line_loop, lines, triangle_strip, triangle_fan, triangles };

		enum class Element_type { byte_t, ubyte_t, short_t, ushort_t, int_t, uint_t, float_t };

		struct Element {
			std::string  name;
			int          size;
			Element_type type;
			bool         normalized;
			const void*  offset;
			std::size_t  buffer;
			uint8_t      divisor;
		};

	  public:
		template <typename... T>
		explicit Vertex_layout(Mode mode, T&&... elements)
		  : mode_(mode), elements_{std::forward<T>(elements)...}
		{
		}

		template <typename F>
		void foreach(F&& callback) const
		{
			int index = 0;
			for(auto& e : elements_) {
				callback(e.name, index++);
			}
		}

		/// returns true if instanced rendering is used
		bool build(std::initializer_list<const Buffer*> buffers) const;
		template <typename... Buffers>
		bool build(const Buffers&... buffers) const
		{
			return build({&buffers...});
		}

	  private:
		const Mode                 mode_;
		const std::vector<Element> elements_;
	};
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              int8_t Base::*value,
	                              std::size_t   buffer     = 0,
	                              uint8_t       divisor    = 0,
	                              bool          normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              uint8_t Base::*value,
	                              std::size_t    buffer     = 0,
	                              uint8_t        divisor    = 0,
	                              bool           normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              int16_t Base::*value,
	                              std::size_t    buffer     = 0,
	                              uint8_t        divisor    = 0,
	                              bool           normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              uint16_t Base::*value,
	                              std::size_t     buffer     = 0,
	                              uint8_t         divisor    = 0,
	                              bool            normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              int32_t Base::*value,
	                              std::size_t    buffer     = 0,
	                              uint8_t        divisor    = 0,
	                              bool           normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              uint32_t Base::*value,
	                              std::size_t     buffer     = 0,
	                              uint8_t         divisor    = 0,
	                              bool            normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              float Base::*value,
	                              std::size_t  buffer     = 0,
	                              uint8_t      divisor    = 0,
	                              bool         normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              double Base::*value,
	                              std::size_t   buffer     = 0,
	                              uint8_t       divisor    = 0,
	                              bool          normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              Vec2 Base::*value,
	                              std::size_t buffer     = 0,
	                              uint8_t     divisor    = 0,
	                              bool        normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              Vec3 Base::*value,
	                              std::size_t buffer     = 0,
	                              uint8_t     divisor    = 0,
	                              bool        normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              glm::vec2 Base::*value,
	                              std::size_t      buffer     = 0,
	                              uint8_t          divisor    = 0,
	                              bool             normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              glm::vec3 Base::*value,
	                              std::size_t      buffer     = 0,
	                              uint8_t          divisor    = 0,
	                              bool             normalized = false);
	template <class Base>
	Vertex_layout::Element vertex(const std::string& name,
	                              glm::vec4 Base::*value,
	                              std::size_t      buffer     = 0,
	                              uint8_t          divisor    = 0,
	                              bool             normalized = false);

	template <class T>
	Vertex_layout::Element vertex(
	        const std::string& name, std::size_t buffer = 0, uint8_t divisor = 0, bool normalized = false);

} // namespace yggdrasill::graphic

#define VERTEX_OBJECT_INCLUDED
#include "vertex_object.hxx"
