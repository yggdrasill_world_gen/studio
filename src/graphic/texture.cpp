#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "texture.hpp"

#include <glm/glm.hpp>
#include <lodepng.h>

#include <iostream>


namespace yggdrasill::graphic {

	using namespace glm;

	namespace {
		auto to_gl(Texture_format format)
		{
			switch(format) {
				case Texture_format::DEPTH:
					return std::make_tuple(GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE);
				case Texture_format::DEPTH_16:
					return std::make_tuple(GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE);
				case Texture_format::DEPTH_24:
					return std::make_tuple(GL_DEPTH_COMPONENT24, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE);

				case Texture_format::RG: return std::make_tuple(GL_RG, GL_RG, GL_UNSIGNED_BYTE);
				case Texture_format::RG_16F: return std::make_tuple(GL_RG16F, GL_RG, GL_FLOAT);
				case Texture_format::RG_32F: return std::make_tuple(GL_RG32F, GL_RG, GL_FLOAT);

				case Texture_format::RGB_565: return std::make_tuple(GL_RGB565, GL_RGB, GL_UNSIGNED_BYTE);
				case Texture_format::RGB: return std::make_tuple(GL_RGB, GL_RGB, GL_UNSIGNED_BYTE);
				case Texture_format::RGB_16F: return std::make_tuple(GL_RGB16F, GL_RGB, GL_FLOAT);
				case Texture_format::RGB_32F: return std::make_tuple(GL_RGB32F, GL_RGB, GL_FLOAT);

				case Texture_format::RGBA: return std::make_tuple(GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE);
				case Texture_format::RGBA_16F: return std::make_tuple(GL_RGBA16F, GL_RGBA, GL_FLOAT);
				case Texture_format::RGBA_32F: return std::make_tuple(GL_RGBA32F, GL_RGBA, GL_FLOAT);

				case Texture_format::R_ubyte:
					return std::make_tuple(GL_R8UI, GL_RED_INTEGER, GL_UNSIGNED_BYTE);
				case Texture_format::R_ushort:
					return std::make_tuple(GL_R16UI, GL_RED_INTEGER, GL_UNSIGNED_SHORT);
				case Texture_format::R_uint:
					return std::make_tuple(GL_R32UI, GL_RED_INTEGER, GL_UNSIGNED_INT);

				case Texture_format::R_byte: return std::make_tuple(GL_R8I, GL_RED_INTEGER, GL_BYTE);
				case Texture_format::R_short: return std::make_tuple(GL_R16I, GL_RED_INTEGER, GL_SHORT);
				case Texture_format::R_int: return std::make_tuple(GL_R32I, GL_RED_INTEGER, GL_INT);

				case Texture_format::R_float: return std::make_tuple(GL_R32F, GL_RED, GL_FLOAT);
			}

			assert(!"unhangled enum");
			std::abort();
		}
	} // namespace

	Texture::Texture(const std::string& path) : format_(Texture_format::RGBA)
	{
		std::vector<unsigned char> pixels;
		auto                       uwidth  = static_cast<unsigned int>(0);
		auto                       uheight = static_cast<unsigned int>(0);
		if(auto errc = lodepng::decode(pixels, uwidth, uheight, path); errc != 0) {
			std::cerr << "Couldn't load image from '" << path << "': " << lodepng_error_text(errc) << '\n'
			          << std::flush;
			std::abort();
		}
		width_  = static_cast<int>(uwidth);
		height_ = static_cast<int>(uheight);

		bind(0);

		glTexImage2D(GL_TEXTURE_2D, 0, 4, width_, height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	Texture::Texture(int width, int height, Texture_format format, Texture_data data, bool linear_filtered)
	  : width_(0), height_(0), format_(format)
	{
		glBindTexture(GL_TEXTURE_2D, *handle_);

		update_data(width, height, data, format);

		if(linear_filtered) {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		} else {
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0);
	}


	void Texture::update_data(int width, int height, Texture_data data, util::maybe<Texture_format> format)
	{
		bind(0);

		const auto new_format = format.get_or(format_);

		const auto [gl_internal_format, gl_format, gl_type] = to_gl(new_format);

		if(width != width_ || height != height_ || format_ != new_format) {
			// recreate texture
			width_  = width;
			height_ = height;
			format_ = new_format;
			glTexImage2D(GL_TEXTURE_2D, 0, gl_internal_format, width_, height_, 0, gl_format, gl_type, nullptr);
		}

		if(data.data) { // upload data
			const auto full_rows = data.element_count / width;
			if(full_rows > 0)
				glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width_, full_rows, gl_format, gl_type, data.data);

			if(full_rows < height_) {
				const auto remaining_width = data.element_count - width * full_rows;
				const auto sub_data = static_cast<const char*>(data.data) + data.element_size * width * full_rows;
				glTexSubImage2D(
				        GL_TEXTURE_2D, 0, 0, full_rows, remaining_width, height_ - full_rows, gl_format, gl_type, sub_data);
			}
		}
	}

	void Texture::bind(int index) const
	{
		auto tex = GL_TEXTURE0 + index;
		YGGDRASILL_INVARIANT(tex < GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, "to many textures");

		glActiveTexture(GLenum(GL_TEXTURE0 + index));
		glBindTexture(GL_TEXTURE_2D, *handle_);
	}


	Render_texture::Render_texture(int width, int height, bool depth_buffer)
	  : Texture(width, height, Texture_format::RGBA)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, *framebuffer_);
		ON_EXIT(glBindFramebuffer(GL_FRAMEBUFFER, 0));

		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, unsafe_low_level_handle(), 0);

		if(depth_buffer) {
			auto& db = depth_buffer_.emplace();
			glBindRenderbuffer(GL_RENDERBUFFER, *db);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16, width, height);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, *db);
		}

		YGGDRASILL_INVARIANT(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE,
		                     "Couldn't create framebuffer!");
	}

	auto Render_texture::bind_target() -> util::scope_guard<void (*)()>
	{
		glBindFramebuffer(GL_FRAMEBUFFER, *framebuffer_);
		glViewport(0, 0, width(), height());
		glScissor(0, 0, width(), height());
		return {+[] { glBindFramebuffer(GL_FRAMEBUFFER, 0); }};
	}


	void write_png(Render_texture& texture, std::string_view path, bool fill_alpha)
	{
		using Pixel = std::array<unsigned char, 4>;

		auto _ = texture.bind_target();

		auto pixels = std::vector<Pixel>();
		pixels.resize(texture.width() * texture.height());

		glReadPixels(0, 0, texture.width(), texture.height(), GL_RGBA, GL_UNSIGNED_BYTE, pixels.data());

		// flip y axis
		const auto  row_len = texture.width();
		auto* const begin   = pixels.data();
		auto* const end     = pixels.data() + pixels.size();
		for(auto row = 0; row < texture.height() / 2; ++row) {
			std::swap_ranges(begin + (row * row_len), begin + ((row + 1) * row_len), end - ((row + 1) * row_len));
		}

		if(fill_alpha) {
			// fill any empty areas (alpha<0.5) with the color above/below them
			for(auto column = 0; column < texture.width(); ++column) {
				auto fill = [&](const int begin, const int end, const int step) {
					auto last_color = Pixel{0, 0, 0, 255};

					for(auto row = begin; step > 0 ? row <= end : row >= end; row += step) {
						auto& p = pixels[row * texture.width() + column];
						if(p[3] >= 128)
							last_color = p;
						else
							p = last_color;
					}
				};

				fill(texture.height() / 2, 0, -1);
				fill(texture.height() / 2, texture.height() - 1, 1);
			}
		}

		lodepng::encode(std::string(path),
		                reinterpret_cast<const unsigned char*>(pixels.data()),
		                texture.width(),
		                texture.height());
	}

	int get_max_texture_size()
	{
		static int cached_value = [] {
			auto v = GLint(4096);
			glGetIntegerv(GL_MAX_TEXTURE_SIZE, &v);
			return v;
		}();

		return cached_value;
	}
} // namespace yggdrasill::graphic
