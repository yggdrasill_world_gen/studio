#pragma once

#include <glm/matrix.hpp>
#include <glm/vec4.hpp>

#include <utility>

namespace yggdrasill::graphic {

	template <typename T>
	class GL_handle {
	  public:
		using Deleter = void (*)(T);

		constexpr GL_handle() : handle_(0), deleter_(nullptr) {}
		constexpr GL_handle(T h, Deleter deleter) : handle_(h), deleter_(deleter) {}
		constexpr GL_handle(const GL_handle&) = delete;
		constexpr GL_handle(GL_handle&& rhs) noexcept
		  : handle_(std::exchange(rhs.handle_, 0)), deleter_(rhs.deleter_)
		{
		}
		~GL_handle()
		{
			if(handle_ != 0)
				deleter_(handle_);
		}

		constexpr GL_handle& operator=(const GL_handle&) = delete;
		GL_handle&           operator=(GL_handle&& rhs) noexcept
		{
			if(this == &rhs)
				return *this;

			if(handle_ != 0)
				deleter_(handle_);

			handle_  = std::exchange(rhs.handle_, 0);
			deleter_ = rhs.deleter_;

			return *this;
		}

		constexpr T operator*() const { return handle_; }

	  private:
		T       handle_;
		Deleter deleter_;
	};

	inline glm::vec3 transform(const glm::mat4& m, const glm::vec4& v)
	{
		auto r = m * v;
		return {r.x, r.y, r.z};
	}

	inline void enable_multisample()
	{
#if !defined(__EMSCRIPTEN__) && defined(GL_MULTISAMPLE)
		if(__GLEW_EXT_multisample_compatibility)
			glEnable(GL_MULTISAMPLE);
#endif
	}
	inline void disable_multisample()
	{
#if !defined(__EMSCRIPTEN__) && defined(GL_MULTISAMPLE)
		if(__GLEW_EXT_multisample_compatibility)
			glDisable(GL_MULTISAMPLE);
#endif
	}

} // namespace yggdrasill::graphic
