#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "vertex_object.hpp"

namespace yggdrasill::graphic {

	namespace {
		[[maybe_unused]] GLenum to_gl(Vertex_layout::Mode m)
		{
			switch(m) {
				case Vertex_layout::Mode::points: return GL_POINTS;
				case Vertex_layout::Mode::line_strip: return GL_LINE_STRIP;
				case Vertex_layout::Mode::line_loop: return GL_LINE_LOOP;
				case Vertex_layout::Mode::lines: return GL_LINES;
				case Vertex_layout::Mode::triangle_strip: return GL_TRIANGLE_STRIP;
				case Vertex_layout::Mode::triangle_fan: return GL_TRIANGLE_FAN;
				case Vertex_layout::Mode::triangles: return GL_TRIANGLES;
				default: YGGDRASILL_FAIL("Unsupported VertexLayout::Mode"); return 0;
			}
		}
		[[maybe_unused]] GLenum to_gl(Vertex_layout::Element_type e)
		{
			switch(e) {
				case Vertex_layout::Element_type::byte_t: return GL_BYTE;
				case Vertex_layout::Element_type::ubyte_t: return GL_UNSIGNED_BYTE;
				case Vertex_layout::Element_type::short_t: return GL_SHORT;
				case Vertex_layout::Element_type::ushort_t: return GL_UNSIGNED_SHORT;
				case Vertex_layout::Element_type::int_t: return GL_INT;
				case Vertex_layout::Element_type::uint_t: return GL_UNSIGNED_INT;
				case Vertex_layout::Element_type::float_t: return GL_FLOAT;
				default: YGGDRASILL_FAIL("Unsupported VertexLayout::ElementType"); return 0;
			}
		}
		[[maybe_unused]] GLenum to_gl(Index_type t)
		{
			switch(t) {
				case Index_type::unsigned_byte: return GL_UNSIGNED_BYTE;
				case Index_type::unsigned_short: return GL_UNSIGNED_SHORT;
				case Index_type::unsigned_int: return GL_UNSIGNED_INT;
				default: YGGDRASILL_FAIL("Unsupported Index_type"); return 0;
			}
		}
	} // namespace

	Buffer::Buffer(std::size_t element_size, std::size_t elements, bool dynamic, const void* data, bool index_buffer)
	  : id_(0)
	  , element_size_(element_size)
	  , elements_(elements)
	  , max_elements_(elements)
	  , dynamic_(dynamic)
	  , index_buffer_(index_buffer)
	{
		const GLenum type = !index_buffer_ ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		glGetError(); // reset

		glGenBuffers(1, &id_);
		glBindBuffer(type, id_);
		glBufferData(type, elements_ * element_size_, data, dynamic_ ? GL_STREAM_DRAW : GL_STATIC_DRAW);

		auto error = glGetError();
		YGGDRASILL_INVARIANT(error == GL_NO_ERROR, "Buffer creation failed: " << error);

		if(index_buffer_) {
			if(element_size == sizeof(GLubyte)) {
				index_buffer_type_ = Index_type::unsigned_byte;

			} else if(element_size == sizeof(GLushort)) {
				index_buffer_type_ = Index_type::unsigned_short;

			} else if(element_size == sizeof(GLuint)) {
				index_buffer_type_ = Index_type::unsigned_int;
			} else {
				YGGDRASILL_FAIL("Unsupported index size of " << element_size << " expected one of "
				                                             << sizeof(GLubyte) << ", " << sizeof(GLushort)
				                                             << ", " << sizeof(GLuint));
			}
		}

		std::cout << "Created new VBO for " << elements << " Elements\n";
	}
	Buffer::Buffer(Buffer&& b) noexcept
	  : id_(b.id_)
	  , element_size_(b.element_size_)
	  , elements_(b.elements_)
	  , max_elements_(b.elements_)
	  , dynamic_(b.dynamic_)
	  , index_buffer_(b.index_buffer_)
	  , index_buffer_type_(b.index_buffer_type_)
	{
		b.id_ = 0;
	}

	Buffer::~Buffer() noexcept
	{
		if(id_)
			glDeleteBuffers(1, &id_);
	}

	Buffer& Buffer::operator=(Buffer&& b) noexcept
	{
		YGGDRASILL_INVARIANT(this != &b, "move to self");

		if(id_)
			glDeleteBuffers(1, &id_);

		id_           = b.id_;
		b.id_         = 0;
		element_size_ = b.element_size_;
		elements_     = b.elements_;
		dynamic_      = b.dynamic_;

		return *this;
	}

	void Buffer::set_raw(std::size_t element_size, std::size_t size, const void* data)
	{
		YGGDRASILL_INVARIANT(dynamic_, "set(...) is only allowed for dynamic buffers!");
		YGGDRASILL_INVARIANT(id_ != 0, "Can't access invalid buffer!");

		if(element_size != element_size_) {
			max_elements_ = (max_elements_ * element_size_) / element_size;
			element_size_ = element_size;
		}

		const GLenum type = !index_buffer_ ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		elements_ = size;

		glBindBuffer(type, id_);

		if(max_elements_ >= size) {
			glBufferData(type, max_elements_ * element_size_, nullptr, GL_STREAM_DRAW);
			glBufferSubData(type, 0, size * element_size_, data);
		} else {
			max_elements_ = size;
			glBufferData(type, size * element_size_, data, GL_STREAM_DRAW);
		}
	}

	void* Buffer::map(std::size_t element_size, std::size_t size)
	{
		YGGDRASILL_INVARIANT(dynamic_, "set(...) is only allowed for dynamic buffers!");
		YGGDRASILL_INVARIANT(id_ != 0, "Can't access invalid buffer!");

		if(element_size != element_size_) {
			max_elements_ = (max_elements_ * element_size_) / element_size;
			element_size_ = element_size;
		}

		const GLenum type = !index_buffer_ ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;

		glGetError(); // reset

		glBindBuffer(type, id_);

		elements_ = size;
		if(size > max_elements_) {
			max_elements_ = size;
			glBufferData(type, max_elements_ * element_size_, nullptr, GL_STREAM_DRAW);
		}

		void* ptr = glMapBufferRange(GL_ELEMENT_ARRAY_BUFFER,
		                             0,
		                             elements_ * element_size_,
		                             GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);

		YGGDRASILL_INVARIANT(ptr, "glMapBuffer failed: " << glGetError());

		return ptr;
	}
	void Buffer::unmap()
	{
		const GLenum type = !index_buffer_ ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;
		glUnmapBuffer(type);
	}

	void Buffer::bind() const
	{
		glGetError(); // reset error-flag

		const GLenum type = !index_buffer_ ? GL_ARRAY_BUFFER : GL_ELEMENT_ARRAY_BUFFER;
		glBindBuffer(type, id_);
		auto error = glGetError();
		YGGDRASILL_INVARIANT(error == GL_NO_ERROR, "Buffer::bind() failed with " << error);
	}


	bool Vertex_layout::build(std::initializer_list<const Buffer*> buffers) const
	{
		glGetError(); // reset error-flag

		glBindVertexArray(0);

		bool instanced = false;

		std::size_t   bound_buffer = static_cast<std::size_t>(-1);
		const Buffer* buffer       = nullptr;
		int           index        = 0;
		for(auto& e : elements_) {
			if(bound_buffer != e.buffer) {
				bound_buffer = e.buffer;
				buffer       = (buffers.begin())[e.buffer];
				buffer->bind();
			}

			glEnableVertexAttribArray(index);

			if(e.type == Element_type::float_t)
				glVertexAttribPointer(index,
				                      e.size,
				                      to_gl(e.type),
				                      e.normalized,
				                      static_cast<GLsizei>(buffer->element_size_),
				                      e.offset);
			else
				glVertexAttribIPointer(
				        index, e.size, to_gl(e.type), static_cast<GLsizei>(buffer->element_size_), e.offset);

#ifdef ANDROID
			INVARIANT(e.divisor == 0, "Instancing is not supported on Android devises");
#else
			glVertexAttribDivisor(index, e.divisor);
			instanced |= e.divisor > 0;
#endif

			auto error = glGetError();
			YGGDRASILL_INVARIANT(error == GL_NO_ERROR, "Vertex_layout::build(...) failed with " << error);

			index++;
		}

		return instanced;
	}

} // namespace yggdrasill::graphic
