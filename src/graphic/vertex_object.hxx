#pragma once

#ifndef VERTEX_OBJECT_INCLUDED
#include "vertex_object.hpp"
#endif

namespace yggdrasill::graphic {

	template <class T>
	void Buffer::set(const std::vector<T>& container)
	{
		set_raw(sizeof(T), container.size(), &container[0]);
	}
	template <class Iter>
	void Buffer::set(Iter begin, Iter end)
	{
		set_raw(sizeof(decltype(*begin)), static_cast<std::size_t>(std::distance(begin, end)), &*begin);
	}

	template <class T>
	Buffer create_dynamic_buffer(std::size_t elements, bool index_buffer)
	{
		return Buffer{sizeof(T), elements, true, nullptr, index_buffer};
	}

	template <class C>
	Buffer create_buffer(const C& container, bool dynamic, bool index_buffer)
	{
		return Buffer{sizeof(typename C::value_type), container.size(), dynamic, container.data(), index_buffer};
	}

	template <class I>
	Buffer create_buffer(I begin, I end, bool dynamic, bool index_buffer)
	{
		return Buffer{sizeof(*begin), static_cast<std::size_t>(std::distance(begin, end)), dynamic, &*begin, index_buffer};
	}

	namespace details {
		template <class P, class M>
		const void* calc_offset(const M P::*member)
		{
			return reinterpret_cast<const void*>(&(reinterpret_cast<P*>(0)->*member));
		}

		template <class T>
		constexpr auto get_vertex_type() -> Vertex_layout::Element_type
		{
			static_assert(sizeof(T) == 0, "Unsupported type for a vertex");
			return Vertex_layout::Element_type::int_t;
		}
		template <>
		constexpr auto get_vertex_type<std::int8_t>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::byte_t;
		}
		template <>
		constexpr auto get_vertex_type<std::uint8_t>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::ubyte_t;
		}
		template <>
		constexpr auto get_vertex_type<std::int16_t>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::short_t;
		}
		template <>
		constexpr auto get_vertex_type<std::uint16_t>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::ushort_t;
		}
		template <>
		constexpr auto get_vertex_type<std::int32_t>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::int_t;
		}
		template <>
		constexpr auto get_vertex_type<std::uint32_t>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::uint_t;
		}
		template <>
		constexpr auto get_vertex_type<float>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::float_t;
		}
		template <>
		constexpr auto get_vertex_type<Vec2>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::float_t;
		}
		template <>
		constexpr auto get_vertex_type<Vec3>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::float_t;
		}
		template <>
		constexpr auto get_vertex_type<glm::vec2>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::float_t;
		}
		template <>
		constexpr auto get_vertex_type<glm::vec3>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::float_t;
		}
		template <>
		constexpr auto get_vertex_type<glm::vec4>() -> Vertex_layout::Element_type
		{
			return Vertex_layout::Element_type::float_t;
		}

		template <class T>
		constexpr auto get_vertex_comp_count() -> int
		{
			return 1;
		}
		template <>
		constexpr auto get_vertex_comp_count<Vec2>() -> int
		{
			return 2;
		}
		template <>
		constexpr auto get_vertex_comp_count<Vec3>() -> int
		{
			return 3;
		}
		template <>
		constexpr auto get_vertex_comp_count<glm::vec2>() -> int
		{
			return 2;
		}
		template <>
		constexpr auto get_vertex_comp_count<glm::vec3>() -> int
		{
			return 3;
		}
		template <>
		constexpr auto get_vertex_comp_count<glm::vec4>() -> int
		{
			return 4;
		}
	} // namespace details

#define VERTEX_FACTORY(TYPE, NAME, COUNT)                                                                                  \
	template <class Base>                                                                                                  \
	Vertex_layout::Element vertex(                                                                                         \
	        const std::string& name, TYPE Base::*value, std::size_t buffer, uint8_t divisor, bool normalized)              \
	{                                                                                                                      \
		return Vertex_layout::Element{                                                                                     \
		        name, COUNT, Vertex_layout::Element_type::NAME, normalized, details::calc_offset(value), buffer, divisor}; \
	}

	VERTEX_FACTORY(int8_t, byte_t, 1)
	VERTEX_FACTORY(uint8_t, ubyte_t, 1)
	VERTEX_FACTORY(int16_t, short_t, 1)
	VERTEX_FACTORY(uint16_t, ushort_t, 1)
	VERTEX_FACTORY(int32_t, int_t, 1)
	VERTEX_FACTORY(uint32_t, uint_t, 1)
	VERTEX_FACTORY(float, float_t, 1)
	VERTEX_FACTORY(Vec2, float_t, 2)
	VERTEX_FACTORY(Vec3, float_t, 3)
	VERTEX_FACTORY(glm::vec2, float_t, 2)
	VERTEX_FACTORY(glm::vec3, float_t, 3)
	VERTEX_FACTORY(glm::vec4, float_t, 4)

#undef VERTEX_FACTORY

	template <class T>
	Vertex_layout::Element vertex(const std::string& name, std::size_t buffer, uint8_t divisor, bool normalized)
	{
		return Vertex_layout::Element{name,
		                              details::get_vertex_comp_count<T>(),
		                              details::get_vertex_type<T>(),
		                              normalized,
		                              nullptr,
		                              buffer,
		                              divisor};
	}

} // namespace yggdrasill::graphic
