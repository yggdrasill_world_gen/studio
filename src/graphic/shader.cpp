#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "shader.hpp"

#include "vertex_object.hpp"

#include "../util/maybe.hpp"

#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <regex>


namespace yggdrasill::graphic {

	void Shader::bind()
	{
		glUseProgram(*handle_);
	}
	void Shader::unbind()
	{
		glUseProgram(0);
	}


#define SHADER_SET_UNIFORM(TYPE, CALL)                                                         \
	Shader& Shader::set_uniform(const char* name, TYPE value)                                  \
	{                                                                                          \
		auto it = uniform_locations_.find(name);                                               \
		if(it == uniform_locations_.end()) {                                                   \
			it = uniform_locations_.emplace(name, glGetUniformLocation(*handle_, name)).first; \
		}                                                                                      \
		CALL;                                                                                  \
		return *this;                                                                          \
	}

	SHADER_SET_UNIFORM(int, glUniform1i(it->second, value))
	SHADER_SET_UNIFORM(float, glUniform1f(it->second, value))
	SHADER_SET_UNIFORM(const glm::vec2&, glUniform2fv(it->second, 1, glm::value_ptr(value)))
	SHADER_SET_UNIFORM(const glm::vec3&, glUniform3fv(it->second, 1, glm::value_ptr(value)))
	SHADER_SET_UNIFORM(const glm::vec4&, glUniform4fv(it->second, 1, glm::value_ptr(value)))
	SHADER_SET_UNIFORM(const glm::mat2&, glUniformMatrix2fv(it->second, 1, GL_FALSE, glm::value_ptr(value)))
	SHADER_SET_UNIFORM(const glm::mat3&, glUniformMatrix3fv(it->second, 1, GL_FALSE, glm::value_ptr(value)))
	SHADER_SET_UNIFORM(const glm::mat4&, glUniformMatrix4fv(it->second, 1, GL_FALSE, glm::value_ptr(value)))
	SHADER_SET_UNIFORM(const std::vector<float>&,
	                   glUniform1fv(it->second, static_cast<GLsizei>(value.size()), value.data()))
	SHADER_SET_UNIFORM(const std::vector<glm::vec2>&,
	                   glUniform2fv(it->second,
	                                static_cast<GLsizei>(value.size()),
	                                reinterpret_cast<const GLfloat*>(value.data())))
	SHADER_SET_UNIFORM(const std::vector<glm::vec3>&,
	                   glUniform3fv(it->second,
	                                static_cast<GLsizei>(value.size()),
	                                reinterpret_cast<const GLfloat*>(value.data())))
	SHADER_SET_UNIFORM(const std::vector<glm::vec4>&,
	                   glUniform4fv(it->second,
	                                static_cast<GLsizei>(value.size()),
	                                reinterpret_cast<const GLfloat*>(value.data())))

#undef SHADER_SET_UNIFORM



	Shader_compiler::Shader_compiler(std::string name)
	  : name_(std::move(name)), shader_program_(glCreateProgram(), glDeleteProgram)
	{
	}

	namespace {
		std::string get_gl_log(unsigned int handle)
		{
			std::string log_buffer;
			auto        log_length = 0;
			glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &log_length);
			if(log_length > 1) {
				log_buffer = std::string(std::size_t(log_length), ' ');

				glGetShaderInfoLog(handle, log_length, &log_length, log_buffer.data());
				log_buffer.resize(log_length);
			}

			return log_buffer;
		}
		std::string get_gl_link_log(unsigned int handle)
		{
			std::string log_buffer;
			auto        log_length = 0;
			glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &log_length);
			if(log_length > 1) {
				log_buffer = std::string(std::size_t(log_length), ' ');

				glGetProgramInfoLog(handle, log_length, &log_length, log_buffer.data());
				log_buffer.resize(log_length);
			}

			return log_buffer;
		}

		void attach_source(unsigned int handle, std::string_view name, std::string_view source)
		{
			auto src_ptr = source.data();
			auto length  = static_cast<int>(source.size());
			glShaderSource(handle, 1, &src_ptr, &length);
			glCompileShader(handle);

			auto log_buffer = get_gl_log(handle);

			GLint success = 0;
			glGetShaderiv(handle, GL_COMPILE_STATUS, &success);
			if(success != GL_TRUE) {
				std::cerr << "Shader compilation failed (" << success << ") for \"" << name
				          << "\": " << log_buffer << "\n";
				std::exit(1);
			} else if(!log_buffer.empty()) {
				std::cout << "Shader compilation for \"" << name << "\": " << log_buffer << "\n";
			}
		}
	} // namespace

	Shader_compiler& Shader_compiler::vertex_shader(std::string_view name, std::string_view source) &
	{
		auto shader = *shaders_.emplace_back(glCreateShader(GL_VERTEX_SHADER), glDeleteShader);
		attach_source(shader, name, source);
		glAttachShader(*shader_program_, shader);
		return *this;
	}

	Shader_compiler& Shader_compiler::fragment_shader(std::string_view name, std::string_view source) &
	{
		auto shader = *shaders_.emplace_back(glCreateShader(GL_FRAGMENT_SHADER), glDeleteShader);
		attach_source(shader, name, source);
		glAttachShader(*shader_program_, shader);
		return *this;
	}

	Shader_compiler& Shader_compiler::vertex_layout(const Vertex_layout& vl) &
	{
		vl.foreach([&](const auto& name, const auto& binding) {
			glBindAttribLocation(*shader_program_, binding, name.c_str());
		});
		return *this;
	}

	Shader Shader_compiler::build() &&
	{
		glLinkProgram(*shader_program_);

		auto log_buffer = get_gl_link_log(*shader_program_);

		GLint success = 0;
		glGetProgramiv(*shader_program_, GL_LINK_STATUS, &success);
		if(success != GL_TRUE) {
			std::cerr << "Shader linking failed (" << success << ") for \"" << name_ << "\": " << log_buffer
			          << "\n";
			std::exit(1);
		} else if(!log_buffer.empty()) {
			std::cout << "Shader linking for \"" << name_ << "\": " << log_buffer << "\n";
		}

		glValidateProgram(*shader_program_);
		if(auto log = get_gl_link_log(*shader_program_); !log.empty()) {
			std::cout << "Shader validation errors for \"" << name_ << "\": " << log_buffer << "\n";
		}

		for(auto& s : shaders_) {
			glDetachShader(*shader_program_, *s);
		}

		shaders_.clear();
		return {std::move(shader_program_)};
	}

} // namespace yggdrasill::graphic
