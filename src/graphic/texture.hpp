/** simple wrapper for OpenGL textures ***************************************
 *                                                                           *
 * Copyright (c) 2014 Florian Oetke                                          *
 *  This file is distributed under the MIT License                           *
 *  See LICENSE file for details.                                            *
\*****************************************************************************/

#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "gl_handle.hpp"

#include "../util/func_traits.hpp"
#include "../util/maybe.hpp"

#include <glm/vec3.hpp>
#include <glm/vec4.hpp>

#include <memory>
#include <stdexcept>
#include <string>


namespace yggdrasill::graphic {

	enum class Texture_format {
		RGB_565,
		DEPTH,
		RG,
		RGB,
		RGBA,
		DEPTH_16,
		RG_16F,
		RGB_16F,
		RGBA_16F,
		DEPTH_24,
		RG_32F,
		RGB_32F,
		RGBA_32F,
		R_ubyte,
		R_ushort,
		R_uint,
		R_byte,
		R_short,
		R_int,
		R_float
	};

	struct Texture_data {
		const void* data;
		int         element_count;
		int         element_size;

		Texture_data() : data(nullptr), element_count(0), element_size(0) {}
		Texture_data(void* data, int element_count, int element_size)
		  : data(data), element_count(element_count), element_size(element_size)
		{
		}
		template <typename Container, typename = std::enable_if_t<!std::is_same_v<std::remove_cv_t<Container>, Texture_data>>>
		/*implicit*/ Texture_data(const Container& c)
		  : data(c.data())
		  , element_count(static_cast<int>(c.size()))
		  , element_size(static_cast<int>(sizeof(*c.data())))
		{
		}
	};

	class Texture {
	  public:
		explicit Texture(const std::string& path);
		Texture(int width, int height, Texture_format format, Texture_data data = {}, bool linear_filtered = true);

		void update_data(int width, int height, Texture_data data, util::maybe<Texture_format> format = {});

		void bind(int index = 0) const;

		[[nodiscard]] auto width() const noexcept { return width_; }
		[[nodiscard]] auto height() const noexcept { return height_; }

		[[nodiscard]] auto unsafe_low_level_handle() const { return *handle_; }

	  protected:
		using Handle = Gl_handle<Gl_handle_trait_array<glGenTextures, glDeleteTextures>>;

		Handle         handle_;
		int            width_, height_;
		Texture_format format_;
	};
	using Texture_ptr = std::shared_ptr<const Texture>;

	class Render_texture : public Texture {
	  public:
		Render_texture(int width, int height, bool depth_buffer);

		auto bind_target() -> util::scope_guard<void (*)()>;

	  private:
		YGGDRASILL_GL_HANDLE_TRAIT_ARRAY(Fb_handle_trait, glGenFramebuffers, glDeleteFramebuffers);
		YGGDRASILL_GL_HANDLE_TRAIT_ARRAY(Rb_handle_trait, glGenRenderbuffers, glDeleteRenderbuffers);
		using Fb_handle = Gl_handle<Fb_handle_trait>;
		using Rb_handle = Gl_handle<Rb_handle_trait>;

		Fb_handle              framebuffer_;
		util::maybe<Rb_handle> depth_buffer_;
	};

	/// warning: blocks the CPU waiting for GPU data
	extern void write_png(Render_texture&, std::string_view path, bool fill_alpha);

	extern int get_max_texture_size();

} // namespace yggdrasill::graphic
