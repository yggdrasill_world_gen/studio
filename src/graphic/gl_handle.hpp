#pragma once

#include <GLFW/glfw3.h>

#include <utility>

namespace yggdrasill::graphic {

	template <typename Trait>
	class Gl_handle {
	  public:
		Gl_handle() : raw_handle_(Trait::create()) {}
		Gl_handle(Gl_handle&& rhs) noexcept : raw_handle_(std::exchange(rhs.raw_handle_, 0)) {}
		Gl_handle(const Gl_handle&) = delete;
		~Gl_handle() { Trait::destroy(raw_handle_); }

		Gl_handle& operator=(const Gl_handle&) noexcept = delete;
		Gl_handle& operator=(Gl_handle&& rhs) noexcept
		{
			if(&rhs == this)
				return *this;

			Trait::destroy(raw_handle_);
			raw_handle_ = std::exchange(rhs.raw_handle_, 0);
			return *this;
		}
		constexpr auto operator*() const noexcept { return raw_handle_; }

		friend void swap(Gl_handle<Trait>& lhs, Gl_handle<Trait>& rhs)
		{
			std::swap(lhs.raw_handle_, rhs.raw_handle_);
		}

	  private:
		typename Trait::value_type raw_handle_;
	};


	template <auto Create, auto Destroy>
	struct Gl_handle_trait_simple {
		using value_type = GLuint;

		static value_type create() { return Create(); }
		static void       destroy(value_type h) { Destroy(h); }
	};

	template <void (*Create)(GLsizei, GLuint*), void (*Destroy)(GLsizei, const GLuint*)>
	struct Gl_handle_trait_array {
		using value_type = GLuint;

		static value_type create()
		{
			auto h = value_type(0);
			Create(1, &h);
			return h;
		}
		static void destroy(value_type h) { Destroy(1, &h); }
	};

#define YGGDRASILL_GL_HANDLE_TRAIT_ARRAY(NAME, C, D) \
	struct NAME {                                    \
		using value_type = GLuint;                   \
		static value_type create()                   \
		{                                            \
			auto h = value_type(0);                  \
			C(1, &h);                                \
			return h;                                \
		}                                            \
		static void destroy(value_type h)            \
		{                                            \
			D(1, &h);                                \
		}                                            \
	}

} // namespace yggdrasill::graphic
