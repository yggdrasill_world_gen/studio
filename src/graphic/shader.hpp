/** simple wrapper for OpenGL shader/programs ********************************
 *                                                                           *
 * Copyright (c) 2014 Florian Oetke                                          *
 *  This file is distributed under the MIT License                           *
 *  See LICENSE file for details.                                            *
\*****************************************************************************/

#pragma once

#include "common.hpp"

#include <glm/glm.hpp>

#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

namespace yggdrasill::graphic {

	class Vertex_layout;

	class Shader {
	  public:
		void bind();
		void unbind();

		Shader& set_uniform(const char* name, int value);
		Shader& set_uniform(const char* name, float value);
		Shader& set_uniform(const char* name, const glm::vec2& value);
		Shader& set_uniform(const char* name, const glm::vec3& value);
		Shader& set_uniform(const char* name, const glm::vec4& value);
		Shader& set_uniform(const char* name, const glm::mat2& value);
		Shader& set_uniform(const char* name, const glm::mat3& value);
		Shader& set_uniform(const char* name, const glm::mat4& value);
		Shader& set_uniform(const char* name, const std::vector<float>&);
		Shader& set_uniform(const char* name, const std::vector<glm::vec2>&);
		Shader& set_uniform(const char* name, const std::vector<glm::vec3>&);
		Shader& set_uniform(const char* name, const std::vector<glm::vec4>&);

	  private:
		friend class Shader_compiler;

		GL_handle<unsigned int>                          handle_;
		std::map<std::string, unsigned int, std::less<>> uniform_locations_; //< map for heterogeneous lookup

		Shader(GL_handle<unsigned int> handle) : handle_(std::move(handle)) {}
	};


	class Shader_compiler {
	  public:
		Shader_compiler(std::string name);

		Shader_compiler&  vertex_shader(std::string_view name, std::string_view source) &;
		Shader_compiler&& vertex_shader(std::string_view name, std::string_view source) &&
		{
			return std::move(vertex_shader(name, source));
		}

		Shader_compiler&  fragment_shader(std::string_view name, std::string_view source) &;
		Shader_compiler&& fragment_shader(std::string_view name, std::string_view source) &&
		{
			return std::move(fragment_shader(name, source));
		}

		Shader_compiler&  vertex_layout(const Vertex_layout&) &;
		Shader_compiler&& vertex_layout(const Vertex_layout& layout) &&
		{
			return std::move(vertex_layout(layout));
		}

		Shader build() &&;

	  private:
		std::string                          name_;
		GL_handle<unsigned int>              shader_program_;
		std::vector<GL_handle<unsigned int>> shaders_;
	};

} // namespace yggdrasill::graphic
