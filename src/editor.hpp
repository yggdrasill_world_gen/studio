#pragma once

#include "selection.hpp"
#include "settings.hpp"
#include "window.hpp"

#include "gui/config.hpp"
#include "renderer/view_renderer.hpp"
#include "simulation/graph.hpp"
#include "simulation/module_commands.hpp"
#include "windows/viewport.hpp"

#include <imgui.h>

#include <cstdint>
#include <string>
#include <vector>

namespace yggdrasill {

	class Editor {
	  public:
		Editor(const std::string& plugin_dir);
		~Editor();

		void update();

		bool quit() const { return quit_; }
		void reset_quit() { quit_ = false; }

		void safe_destructive_operation(std::function<void()> execute);
		void save_as(std::function<void()> after = {});
		void quick_save(std::function<void()> after = {});

	  private:
		struct Status_message {
			std::string message;
			ImVec4      color;
			float       time;
		};

		gui::Config& viewport_cfg_;
		gui::Config& cfg_;
		Settings     global_settings_;

		std::optional<std::string> save_file_;
		bool                       modified_since_save_ = false;
		std::uint64_t              saved_version_       = 0;

		simulation::Module_command_manager command_manager_;
		simulation::Graph                  graph_;
		simulation::Cursor                 current_state_;

		renderer::View_renderer                         renderer_;
		std::vector<std::unique_ptr<windows::Viewport>> viewports_;
		std::vector<std::unique_ptr<Window>>            windows_;
		Window*                                         create_world_window_;

		Status_message status_message_;

		Selection selection_;
		ImGuiButtonFlags camera_buttons_ = ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight
		                                   | ImGuiButtonFlags_MouseButtonMiddle;

		bool dark_mode_;
		bool validate_mesh_   = false;
		bool validate_layers_ = false;
		bool reset_layout_    = false;
		bool quit_            = false;

		Dockspace main_dock_   = 0;
		Dockspace left_dock_   = 0;
		Dockspace bottom_dock_ = 0;

		void update_style();

		auto create_viewport(gui::Config& cfg, const std::string& id = {}) -> std::unique_ptr<windows::Viewport>;

		void draw_dockspace();
		void create_dockspace();

		void reset_layout();

		void draw_menu();
		void draw_statusbar();

		void update_generator();

		auto create_world_cfg(const Seed& seed) const -> std::shared_ptr<Dictionary>;

		void save_to(std::string_view path, std::function<void()> after);
	};

} // namespace yggdrasill
