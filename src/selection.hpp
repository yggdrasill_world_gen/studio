#pragma once

#include "renderer/picking_result.hpp"
#include "util/func_traits.hpp"
#include "util/maybe.hpp"

#include <yggdrasill/commons.hpp>
#include <yggdrasill/mesh.hpp>

#include <imgui.h>

#include <string>
#include <utility>
#include <variant>
#include <vector>


namespace yggdrasill {

	struct Highlight_world_position {
		Vertex nearest_vertex = {};
		Vec3   world_position = {};

		friend bool operator==(const Highlight_world_position&, const Highlight_world_position&) = default;
	};

	class Selection {
	  public:
		/// the mouse is currently over one of the views
		auto hovered() const { return hovered_; }

		auto meter_per_pixel() const { return elements_.meter_per_pixel; }

		/// the vertex the mouse is currently over/near
		auto vertex() const { return elements_.vertex; }
		/// the face the mouse is currently over/near
		auto face() const { return elements_.face; }
		/// the primal_edge the mouse is currently over/near
		auto primal_edge() const { return elements_.primal_edge; }
		/// the dual_edge the mouse is currently over/near
		auto dual_edge() const { return elements_.dual_edge; }
		/// the world-space position the mouse is currently over
		auto world_position() const { return elements_.interpolated_position; }

		void highlight(Vertex v, ImU32 color, float thickness = 4.f)
		{
			highlight_element(v, color, thickness);
		}
		void highlight(Primal_edge e, ImU32 color, float thickness = 2.f)
		{
			highlight_element(e, color, thickness);
		}
		void highlight(Dual_edge e, ImU32 color, float thickness = 2.f)
		{
			highlight_element(e, color, thickness);
		}
		void highlight(Face f, ImU32 color) { highlight_element(f, color, 0.f); }
		void highlight(Highlight_world_position p, ImU32 color, float thickness = 4.f)
		{
			highlight_.push_back(Highlighting{p, color, thickness});
		}
		void highlight_area(Highlight_world_position p, float radius, ImU32 color)
		{
			highlight_.push_back(Highlighting{
			        Highlight_area{p, radius},
                    color, 0.f
            });
		}
		void highlight_line(
		        Highlight_world_position a, Highlight_world_position b, ImU32 color, float thickness = 2.f)
		{
			highlight_subdivided_line(a, b, 1'000'000.f, color, color, thickness);
		}
		void highlight_subdivided_line(Highlight_world_position a,
		                               Highlight_world_position b,
		                               float                    segment_length,
		                               ImU32                    color_a,
		                               ImU32                    color_b,
		                               float                    thickness = 2.f)
		{
			highlight_.push_back(Highlighting{
			        Highlight_line{a, b, segment_length, color_b},
                    color_a, thickness
            });
		}

		void add_label(Vertex nearest_vertex, std::string text, util::maybe<ImU32> color = {})
		{
			labels_.push_back(Label{nearest_vertex, util::nothing, text, color});
		}
		void add_label(Highlight_world_position p, std::string text, util::maybe<ImU32> color = {})
		{
			labels_.push_back(Label{p.nearest_vertex, p.world_position, text, color});
		}

		void update(util::maybe<renderer::Picking_result> hovered)
		{
			if(hovered.is_some()) {
				elements_ = hovered.get_or_throw();
				hovered_  = true;
			} else {
				hovered_ = false;
			}
			highlight_.clear();
			labels_.clear();
		}

		/**
		 * f has to be callable with:
		 * - f(Vertex, ImU32 color, float thickness)
		 * - f(Primal_edge, ImU32 color, float thickness)
		 * - f(Dual_edge, ImU32 color, float thickness)
		 * - f(Face, ImU32 color)
		 * - f(Highlight_world_position,  ImU32 color, float thickness)
		 * - f(Highlight_world_position, float radius_in_meter, ImU32 color)
		 * - f(Highlight_world_position a, Highlight_world_position b, float segment_length, ImU32, ImU32, float thickness)
		 */
		template <typename F>
		void foreach_highlight(F&& f) const
		{
			for(auto&& h : highlight_) {
				visit(
				        h.position,
				        [&](const Highlight_area& area) { f(area.p, area.radius, h.color); },
				        [&](const Face& face) { f(face, h.color); },
				        [&](const Highlight_line& line) {
					        f(line.a, line.b, line.segment_length, h.color, line.secondary_color, h.thickness);
				        },
				        [&](auto& p) { f(p, h.color, h.thickness); });
			}
		}
		template <typename... Fs>
		void foreach_highlight(Fs&&... fs) const requires(sizeof...(Fs) > 1)
		{
			foreach_highlight(overloaded{std::forward<Fs>(fs)...});
		}

		template <typename F>
		void foreach_label(F&& f) const
		{
			for(auto&& l : labels_) {
				f(l.nearest_vertex, l.world_position, l.text, l.color);
			}
		}

		friend bool operator==(const Selection& lhs, const Selection& rhs)
		{
			return lhs.highlight_ == rhs.highlight_ && lhs.labels_ == rhs.labels_;
		}

	  private:
		struct Highlight_area {
			Highlight_world_position p;
			float                    radius;

			friend bool operator==(const Highlight_area&, const Highlight_area&) = default;
		};
		struct Highlight_line {
			Highlight_world_position a;
			Highlight_world_position b;
			float                    segment_length;
			ImU32                    secondary_color;

			friend bool operator==(const Highlight_line&, const Highlight_line&) = default;
		};

		using Highlighting_position =
		        std::variant<Vertex, Primal_edge, Dual_edge, Face, Highlight_world_position, Highlight_area, Highlight_line>;

		struct Highlighting {
			Highlighting_position position;
			ImU32                 color;
			float                 thickness;

			friend bool operator==(const Highlighting&, const Highlighting&) = default;
		};

		struct Label {
			Vertex             nearest_vertex;
			util::maybe<Vec3>  world_position;
			std::string        text;
			util::maybe<ImU32> color;

			friend bool operator==(const Label&, const Label&) = default;
		};

		renderer::Picking_result  elements_;
		bool                      hovered_ = false;
		std::vector<Highlighting> highlight_;
		std::vector<Label>        labels_;

		template <typename E>
		void highlight_element(E e, ImU32 color, float thickness)
		{
			if(e != E{})
				highlight_.push_back(Highlighting{e, color, thickness});
		}
	};

} // namespace yggdrasill
