#include "tools.hpp"

#include "../tool.hpp"
#include "../gui/ui_elements.hpp"
#include "../simulation/cursor.hpp"
#include "../tools/elevation.hpp"
#include "../tools/measure.hpp"
#include "../tools/mesh_resolution.hpp"
#include "../tools/plate_id.hpp"
#include "../tools/select.hpp"
#include "../tools/velocity.hpp"
#include "../util/ranges.hpp"

#include <imgui.h>

namespace yggdrasill {
	extern ImFont* font_largest;
} // namespace yggdrasill

namespace yggdrasill::windows {

	namespace {
		constexpr auto button_complement(int mask)
		{
			return (mask & ImGuiButtonFlags_MouseButtonLeft ? 0 : ImGuiButtonFlags_MouseButtonLeft)
			       | (mask & ImGuiButtonFlags_MouseButtonRight ? 0 : ImGuiButtonFlags_MouseButtonRight)
			       | (mask & ImGuiButtonFlags_MouseButtonMiddle ? 0 : ImGuiButtonFlags_MouseButtonMiddle);
		}
		auto button_down_mask()
		{
			return (ImGui::IsMouseDown(ImGuiMouseButton_Left) ? ImGuiButtonFlags_MouseButtonLeft : 0)
			       | (ImGui::IsMouseDown(ImGuiMouseButton_Right) ? ImGuiButtonFlags_MouseButtonRight : 0)
			       | (ImGui::IsMouseDown(ImGuiMouseButton_Middle) ? ImGuiButtonFlags_MouseButtonMiddle : 0);
		}

		class Tools : public Window {
			static constexpr auto id_ = "Tools";

		  public:
			Tools(Dockspace*          dock,
			      gui::Config&        cfg,
			      bool                visible,
			      simulation::Cursor& simulation_state,
			      Selection&          selection,
			      ImGuiButtonFlags&   camera_buttons)
			  : Window(dock, cfg[id_], visible)
			  , simulation_state_(simulation_state)
			  , selection_(selection)
			  , camera_buttons_(camera_buttons)
			  , tools_(util::vector_from(tools::select(simulation_state_.graph(), window_cfg()),
			                             tools::measure(simulation_state_.graph()),
			                             tools::elevation(simulation_state_.graph()),
			                             tools::velocity(simulation_state_.graph()),
			                             tools::plate_id(simulation_state_.graph()),
			                             tools::mesh_resolution(simulation_state_.graph())))
			  , selected_tool_(tools_.front().get())
			{
			}

			bool draw_status() override
			{
				selected_tool_->draw_status_active(simulation_state_, selection_);
				for(auto& tool : tools_)
					tool->draw_status_always(simulation_state_, selection_);

				return false;
			}

			const char* id() const override { return id_; }

		  private:
			using Tool_list = std::vector<std::unique_ptr<tools::Tool>>;

			simulation::Cursor& simulation_state_;
			Selection&          selection_;
			ImGuiButtonFlags&   camera_buttons_;
			const Tool_list     tools_;
			tools::Tool*        selected_tool_;
			bool                tool_active_ = false;

			bool do_update() override
			{
				auto open = true;
				if(ImGui::Begin(id(), &open)) {
					tool_selection();

					ImGui::Spacing();
					ImGui::Separator();
					ImGui::Spacing();

					tool_settings();
				}
				ImGui::End();

				camera_buttons_ = button_complement(selected_tool_->used_buttons());

				const auto pressed_buttons = button_down_mask() & selected_tool_->used_buttons();

				if((simulation_state_.editable() || selected_tool_->readonly()) && pressed_buttons != 0
				   && !ImGui::GetIO().KeyCtrl) {
					// Hovered is checked separately, so we don't invalidate/recreate the command when the
					// user slightly leaves the window or the picking gives no result after larger modifications.
					if(selection_.hovered()) {
						tool_active_ = true;
						selected_tool_->on_use(simulation_state_, selection_, pressed_buttons);
					}
				} else if(tool_active_) {
					tool_active_ = false;
					selected_tool_->on_use(simulation_state_, selection_, 0);
				}

				return open;
			}

			void tool_selection()
			{
				ImGui::PushFont(font_largest);
				ON_EXIT(ImGui::PopFont());

				gui::radio_buttons([&](auto&& button) {
					for(auto& tool : tools_) {
						if(button(selected_tool_ == tool.get(), tool->button_label(), tool->tooltip()))
							selected_tool_ = tool.get();
					}
				});
			}

			void tool_settings()
			{
				if(!simulation_state_.ready()) {
					ImGui::PushStyleColor(ImGuiCol_Text, "#f00"_color);
					ImGui::TextWrapped(
					        "There is no world state here to edit or view. Use the timeline to seek to a "
					        "node that has already been fully "
					        "computed.");
					ImGui::PopStyleColor();

				} else if(simulation_state_.editable() || selected_tool_->readonly()) {
					selected_tool_->update(simulation_state_, selection_);

				} else {
					ImGui::PushStyleColor(ImGuiCol_Text, "#f00"_color);
					ImGui::TextWrapped(
					        "Manual modifications are disabled while the simulation is running.\n\nWait for "
					        "the "
					        "simulation to finish or use the timeline to seek to a node that has already "
					        "been fully "
					        "computed.");
					ImGui::PopStyleColor();
				}
			}
		};
	} // namespace

	std::unique_ptr<Window> tools(Dockspace*          dock,
	                              gui::Config&        cfg,
	                              bool                visible,
	                              simulation::Cursor& simulation_state,
	                              Selection&          selection,
	                              ImGuiButtonFlags&   camera_buttons)
	{
		return std::make_unique<Tools>(dock, cfg, visible, simulation_state, selection, camera_buttons);
	}

} // namespace yggdrasill::windows
