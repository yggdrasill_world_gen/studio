#pragma once

#include "../window.hpp"

#include <memory>
#include <span>

namespace yggdrasill {
	namespace renderer {
		class View_renderer;
	}
} // namespace yggdrasill

namespace yggdrasill::windows {

	class Viewport;

	extern void open_image_export_popup(renderer::View_renderer&, const windows::Viewport&);
	extern void open_mesh_export_popup(renderer::View_renderer&, const windows::Viewport&);

} // namespace yggdrasill::windows
