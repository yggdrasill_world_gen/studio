#pragma once

#include "../layer_renderer.hpp"
#include "../selection.hpp"
#include "../window.hpp"
#include "../graphic/layer_backed.hpp"
#include "../gui/config.hpp"
#include "../renderer/camera.hpp"
#include "../renderer/picking_result.hpp"
#include "../renderer/view_renderer.hpp"
#include "../util/string.hpp"

#include <yggdrasill/world.hpp>

#include <imgui.h>

#include <any>
#include <vector>


namespace yggdrasill::windows {

	template <typename F>
	concept Layer_view_factory =
	        std::is_same_v<std::unique_ptr<Layer_renderer>, std::invoke_result_t<F, graphic::Layer_cache&>>;

	inline auto hidden(Layer_view_factory auto&& factory)
	{
		return [factory = std::forward<decltype(factory)>(factory)](graphic::Layer_cache& cache) {
			auto v = factory(cache);
			v->draw(false);
			return v;
		};
	}
	inline auto alpha(Layer_view_factory auto&& factory, float alpha)
	{
		return [factory = std::forward<decltype(factory)>(factory), alpha](graphic::Layer_cache& cache) {
			auto v = factory(cache);
			v->alpha(alpha);
			return v;
		};
	}

	extern void reset_viewport_ids();

	/// A single editor window, consisting of a list of Layer_views and a camera position.
	///
	/// Camera positions are referenced by name and may be shared between multiple Viewport instances.
	/// The state of this class, as well as its Layer_views, is serialized into the imgui.ini file and restored on startup.
	///
	/// Each Viewport has a unique string id, that is either passed on construction (for loading from file) or generated (if empty).
	/// IDs passed into the constructor need to follow the schema "Viewport N", where N is an increasing integer.
	class Viewport : public Window {
	  public:
		template <typename... Factories>
		requires(sizeof...(Factories) > 0)
		Viewport(Dockspace*               dock,
		         gui::Config&             cfg,
		         const std::string&       id,
		         const ImGuiButtonFlags&  camera_buttons,
		         renderer::View_renderer& renderer,
		         Factories&&... fs)
		  : Viewport(dock, cfg, id, camera_buttons, renderer)
		{
			layers_.reserve(layers_.size() + sizeof...(Factories));
			(layers_.emplace_back(fs(renderer.layer_cache())), ...);
			load_config();
		}
		~Viewport();

		Viewport(Viewport&&) noexcept            = delete;
		Viewport(const Viewport&)                = delete;
		Viewport& operator=(Viewport&&) noexcept = delete;
		Viewport& operator=(const Viewport&)     = delete;

		const char* id() const override { return id_.c_str(); }

		void save_config();
		void load_config();

		auto camera() const -> const renderer::Camera&;
		auto layers() const -> std::span<const std::unique_ptr<Layer_renderer>>;
		auto view_size() const -> glm::vec2 { return last_view_size_; }

	  private:
		struct Id_sanitized_tag {};

		std::string              id_;
		gui::Config::Properties* cfg_;
		const ImGuiButtonFlags*  camera_buttons_;

		float     properties_width_;
		glm::vec2 last_view_size_{1, 1};

		std::string active_camera_   = std::string{renderer::default_camera_name};
		std::string new_camera_name_ = "New Cam";
		bool        zoom_changed_    = false;

		std::vector<std::unique_ptr<Layer_renderer>> layers_;

		renderer::View_renderer& renderer_;

		// uses delegating constructor to sanitize the passed id, before the base-class constructor is executed
		Viewport(Dockspace*               dock,
		         gui::Config&             cfg,
		         const std::string&       id,
		         const ImGuiButtonFlags&  camera_buttons,
		         renderer::View_renderer& renderer);

		Viewport(Dockspace*               dock,
		         gui::Config&             cfg,
		         const std::string&       id,
		         const ImGuiButtonFlags&  camera_buttons,
		         renderer::View_renderer& renderer,
		         Id_sanitized_tag);

		void handle_view_controls(renderer::Camera&);
		void draw_side_panel(renderer::Camera_map& cameras);

		bool do_update() override;
	};

} // namespace yggdrasill::windows
