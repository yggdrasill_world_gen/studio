#include "export.hpp"

#include "viewport.hpp"

#include "../gui/file_dialogs.hpp"
#include "../gui/popup.hpp"
#include "../gui/ui_elements.hpp"
#include "../renderer/equirectangular_projection.hpp"
#include "../renderer/view_renderer.hpp"
#include "../util/ranges.hpp"

#include <yggdrasill/mesh_utils.hpp>

#include <imgui.h>
#include <range/v3/all.hpp>
#include <yggdrasill_icons.hpp>

namespace yggdrasill::windows {

	namespace {
		enum class Export_state { input, delay_open_save_dialog, open_save_dialog, wait_for_file, done };

		constexpr auto layer_position  = Layer_definition<Vec3, Ref_type::vertex>("position");
		constexpr auto layer_elevation = Layer_definition<float, Ref_type::vertex>("elevation");

		auto clipped_view_size(const Viewport& vp) -> glm::vec2
		{
			const auto content_size = vp.camera().calc_content_size(vp.view_size());
			return min(content_size, vp.view_size());
		}

		void width_height_input(int& width, int& height, float width_over_height)
		{
			gui::label("Width");
			ImGui::SetNextItemWidth(200);
			if(ImGui::InputInt("##width", &width)) {
				height = static_cast<int>(std::round(width / width_over_height));
			}

			gui::label("Height");
			ImGui::SetNextItemWidth(200);
			if(ImGui::InputInt("##height", &height)) {
				width = static_cast<int>(std::round(height / width_over_height));
			}

			if(width > height) {
				width  = std::clamp(width, 8, graphic::get_max_texture_size());
				height = std::max(2, static_cast<int>(std::round(width / width_over_height)));
			} else {
				height = std::clamp(height, 8, graphic::get_max_texture_size());
				width  = std::max(2, static_cast<int>(std::round(height * width_over_height)));
			}
		}
	} // namespace

	void open_image_export_popup(renderer::View_renderer& renderer, const windows::Viewport& viewport)
	{
		auto view_size = clipped_view_size(viewport);
		auto width     = static_cast<int>(view_size.x);
		auto height    = static_cast<int>(view_size.y);

		gui::open_modal_popup(
		        "Export Screenshot of View",
		        [&renderer, &viewport, width, height, view_size, state = Export_state::input]() mutable {
			        switch(state) {
				        case Export_state::input: {
					        view_size = clipped_view_size(viewport);

					        width_height_input(width, height, view_size.x / view_size.y);

					        // buttons
					        ImGui::Spacing();
					        ImGui::SetItemDefaultFocus();

					        if(!renderer.world()) {
						        ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
						        ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
					        }
					        const auto ok_pressed = ImGui::Button(YGGDRASILL_ICON_SAVE " Export")
					                                || ImGui::IsKeyPressed(ImGuiKey_Enter, false);
					        if(!renderer.world()) {
						        ImGui::PopItemFlag();
						        ImGui::PopStyleVar();
					        }
					        if(ok_pressed && renderer.world()) {
						        state = Export_state::delay_open_save_dialog;
					        }
					        ImGui::SameLine();
					        if(ImGui::Button(YGGDRASILL_ICON_TIMES " Cancel")
					           || ImGui::IsKeyPressed(ImGuiKey_Escape, false)) {
						        return true;
					        }
					        return false;
				        }

				        case Export_state::delay_open_save_dialog:
					        state = Export_state::open_save_dialog;
					        break;

				        case Export_state::open_save_dialog:
					        state = Export_state::wait_for_file;
					        gui::save_dialog("Export View - Save Location",
					                         "world.png",
					                         {"PNG Image", "*.png"},
					                         [&](std::string_view path) {
						                         assert(state == Export_state::wait_for_file);
						                         if(path.empty()) {
							                         state = Export_state::input;
							                         return;
						                         }
						                         auto camera = viewport.camera();
						                         camera.viewport_shift *=
						                                 static_cast<float>(width) / view_size.x;

						                         auto texture = renderer.draw_to_texture(
						                                 camera, viewport.layers(), width, height);
						                         if(!texture) {
							                         std::cerr << "renderer.draw_to_texture() didn't return "
							                                      "a valid texture\n";
							                         std::abort();
						                         }
						                         write_png(*texture,
						                                   path,
						                                   camera.view_mode != renderer::View_mode::spherical);
						                         state = Export_state::done;
					                         });
					        break;

				        case Export_state::wait_for_file: break;
				        case Export_state::done: return true;
			        }

			        ImGui::TextColored({1, 0, 0, 1}, "Rendering in progress.");
			        ImGui::TextColored({1, 0, 0, 1}, "The application might not react during the process.");
			        return false;
		        });
	}


	namespace {
		enum class Model_mode {
			textured_obj = 0, /// renderable OBJ file with material and UV-mapped textures based on visible layers
			data_ply = 1 /// single PLY file, that contains all layer and face data, but can't be rendered by most applications
		};
		struct Model_settings {
			Model_mode mode                = Model_mode::textured_obj;
			int        texture_width       = 4096;
			int        texture_height      = 2048;
			bool       normalize_positions = true;
			bool       combine_textures    = true;
		};
		void write_ply_file(Const_world_view world, std::string_view file_location, const Model_settings& settings);
		void write_obj_file(Const_world_view world,
		                    renderer::View_renderer&,
		                    const windows::Viewport&,
		                    std::string_view      file_location,
		                    const Model_settings& settings);
	} // namespace

	void open_mesh_export_popup(renderer::View_renderer& renderer, const windows::Viewport& viewport)
	{
		gui::open_modal_popup(
		        "Export Mesh File",
		        [&renderer, &viewport, settings = Model_settings{}, state = Export_state::input]() mutable {
			        switch(state) {
				        case Export_state::input: {
					        gui::label("Format");
					        constexpr auto format_labels = std::array{"Textured *.OBJ", "Data *.PLY"};
					        gui::dropdown("##format", settings.mode, format_labels);

					        gui::label("Unit Sphere");
					        ImGui::Checkbox("##unitSphere", &settings.normalize_positions);

					        gui::enabled_if(settings.mode == Model_mode::textured_obj, [&] {
						        width_height_input(settings.texture_width, settings.texture_height, 2.f / 1.f);

						        gui::label("Single Texture");
						        ImGui::Checkbox("##combineTextures", &settings.combine_textures);
					        });

					        // buttons
					        ImGui::Spacing();
					        ImGui::SetItemDefaultFocus();

					        const auto ok_pressed = gui::enabled_if(
					                renderer.world() && renderer.world().layer(layer_elevation), [&] {
						                return ImGui::Button(YGGDRASILL_ICON_SAVE " Export")
						                       || ImGui::IsKeyPressed(ImGuiKey_Enter, false);
					                });
					        if(ok_pressed && renderer.world()) {
						        state = Export_state::delay_open_save_dialog;
					        }
					        ImGui::SameLine();
					        if(ImGui::Button(YGGDRASILL_ICON_TIMES " Cancel")
					           || ImGui::IsKeyPressed(ImGuiKey_Escape, false)) {
						        return true;
					        }
					        return false;
				        }

				        case Export_state::delay_open_save_dialog:
					        state = Export_state::open_save_dialog;
					        break;

				        case Export_state::open_save_dialog: {
					        state = Export_state::wait_for_file;

					        const auto extension = [&]() -> std::string {
						        switch(settings.mode) {
							        case Model_mode::textured_obj: return "obj";
							        case Model_mode::data_ply: return "ply";
						        }
						        std::abort();
					        }();

					        gui::save_dialog(
					                "Export Model - Save Location",
					                "world." + extension,
					                {"3D Model", "*." + extension},
					                [&](std::string_view path) {
						                assert(state == Export_state::wait_for_file);
						                if(path.empty()) {
							                state = Export_state::input;
							                return;
						                }

						                switch(settings.mode) {
							                case Model_mode::textured_obj:
								                write_obj_file(renderer.world(), renderer, viewport, path, settings);
								                break;
							                case Model_mode::data_ply:
								                write_ply_file(renderer.world(), path, settings);
								                break;
						                }

						                state = Export_state::done;
					                });
					        break;
				        }

				        case Export_state::wait_for_file: break;
				        case Export_state::done: return true;
			        }

			        ImGui::TextColored({1, 0, 0, 1}, "Export in progress.");
			        ImGui::TextColored({1, 0, 0, 1}, "The application might not react during the process.");
			        return false;
		        });
	}

	namespace {
		template <typename KeyType>
		void write_ply_layer_header(Const_world_view world, std::ostream& file, const Model_settings& settings)
		{
			world.foreach_layer([&](auto layer) {
				if constexpr(
				        std::is_same_v<typename std::decay_t<decltype(layer)>::key_type, std::decay_t<KeyType>>) {
					switch(layer.definition().data_type()) {
						case Type::bool_t:
							file << "property uchar " << layer.definition().id() << '\n';
							break;
						case Type::int8_t: file << "property char " << layer.definition().id() << '\n'; break;
						case Type::int32_t: file << "property int " << layer.definition().id() << '\n'; break;
						case Type::float_t:
							file << "property float " << layer.definition().id() << '\n';
							break;
						case Type::double_t:
							file << "property double " << layer.definition().id() << '\n';
							break;
						case Type::vec2_t:
							file << "property float " << layer.definition().id() << "_x\n";
							file << "property float " << layer.definition().id() << "_y\n";
							break;
						case Type::vec3_t:
							file << "property float " << layer.definition().id() << "_x\n";
							file << "property float " << layer.definition().id() << "_y\n";
							file << "property float " << layer.definition().id() << "_z\n";
							break;

						default:
							std::cout << "[Note] Skipped layer of type "
							          << type_name(layer.definition().data_type())
							          << ", which is unsupported by PLY files\n";
							break;
					}
				}
			});
		}

		template <typename KeyType>
		void write_ply_layer_values(
		        Const_world_view world, std::ostream& file, const Model_settings& settings, KeyType key)
		{
			world.foreach_layer([&](auto layer) {
				if constexpr(std::is_same_v<typename std::decay_t<decltype(layer)>::key_type, KeyType>) {
					overloaded{[&](YGDL_Bool v) { file << (v == YGDL_TRUE ? 1 : 0) << ' '; },
					           [&](int8_t v) { file << static_cast<int>(v) << ' '; },
					           [&](int32_t v) { file << v << ' '; },
					           [&](float v) { file << v << ' '; },
					           [&](double v) { file << v << ' '; },
					           [&](Vec2 v) { file << v.x << ' ' << v.y << ' '; },
					           [&](Vec3 v) { file << v.x << ' ' << v.y << ' ' << v.z << ' '; },
					           [](auto&) {}}(layer[key]);
				}
			});
		}

		void write_ply_file(Const_world_view world, std::string_view file_location, const Model_settings& settings)
		{

			auto       mesh      = world.mesh();
			auto       positions = world.required_layer(layer_position);
			const auto radius    = world.required_unstructured_layer("meta")["radius"].get<float>();

			auto file = std::ofstream{std::string(file_location)};
			file << "ply\n";
			file << "format ascii 1.0\n";
			file << "comment Generated by Yggdrasill Studio\n";

			file << "element vertex " << (mesh.vertex_max_index() + 1) << "\n";
			file << "property float x\n";
			file << "property float y\n";
			file << "property float z\n";

			write_ply_layer_header<Vertex>(world, file, settings);

			file << "element face " << (mesh.face_max_index() + 1) << "\n";
			file << "property list uchar int vertex_index\n";
			write_ply_layer_header<Face>(world, file, settings);

			file << "end_header\n";

			for(auto i = 0; i <= mesh.vertex_max_index(); ++i) {
				const auto v = Vertex{i};
				auto       p = positions[v];
				if(!settings.normalize_positions) {
					p *= radius;
				}
				file << p.x << ' ' << p.y << ' ' << p.z << ' ';

				write_ply_layer_values(world, file, settings, v);
				file << '\n';
			}

			for(const Face face : mesh.faces()) {
				const auto vertices = face.vertices(mesh);
				file << "3 " << vertices[0].index() << ' ' << vertices[1].index() << ' '
				     << vertices[2].index() << ' ';
				write_ply_layer_values(world, file, settings, face);
				file << '\n';
			}
			file << '\n';
			file.flush();
		}

		void split_pole_faces(World& world, const float pole_y)
		{
			static constexpr auto epsilon2 = 0.00001f;

			auto [positions]          = world.lock_layer(layer_position);
			const auto radius         = world.required_unstructured_layer("meta")["radius"].get<float>();
			auto       closest_vertex = no_vertex;

			// find the vertex with y closest to pole_y
			const auto v = world.mesh().vertices().max([&](auto v) { return positions[v].y * radius * pole_y; });
			if(v == world.mesh().vertices().end()) {
				std::cerr << "no valid vertices in exported mesh!\n";
				return;
			} else {
				closest_vertex = *v;
			}

			// TODO: logic below is still not ideal; the following should be enough (I think):
			// 1. locate the vertex closest to Vec3{0, pole_y, 0}
			// 2. locate the out-going edge that passes closest to Vec3{0, pole_y, 0};
			// 3. split that edge, so that the new vertex is close to the pole
			// 4. set the vertex position to Vec3{0, pole_y, 0};

			for(auto i = 0; i < 32; i++) {
				const auto normalized_position = normalized(positions[closest_vertex]);

				const auto closest_edge = closest_vertex.edges(world.mesh()).max([&](const auto e) {
					return dist2(normalized(positions[e.dest(world.mesh())]), normalized_position);
				});

				const auto closest_dist =
				        dist2(normalized(positions[closest_edge->dest(world.mesh())]), normalized_position);
				if(closest_dist < epsilon2) {
					break; // done
				}

				auto [mesh] = world.lock_mesh();
				mesh.split(*closest_edge, 0.5f);

				const auto new_closest_vertex =
				        closest_edge->dest(mesh).neighbors(mesh, Neighbor_type::with_dual_edges).max([&](const auto& v) {
					        return positions[v].y * radius * pole_y;
				        });
				if(positions[*new_closest_vertex].y * pole_y > positions[closest_vertex].y * pole_y)
					closest_vertex = *new_closest_vertex;
			};
		}

		World pole_artifact_workaround(Const_world_view world_in)
		{
			auto out = World(world_in);

			// split any faces near the poles, until there are exactly two vertices that coincide with the poles
			split_pole_faces(out, 1);
			split_pole_faces(out, -1);

			return out;
		}

		void write_obj_file(Const_world_view         world_in,
		                    renderer::View_renderer& renderer,
		                    const windows::Viewport& viewport,
		                    std::string_view         file_location,
		                    const Model_settings&    settings)
		{
			world_in.validate();

			const auto line_sep = file_location.find_last_of('/');
			const auto filename =
			        file_location.substr(line_sep + 1, file_location.find_last_of('.') - 1 - line_sep);
			const auto filename_without_ext =
			        std::string(file_location.substr(0, file_location.find_last_of('.')));

			auto world                   = pole_artifact_workaround(world_in);
			auto [positions, elevations] = world.required_layer(layer_position, layer_elevation);
			const auto flat_mesh         = renderer::equirectangular_projection(world, glm::mat4(1.f));

			auto file = std::ofstream{std::string(file_location)};
			file << "o mesh\n";
			if(settings.combine_textures) {
				file << "mtllib " << filename << ".mtl\n";
			}

			const auto elevation_relief_scale = viewport.camera().elevation_relief_scale;
			const auto radius = world.required_unstructured_layer("meta")["radius"].get<float>();
			for(auto&& [p, elevation] : util::join(positions, elevations)) {
				auto pos =
				        p + normalized(p) * (elevation * elevation_relief_scale) * (elevation < 0.0f ? 0.4f : 1.0f);
				if(settings.normalize_positions)
					pos /= radius;

				file << "v " << pos.x << ' ' << pos.y << ' ' << pos.z << '\n';
			}
			for(auto& p : positions) {
				auto n = normalized(p);
				file << "vn " << n.x << ' ' << n.y << ' ' << n.z << '\n';
			}

			for(auto uv : flat_mesh.positions) {
				file << "vt " << uv.x << ' ' << uv.y << '\n';
			}

			if(settings.combine_textures) {
				file << "usemtl default\n";
			}

			for(std::size_t i = 1; i <= flat_mesh.indices.size(); i += 3) {
				const auto a_uv   = i;
				const auto a_data = flat_mesh.indices[a_uv - 1].index() + 1;
				const auto b_uv   = i + 1;
				const auto b_data = flat_mesh.indices[b_uv - 1].index() + 1;
				const auto c_uv   = i + 2;
				const auto c_data = flat_mesh.indices[c_uv - 1].index() + 1;

				// clang-format off
				file << "f " << a_data << '/' << a_uv << '/' << a_data
				     << ' '  << b_data << '/' << b_uv << '/' << b_data
				     << ' '  << c_data << '/' << c_uv << '/' << c_data << '\n';
				// clang-format on
			}
			file << '\n';
			file.flush();

			if(settings.combine_textures) {
				auto mat = std::ofstream{filename_without_ext + ".mtl"};
				mat << R"xxx(newmtl default
Ka 1.000000 1.000000 1.000000
Kd 1.000000 1.000000 1.000000
Ks 0.000000 0.000000 0.000000
Tr 1.000000
Ni 1.000000
d 1.000000
illum 1
map_Kd )xxx";
				mat << filename << ".png\n\n";
			}


			// write textures
			const auto camera = renderer::Camera{
			        .zoom            = 1.f,
			        .rotation        = {0.f, 0.f, 0.f},
			        .view_mode       = renderer::View_mode::equirectangular,
			        .clipping_cutoff = 1.f
            };

			if(settings.combine_textures) {
				const auto texture_path = filename_without_ext + ".png";

				auto texture = renderer.draw_to_texture(
				        world, camera, viewport.layers(), settings.texture_width, settings.texture_height);
				if(!texture) {
					std::cerr << "renderer.draw_to_texture() didn't "
					             "return a valid texture\n";
					std::abort();
				}
				write_png(*texture, texture_path, true);

			} else {
				for(auto& layer : viewport.layers()) {
					if(layer->draw()) {
						const auto texture_path = filename_without_ext + "_" + layer->name().str() + ".png";

						auto texture = renderer.draw_to_texture(
						        world, camera, std::span{&layer, 1}, settings.texture_width, settings.texture_height);
						if(!texture) {
							std::cerr << "renderer.draw_to_texture() didn't "
							             "return a valid texture\n";
							std::abort();
						}
						write_png(*texture, texture_path, true);
					}
				}
			}
		}
	} // namespace

} // namespace yggdrasill::windows
