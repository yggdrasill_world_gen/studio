#include "config.hpp"

#include "../settings.hpp"
#include "../gui/ui_elements.hpp"
#include "../gui/yggdrasill_type_widgets.hpp"
#include "../simulation/module_commands.hpp"
#include "../util/func_traits.hpp"

#include <imgui.h>
#include <range/v3/all.hpp>
#include <yggdrasill_icons.hpp>

#include <cinttypes>

namespace yggdrasill::windows {

	namespace {
		constexpr auto width = 200.f;

		class Common_base {
		  public:
			virtual auto config_edit_state() -> util::maybe<gui::Edit_state&> = 0;
			virtual void reset_config_edit()                                  = 0;
			virtual void start_config_edit()                                  = 0;
			virtual bool config_edit_resettable()                             = 0;

			void simple_simulation_config(Const_dictionary_view dict, gui::Edit_state* edit_state = nullptr)
			{
				if(!edit_state) {
					ImGui::PushItemFlag(ImGuiItemFlags_Disabled, true);
					ImGui::PushStyleVar(ImGuiStyleVar_Alpha, ImGui::GetStyle().Alpha * 0.5f);
				}
				ON_EXIT_IF(!edit_state, {
					ImGui::PopItemFlag();
					ImGui::PopStyleVar();
				});

				if(!dict["seed"].exists()) {
					return;
				}

				// seed
				auto seed = dict["seed"].get<String>();
				gui::label("Seed", width);
				const auto button_width = ImGui::CalcTextSize(YGGDRASILL_ICON_DICE).x
				                          + ImGui::GetStyle().FramePadding.y * 2
				                          + ImGui::GetStyle().ItemSpacing.y * 2;
				ImGui::SetNextItemWidth(-button_width);
				ImGui::InputText("##seed",
				                 seed.data(),
				                 seed.size(),
				                 ImGuiInputTextFlags_CharsHexadecimal | ImGuiInputTextFlags_CharsUppercase
				                         | ImGuiInputTextFlags_CharsNoBlank);
				seed.resize(8 * 4, '0');
				gui::ui_help_tooltip(
				        "Initial state for the random number generator used to generate the world");
				ImGui::SameLine();
				if(gui::button(YGGDRASILL_ICON_DICE, true, "Generate Random Seed"))
					seed = Seed::random().str();
				if(edit_state && seed != dict["seed"]) {
					edit_state->dict_set("seed", seed);
				}


				// planet radius
				auto radius    = dict["generate"]["radius"].get<float>();
				auto radius_km = radius / 1000.f;
				gui::label("Radius", width);
				ImGui::InputFloat("##radius", &radius_km, 0.f, 0.f, "%.1f km");
				gui::ui_help_tooltip("The radius of the planet");
				radius = std::clamp(radius_km * 1000.f, 100'000.f, 10'000'000.f);
				if(edit_state && radius != dict["generate"]["radius"]) {
					auto _ = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
					edit_state->dict_set("radius", radius);
				}


				// vertex perturbation
				auto perturbation = dict["generate"]["perturbation"].get<float>();
				gui::label("Perturbation", width);
				gui::ui_help_tooltip(
				        "The strength of the random perturbation applied to the generated vertices. Zero "
				        "means the "
				        "vertices position will follow a visible pattern.");
				ImGui::SliderFloat("##perturbation", &perturbation, 0.f, 1.f);
				if(edit_state && perturbation != dict["generate"]["perturbation"]) {
					auto _ = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
					edit_state->dict_set("perturbation", perturbation);
				}

				// plate number
				auto plate_number_min = dict["generate"]["plates"]["min"].get<int32_t>();
				auto plate_number_max = dict["generate"]["plates"]["max"].get<int32_t>();
				gui::label("Plates", width);
				gui::range_slider_int("##plate_number", &plate_number_min, &plate_number_max, 1, 100);
				gui::ui_help_tooltip(
				        "The range the random number of initial tectonic plates is picked from.");
				if(edit_state && plate_number_min != dict["generate"]["plates"]["min"]) {
					auto g = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
					auto p = edit_state->push_path("plates", gui::Edit_state::Path_element_type::dict);
					edit_state->dict_set("min", plate_number_min);
				}
				if(edit_state && plate_number_max != dict["generate"]["plates"]["max"]) {
					auto g = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
					auto p = edit_state->push_path("plates", gui::Edit_state::Path_element_type::dict);
					edit_state->dict_set("max", plate_number_max);
				}

				// elevation
				auto elevation_min = dict["elevation"]["min"].get<float>() / 1000;
				auto elevation_max = dict["elevation"]["max"].get<float>() / 1000;
				gui::label("Elevation", width);
				gui::range_slider_float(
				        "##elevation", &elevation_min, &elevation_max, -20.f, 20.f, "(%.0f km, %.0f km)");
				gui::ui_help_tooltip(
				        "The range that restricts the allowed elevation values. The usual range (and the one "
				        "used by "
				        "the visualization in the editor) is +-10km.");
				if(edit_state && elevation_min != dict["elevation"]["min"].get<float>() / 1000) {
					auto _ = edit_state->push_path("elevation", gui::Edit_state::Path_element_type::dict);
					edit_state->dict_set("min", elevation_min * 1000.f);
				}
				if(edit_state && elevation_max != dict["elevation"]["max"].get<float>() / 1000) {
					auto _ = edit_state->push_path("elevation", gui::Edit_state::Path_element_type::dict);
					edit_state->dict_set("max", elevation_max * 1000.f);
				}

				auto plate_rifting_per_mj = dict["plate"]["rifting"]["events_per_year"].get<float>() * 1e6f;
				gui::label("Plate Rifting", width);
				const auto rifting_changed = ImGui::SliderFloat(
				        "##plate_rifting", &plate_rifting_per_mj, 0.f, 1.f, "%.4f events/Myr");
				gui::ui_help_tooltip(
				        "The average number of plate rifting event per million years, i.e. how often large "
				        "continents "
				        "should split apart.");
				if(rifting_changed && edit_state) {
					auto p = edit_state->push_path("plate", gui::Edit_state::Path_element_type::dict);
					auto r = edit_state->push_path("rifting", gui::Edit_state::Path_element_type::dict);
					edit_state->dict_set("events_per_year", plate_rifting_per_mj / 1e6f);
				}

				if(ImGui::CollapsingHeader("Ocean", ImGuiTreeNodeFlags_DefaultOpen)) {
					// ocean probability
					auto ocean_percentage = dict["generate"]["ocean"]["probability"].get<float>() * 100;
					gui::label("Surface Percentage", width);
					const auto op_changed =
					        ImGui::SliderFloat("##ocean_percentage", &ocean_percentage, 0.f, 100.f, "%.0f %%");
					gui::ui_help_tooltip(
					        "The percentage of the initial tectonic plates that should be oceanic, i.e. "
					        "roughly the "
					        "percentage of the surface that should be water.");
					if(op_changed && edit_state) {

						auto g = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
						auto o = edit_state->push_path("ocean", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("probability", ocean_percentage / 100);
					}

					// min/max resolution for mesh refinement
					auto ocean_resolution_min = dict["refine"]["ocean"]["min_dist"].get<float>() / 1000;
					auto ocean_resolution_max = dict["refine"]["ocean"]["max_dist"].get<float>() / 1000;
					gui::label("Resolution", width);
					gui::range_slider_float("##ocean_resolution",
					                        &ocean_resolution_min,
					                        &ocean_resolution_max,
					                        1.f,
					                        10'000.f,
					                        "(%.0f km, %.0f km)");
					gui::ui_help_tooltip(
					        "The min/max distance between vertices on oceanic plates, that the algorithm "
					        "should "
					        "optimize towards");
					if(edit_state
					   && ocean_resolution_min != dict["refine"]["ocean"]["min_dist"].get<float>() / 1000) {
						auto r = edit_state->push_path("refine", gui::Edit_state::Path_element_type::dict);
						auto o = edit_state->push_path("ocean", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("min_dist", ocean_resolution_min * 1000.f);
					}
					if(edit_state
					   && ocean_resolution_max != dict["refine"]["ocean"]["max_dist"].get<float>() / 1000) {
						auto r = edit_state->push_path("refine", gui::Edit_state::Path_element_type::dict);
						auto o = edit_state->push_path("ocean", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("max_dist", ocean_resolution_max * 1000.f);
					}

					// initial elevation
					auto elevation = dict["generate"]["ocean"]["elevation"].get<float>() / 1000;
					gui::label("Initial Elevation", width);
					const auto ec = ImGui::SliderFloat(
					        "##ocean_elevation", &elevation, elevation_min, elevation_max, "%.1f km");
					gui::ui_help_tooltip("The average initial elevation of oceanic crust");
					if(ec && edit_state) {
						auto g = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
						auto o = edit_state->push_path("ocean", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("elevation", elevation * 1000.f);
					}

					// initial velocity
					auto velocity = dict["generate"]["ocean"]["velocity"].get<float>();
					gui::label("Initial Velocity", width);
					const auto vc = ImGui::SliderFloat("##ocean_velocity", &velocity, 0.f, 1.f, "%.3f m/year");
					gui::ui_help_tooltip("The maximum initial velocity of oceanic crust");
					if(vc && edit_state) {
						auto g = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
						auto o = edit_state->push_path("ocean", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("velocity", velocity);
					}
				}


				if(ImGui::CollapsingHeader("Continent", ImGuiTreeNodeFlags_DefaultOpen)) {
					auto continent_resolution_min = dict["refine"]["continent"]["min_dist"].get<float>() / 1000;
					auto continent_resolution_max = dict["refine"]["continent"]["max_dist"].get<float>() / 1000;
					gui::label("Resolution", width);
					gui::ui_help_tooltip(
					        "The min/max distance between vertices on continental plates, that the algorithm "
					        "should "
					        "optimize towards");
					gui::range_slider_float("##continent_resolution",
					                        &continent_resolution_min,
					                        &continent_resolution_max,
					                        1.f,
					                        10'000.f,
					                        "(%.0f km, %.0f km)");
					if(edit_state
					   && continent_resolution_min != dict["refine"]["continent"]["min_dist"].get<float>() / 1000) {
						auto r = edit_state->push_path("refine", gui::Edit_state::Path_element_type::dict);
						auto c = edit_state->push_path("continent", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("min_dist", continent_resolution_min * 1000.f);
					}
					if(edit_state
					   && continent_resolution_max != dict["refine"]["continent"]["max_dist"].get<float>() / 1000) {
						auto r = edit_state->push_path("refine", gui::Edit_state::Path_element_type::dict);
						auto c = edit_state->push_path("continent", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("max_dist", continent_resolution_max * 1000.f);
					}

					// initial elevation
					auto elevation = dict["generate"]["continent"]["elevation"].get<float>() / 1000;
					gui::label("Initial Elevation", width);
					const auto ec = ImGui::SliderFloat(
					        "##continent_elevation", &elevation, elevation_min, elevation_max, "%.1f km");
					gui::ui_help_tooltip("The average initial elevation of continental crust");
					if(ec && edit_state) {
						auto g = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
						auto c = edit_state->push_path("continent", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("elevation", elevation * 1000.f);
					}

					// initial velocity
					auto velocity = dict["generate"]["continent"]["velocity"].get<float>();
					gui::label("Initial Velocity", width);
					const auto vc =
					        ImGui::SliderFloat("##continent_velocity", &velocity, 0.f, 1.f, "%.3f m/year");
					gui::ui_help_tooltip("The maximum initial velocity of continental crust");
					if(vc && edit_state) {
						auto g = edit_state->push_path("generate", gui::Edit_state::Path_element_type::dict);
						auto c = edit_state->push_path("continent", gui::Edit_state::Path_element_type::dict);
						edit_state->dict_set("velocity", velocity);
					}
				}
			}

			void simulation_config(Const_dictionary_view dict, bool editable)
			{
				ImGui::NewLine();
				if(auto edit_state = config_edit_state(); edit_state.is_some()) {
					if(config_edit_resettable() && gui::right_aligned_button(YGGDRASILL_ICON_LOCK " Lock")) {
						reset_config_edit();
					} else {
						simple_simulation_config(dict, &edit_state.get_or_throw());

						ImGui::Spacing();
						if(ImGui::CollapsingHeader("Advanced Options")) {
							gui::dictionary_edit("shared_settings", dict, edit_state.get_or_throw());
						}
					}

				} else {
					if(gui::right_aligned_button(YGGDRASILL_ICON_LOCK_OPEN " Unlock", editable)) {
						start_config_edit();
					}
					simple_simulation_config(dict);

					ImGui::Spacing();
					if(ImGui::CollapsingHeader("Advanced Options")) {
						gui::dictionary_view("shared_settings", dict);
					}
				}
			}
		};

		class Config_window : public Window, private Common_base {
			static constexpr auto id_ = "Configuration";

		  public:
			Config_window(Dockspace*                                dock,
			              gui::Config&                              cfg,
			              bool                                      visible,
			              simulation::Cursor&                       simulation_state,
			              const simulation::Module_command_manager& module_manager,
			              Settings&                                 settings)
			  : Window(dock, cfg[id_], visible)
			  , simulation_state_(simulation_state)
			  , module_manager_(module_manager)
			  , settings_(settings)
			{
			}

			[[nodiscard]] const char* id() const override { return id_; }

		  private:
			simulation::Cursor&                          simulation_state_;
			const simulation::Module_command_manager&    module_manager_;
			std::optional<gui::Command_based_edit_state> edit_state_;
			const simulation::Node*                      last_simulation_state_;
			Settings&                                    settings_;

			bool do_update() override
			{
				auto open = true;
				if(ImGui::Begin(id(), &open)) {
					if(ImGui::BeginTabBar("tabs")) {
						if(ImGui::BeginTabItem("General")) {
							general_config();
							ImGui::EndTabItem();
						}
						if(ImGui::BeginTabItem("Graph")) {
							simulation_config();
							ImGui::EndTabItem();
						}
						ImGui::EndTabBar();
					}
				}
				ImGui::End();
				return open;
			}

			void general_config()
			{
				ImGui::TextUnformatted("Renderer:");

				gui::label("Scale (Static)");
				auto scale_static = settings_.render_scale_static();
				ImGui::SliderFloat("##s1", &scale_static, 0.1f, 2.f);
				settings_.render_scale_static(scale_static);

				gui::label("Scale (Dynamic)");
				auto scale_dynamic = settings_.render_scale_interactive();
				ImGui::SliderFloat("##s2", &scale_dynamic, 0.1f, 2.f);
				settings_.render_scale_interactive(scale_dynamic);
			}

			auto config_edit_state() -> util::maybe<gui::Edit_state&> override
			{
				if(edit_state_)
					return *edit_state_;
				else
					return util::nothing;
			}
			void reset_config_edit() override { edit_state_.reset(); }
			void start_config_edit() override
			{
				last_simulation_state_ = simulation_state_.node().get();
				edit_state_.emplace(
				        "Config Modified",
				        simulation::Command_type::user_config_edit,
				        [this](simulation::State& state, auto&& operation) {
					        auto&& cfg = state.mutable_config();
					        operation(cfg);
					        auto vr = module_manager_.validate(cfg);
					        if(!vr) {
						        using namespace ranges;
						        auto errors = vr.errors()
						                      | views::transform([](auto& e) { return e.error.message(); })
						                      | views::join("\n") | to<std::string>();
						        throw Module_exception("Validation failed: " + errors);
					        }
				        },
				        [this]() -> Const_dictionary_view { return *simulation_state_.config(); },
				        simulation_state_,
				        [this](const simulation::Node& new_node) {
					        if(last_simulation_state_ == new_node.predecessor())
						        last_simulation_state_ = &new_node;
				        });
			}
			bool config_edit_resettable() override { return true; }

			void simulation_config()
			{
				auto node = simulation_state_.node();
				if(!node)
					return;

				if(simulation_state_ != *last_simulation_state_ || !simulation_state_.editable()) {
					edit_state_.reset();
					last_simulation_state_ = node.get();
				}

				Common_base::simulation_config(node->state().config(), simulation_state_.editable());
			}
		};

		class Create_world_window : public Window, private Common_base {
			static constexpr auto id_ = "Create New World";

		  public:
			Create_world_window(Dockspace*                          dock,
			                    gui::Config&                        cfg,
			                    bool                                visible,
			                    simulation::Cursor&                 simulation_state,
			                    simulation::Module_command_manager& module_manager)
			  : Window(dock, cfg[id_], visible)
			  , simulation_state_(simulation_state)
			  , module_manager_(module_manager)
			{
			}

			[[nodiscard]] const char* id() const override { return id_; }

		  private:
			simulation::Cursor&                   simulation_state_;
			simulation::Module_command_manager&   module_manager_;
			std::shared_ptr<Dictionary>           config_;
			std::optional<gui::Direct_edit_state> edit_state_;
			int                                   plate_tectonics_iterations_ = 20;

			auto config_edit_state() -> util::maybe<gui::Edit_state&> override { return *edit_state_; }
			void reset_config_edit() override {}
			void start_config_edit() override {}
			bool config_edit_resettable() override { return false; }

			void import_config()
			{
				auto current_node = simulation_state_.node();
				for(auto* node = current_node.get(); node; node = node->predecessor()) {
					if(node->ready() && node->state().config()["seed"].exists()) {
						config_ = std::make_shared<Dictionary>(node->state().config());
						edit_state_.emplace(*config_);
						return;
					}
				}

				config_ = module_manager_.create_base_config(Seed::random());
				edit_state_.emplace(*config_);
			}

			bool do_update() override
			{
				auto open = true;
				if(ImGui::Begin(id(), &open)) {
					if(!config_ || !edit_state_) {
						import_config();
					}
					ImGui::Spacing();

					Common_base::simulation_config(*config_, true);

					gui::label("Plate Tectonic Iterations", width);
					ImGui::InputInt("##plate_tectonics_iterations", &plate_tectonics_iterations_);

					auto valid = true;
					if(auto result = module_manager_.validate(*config_); !result) {
						using namespace ranges::views;
						auto errors = result.errors() | transform([](auto& e) { return e.error.message(); })
						              | join("\n") | ranges::to<std::string>();

						ImGui::TextColored({1, 0, 0, 1}, "%s", "The current configuration is not valid:");
						ImGui::TextColored({1, 0, 0, 1}, "%s", errors.c_str());
						valid = false;
					}

					ImGui::Spacing();
					if(gui::button(YGGDRASILL_ICON_FILE " Reset",
					               true,
					               "Override the current settings above, with the default settings and a "
					               "random seed")) {
						config_ = module_manager_.create_base_config(Seed::random());
						edit_state_.emplace(*config_);
					}
					ImGui::SameLine();
					if(gui::button(YGGDRASILL_ICON_COPY " Import",
					               true,
					               "Override the current settings above, with those that were used to "
					               "generate the "
					               "current world")) {
						import_config();
					}
					ImGui::SameLine();
					if(gui::button(YGGDRASILL_ICON_CHECK " Create", valid)) {
						create_world();
					}
					ImGui::SameLine();
					if(gui::button(YGGDRASILL_ICON_DICE " Create Random",
					               valid,
					               "Create a new world with a settings above but with a new random seed")) {
						(*config_)["seed"] = Seed::random().str();
						create_world();
					}
					ImGui::SameLine();
					if(gui::button(YGGDRASILL_ICON_TIMES " Cancel", true, "Close this window")) {
						open = false;
					}
				}
				ImGui::End();
				return open;
			}

			void create_world()
			{
				auto& simulation = simulation_state_.graph();
				simulation_state_.seek(
				        simulation.add_world("New World", module_manager_.create_initial_state(config_)));
				module_manager_.execute(module_manager_.list()[0], simulation);

				for(int i = 1; i <= plate_tectonics_iterations_; i++) {
					module_manager_.execute(module_manager_.list()[1], simulation);
				}
				simulation_state_.seek(simulation.main_branch());
			}
		};

	} // namespace


	std::unique_ptr<Window> create_world_config(Dockspace*                          dock,
	                                            gui::Config&                        cfg,
	                                            bool                                visible,
	                                            simulation::Cursor&                 simulation_state,
	                                            simulation::Module_command_manager& module_manager)
	{
		return std::make_unique<Create_world_window>(dock, cfg, visible, simulation_state, module_manager);
	}

	std::unique_ptr<Window> config(Dockspace*                                dock,
	                               gui::Config&                              cfg,
	                               bool                                      visible,
	                               simulation::Cursor&                       simulation_state,
	                               const simulation::Module_command_manager& module_manager,
	                               Settings&                                 settings)
	{
		return std::make_unique<Config_window>(dock, cfg, visible, simulation_state, module_manager, settings);
	}

} // namespace yggdrasill::windows
