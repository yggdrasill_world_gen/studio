#include "viewport.hpp"

#include "../layer_renderer.hpp"
#include "../gui/ui_elements.hpp"
#include "../renderer/equirectangular_projection.hpp"
#include "../simulation/graph.hpp"
#include "../views/base.hpp"

#include <glm/gtx/quaternion.hpp>
#include <imgui.h>
#include <imgui_internal.h>

#include <sstream>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

namespace yggdrasill::windows {

	namespace {
		constexpr auto properties_width_default = 350.f;
		constexpr auto properties_width_min     = 8.f;

		constexpr auto zoom_speed     = -0.03f;
		constexpr auto rotation_speed = 0.005f;

		int next_id = 1;

		std::string calc_id(const std::string& id)
		{
			if(id.empty()) {
				std::cout << "Created new viewport id " << next_id << "\n";

				auto ss = std::stringstream{};
				ss << "Viewport ";
				ss << next_id++;
				return ss.str();

			} else {
				const auto space = id.rfind(' ');
				assert(space != std::string::npos);

				const auto int_id = std::atoi(id.c_str() + space);
				std::cout << "Reused existing viewport id " << int_id << "\n";
				next_id = std::max(next_id, int_id + 1);
				return id;
			}
		}
	} // namespace

	void reset_viewport_ids()
	{
		next_id = 1;
	}

	Viewport::Viewport(Dockspace*               dock,
	                   gui::Config&             cfg,
	                   const std::string&       id,
	                   const ImGuiButtonFlags&  camera_buttons,
	                   renderer::View_renderer& renderer)
	  : Viewport(dock, cfg, calc_id(id), camera_buttons, renderer, Id_sanitized_tag{})
	{
	}

	Viewport::Viewport(Dockspace*               dock,
	                   gui::Config&             cfg,
	                   const std::string&       id,
	                   const ImGuiButtonFlags&  camera_buttons,
	                   renderer::View_renderer& renderer,
	                   Id_sanitized_tag)
	  : Window(dock, cfg[id], true)
	  , id_(id)
	  , cfg_(&cfg[id])
	  , camera_buttons_(&camera_buttons)
	  , properties_width_(properties_width_default)
	  , renderer_(renderer)
	{
		layers_.emplace_back(views::base_view(renderer_.layer_cache()));
	}

	Viewport::~Viewport() = default;


	bool Viewport::do_update()
	{
		auto& camera  = renderer_.cameras()[active_camera_];
		auto  visible = true;

		ImGui::SetNextWindowSize({600, 600}, ImGuiCond_FirstUseEver);
		if(ImGui::Begin(id_.c_str(), &visible, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoScrollWithMouse)) {
			// layout
			ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0, 0));
			ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));

			properties_width_ = std::clamp(properties_width_, 8.f, ImGui::GetWindowWidth() - 8.f);
			const auto properties_collapsed = properties_width_ <= properties_width_min;

			const auto flat = camera.view_mode != renderer::View_mode::spherical;

			if(flat) {
				auto render_size = ImGui::GetContentRegionAvail();
				render_size.x -= properties_width_;
				ImGui::SetNextWindowContentSize(
				        camera.calc_content_size(glm::vec2(render_size) - ImGui::GetStyle().ScrollbarSize));

				if(zoom_changed_) {
					ImGui::SetNextWindowScroll({camera.viewport_shift.x, camera.viewport_shift.y});
				}
			}

			ImGui::BeginChild("##render",
			                  ImVec2(-properties_width_, 0),
			                  true,
			                  flat ? ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_AlwaysHorizontalScrollbar
			                                  | ImGuiWindowFlags_AlwaysVerticalScrollbar
			                       : 0);
			ImGui::EndChild();
			ImGui::PopStyleVar();

			// resize-drag / splitter
			ImGui::SameLine();
			const auto splitter_p = ImGui::GetCursorScreenPos();
			ImGui::InvisibleButton("splitter", ImVec2(8.0f, ImGui::GetWindowHeight()));
			gui::ui_help_tooltip("Drag to resize settings drawer.\nDouble click to collapse/expand.");
			if(ImGui::IsItemHovered()) {
				ImGui::SetMouseCursor(ImGuiMouseCursor_ResizeEW);
				if(ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left)) {
					properties_width_ = properties_collapsed ? properties_width_default : properties_width_min;
					cfg_->set("properties_width", properties_width_);
				}
			}
			if(ImGui::IsItemActive()) {
				properties_width_ = std::clamp(
				        properties_width_ - ImGui::GetIO().MouseDelta.x, 8.f, ImGui::GetWindowWidth() - 8.f);
				cfg_->set("properties_width", properties_width_);
			}
			const auto spliter_color = ImGui::GetColorU32(
			        ImGui::IsItemHovered() ? ImGuiCol_SeparatorActive
			                               : (properties_collapsed ? ImGuiCol_CheckMark : ImGuiCol_Separator));
			const auto splitter_border = ImGui::GetStyle().WindowBorderSize * 2.f;
			ImGui::GetWindowDrawList()->AddLine(splitter_p + ImVec2((8.f - splitter_border) / 2.f, 0.f),
			                                    splitter_p
			                                            + ImVec2((8.f - splitter_border) / 2.f,
			                                                     ImGui::GetWindowHeight() + splitter_border * 2),
			                                    spliter_color,
			                                    splitter_border);

			ImGui::SameLine();

			ImGui::BeginChild("##properties", ImVec2(0, 0), true);
			ImGui::EndChild();

			ImGui::PopStyleVar();


			// actual content
			if(ImGui::BeginChild("##properties", ImVec2(0, 0), true)) {
				draw_side_panel(renderer_.cameras());
			}
			ImGui::EndChild();

			if(ImGui::BeginChild("##render")) {
				camera.viewport_shift = glm::vec2{ImGui::GetScrollMaxX() <= 0 ? 0.f : ImGui::GetScrollX(),
				                                  ImGui::GetScrollMaxY() <= 0 ? 0.f : ImGui::GetScrollY()};

				ImGui::PushClipRect(ImGui::GetWindowPos(), ImGui::GetWindowPos() + ImGui::GetWindowSize(), true);
				last_view_size_ = ImGui::GetWindowDrawList()->GetClipRectMax()
				                  - ImGui::GetWindowDrawList()->GetClipRectMin();
				ImGui::PopClipRect();
				renderer_.draw(this, camera, layers_);

				if(ImGui::IsWindowHovered(ImGuiHoveredFlags_AllowWhenBlockedByActiveItem)) {
					handle_view_controls(camera);
				}
			}
			ImGui::EndChild();
		}
		ImGui::End();

		return visible;
	}

	static bool mouse_down(ImGuiButtonFlags flags)
	{
		if(flags & ImGuiButtonFlags_MouseButtonLeft && ImGui::IsMouseDown(ImGuiMouseButton_Left))
			return true;
		if(flags & ImGuiButtonFlags_MouseButtonMiddle && ImGui::IsMouseDown(ImGuiMouseButton_Middle))
			return true;
		if(flags & ImGuiButtonFlags_MouseButtonRight && ImGui::IsMouseDown(ImGuiMouseButton_Right))
			return true;
		return false;
	}

	void Viewport::handle_view_controls(renderer::Camera& camera)
	{
		auto&         io     = ImGui::GetIO();
		ImGuiContext& g      = *ImGui::GetCurrentContext();
		auto          window = g.CurrentWindow;

		const auto last_zoom = camera.zoom;

		// zoom
		if(io.KeyCtrl && std::abs(io.MouseWheel) > 0.001f) {
			camera.zoom = std::clamp(camera.zoom + io.MouseWheel * zoom_speed, 0.01f, 1.f);

			if(camera.view_mode != renderer::View_mode::spherical) {
				// change scroll to keep same point focused
				auto&      scroll     = window->Scroll;
				const auto focus      = glm::vec2(io.MousePos) - ImGui::GetWindowPos();
				camera.viewport_shift = (focus + scroll) * (last_zoom / camera.zoom) - focus;
			}

			renderer_.cameras().save(active_camera_, camera);
		}


		// rotate
		if(!io.KeyCtrl && camera.view_mode == renderer::View_mode::spherical
		   && (std::abs(io.MouseWheel) > 0.001f || std::abs(io.MouseWheelH) > 0.001f)) {
			auto speed_mod     = std::lerp(0.5f, 10.f, camera.zoom);
			auto dx            = (io.KeyShift ? -io.MouseWheel : io.MouseWheelH) * rotation_speed * speed_mod;
			auto dy            = (io.KeyShift ? 0.f : io.MouseWheel) * rotation_speed * speed_mod;
			auto q             = glm::quat{glm::radians(camera.rotation)};
			q                  = glm::normalize(glm::quat(glm::vec3(dy, dx, 0.f)) * q);
			camera.rotation    = glm::degrees(glm::eulerAngles(q));
			camera.auto_rotate = false;
			renderer_.cameras().save(active_camera_, camera);
		}


		// movement
		bool hovered = false;
		bool held    = false;
		if(g.HoveredId == 0)
			ImGui::ButtonBehavior(
			        window->Rect(), window->GetID("##scrolldraggingoverlay"), &hovered, &held, ImGuiButtonFlags_None);

		if(hovered
		   && mouse_down(ImGui::GetIO().KeyCtrl ? ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight
		                                                  | ImGuiButtonFlags_MouseButtonMiddle
		                                        : *camera_buttons_)) {
			camera.auto_rotate = false;

			if(camera.view_mode == renderer::View_mode::spherical) {
				auto q = glm::quat{glm::radians(camera.rotation)};

				auto speed_mod  = std::lerp(0.05f, 1.f, camera.zoom);
				auto dx         = io.MouseDelta.x * rotation_speed * speed_mod;
				auto dy         = io.MouseDelta.y * rotation_speed * speed_mod;
				q               = glm::normalize(glm::quat(glm::vec3(dy, dx, 0.f)) * q);
				camera.rotation = glm::degrees(glm::eulerAngles(q));
				renderer_.cameras().save(active_camera_, camera);

			} else {
				const auto x =
				        std::clamp(g.CurrentWindow->Scroll.x - io.MouseDelta.x, 0.f, ImGui::GetScrollMaxX());
				const auto y =
				        std::clamp(g.CurrentWindow->Scroll.y - io.MouseDelta.y, 0.f, ImGui::GetScrollMaxY());
				ImGui::SetScrollX(x);
				ImGui::SetScrollY(y);
				camera.viewport_shift = glm::vec2{x, y};
				renderer_.cameras().save(active_camera_, camera);
			}
		}

		zoom_changed_ = camera.zoom != last_zoom;
	}

	static constexpr std::array<const char*, 2> projection_values{"Spherical", "Equirectangular"};

	void Viewport::draw_side_panel(renderer::Camera_map& cameras)
	{
		static constexpr auto alpha_width = 100.f;

		if(ImGui::TreeNodeEx("Layer", ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_DefaultOpen)) {
			if(ImGui::BeginTable("layers", 3)) {
				ImGui::TableSetupColumn("Layer", ImGuiTableColumnFlags_WidthStretch);
				ImGui::TableSetupColumn("Draw", ImGuiTableColumnFlags_WidthFixed);
				ImGui::TableSetupColumn("Alpha", ImGuiTableColumnFlags_WidthFixed, alpha_width);
				ImGui::TableHeadersRow();

				auto move = std::optional<std::pair<std::ptrdiff_t, std::ptrdiff_t>>();

				for(auto i = std::ptrdiff_t(1); i < std::ptrdiff_t(layers_.size()); ++i) {
					auto& layer = layers_[i];

					ImGui::PushID(layer->name());
					ImGui::TableNextColumn();
					ImGui::Selectable(layer->name());

					ImGuiDragDropFlags src_flags = 0;
					src_flags |= ImGuiDragDropFlags_SourceNoDisableHover; // Keep the source displayed as hovered
					src_flags |= ImGuiDragDropFlags_SourceNoHoldToOpenOthers; // Because our dragging is local, we disable the feature of opening foreign treenodes/tabs while dragging
					//src_flags |= ImGuiDragDropFlags_SourceNoPreviewTooltip; // Hide the tooltip
					if(ImGui::BeginDragDropSource(src_flags)) {
						if(!(src_flags & ImGuiDragDropFlags_SourceNoPreviewTooltip))
							ImGui::Text("Moving \"%s\"", layer->name().c_str());
						ImGui::SetDragDropPayload("reorder_layer", &i, sizeof(i));
						ImGui::EndDragDropSource();
					}

					if(ImGui::BeginDragDropTarget()) {
						ImGuiDragDropFlags target_flags = 0;
						target_flags |= ImGuiDragDropFlags_AcceptBeforeDelivery; // Don't wait until the delivery (release mouse button on a target) to do something
						target_flags |= ImGuiDragDropFlags_AcceptNoDrawDefaultRect; // Don't display the yellow rectangle
						if(const ImGuiPayload* payload =
						           ImGui::AcceptDragDropPayload("reorder_layer", target_flags)) {
							move = {*reinterpret_cast<std::size_t*>(payload->Data), i};
						}
						ImGui::EndDragDropTarget();
					}


					ImGui::TableNextColumn();
					auto draw = layer->draw();
					if(ImGui::Checkbox("##draw", &draw)) {
						layer->draw(draw);
						cfg_->set(layer->name().str() + "_draw", draw);
					}

					ImGui::TableNextColumn();

					if(layer->uses_alpha()) {
						ImGui::SetNextItemWidth(alpha_width);
						auto alpha = layer->alpha();
						if(ImGui::SliderFloat("##alpha", &alpha, 0.01f, 1.f)) {
							layer->alpha(alpha);
							cfg_->set(layer->name().str() + "_alpha", std::to_string(alpha));
						}
					}

					ImGui::PopID();
				}

				if(move) {
					auto [from, to] = *move;
					auto b          = layers_.begin();
					if(from < to) {
						std::rotate(b + from, b + from + 1, b + to + 1);
					} else if(from > to) {
						std::rotate(b + to, b + from, b + from + 1);
					}

					save_config();
				}

				ImGui::EndTable();
			}
			ImGui::TreePop();
		}


		if(ImGui::TreeNodeEx("View", ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_DefaultOpen)) {
			ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x);
			auto _ = util::scope_guard{[] { ImGui::PopItemWidth(); }};

			auto* camera = &cameras[active_camera_];

			// select camera
			gui::label("Camera");
			if(ImGui::BeginCombo("##Camera", active_camera_.c_str())) {
				for(auto&& [name, cam] : cameras) {
					if(ImGui::Selectable(name.c_str(), &cam == camera)) {
						active_camera_ = name;
						cfg_->set("active_camera", active_camera_);
						zoom_changed_ = true;
					}
				}

				ImGui::EndCombo();
			}


			// add new camera
			gui::label("Add");
			auto add = ImGui::Button("+");
			ImGui::SameLine();
			add |= gui::input_text("##Add", new_camera_name_);
			if(add) {
				camera         = &cameras.emplace(new_camera_name_, *camera);
				active_camera_ = std::move(new_camera_name_);
				cfg_->set("active_camera", active_camera_);
				new_camera_name_ = "New Cam";
			}


			// modify camera
			gui::label("Projection");
			const auto curr_mode = static_cast<std::size_t>(camera->view_mode);
			if(ImGui::BeginCombo("##Projection", projection_values[curr_mode])) {
				for(std::size_t i = 0; i < projection_values.size(); i++) {
					if(ImGui::Selectable(projection_values[i], i == curr_mode) && i != curr_mode) {
						camera->view_mode = static_cast<renderer::View_mode>(i);
						cameras.save(active_camera_, *camera);
					}
				}

				ImGui::EndCombo();
			}

			gui::label("Rotate");
			if(ImGui::Checkbox("##Rotate", &camera->auto_rotate)) {
				cameras.save(active_camera_, *camera);
			}

			auto rot_changed = false;
			gui::label("Pitch");
			rot_changed |= ImGui::SliderFloat("##Pitch", &camera->rotation[0], 0.f, 360.f, "%.1f");
			gui::label("Yaw");
			rot_changed |= ImGui::SliderFloat("##Yaw", &camera->rotation[1], 0.f, 360.f, "%.1f");
			gui::label("Roll");
			rot_changed |= ImGui::SliderFloat("##Roll", &camera->rotation[2], 0.f, 360.f, "%.1f");
			if(rot_changed) {
				camera->auto_rotate = false;
				cameras.save(active_camera_, *camera);
			}

			auto zoom = 1.f - camera->zoom;
			gui::label("Zoom");
			const auto zoom_changed = ImGui::SliderFloat("##Zoom", &zoom, 0.f, 1.f, "%.2f");
			camera->zoom            = std::clamp(1.f - zoom, 0.01f, 1.f);
			if(zoom_changed)
				cameras.save(active_camera_, *camera);

			gui::label("Relief");
			if(ImGui::SliderFloat("##Relief", &camera->elevation_relief_scale, 0.f, 200.f, "%.0f")) {
				cameras.save(active_camera_, *camera);
			}
			ImGui::TreePop();
		}

		for(auto& layer : layers_) {
			if(layer->has_settings() && layer->draw()
			   && ImGui::TreeNodeEx(layer->name().c_str(),
			                        ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_DefaultOpen)) {
				layer->draw_settings(*cfg_);
				ImGui::TreePop();
			}
		}
	}

	void Viewport::save_config()
	{
		int order = 1;
		for(auto& l : layers_) {
			cfg_->set(l->name().str() + std::string("_order"), std::to_string(order++));
		}
	}

	void Viewport::load_config()
	{
		std::unordered_map<Layer_renderer*, int> order;

		const auto default_order = static_cast<int>(layers_.size()) + 10;
		for(auto& layer : layers_) {
			layer->draw(cfg_->get(layer->name().str() + "_draw", layer->draw()));
			layer->alpha(cfg_->get(layer->name().str() + "_alpha", layer->alpha()));
			order[layer.get()] = cfg_->get(layer->name().str() + "_order", default_order);
		}

		std::stable_sort(layers_.begin(), layers_.end(), [&](auto& lhs, auto& rhs) {
			return order[lhs.get()] < order[rhs.get()];
		});


		properties_width_ = cfg_->get("properties_width", properties_width_);
		active_camera_    = cfg_->get("active_camera", active_camera_);

		save_config();
	}

	auto Viewport::camera() const -> const renderer::Camera&
	{
		return renderer_.cameras()[active_camera_];
	}
	auto Viewport::layers() const -> std::span<const std::unique_ptr<Layer_renderer>>
	{
		return layers_;
	}

} // namespace yggdrasill::windows

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
