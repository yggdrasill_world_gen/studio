#pragma once

#include "../window.hpp"

#include <memory>

namespace yggdrasill {
	class World;
} // namespace yggdrasill

namespace yggdrasill::simulation {
	class Graph;
	class Module_command_manager;
	class Cursor;
} // namespace yggdrasill::simulation

namespace yggdrasill::windows {

	extern std::unique_ptr<Window> timeline(Dockspace*,
	                                        gui::Config&,
	                                        bool visible,
	                                        simulation::Graph&,
	                                        simulation::Module_command_manager&,
	                                        simulation::Cursor& current_world,
	                                        bool&               dark_mode);

}
