#pragma once

#include "../window.hpp"

#include <memory>

using ImGuiButtonFlags = int;

namespace yggdrasill {
	class Selection;
	class Window;
} // namespace yggdrasill
namespace yggdrasill::simulation {
	class Cursor;
}
namespace yggdrasill::gui {
	class Config;
}

namespace yggdrasill::windows {

	extern std::unique_ptr<Window> tools(
	        Dockspace*, gui::Config&, bool visible, simulation::Cursor&, Selection&, ImGuiButtonFlags& camera_buttons);

} // namespace yggdrasill::windows
