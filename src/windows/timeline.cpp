#include "timeline.hpp"

#include "../gui/popup.hpp"
#include "../gui/ui_elements.hpp"
#include "../simulation/cursor.hpp"
#include "../simulation/graph.hpp"
#include "../simulation/module_commands.hpp"
#include "../util/func_traits.hpp"
#include "../util/ranges.hpp"

#include <imgui.h>
#include <range/v3/all.hpp>
#include <yggdrasill_icons.hpp>

#include <array>
#include <stdexcept>
#include <vector>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

namespace yggdrasill {
	extern float   current_time();
	extern ImFont* font_largest;
} // namespace yggdrasill

namespace yggdrasill::windows {
	namespace {
		constexpr auto node_border           = 1.5f;
		const auto     text_offset           = ImVec2{4, 4};
		const auto     text_offset_executing = ImVec2{20, 4};
		constexpr auto inactive_node_alpha   = 0.2f;
		constexpr auto label_padding         = 10.f;
		constexpr auto steps_per_second      = 10.f;
		constexpr auto node_animation_time   = 1.0f; // seconds until a node finds its final position
		constexpr auto scale_divisor_min     = 1.f;
		constexpr auto scale_divisor_max     = 20.f;
		constexpr auto scale_divisor_default = 4.f;

		auto should_join_with_next(const simulation::Node& node) -> const simulation::Node*
		{
			auto successors = node.successors();
			if(successors.size() != 1)
				return nullptr;

			auto&      next = successors.front();
			const auto animation_in_progress =
			        std::max(node.dimensions_change_time(), next.dimensions_change_time()) + node_animation_time
			        > current_time();

			if(node.description() != next.description())
				return nullptr;

			if(node.repeatable() != next.repeatable())
				return nullptr;

			if(node.repeatable() && !node.joinable(next))
				return nullptr;

			if(node.dimensions().x != next.dimensions().x)
				return nullptr;

			if(animation_in_progress) {
				if(node.previous_dimensions().width != next.previous_dimensions().width)
					return nullptr;

				if(node.previous_dimensions().x != next.previous_dimensions().x)
					return nullptr;
			}

			return &next;
		}

		/// 1-N identical simulation::Node objects that can be drawn/processed together
		struct Node_block {
			int                     length      = 0;
			bool                    executing   = false;
			bool                    outdated    = false;
			const simulation::Node* first_node  = nullptr;
			const simulation::Node* last_node   = nullptr;
			const Error*            first_error = nullptr;

			/// create a block by joining the given node with all its identical unique successors
			explicit Node_block(const simulation::Node& node) : first_node(&node)
			{
				for(auto* n = &node; n; n = should_join_with_next(*n)) {
					length++;
					executing = executing || n->execution_state() == simulation::Node::Execution_state::executing;
					outdated = outdated || n->outdated();
					if(!first_error) {
						if(auto& e = n->error())
							first_error = &*e;
					}

					last_node = n;
				}
			}

			/// \returns range of (is_next_node: bool, const simulation::Node&)
			[[nodiscard]] auto successors(const simulation::Cursor& marker, int marker_path_index) const
			{
				using namespace ranges::views;
				const auto next_successor = marker_path_index < 0
				                                    ? -1
				                                    : (std::size_t(marker_path_index) >= marker.path().size()
				                                               ? int(last_node->successors().size()) - 1
				                                               : marker.path()[marker_path_index]);
				return zip(ints(0, ranges::unreachable)
				                   | transform([=](auto index) { return index == next_successor; }),
				           last_node->successors());
			}

			template <typename F>
			void foreach_node(F&& f) const
			{
				auto* current_node = first_node;
				for(auto i = 0; i < length; ++i) {
					f(i, *current_node);
					if(i < length - 1)
						current_node = &current_node->successors()[0];
				}
			}
		};


		class Timeline : public Window {
			static constexpr auto id_ = "Timeline";

		  public:
			Timeline(Dockspace*                          dock,
			         gui::Config&                        cfg,
			         bool                                visible,
			         simulation::Graph&                  simulation,
			         simulation::Module_command_manager& command_manager,
			         simulation::Cursor&                 current_world,
			         bool&                               dark_mode)
			  : Window(dock, cfg[id_], visible)
			  , simulation_(simulation)
			  , command_manager_(command_manager)
			  , current_world_(current_world)
			  , marker_(current_world_)
			  , dark_mode_(dark_mode)
			  , show_advanced_options_(window_cfg().get("show_advanced_options", false))
			  , scale_divisor_(window_cfg().get("scale_divisor", scale_divisor_default))
			{
			}

			[[nodiscard]] const char* id() const override { return id_; }

		  private:
			simulation::Graph&                  simulation_;
			simulation::Module_command_manager& command_manager_;
			simulation::Cursor&                 current_world_;
			simulation::Cursor                  marker_;
			bool&                               dark_mode_;
			bool                                playing_                = true;
			bool                                stop_playing_at_marker_ = false;
			float                               time_since_last_step_   = 0.f;
			bool                                drag_seeking_           = false;
			const simulation::Node*             drag_seek_node_         = nullptr;
			bool                                show_advanced_options_  = false;

			bool                  scroll_to_marker_         = true;
			bool                  scroll_follows_play_head_ = true;
			std::optional<ImVec2> scroll_target_;

			std::optional<ImVec2> smoothed_marker_position_;

			const simulation::Node* resize_node_hovered_ = nullptr;
			const simulation::Node* resize_node_         = nullptr;

			const simulation::Node*               last_known_root_          = nullptr;
			std::weak_ptr<const simulation::Node> last_known_current_world_ = {}; // TODO: rename
			std::weak_ptr<const simulation::Node> last_marker_              = {};

			std::vector<const simulation::Node*> tmp_node_stack_;
			std::vector<int>                     tmp_int_stack_;

			float scale_divisor_;

			gui::Same_line_right right_button_space_;

			float max_timeline_width  = 4;
			float max_timeline_height = 4;


			[[nodiscard]] auto node_size() const { return ImVec2{128 / scale_divisor_, 24}; }
			[[nodiscard]] auto node_padding() const { return ImVec2{32 / scale_divisor_, 8}; }
			[[nodiscard]] auto node_full_size() const { return node_size() + node_padding(); }

			[[nodiscard]] static float animated(const simulation::Node& node, auto&& member)
			{
				const auto alpha =
				        std::min(1.f, (current_time() - node.dimensions_change_time()) / node_animation_time);
				const auto a = static_cast<float>(node.previous_dimensions().*member);
				const auto b = static_cast<float>(node.dimensions().*member);
				return std::lerp(a, b, gui::bounce_out(alpha));
			}
			[[nodiscard]] static float width(const simulation::Node& node)
			{
				return animated(node, &simulation::Node_dimensions::width);
			}
			[[nodiscard]] static float height(const simulation::Node& node)
			{
				return animated(node, &simulation::Node_dimensions::height);
			}
			[[nodiscard]] static float x(const simulation::Node& node)
			{
				return animated(node, &simulation::Node_dimensions::x);
			}
			[[nodiscard]] static float y(const simulation::Node& node)
			{
				return animated(node, &simulation::Node_dimensions::y);
			}

			/// \returns {offset, size} in local-space
			[[nodiscard]] auto node_area(const simulation::Node& node) const
			{
				const auto size      = node_full_size();
				const auto h         = width(simulation_.root());
				const auto area_size = ImVec2{height(node) * size.x, width(node) * size.y};

				return std::pair{
				        ImVec2{y(node) * size.x, (h - x(node) - 0.5f) * size.y - area_size.y},
                        area_size
                };
			}
			[[nodiscard]] auto node_position(const simulation::Node& node) const
			{
				const auto [offset, size] = node_area(node);
				return offset + (size - node_full_size()) * ImVec2{0.f, 0.5f};
			}

			/// returns true if the the interpolation is still ongoing
			bool interpolate_position(const simulation::Node& node, std::optional<ImVec2>& inout)
			{
				auto p = node_position(node) + ImVec2(node_size().x, 0);
				if(inout) {
					inout = *inout + (p - *inout) * std::min(1.f, 10.f * ImGui::GetIO().DeltaTime);
					return glm::length(glm::vec2(p - *inout)) > 0.1f;
				} else {
					inout = p;
					return false;
				}
			}

			[[nodiscard]] ImVec2 interpolated_play_head_pos() const
			{
				// marker position is interpolate between the current and previous world-state node,
				//   based on a remaining time of the play animation
				const auto node = current_world_.node();
				if(!node)
					return {0, 0};

				const auto p2 = node_position(*node);
				const auto p1 = node->predecessor() ? node_position(*node->predecessor()) : p2;
				const auto t  = std::min(1.f, time_since_last_step_ / (1.f / steps_per_second));
				return p1 + (p2 - p1) * t + ImVec2(node_size().x, 0);
			}

			// clang-format off
			[[nodiscard]] ImU32 border_color()const                {return dark_mode_ ? "#888"_color : "#222"_color;}
			[[nodiscard]] ImU32 border_drag_color()const           {return dark_mode_ ? "#aaa"_color : "#444"_color;}
			[[nodiscard]] ImU32 border_drag_hover_color()const     {return dark_mode_ ? "#eee"_color : "#bbb"_color;}
			[[nodiscard]] ImU32 background_color()const            {return dark_mode_ ? "#222"_color : "#fff"_color;}
			[[nodiscard]] ImU32 line_color_faded()const            {return dark_mode_ ? "#333"_color : "#888"_color;}
			[[nodiscard]] ImU32 line_color()const                  {return dark_mode_ ? "#aaa"_color : "#444"_color;}
			[[nodiscard]] ImU32 line_heading_color()const          {return dark_mode_ ? "#fff"_color : "#000"_color;}
			[[nodiscard]] ImU32 line_heading_color_estimate()const {return dark_mode_ ? "#aaa"_color : "#888"_color;}
			[[nodiscard]] ImU32 marker_color()const                {return dark_mode_ ? "#fff"_color : "#000"_color;}
			[[nodiscard]] ImU32 marker_area_color()const           {return dark_mode_ ? "#fff4"_color : "#6664"_color;}
			[[nodiscard]] ImU32 play_head_color()const             {return dark_mode_ ? "#06f"_color : "#02f"_color;}
			[[nodiscard]] ImU32 play_area_color()const             {return dark_mode_ ? "#06f2"_color : "#02f2"_color;}
			// clang-format on

			[[nodiscard]] ImU32 color(const simulation::Node& node) const
			{
				using enum simulation::Node::Execution_state;
				using enum simulation::Command_type;

				auto base_color = [&]() -> ImU32 {
					switch(node.execution_state()) {
						case created: return dark_mode_ ? "#777046"_color : "#f5f5dd"_color;
						case executing: return dark_mode_ ? "#9e8733"_color : "#f1e388"_color;
						case executed:
							switch(node.type()) {
								case root: return dark_mode_ ? "#6B6B6B"_color : "#F2F2F2"_color;
								case new_world: return dark_mode_ ? "#71A167"_color : "#DBF4D6"_color;
								case simulation: return dark_mode_ ? "#3f6737"_color : "#a3ee96"_color;
								case user_world_edit: return dark_mode_ ? "#319596"_color : "#B7F5F5"_color;
								case user_config_edit: return dark_mode_ ? "#317496"_color : "#b7e1f5"_color;
								case unknown: return dark_mode_ ? "#503767"_color : "#BD96EE"_color;
							}
							std::cerr << "unhandled Command_type: " << int(node.type()) << '\n';
							std::abort();

						case error: return dark_mode_ ? "#a62121"_color : "#ff6666"_color;
					}
					std::cerr << "unhandled simulation::Node::Execution_state: " << int(node.execution_state())
					          << '\n';
					std::abort();
					return 0;
				}();

				auto c = ImGui::ColorConvertU32ToFloat4(base_color);
				ImGui::ColorConvertRGBtoHSV(c.x, c.y, c.z, c.x, c.y, c.z);
				c.y *= node == current_world_ ? 1.f : 0.6f;
				ImGui::ColorConvertHSVtoRGB(c.x, c.y, c.z, c.x, c.y, c.z);
				base_color = ImGui::ColorConvertFloat4ToU32(c);

				return gui::apply_style_alpha(base_color);
			}

			void handle_node_resize();
			void draw_seek_controls();

			void draw_timeline_background(ImVec2 top_left);
			void draw_nodes(ImVec2 top_left);
			void draw_node(ImVec2 top_left, const Node_block&, int marker_path_index);
			void update_node_input(ImVec2 top_left, const Node_block&);
			void draw_cursors(ImVec2 top_left);

			void do_always_update() override
			{
				ON_EXIT(last_known_current_world_ = current_world_.node());
				ON_EXIT(last_known_root_ = &simulation_.root());

				if(last_known_current_world_.expired() || last_known_root_ != &simulation_.root()
				   || marker_.invalidated()) {
					// first frame (after creating a new world) => set marker to end
					marker_.seek(simulation_.main_branch());

				} else if(last_known_current_world_ != current_world_) {
					// someone else executed new commands => seek to end
					marker_ = current_world_;
				}

				update_play_state();
				current_world_.editable(current_world_ == marker_);

				if(playing_) {
					ImGui::SetMaxWaitBeforeNextFrame(1.f / steps_per_second);
				}
			}
			void update_play_state()
			{
				// marker has been moved => set current_world_ to the closest ready node towards it
				if(last_marker_ != marker_ || !current_world_.is_before(marker_)) {
					auto marker_node = marker_.node();
					last_marker_     = marker_node;

					if(marker_node->ready()) {
						current_world_ = marker_;
						playing_       = false;
					} else {
						playing_                = true;
						stop_playing_at_marker_ = true;
						current_world_          = marker_;
						auto current_world_node = current_world_.node();
						for(auto n = current_world_node.get(); n && !n->ready();) {
							n = current_world_.seek_backward();
						}
					}
				}

				if(playing_) {
					time_since_last_step_ += ImGui::GetIO().DeltaTime;
					if(time_since_last_step_ > 1.f / steps_per_second) {
						// if we already reached the marked position, continue onto the last-added successor
						if(marker_.path().size() <= current_world_.path().size()) {
							if(!stop_playing_at_marker_ && marker_.seek_forward()) {
								while(marker_.seek_forward())
									;
								last_marker_ = marker_.node();
							} else {
								playing_ = false; // reached the end
							}
						}

						// if we haven't reached the marked position and the next node is ready, move to it
						if(marker_.path().size() > current_world_.path().size()) {
							auto next = marker_.path()[current_world_.path().size()];
							if(current_world_.node()->successors()[next].ready()) {
								current_world_.seek_forward(next);
								time_since_last_step_ = 0.f;
							}
						}
					}
				}
			}

			bool do_update() override
			{

				auto open = true;
				if(ImGui::Begin(id(), &open)) {
					constexpr auto mod_panel_width = 230.f;

					draw_seek_controls();

					// timeline itself
					ImGui::PushStyleColor(ImGuiCol_ChildBg, background_color());
					ON_EXIT(ImGui::PopStyleColor());
					max_timeline_width  = std::max(max_timeline_width, 2 + width(simulation_.root()));
					max_timeline_height = std::max(max_timeline_height, 1 + height(simulation_.root()));
					ImGui::SetNextWindowContentSize(ImVec2{max_timeline_height, max_timeline_width}
					                                * (node_size() + node_padding()));
					if(ImGui::BeginChild(
					           "tree", ImVec2(-mod_panel_width, 0), true, ImGuiWindowFlags_HorizontalScrollbar)) {
						auto& io = ImGui::GetIO();
						if(ImGui::IsWindowHovered() && std::abs(io.MouseWheel) > 1e-8f && io.KeyCtrl) {
							scale_divisor_ = std::clamp(
							        scale_divisor_ - io.MouseWheel / 4.f, scale_divisor_min, scale_divisor_max);
							window_cfg().set("scale_divisor", std::to_string(scale_divisor_));
							smoothed_marker_position_.reset();
						}

						if(auto n = marker_.node(); n && interpolate_position(*n, smoothed_marker_position_)) {
							ImGui::SetMaxWaitBeforeNextFrame(1.f / 30.f);
						}

						if(resize_node_) {
							handle_node_resize();

						} else if(ImGui::IsWindowHovered(ImGuiHoveredFlags_ChildWindows
						                                 | ImGuiHoveredFlags_AllowWhenBlockedByPopup)
						          && ImGui::IsMouseClicked(ImGuiMouseButton_Left) && !ImGui::IsAnyItemActive()) {
							if(resize_node_hovered_) {
								resize_node_ = resize_node_hovered_;
							} else {
								drag_seeking_ = true;
							}
						} else if(drag_seeking_ && ImGui::IsMouseReleased(ImGuiMouseButton_Left)) {
							drag_seeking_ = false;
						}

						const auto was_resize_hovered = resize_node_hovered_ != nullptr;
						resize_node_hovered_          = nullptr;

						const auto top_left      = ImGui::GetCursorScreenPos() + ImVec2(0, label_padding);
						const auto node_top_left = top_left + ImVec2(0, node_full_size().y);
						draw_timeline_background(top_left);
						draw_nodes(node_top_left);

						if((resize_node_hovered_ || resize_node_) && simulation_.editable()) {
							ImGui::SetMouseCursor(ImGuiMouseCursor_::ImGuiMouseCursor_ResizeEW);
						} else if(was_resize_hovered) {
							ImGui::SetMouseCursor(ImGuiMouseCursor_::ImGuiMouseCursor_Arrow);
						}

						if(drag_seeking_ && drag_seek_node_) {
							marker_.seek(*drag_seek_node_);
							drag_seek_node_ = nullptr;
						}

						draw_cursors(node_top_left);
						update_timeline_scroll_position(node_top_left);
					}
					ImGui::EndChild();

					ImGui::SameLine();
					if(ImGui::BeginChild("modification_controls", ImVec2(0, 0), true)) {
						gui::enabled_if(simulation_.editable(), [&] {
							auto       marker_node = marker_.node();
							const auto at_start    = marker_node.get() == &simulation_.root();

							if(gui::button(YGGDRASILL_ICON_CLONE, !at_start, "Duplicate current branch")) {
								marker_.seek(simulation_.duplicate_branch(*marker_node));
							}

							right_button_space_.clear();
							if(right_button_space_.add_button(YGGDRASILL_ICON_MINUS_SQUARE,
							                                  !at_start,
							                                  "Deletes the current node [DEL]")) {
								delete_current_node();
							}
							if(right_button_space_.add_button(YGGDRASILL_ICON_DELETE_BRANCH,
							                                  !at_start,
							                                  "Deletes the current branch (this node and all "
							                                  "nodes on its right) [DEL+CTRL]")) {
								delete_current_branch();
							}

							ImGui::Spacing();
							ImGui::Separator();
							ImGui::Spacing();

							ImGui::AlignTextToFramePadding();
							ImGui::TextUnformatted("History Branching: ");
							gui::ui_help_tooltip(
							        "Determines how modifications in the middle of the timeline are "
							        "integrated "
							        "into the "
							        "history");
							ImGui::PushFont(font_largest);
							gui::radio_buttons([&](auto&& button) {
								const auto mode = simulation_.branch_mode();
								using enum simulation::Branching_mode;

								if(button(mode == linear,
								          YGGDRASILL_ICON_BRANCHING_LINEAR,
								          "Linear: Modifications don't create branches but instead modify "
								          "the "
								          "history and "
								          "invalidate/recompute all future nodes on this branch"))
									simulation_.branch_mode(linear);

								if(button(mode == branch_empty_once || mode == branch_empty,
								          mode == branch_empty_once ? YGGDRASILL_ICON_BRANCHING_EMPTY_ONCE
								                                    : YGGDRASILL_ICON_BRANCHING_EMPTY,
								          "Empty Branch: Modifications create new empty branches, starting "
								          "from "
								          "the "
								          "current node"))
									simulation_.branch_mode(mode == branch_empty_once ? branch_empty
									                                                  : branch_empty_once);

								if(button(mode == branch_copy_once || mode == branch_copy,
								          mode == branch_copy_once ? YGGDRASILL_ICON_BRANCHING_COPY_ONCE
								                                   : YGGDRASILL_ICON_BRANCHING_COPY,
								          "Duplicate Branch: Modifications duplicate the current branch and "
								          "insert the "
								          "change at the start of the new duplicated branch"))
									simulation_.branch_mode(mode == branch_copy_once ? branch_copy
									                                                 : branch_copy_once);
							});
							ImGui::PopFont();

							ImGui::Spacing();
							ImGui::Separator();
							ImGui::Spacing();

							ImGui::AlignTextToFramePadding();
							ImGui::TextUnformatted("Create Node: ");
							gui::ui_help_tooltip("Click to create one node, hold to create multiple");

							ImGui::PushStyleVar(ImGuiStyleVar_ButtonTextAlign, ImVec2(0, 0.5f));
							ON_EXIT(ImGui::PopStyleVar());

							gui::enabled_if(marker_node->type() != simulation::Command_type::root, [&] {
								auto cmd_button = [&](auto& cmd) {
									auto label = std::string(cmd.name()); // TODO: remove once ImGui supports string_view
									if(ImGui::ButtonEx(label.c_str(),
									                   ImVec2{ImGui::GetContentRegionAvail().x, 0},
									                   ImGuiButtonFlags_Repeat)) {
										command_manager_.execute(cmd, marker_);
										marker_.seek(simulation_.main_branch());
									}
								};

								for(auto&& cmd : util::skip(1, command_manager_.list())) {
									if(cmd.meta_command())
										cmd_button(cmd);
								}

								ImGui::Spacing();
								const auto advanced_open = ImGui::CollapsingHeader("Advanced Options");
								gui::ui_help_tooltip(
								        "List all loaded modules, instead of just those that are designed to "
								        "be "
								        "user-facing");

								if(advanced_open) {
									for(auto&& cmd : command_manager_.list()) {
										if(!cmd.meta_command())
											cmd_button(cmd);
									}
								}
							});
						});
					}
					ImGui::EndChild();
				}
				ImGui::End();

				return open;
			}

			void delete_current_node()
			{
				if(marker_.path().empty())
					return;

				gui::open_modal_popup("Delete Node?", [&] {
					ImGui::TextUnformatted("Are you sure you want to delete this node?");
					ImGui::TextUnformatted("This operation can't be undone!");

					return gui::dialog_buttons(YGGDRASILL_ICON_CHECK " Delete Node", [&] {
						marker_.undo(true);
						if(marker_.is_before(current_world_)) {
							if(marker_.ready()) {
								current_world_ = marker_;
							} else {
								current_world_.seek(simulation_.root());
							}
						}
						scroll_to_marker_ = true;
					});
				});
			}
			void delete_current_branch()
			{
				if(marker_.path().empty())
					return;

				gui::open_modal_popup("Delete Branch?", [&] {
					ImGui::TextUnformatted(
					        "Are you sure you want to delete this node and ALL subsequent nodes on this "
					        "branch?");
					ImGui::TextUnformatted("This operation can't be undone!");

					return gui::dialog_buttons(YGGDRASILL_ICON_CHECK " Delete Entire Branch", [&] {
						marker_.undo(false);
						if(marker_.is_before(current_world_)) {
							if(marker_.ready()) {
								current_world_ = marker_;
							} else {
								current_world_.seek(simulation_.root());
							}
						}
						scroll_to_marker_ = true;
					});
				});
			}

			void update_timeline_scroll_position(ImVec2 top_left)
			{
				constexpr auto scroll_epsilon = 400.f;
				constexpr auto scroll_speed   = 4000.f;

				constexpr auto auto_scroll_distance  = 100.f;
				constexpr auto auto_scroll_max_speed = 1000.f;

				const auto dt = ImGui::GetIO().DeltaTime;

				if(drag_seeking_) {
					// don't take away the users control
					scroll_target_.reset();
					scroll_follows_play_head_ = false;

					// scroll automatically if something is dragged to the window border
					const auto local_mouse = ImGui::GetMousePos() - ImGui::GetWindowPos();

					if(local_mouse.x < auto_scroll_distance) {
						ImGui::SetScrollX(ImGui::GetScrollX()
						                  - dt * auto_scroll_max_speed
						                            * (1.f - local_mouse.x / auto_scroll_distance));
					} else if(local_mouse.x > ImGui::GetWindowWidth() - auto_scroll_distance) {
						ImGui::SetScrollX(ImGui::GetScrollX()
						                  + dt * auto_scroll_max_speed
						                            * (1.f
						                               - (ImGui::GetWindowWidth() - local_mouse.x)
						                                         / auto_scroll_distance));
					}

					if(local_mouse.y < auto_scroll_distance) {
						ImGui::SetScrollY(ImGui::GetScrollY()
						                  - dt * auto_scroll_max_speed
						                            * (1.f - local_mouse.y / auto_scroll_distance));
					} else if(local_mouse.y > ImGui::GetWindowHeight() - auto_scroll_distance) {
						ImGui::SetScrollY(ImGui::GetScrollY()
						                  + dt * auto_scroll_max_speed
						                            * (1.f
						                               - (ImGui::GetWindowHeight() - local_mouse.y)
						                                         / auto_scroll_distance));
					}

					return;
				}

				auto window                   = ImGui::GetCurrentWindow();
				auto scroll_target_node_space = std::optional<ImVec2>{};
				auto force_scrolling          = false;
				if(playing_ && scroll_follows_play_head_) {
					// disable automatic scrolling, when the user interacts with the scrollbars
					ImGuiID active_id = ImGui::GetActiveID();
					if(active_id
					   && (active_id == ImGui::GetWindowScrollbarID(window, ImGuiAxis_X)
					       || active_id == ImGui::GetWindowScrollbarID(window, ImGuiAxis_Y))) {
						scroll_follows_play_head_ = false;
						scroll_target_.reset();
						return;
					}

					scroll_target_node_space = interpolated_play_head_pos();
					force_scrolling          = true;

				} else if(scroll_to_marker_) {
					scroll_to_marker_        = false;
					scroll_target_node_space = node_position(*marker_.node());
				}

				// the remainder of the function handles smooth scrolling to the target position
				if(scroll_target_node_space) {
					const auto scroll_target_local = *scroll_target_node_space + top_left - window->Pos;
					scroll_target_                 = window->Scroll + scroll_target_local
					                 - (window->SizeFull + window->ScrollbarSizes) / 2.f;
					scroll_target_->x = std::clamp(scroll_target_->x, 0.f, window->ScrollMax.x);
					scroll_target_->y = std::clamp(scroll_target_->y, 0.f, window->ScrollMax.y);

					const auto scroll_required_x = std::abs(scroll_target_->x - window->Scroll.x) > scroll_epsilon;
					const auto scroll_required_y = std::abs(scroll_target_->y - window->Scroll.y) > scroll_epsilon;

					if(!scroll_required_x) {
						if(force_scrolling)
							ImGui::SetScrollX(scroll_target_->x);
						else
							scroll_target_->x = window->Scroll.x;
					}
					if(!scroll_required_y) {
						if(force_scrolling)
							ImGui::SetScrollY(scroll_target_->y);
						else
							scroll_target_->y = window->Scroll.y;
					}

					if(!scroll_required_x && !scroll_required_y) {
						scroll_target_.reset();
					}
				}

				if(scroll_target_) {
					auto move_towards = [&](float current, float target) {
						if(target < current)
							return std::max(target, current - (dt * scroll_speed));
						else
							return std::min(target, current + (dt * scroll_speed));
					};

					auto x = move_towards(ImGui::GetScrollX(), scroll_target_->x);
					auto y = move_towards(ImGui::GetScrollY(), scroll_target_->y);
					ImGui::SetScrollX(x);
					ImGui::SetScrollY(y);

					if(std::abs(x - scroll_target_->x) < 0.1f && std::abs(y - scroll_target_->y) < 0.1f)
						scroll_target_.reset();
				}
			}
		};

		void Timeline::handle_node_resize()
		{
			if(!simulation_.editable())
				return;

			const auto width       = node_size().x;
			const auto local_mouse = ImGui::GetMousePos() - ImGui::GetCursorScreenPos();

			const auto diff = local_mouse.x - node_position(*resize_node_).x - width;

			if(diff < -width * 0.9f && resize_node_->predecessor()
			   && should_join_with_next(*resize_node_->predecessor())) {
				// resized smaller and there would still be a node left => delete resize_node_
				auto* node   = resize_node_;
				resize_node_ = resize_node_->predecessor();
				marker_.seek(*resize_node_);
				simulation_.undo(*node, true);

			} else if(diff > width * 0.9f) {
				// resized larger => insert new node
				resize_node_ = &simulation_.duplicate(*resize_node_);
				marker_.seek(*resize_node_);
			}

			if(!ImGui::IsMouseDown(ImGuiMouseButton_Left)) {
				ImGui::SetMouseCursor(ImGuiMouseCursor_::ImGuiMouseCursor_Arrow);
				resize_node_ = nullptr;
			}
		}

		void Timeline::draw_seek_controls()
		{
			if(ImGui::IsWindowFocused(ImGuiFocusedFlags_ChildWindows)) {
				if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_LeftArrow))) {
					marker_.seek_backward();
					scroll_to_marker_ = true;
				} else if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_RightArrow))) {
					marker_.seek_forward();
					scroll_to_marker_ = true;
				} else if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_UpArrow))) {
					marker_.seek_later_sibling();
					scroll_to_marker_ = true;
				} else if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_DownArrow))) {
					marker_.seek_earlier_sibling();
					scroll_to_marker_ = true;

				} else if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Space))) {
					playing_                  = !playing_;
					scroll_follows_play_head_ = true;
				} else if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Home))) {
					marker_.seek(simulation_.root());
					scroll_to_marker_ = true;
				} else if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_End))) {
					marker_.seek(simulation_.main_branch());
					scroll_to_marker_ = true;

				} else if(simulation_.editable() && ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Delete))) {
					if(ImGui::GetIO().KeyCtrl) {
						delete_current_branch();
					} else {
						delete_current_node();
					}
				}
			}

			const auto at_start = marker_ == simulation_.root();
			const auto at_end   = marker_.successor_count() == 0;

			if(gui::button(YGGDRASILL_ICON_FAST_BACKWARD, !at_start, "Goto beginning")) {
				marker_.seek(simulation_.root());
				scroll_to_marker_ = true;
			}
			ImGui::SameLine();
			if(gui::button(YGGDRASILL_ICON_STEP_BACKWARD,
			               !playing_ && !at_start,
			               "Single Step backwards [LEFT]")) {
				marker_.seek_backward();
				scroll_to_marker_ = true;
			}

			ImGui::SameLine();
			if(gui::button(YGGDRASILL_ICON_PLAY_FROM_START,
			               !playing_ && marker_ == current_world_,
			               "Play from beginning to current position")) {
				current_world_.seek(simulation_.root());
				last_known_current_world_ = current_world_.node();
				playing_                  = true;
				scroll_follows_play_head_ = true;
				stop_playing_at_marker_   = true;
			}
			ImGui::SameLine();
			if(playing_) {
				if(gui::button(YGGDRASILL_ICON_PAUSE, true, "Pause [SPACE]")) {
					playing_ = false;
				}
			} else {
				if(marker_ != current_world_) {
					if(gui::button(YGGDRASILL_ICON_PLAY, true, "Resume [SPACE]")) {
						playing_ = true;
					}
				} else if(gui::button(YGGDRASILL_ICON_PLAY, !at_end, "Play from current position [SPACE]")) {
					playing_                  = true;
					scroll_follows_play_head_ = true;
					// if we are already at the current position => continue playing until we reach the end of the branch
					stop_playing_at_marker_ = marker_ != current_world_;
				}
			}

			ImGui::SameLine();
			if(gui::button(
			           YGGDRASILL_ICON_STEP_FORWARD, !playing_ && !at_end, "Single Step forwards [RIGHT]")) {
				marker_.seek_forward();
				scroll_to_marker_ = true;
			}
			ImGui::SameLine();
			if(gui::button(YGGDRASILL_ICON_FAST_FORWARD, !at_end, "Goto last node in current branch")) {
				while(marker_.seek_forward())
					;
				scroll_to_marker_ = true;
			}
			ImGui::SameLine();
			if(gui::button(YGGDRASILL_ICON_GOTO_LAST_CHANGE,
			               true,
			               "Go to location of last changed/added/removed node")) {
				marker_.seek(simulation_.main_branch());
				playing_          = false;
				scroll_to_marker_ = true;
			}

			ImGui::SameLine();
			ImGui::SetNextItemWidth(200.f);
			if(auto zoom = (1.f / scale_divisor_);
			   ImGui::SliderFloat("Zoom", &zoom, 1.f / scale_divisor_max, 1.f / scale_divisor_min)) {
				scale_divisor_ = 1.f / zoom;
				window_cfg().set("scale_divisor", std::to_string(scale_divisor_));
				smoothed_marker_position_.reset();
			}
		}

		void Timeline::draw_timeline_background(ImVec2 top_left)
		{
			// draw root.height many marker lines at the end of each node
			const auto timeline_height =
			        std::max(ImGui::GetWindowHeight(),
			                 (width(simulation_.root()) + 1) * (node_size().y + node_padding().y));
			const auto node_width   = node_size().x + node_padding().x;
			const auto marker_count = simulation_.root().dimensions().height + 1;

			auto&      draw_list = *ImGui::GetWindowDrawList();
			const auto x_min     = draw_list.GetClipRectMin().x;
			const auto x_max     = draw_list.GetClipRectMax().x;

			const auto start_i = std::max(1, int(std::floor((x_min - top_left.x) / node_width)));
			const auto end_i   = std::min(marker_count, int(std::ceil((x_max - top_left.x) / node_width)));
			for(int i = start_i; i < end_i; ++i) {
				const auto x = top_left.x + float(i) * (node_size().x + node_padding().x) - node_padding().x;
				draw_list.AddLine(
				        ImVec2{x, top_left.y}, ImVec2{x, top_left.y + timeline_height}, line_color_faded(), 1.f);
			}

			// draw labels matching the currently selected branch
			int  i                 = 1;
			auto time              = 0.f;
			auto time_estimated    = false;
			auto end_of_last_label = top_left.x;
			foreach_node(simulation::last_leaf_node{marker_}, [&](auto& node) {
				// either get the current nodes timestamp or estimate it
				if(!node.ready()) {
					if(time >= 0.f && node.estimated_delta_time()) {
						time += *(node.estimated_delta_time());
					} else {
						time = -1.f;
					}
					time_estimated = true;

				} else if(const auto metadata = node.state().world().unstructured_layer("meta")) {
					time           = (*metadata)["age"].get(0.f);
					time_estimated = false;
				}

				if(i >= start_i && i <= end_i) { // if visible
					auto label = [&]() -> std::string {
						if(time < 0.f)
							return "?";

						auto ss = std::ostringstream();
						ss << std::setprecision(2) << std::fixed;
						ss << (time / 1e6f) << " Myr";
						return std::move(ss).str();
					}();

					const auto x = top_left.x + float(i) * (node_size().x + node_padding().x) - node_padding().x;
					const auto size                 = ImGui::CalcTextSize(label.c_str());
					const auto has_sufficient_space = x - size.x / 2.f > end_of_last_label;

					if(has_sufficient_space) {
						end_of_last_label = x + size.x / 2.f;
						draw_list.AddText(GImGui->Font,
						                  GImGui->FontSize,
						                  ImVec2{x - size.x / 2.f, top_left.y - size.y},
						                  time_estimated ? line_heading_color_estimate() : line_heading_color(),
						                  label.c_str());

						draw_list.AddLine(
						        ImVec2{x, top_left.y}, ImVec2{x, top_left.y + timeline_height}, line_color(), 1.f);
					}
				}
				i++;
			});
		}
		void Timeline::draw_nodes(ImVec2 top_left)
		{
			using namespace ranges::views;

			auto&      draw_list = *ImGui::GetWindowDrawList();
			const auto clip_min  = draw_list.GetClipRectMin();
			const auto clip_max  = draw_list.GetClipRectMax();

			tmp_node_stack_.clear();
			tmp_int_stack_.clear();
			tmp_node_stack_.push_back(&simulation_.root());
			tmp_int_stack_.push_back(0);

			while(!tmp_node_stack_.empty()) {
				auto& node = *tmp_node_stack_.back();
				tmp_node_stack_.pop_back();

				auto marker_path_index = tmp_int_stack_.back();
				tmp_int_stack_.pop_back();

				auto [area_offset, area_size] = node_area(node);
				area_offset += top_left;

				// skip remaining branch if its area is outside the clipping rect
				if(area_offset.x > clip_max.x || area_offset.y > clip_max.y
				   || area_offset.y + area_size.y < clip_min.y)
					continue;

				// join multiple identical nodes into a single block
				const auto join = Node_block(node);
				if(marker_path_index >= 0)
					marker_path_index += join.length - 1;

				// draw/update node, if its visible
				const auto draw_area_end =
				        area_offset + node_full_size() * ImVec2(float(join.length), width(node));
				if(ImGui::IsRectVisible(area_offset, draw_area_end)) {
					update_node_input(top_left, join);
					draw_node(top_left, join, marker_path_index);
				}

				// continue traversal
				for(auto&& [next_successor, child_node] : join.successors(marker_, marker_path_index)) {
					tmp_node_stack_.push_back(&child_node);
					tmp_int_stack_.push_back(next_successor ? marker_path_index + 1 : -1);
				}
			}
		}
		void Timeline::draw_node(ImVec2 top_left, const Node_block& node_block, int marker_path_index)
		{
			auto&      draw_list = *ImGui::GetWindowDrawList();
			const auto p_min     = top_left + node_position(*node_block.first_node);
			const auto p_max     = top_left + node_position(*node_block.last_node) + node_size();

			// draw tooltip on mouseover
			auto draw_tooltip = [&](const simulation::Node& node, ImVec2 min, ImVec2 max) {
				if(ImGui::IsMouseHoveringRect(min, max) && ImGui::IsWindowHovered()) {
					ImGui::BeginTooltip();
					ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);

					ImGui::TextUnformatted(node.description().data(),
					                       node.description().data() + node.description().size());
					ImGui::Spacing();

					if(node.error()) {
						ImGui::PushStyleColor(ImGuiCol_Text, "#c00"_color);
						ImGui::TextUnformatted("Error:");
						auto msg = node.error()->message();
						ImGui::TextUnformatted(msg.data(), msg.data() + msg.size());
						ImGui::PopStyleColor();
					}

					ImGui::PopTextWrapPos();
					ImGui::EndTooltip();
				}
			};

			node_block.foreach_node([&](auto, auto& n) {
				const auto p = top_left + node_position(n) - ImVec2{node_padding().x / 2, 0};
				draw_tooltip(n, p, p + node_size() + ImVec2{node_padding().x, 0});
			});


			if(marker_path_index < 0)
				ImGui::PushStyleVar(ImGuiStyleVar_Alpha, inactive_node_alpha);
			ON_EXIT(if(marker_path_index < 0) ImGui::PopStyleVar());

			// draw colored rect for each node
			node_block.foreach_node([&](auto i, auto& current_node) {
				auto flags = int(ImDrawFlags_RoundCornersNone);
				if(i == 0)
					flags |= ImDrawFlags_RoundCornersLeft;
				if(i == node_block.length - 1)
					flags |= ImDrawFlags_RoundCornersRight;

				const auto p = top_left + node_position(current_node) - ImVec2(i > 0 ? node_padding().x : 0, 0);
				const auto p_end = top_left + node_position(current_node) + node_size();
				draw_list.AddRectFilled(p, p_end, color(current_node), 4.f, flags);
			});

			// draw border around complete block
			draw_list.AddRect(p_min,
			                  p_max,
			                  gui::apply_alpha(border_color(), marker_path_index >= 0 ? 1.f : inactive_node_alpha),
			                  4.f,
			                  ImDrawFlags_RoundCornersAll,
			                  node_border);

			// draw label
			{
				auto label = std::ostringstream();
				label << node_block.first_node->description();
				if(node_block.outdated)
					label << '*';
				if(node_block.length > 1)
					label << " (x" << node_block.length << ")";

				draw_list.PushClipRect(p_min, p_max, true);
				ON_EXIT(draw_list.PopClipRect());

				auto text_p = p_min + (node_block.executing ? text_offset_executing : text_offset);
				ImGui::RenderTextEllipsis(
				        &draw_list, text_p, p_max, p_max.x, p_max.x, label.str().c_str(), nullptr, nullptr);

				if(node_block.executing) {
					gui::spinner(
					        draw_list, p_min + ImVec2{10.f, node_size().y / 2.f}, 6.f, 2.f, "#0EC72D"_color);
				}
			}

			// draw connectors
			const auto connector_begin =
			        top_left + node_position(*node_block.last_node) + node_size() * ImVec2(1.f, 0.5f);
			const auto connector_curve_offset = ImVec2{node_padding().x / 3.f, 0.f};
			for(auto&& [next_successor, child_node] : node_block.successors(marker_, marker_path_index)) {
				const auto connector_end = top_left + node_position(child_node) + node_size() * ImVec2(0.f, 0.5f);

				draw_list.AddBezierCubic(
				        connector_begin,
				        connector_begin + connector_curve_offset,
				        connector_end - connector_curve_offset,
				        connector_end,
				        gui::apply_alpha(border_color(), next_successor ? 1.f : inactive_node_alpha),
				        2.f);
			}

			// draw resize indicator
			if(node_block.last_node->repeatable()) {
				const auto resize_hovered = resize_node_hovered_ == node_block.last_node;
				const auto node_height    = node_size().y;
				draw_list.AddLine(ImVec2{p_max.x, p_min.y + node_height * 0.1f},
				                  p_max + ImVec2{0.f, -node_height * 0.1f},
				                  resize_hovered ? border_drag_hover_color() : border_drag_color(),
				                  node_border * 2.f);
			}
		}
		void Timeline::update_node_input(ImVec2 top_left, const Node_block& node_block)
		{
			if(resize_node_)
				return;

			if(drag_seeking_) {
				node_block.foreach_node([&](auto, auto& node) {
					auto [area_offset, area_size] = node_area(node);
					area_offset += top_left;

					if(ImGui::IsMouseHoveringRect(area_offset, area_offset + area_size)) {
						drag_seek_node_ = &node;
						smoothed_marker_position_ =
						        ImVec2{ImGui::GetMousePos().x - top_left.x, node_position(node).y};
					}
				});

			} else if(node_block.last_node->repeatable()) {
				constexpr auto resize_width = 10;

				auto p = node_position(*node_block.last_node);
				p += top_left;
				p.x += node_size().x - resize_width / 2.f;

				if(ImGui::IsMouseHoveringRect(p, p + ImVec2(resize_width, node_size().y))) {
					resize_node_hovered_ = node_block.last_node;
				}
			}
		}

		void Timeline::draw_cursors(ImVec2 top_left)
		{
			if(!smoothed_marker_position_)
				return;

			auto&      draw_list = *ImGui::GetWindowDrawList();
			const auto height    = std::max(ImGui::GetWindowHeight(),
                                         (width(simulation_.root()) + 1) * (node_size().y + node_padding().y));

			// Marker (user controlled position)
			const auto marker_pos      = top_left + *smoothed_marker_position_;
			const auto marker_node_pos = top_left + node_position(*marker_.node());
			draw_list.AddLine(ImVec2{marker_pos.x, top_left.y},
			                  ImVec2{marker_pos.x, top_left.y + height},
			                  marker_color(),
			                  1.f);
			draw_list.AddLine(marker_pos + ImVec2{0, -node_padding().y / 2.f},
			                  marker_pos + ImVec2{0, node_size().y + node_padding().y / 2.f},
			                  marker_color(),
			                  4.f);
			draw_list.AddRectFilled(marker_node_pos - node_padding(),
			                        marker_pos + ImVec2(0, node_full_size().y),
			                        marker_area_color(),
			                        16.f,
			                        ImDrawFlags_RoundCornersLeft);

			// Current-World (actual displayed position)
			if(marker_ != current_world_ && current_world_.is_before(marker_)) {
				const auto current_world_pos = top_left + interpolated_play_head_pos();

				draw_list.AddLine(ImVec2{current_world_pos.x, top_left.y},
				                  ImVec2{current_world_pos.x, top_left.y + height},
				                  play_head_color(),
				                  1.f);
				draw_list.AddLine(current_world_pos + ImVec2{0, -node_padding().y / 2.f},
				                  current_world_pos + ImVec2{0, node_size().y + node_padding().y / 2.f},
				                  play_head_color(),
				                  4.f);

				// draw outline around play area
				const auto padding = node_padding().y;
				auto       prev_tr = std::optional<ImVec2>{};
				auto       prev_br = std::optional<ImVec2>{};
				foreach_node(current_world_, marker_, [&](auto& node) {
					auto [tl, br] = [&] {
						const auto p = top_left + node_position(node);

						if(node == current_world_)
							return std::tuple{current_world_pos, p + node_size()};
						else if(node == marker_)
							return std::tuple{p, marker_pos + ImVec2(0, node_size().y)};
						else
							return std::tuple{p, p + node_size()};
					}();
					tl.y -= padding;
					br.y += padding;

					draw_list.AddRectFilled(tl, br, play_area_color());

					// note: the 1 pixel offset is required here, to compensate for AA artefacts, that would otherwise cause noticeable brighter lines where two quads overlap
					if(prev_tr) {
						draw_list.AddQuadFilled(*prev_tr,
						                        *prev_br,
						                        ImVec2(tl.x - 1, br.y),
						                        ImVec2(tl.x - 1, tl.y),
						                        play_area_color());
					}
					prev_tr = ImVec2(br.x + 1, tl.y);
					prev_br = ImVec2(br.x + 1, br.y);
				});
			}
		}

	} // namespace

	std::unique_ptr<Window> timeline(Dockspace*                          dock,
	                                 gui::Config&                        cfg,
	                                 bool                                visible,
	                                 simulation::Graph&                  simulation,
	                                 simulation::Module_command_manager& command_manager,
	                                 simulation::Cursor&                 current_world,
	                                 bool&                               dark_mode)
	{
		return std::make_unique<Timeline>(dock, cfg, visible, simulation, command_manager, current_world, dark_mode);
	}

} // namespace yggdrasill::windows

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
