#pragma once

#include "../window.hpp"

#include <memory>

namespace yggdrasill {
	class Settings;
	class Dictionary;
} // namespace yggdrasill

namespace yggdrasill::simulation {
	class Cursor;
	class Module_command_manager;
} // namespace yggdrasill::simulation

namespace yggdrasill::windows {

	extern void world_gen_config(Dictionary&);

	extern std::unique_ptr<Window> create_world_config(
	        Dockspace*, gui::Config&, bool visible, simulation::Cursor&, simulation::Module_command_manager&);

	extern std::unique_ptr<Window> config(Dockspace*,
	                                      gui::Config&,
	                                      bool visible,
	                                      simulation::Cursor&,
	                                      const simulation::Module_command_manager&,
	                                      Settings&);

} // namespace yggdrasill::windows
