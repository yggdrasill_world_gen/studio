#pragma once

#include "cursor.hpp"
#include "graph.hpp"

#include <yggdrasill/generator.hpp>

#include <filesystem>
#include <shared_mutex>
#include <string_view>
#include <vector>

namespace yggdrasill::simulation {
	class Module_command_manager;

	auto default_config() -> const Dictionary&;

	class Module_command_handle {
	  public:
		Module_command_handle() : index_(std::size_t(-1)), name_("UNSET"), meta_command_(false) {}

		[[nodiscard]] auto name() const { return name_; }
		[[nodiscard]] auto meta_command() const { return meta_command_; }

		friend bool operator==(const Module_command_handle& lhs, const Module_command_handle& rhs)
		{
			return lhs.index_ == rhs.index_;
		}
		friend std::strong_ordering operator<=>(const Module_command_handle& lhs, const Module_command_handle& rhs)
		{
			return lhs.index_ <=> rhs.index_;
		}

		explicit operator bool() const { return index_ != std::size_t(-1); }

	  private:
		friend class Module_command_manager;

		std::size_t      index_;
		std::string_view name_;
		bool             meta_command_;

		Module_command_handle(std::size_t index, std::string_view name, bool meta_command)
		  : index_(index), name_(name), meta_command_(meta_command)
		{
		}
	};

	class Validation_result {
	  public:
		struct Module_error {
			Module_view module;
			Error       error;

			Module_error(Module_view module, Error error) : module(std::move(module)), error(std::move(error))
			{
			}
		};

		Validation_result() = default;
		explicit Validation_result(std::vector<Module_error> errors) : errors_(std::move(errors)) {}

		explicit operator bool() const { return errors_.empty(); }

		[[nodiscard]] auto errors() const -> std::span<const Module_error> { return errors_; }

		void log_errors() const;
		void abort_on_errors() const;

	  private:
		std::vector<Module_error> errors_;
	};

	class Module_command_manager {
	  public:
		explicit Module_command_manager(const std::filesystem::path& plugin_directory);

		void register_commands(Graph&);

		void refresh();

		auto create_base_config(const Seed& seed) const -> std::shared_ptr<Dictionary>;
		auto create_initial_state(std::shared_ptr<Dictionary>) const -> State;

		[[nodiscard]] auto list() const -> std::span<const Module_command_handle> { return command_handles_; }

		void execute(const Module_command_handle&, Graph&, const Node* after = nullptr);
		void execute(const Module_command_handle&, Cursor&);

		auto validate(Dictionary_view cfg) const -> Validation_result;

	  private:
		friend class Simulation_config;

		using Command_factory =
		        std::function<std::unique_ptr<Command>(const std::shared_lock<std::shared_mutex>& lock)>;

		yggdrasill::Generator     generator_;
		mutable std::shared_mutex refresh_mutex_;

		std::vector<Command_factory>       command_factories_;
		std::vector<Module_command_handle> command_handles_;

		void reset_commands();
		auto create_command(const Module_command_handle&) const -> std::unique_ptr<Command>;

		template <bool Repeatable, std::size_t N>
		void add_meta_command(std::string_view                name,
		                      std::array<std::string_view, N> modules,
		                      int                             iterations,
		                      float                           estimated_delta_time);
	};

} // namespace yggdrasill::simulation
