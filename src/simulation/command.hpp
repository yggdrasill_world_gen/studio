#pragma once

#include "../util/string.hpp"

#include <yggdrasill/dictionary.hpp>
#include <yggdrasill/world.hpp>

#include <memory>
#include <optional>

namespace yggdrasill::simulation {

	class State {
	  public:
		State(World world, std::shared_ptr<Dictionary> config)
		  : world_(std::move(world)), config_(std::move(config))
		{
		}

		auto world() -> auto& { return world_; }
		auto world() const -> const auto& { return world_; }

		auto mutable_config() -> Dictionary&
		{
			if(config_.use_count() > 1)
				config_ = std::make_shared<Dictionary>(*config_);
			return *config_;
		}
		auto config() const -> const Dictionary& { return *config_; }

	  private:
		World                       world_;
		std::shared_ptr<Dictionary> config_;
	};

	enum class Command_type : int8_t {
		root,       ///< automatically created, single empty initial node
		new_world,  ///< automatically created for new or imported worlds
		simulation, ///< module based node
		user_world_edit,
		user_config_edit,
		unknown
	};

	class Command {
	  public:
		Command() = default;
		virtual ~Command();

		virtual std::unique_ptr<Command> clone() const = 0;

		[[nodiscard]] virtual std::string_view description() const = 0;
		virtual void                           execute(State&)     = 0;

		virtual std::string_view serialize(Dictionary_view) = 0; // { return {}; }

		[[nodiscard]] virtual Command_type type() const = 0;

		/// if true, the command is always executed on the main thread, when it's added to the tree.
		[[nodiscard]] virtual bool force_synchronous() const { return false; }

		/// the underlying implementation changed and re-execution could yield a new result
		[[nodiscard]] virtual bool outdated() const { return false; }

		/// is it reasonable to execute this command multiple times or not (e.g. if it's idempotent)
		[[nodiscard]] virtual bool repeatable() const { return true; }

		[[nodiscard]] virtual std::optional<float> estimated_delta_time() const { return std::nullopt; }

		[[nodiscard]] virtual bool operator==(const Command& rhs) const { return this == &rhs; }

	  protected:
		Command(Command&&)                 = default;
		Command(const Command&)            = default;
		Command& operator=(Command&&)      = default;
		Command& operator=(const Command&) = default;
	};

	template <typename T>
	class Command_base : public Command {
	  public:
		std::unique_ptr<Command> clone() const override final
		{
			return std::make_unique<T>(static_cast<const T&>(*this));
		}
	};

	class Empty_command final : public Command_base<Empty_command> {
	  public:
		Empty_command(String description, Command_type type)
		  : description_(std::move(description)), type_(type)
		{
		}

		[[nodiscard]] std::string_view description() const override { return description_; }
		[[nodiscard]] Command_type     type() const override { return type_; }

		bool force_synchronous() const override { return true; }
		bool repeatable() const override { return false; }

		std::optional<float> estimated_delta_time() const override { return 0.f; }

		void execute(State& state) override {}


		static constexpr std::string_view id = "empty";
		std::string_view                  serialize(Dictionary_view cfg) override
		{
			cfg["desc"] = description_;
			cfg["type"] = type_;
			return id;
		}
		static std::unique_ptr<Command> deserialize(Const_dictionary_view cfg)
		{
			return std::make_unique<Empty_command>(cfg["desc"].get<String>(), cfg["type"].get<Command_type>());
		}

	  private:
		std::string  description_;
		Command_type type_;
	};

} // namespace yggdrasill::simulation
