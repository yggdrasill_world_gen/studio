#pragma once

#include "command.hpp"
#include "node.hpp"

#include "../util/string.hpp"

#include <yggdrasill/world.hpp>

#include <range/v3/all.hpp>

#include <atomic>
#include <cassert>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

namespace yggdrasill::simulation {

	/// thrown by Graph::execute, if the commands need to be executed synchronous, by the simulation hasn't advanced to the current node, yet
	class Simulation_not_ready : public std::exception {};

	/// thrown by mutating Graph methods if it is currently locked by a load/save operation
	class Not_editable : public std::exception {};

	enum class Branching_mode {
		linear            = 0,
		branch_empty      = 0b10,
		branch_empty_once = 0b11,
		branch_copy       = 0b100,
		branch_copy_once  = 0b101
	};


	class Graph {
	  public:
		Graph();
		Graph(const Graph&)            = delete;
		Graph(Graph&&)                 = delete;
		Graph& operator=(const Graph&) = delete;
		Graph& operator=(Graph&&)      = delete;
		~Graph();

		void register_command(std::string                                                    id,
		                      std::function<std::unique_ptr<Command>(Const_dictionary_view)> factory);
		template <typename CommandType>
		void register_command()
		{
			register_command(std::string(CommandType::id), CommandType::deserialize);
		}

		void start_load(std::string path, std::function<void()> continuation);
		void start_save(std::string path, const Node* cursor, std::function<void()> continuation);

		void rethrow_io_error();


		/// might throw Simulation_not_ready for synchronous commands
		auto& execute(std::unique_ptr<Command> command, const Node* after = nullptr)
		{
			const auto branch = static_cast<Branching_mode>(static_cast<std::uint32_t>(branch_mode_) & ~1);
			if(static_cast<std::uint32_t>(branch_mode_) & 1)
				branch_mode_ = Branching_mode::linear;

			add_node(create_node(std::move(command), after), branch);
			return main_branch_;
		}
		template <typename CommandType, typename... Ts>
		requires(std::constructible_from<CommandType, Ts && ...>)
		auto& execute(Ts&&... ts)
		{
			return execute(std::make_unique<CommandType>(std::forward<Ts>(ts)...), nullptr);
		}
		template <typename CommandType, typename... Ts>
		requires(std::constructible_from<CommandType, Ts && ...>)
		auto& execute_after(const Node* after, Ts&&... ts)
		{
			return execute(std::make_unique<CommandType>(std::forward<Ts>(ts)...), after);
		}

		void invalidate(const Node&);
		/// Executes the given node's command again on its own internal state.
		/// If the command is idempotent the behavior is identical to calling invalidate(), except that no references are invalidated in the process.
		/// If the command is not idempotent or the state is modified in a way that circumvents the World-objects version system, the behavior is undefined.
		void unsafe_rerun_inplace(const Node&);

		auto duplicate(const Node&) -> const Node&;
		auto duplicate_branch(const Node&) -> const Node&;

		auto add_world(std::string_view description, State state) -> const Node&;

		void undo(const Node& node, bool keep_branch);

		void clear();

		void rethrow_execution_error();

		bool editable() const { return io_operation_state_ == Io_state::inactive; }

		/// check if the given branch (or the main-branch) is ready, i.e. if synchronous commands can be executed on it
		bool ready(const Node* after = nullptr) const { return (after ? after : main_branch_)->ready(); }

		[[nodiscard]] const auto& root() const { return *root_; }
		[[nodiscard]] const auto& main_branch() const { return *main_branch_; }

		void               branch_mode(Branching_mode bm) { branch_mode_ = bm; }
		[[nodiscard]] auto branch_mode() const { return branch_mode_; }

		[[nodiscard]] auto version() const { return version_.load(); }

	  private:
		enum class Io_state { inactive, active, done, error };
		enum class Thread_state { fetching, running, waiting, quitting, done };

		std::shared_ptr<Node> root_;
		Node*                 main_branch_;
		Branching_mode        branch_mode_ = Branching_mode::branch_empty;

		util::String_map<std::string, std::function<std::unique_ptr<Command>(Const_dictionary_view)>> command_factories_;
		std::thread           io_thread_;
		std::exception_ptr    io_error_           = nullptr;
		std::atomic<Io_state> io_operation_state_ = Io_state::inactive;
		std::function<void()> io_continuation_;

		std::atomic<Thread_state> thread_state_ = Thread_state::done;
		std::condition_variable   thread_state_cv_;
		std::mutex                mutex_;
		std::thread               thread_;
		std::exception_ptr        async_error_     = nullptr;
		std::atomic<bool>         async_error_set_ = false;

		std::atomic<std::uint64_t> version_ = 0;

		/// temporary stack for DFS in fetch_next_task. Is a member to avoid re-allocations.
		/// pointer to shared_ptr, to avoid atomic-increment when directly storing/copying shared_ptr
		///   (works because objects are guaranteed to life to the end of the function call)
		std::vector<std::shared_ptr<Node>*> fetch_next_task_stack_;

		/// temporary stack for internal node traversal (only used inside non-const methods)
		std::vector<Node*>                     traversal_stack_;
		std::vector<std::tuple<int, int, int>> subtree_dimensions_stack_;

		void check_io_lock() const
		{
			if(io_operation_state_ != Io_state::inactive)
				throw Not_editable{};
		}

		void recalculate_subtree_dimensions(Node&);
		void invalidate_execution_results(Node& node, const std::unique_lock<std::mutex>& m)
		{
			traversal_stack_.clear();
			traversal_stack_.push_back(&node);
			invalidate_tstack_execution_results(m);
		}
		void invalidate_tstack_execution_results(const std::unique_lock<std::mutex>&);

		void notify_simulation_thread();
		void simulation_thread_main();
		auto fetch_next_task() -> std::shared_ptr<Node>;
		void prepare_execution(Node&) const;
		auto execute_task(Node& task) -> std::exception_ptr;

		auto create_node(std::unique_ptr<Command> command, const Node* after) -> std::shared_ptr<Node>;
		void add_node(std::shared_ptr<Node> node, Branching_mode branch);

		void duplicate_children_into(const Node& source, Node& dest);
	};

} // namespace yggdrasill::simulation
