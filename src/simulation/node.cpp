#include "node.hpp"


namespace yggdrasill {
	extern float current_time();
} // namespace yggdrasill

namespace yggdrasill::simulation {

	Node::Node(Private_api_token, std::unique_ptr<Command> command, Node* predecessor)
	  : predecessor_(predecessor)
	  , command_(std::move(command))
	  , dimensions_{1, 1, predecessor ? predecessor->dimensions_.x : 0, predecessor ? predecessor->dimensions_.y : 0}
	  , previous_dimensions_(dimensions_)
	{
	}

	void Node::change_dimensions(const Node_dimensions& new_value)
	{
		previous_dimensions_    = std::exchange(dimensions_, new_value);
		dimensions_change_time_ = current_time();
	}

} // namespace yggdrasill::simulation
