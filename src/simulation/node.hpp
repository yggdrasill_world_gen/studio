#pragma once

#include "command.hpp"

#include <range/v3/all.hpp>

#include <atomic>
#include <memory>
#include <optional>
#include <vector>

namespace yggdrasill::simulation {

	class Graph;


	struct Node_dimensions {
		/// width of this sub-tree, i.e. the sum of all leaf-nodes
		int width = 1;
		/// height of this sub-tree, i.e. the length of its longest branch
		int height = 1;
		/// position on the width axis (from oldest/root [0] to last added [width()-1])
		int x = 0;
		/// position on height axis, i.e. the depth of is node in the tree (from root [0] to deepest leaf [height()-1]
		int y = 0;

		auto copy() const { return *this; }

		// clang-format off
		auto& set_width(int v) {width  = v; return *this;}
		auto& set_height(int v){height = v;	return *this;}
		auto& set_x(int v)     {x      = v; return *this;}
		auto& set_y(int v)     {y      = v; return *this;}

		auto& add_width(int v) {width  += v; return *this;}
		auto& add_height(int v){height += v; return *this;}
		auto& add_x(int v)     {x      += v; return *this;}
		auto& add_y(int v)     {y      += v; return *this;}
		// clang-format on
	};


	class Node : public std::enable_shared_from_this<Node> {
	  public:
		class Private_api_token {
			friend class Node;
			friend class Graph;

			Private_api_token() = default;
		};
		enum class Execution_state { created, executing, executed, error };


		Node(Private_api_token, std::unique_ptr<Command> command, Node* predecessor);

		[[nodiscard]] std::string_view description() const { return command_->description(); }

		[[nodiscard]] auto* predecessor() const { return predecessor_; }
		/// range of references to successors, ordered from newest to oldest
		[[nodiscard]] auto successors() const
		{
			return successors_ | ranges::views::transform([](const auto& e) -> const auto& { return *e; });
		}

		[[nodiscard]] bool joinable(const Node& other) const { return *command_ == *other.command_; }
		[[nodiscard]] bool force_synchronous() const { return command_->force_synchronous(); }
		[[nodiscard]] bool outdated() const { return command_->outdated(); }
		[[nodiscard]] bool repeatable() const { return command_->repeatable(); }
		[[nodiscard]] auto type() const { return command_->type(); }

		[[nodiscard]] auto execution_state() const { return execution_state_.load(); }
		[[nodiscard]] auto ready() const { return execution_state() == Execution_state::executed && state_; }

		[[nodiscard]] auto estimated_delta_time() const { return command_->estimated_delta_time(); }

		[[nodiscard]] auto& dimensions() const { return dimensions_; }
		[[nodiscard]] auto& previous_dimensions() const { return previous_dimensions_; }
		[[nodiscard]] auto  dimensions_change_time() const { return dimensions_change_time_; }

		[[nodiscard]] auto state() const -> const State&
		{
			assert(ready());
			return *state_;
		}

		[[nodiscard]] auto error() const -> auto& { return error_; }

	  private:
		friend class Graph;
		friend class Cursor;

		Node*                              predecessor_;
		std::vector<std::shared_ptr<Node>> successors_;
		std::unique_ptr<Command>           command_;

		Node_dimensions dimensions_;
		Node_dimensions previous_dimensions_;
		float           dimensions_change_time_ = 0;

		std::atomic<Execution_state> execution_state_ = Execution_state::created;
		std::optional<State>         state_;
		std::optional<Error>         error_;

		void change_dimensions(const Node_dimensions& new_value);

		bool is_on_branch(const Node& branch_root) const
		{
			auto n = this;
			while(n) {
				if(n == &branch_root)
					return true;

				n = n->predecessor_;
			}
			return false;
		}
	};

} // namespace yggdrasill::simulation
