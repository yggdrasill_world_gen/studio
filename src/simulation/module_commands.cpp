#include "module_commands.hpp"

#ifdef YGDL_STATIC
namespace yggdrasill::modules {
	extern const Module_info generate_sphere;

	namespace tectonics {
		extern const Module_info generate_plates;
		extern const Module_info simulate_plates;
		extern const Module_info simple_erosion;
		extern const Module_info fix_enclaves;
		extern const Module_info merge_plates;
		extern const Module_info split_plates;
		extern const Module_info spawn_ridges;
		extern const Module_info refinement;
	} // namespace tectonics
} // namespace yggdrasill::modules
#endif

using namespace std::string_view_literals;

namespace yggdrasill::simulation {

	namespace {
		constexpr auto plate_sim_dt         = 1'000'000.f;
		constexpr auto plate_sim_sub_steps  = 64;
		constexpr auto plate_sim_iterations = 1;

		static constexpr std::string_view command_id = "module";

		class Module_command : public Command_base<Module_command> {
		  public:
			Module_command(Module_view m, const std::shared_lock<std::shared_mutex>& lock)
			  : module_(m), refresh_mutex_(*lock.mutex())
			{
			}

			[[nodiscard]] std::string_view description() const override { return module_.id(); }

			[[nodiscard]] Command_type type() const override { return Command_type::simulation; }

			void execute(State& state) override
			{
				auto _ = std::shared_lock(refresh_mutex_);
				module_.execute(state.world(), state.config());
				executed_version_ = module_.version();
			}

			[[nodiscard]] bool outdated() const override
			{
				return executed_version_ && executed_version_ != module_.version();
			}

			[[nodiscard]] bool operator==(const Command& rhs) const override
			{
				if(this == &rhs)
					return true;
				else if(auto* cmd = dynamic_cast<const Module_command*>(&rhs))
					return module_ == cmd->module_;
				else
					return false;
			}

			std::string_view serialize(Dictionary_view cfg) override
			{
				cfg["id"] = module_.id();
				return command_id;
			}

		  private:
			Module_view                 module_;
			std::shared_mutex&          refresh_mutex_;
			std::optional<Data_version> executed_version_;
		};

		template <std::size_t N, bool Repeatable>
		class Meta_module_command : public Command_base<Meta_module_command<N, Repeatable>> {
		  public:
			Meta_module_command(std::string_view          description,
			                    std::span<Module_view, N> modules,
			                    int                       iterations,

			                    float                                      estimated_delta_time,
			                    const std::shared_lock<std::shared_mutex>& lock)
			  : description_(description)
			  , modules_(modules)
			  , iterations_(iterations)
			  , estimated_delta_time_(estimated_delta_time)
			  , refresh_mutex_(*lock.mutex())
			{
			}

			[[nodiscard]] std::string_view description() const override { return description_; }

			[[nodiscard]] Command_type type() const override { return Command_type::simulation; }

			[[nodiscard]] bool repeatable() const override { return Repeatable; }

			void execute(State& state) override
			{
				auto _ = std::shared_lock(refresh_mutex_);
				for(auto i = 0; i < iterations_; ++i)
					for(auto& m : modules_)
						m.execute(state.world(), state.config());

				for(auto i = std::size_t(0); i < N; ++i)
					executed_versions_[i] = modules_[i].version();
			}

			[[nodiscard]] bool outdated() const override
			{
				for(auto i = std::size_t(0); i < N; ++i)
					if(executed_versions_[i] != modules_[i].version())
						return true;

				return false;
			}

			[[nodiscard]] bool operator==(const Command& rhs) const override
			{
				if(this == &rhs)
					return true;
				else if(auto* cmd = dynamic_cast<const Meta_module_command<N, Repeatable>*>(&rhs))
					return iterations_ == cmd->iterations_
					       && std::equal(
					               modules_.begin(), modules_.end(), cmd->modules_.begin(), cmd->modules_.end());
				else
					return false;
			}

			std::string_view serialize(Dictionary_view cfg) override
			{
				cfg["id"] = description_;
				return command_id;
			}

		  private:
			std::string_view            description_;
			std::span<Module_view, N>   modules_;
			int                         iterations_;
			float                       estimated_delta_time_;
			std::shared_mutex&          refresh_mutex_;
			std::array<Data_version, N> executed_versions_;
		};
	} // namespace


	void Validation_result::log_errors() const
	{
		if(errors_.empty())
			return;

		std::cerr << "Unhandled module-validation-error. This is either a bug in the editor or one of the "
		             "following modules. Errors:\n";
		for(auto& e : errors_)
			std::cout << "  - " << e.module.id() << ": " << e.error.message() << " (" << e.error.code() << ")\n";
	}

	void Validation_result::abort_on_errors() const
	{
		if(errors_.empty())
			return;

		log_errors();
		std::cerr << "\nAborting...";
		std::abort();
	}


	Module_command_manager::Module_command_manager(const std::filesystem::path& plugin_directory)
	{
#ifdef YGDL_STATIC
		generator_.add(yggdrasill::modules::generate_sphere);
		generator_.add(yggdrasill::modules::tectonics::generate_plates);
		generator_.add(yggdrasill::modules::tectonics::simulate_plates);
		generator_.add(yggdrasill::modules::tectonics::simple_erosion);
		generator_.add(yggdrasill::modules::tectonics::fix_enclaves);
		generator_.add(yggdrasill::modules::tectonics::merge_plates);
		generator_.add(yggdrasill::modules::tectonics::spawn_ridges);
		generator_.add(yggdrasill::modules::tectonics::split_plates);
		generator_.add(yggdrasill::modules::tectonics::refinement);

#else
		try {
			generator_.load(plugin_directory.generic_string());
		} catch(const std::exception& e) {
			std::cerr << "Couldn't load plugins: " << e.what() << "\n";
			throw e;
		}
#endif

		std::cout << "Loaded modules:\n";
		for(auto&& m : generator_.list()) {
			std::cout << "  - " << m.id() << "\n";
		}
		std::cout << '\n';

		reset_commands();
	}

	void Module_command_manager::register_commands(Graph& graph)
	{
		graph.register_command(std::string(command_id), [&](Const_dictionary_view cfg) -> std::unique_ptr<Command> {
			auto id = cfg["id"].get<std::string_view>();

			auto cmd_iter = std::find_if(command_handles_.begin(), command_handles_.end(), [&](auto& e) {
				return e.name() == id;
			});

			if(cmd_iter != command_handles_.end())
				return create_command(*cmd_iter);

			return {};
		});
	}

	void Module_command_manager::reset_commands()
	{
		command_factories_.clear();
		command_handles_.clear();

		add_meta_command<false>(
		        "Create Planet",
		        std::array{"generate_sphere"sv, "tectonics_generate_plates"sv, "tectonics_refinement"sv},
		        1,
		        0.f);
		add_meta_command<true>("Plate Tectonics",
		                       std::array{"tectonics_simulate_plates"sv,
		                                  "tectonics_fix_enclaves"sv,
		                                  "tectonics_simple_erosion"sv,
		                                  "tectonics_spawn_ridges"sv,
		                                  "tectonics_merge_plates"sv,
		                                  "tectonics_split_plates"sv,
		                                  "tectonics_fix_enclaves"sv,
		                                  "tectonics_refinement"sv},
		                       plate_sim_iterations,
		                       plate_sim_dt * plate_sim_iterations);

		// the add loaded modules as generic commands
		for(auto m : generator_.list()) {
			command_factories_.emplace_back([m](const std::shared_lock<std::shared_mutex>& lock) {
				return std::make_unique<Module_command>(m, lock);
			});
			command_handles_.push_back(Module_command_handle(command_handles_.size(), m.id(), false));
		}
	}

	auto Module_command_manager::create_base_config(const Seed& seed) const -> std::shared_ptr<Dictionary>
	{
		auto cfg                          = std::make_shared<Dictionary>();
		(*cfg)["seed"]                    = seed.str();
		(*cfg)["plate_sim"]["delta_time"] = plate_sim_dt;
		(*cfg)["plate_sim"]["sub_steps"]  = plate_sim_sub_steps;

		// check for errors and populate with default values
		if(auto errors = validate(*cfg); !errors) {
			std::cerr << "Validation failed for the initial (empty) module configuration. Any errors "
			             "below "
			             "are caused by misbehaving modules!\n";
			errors.abort_on_errors();
		}

		return cfg;
	}

	auto Module_command_manager::create_initial_state(std::shared_ptr<Dictionary> cfg) const -> State
	{
		// create state container
		const auto seed = (*cfg)["seed"].get<std::string_view>();
		return State{World{Seed::from_str(seed)}, std::make_shared<Dictionary>(*cfg)};
	}

	void Module_command_manager::refresh()
	{
		if(!generator_.reload_available())
			return; // early-out, if there are no new versions, to avoid lock

		auto _ = std::unique_lock(refresh_mutex_);

		auto modified = generator_.reload();
		reset_commands();
		std::cout << "Reloaded " << modified.size() << " plugins\n";
	}

	void Module_command_manager::execute(
	        const Module_command_handle& module_handle, Graph& simulation, const Node* after)
	{
		simulation.execute(create_command(module_handle), after);
	}

	void Module_command_manager::execute(const Module_command_handle& module_handle, Cursor& after)
	{
		after.execute(create_command(module_handle));
	}

	auto Module_command_manager::validate(Dictionary_view cfg) const -> Validation_result
	{
		auto errors = std::vector<Validation_result::Module_error>();

		for(auto&& m : generator_.list()) {
			auto result = m.validate(cfg);
			if(!result.success())
				errors.emplace_back(m, std::move(result));
		}

		return Validation_result{std::move(errors)};
	}

	std::unique_ptr<Command> Module_command_manager::create_command(const Module_command_handle& module_handle) const
	{
		auto lock = std::shared_lock(refresh_mutex_);

		if(module_handle.index_ >= command_factories_.size()) {
			std::cerr << "Invalid Module_command_handle passed to Module_command_manager::create\n";
			std::abort();
		}

		return command_factories_[module_handle.index_](lock);
	}

	template <bool Repeatable, std::size_t N>
	void Module_command_manager::add_meta_command(std::string_view                name,
	                                              std::array<std::string_view, N> module_ids,
	                                              int                             iterations,
	                                              float                           estimated_delta_time)
	{
		auto module_handles = std::array<Module_view, N>{};
		for(auto i = std::size_t(0); i < N; ++i) {
			const auto id = module_ids[i];

			if(auto m = generator_.get(id)) {
				module_handles[i] = *m;
			} else {
				auto msg = util::concat("Missing module '", id, "' for meta command '", name, "'");
				std::cerr << msg << '\n';
				throw std::runtime_error(msg);
			}
		}

		command_factories_.emplace_back([=](const std::shared_lock<std::shared_mutex>& lock) mutable {
			return std::make_unique<Meta_module_command<N, Repeatable>>(
			        name, module_handles, iterations, estimated_delta_time, lock);
		});

		command_handles_.push_back(Module_command_handle(command_handles_.size(), name, true));
	}

} // namespace yggdrasill::simulation
