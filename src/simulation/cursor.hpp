#pragma once

#include "command.hpp"
#include "node.hpp"

namespace yggdrasill::simulation {

	class Graph;


	/// Handle to a position inside the history graph.
	/// \remark Main access-point for other systems to query the current state and apply changes
	class Cursor {
	  public:
		explicit Cursor(Graph&);

		[[nodiscard]] bool is_before(const Cursor& rhs) const;

		void undo(bool keep_branch);

		/// Execute a command at the current position and advances the cursor.
		auto execute(std::unique_ptr<Command> command) -> const Node&;

		template <typename CommandType, typename... Ts>
		requires(std::constructible_from<CommandType, Ts && ...>)
		auto& execute(Ts&&... ts)
		{
			return execute(std::make_unique<CommandType>(std::forward<Ts>(ts)...));
		}

		auto seek(const Node& node, std::span<index_t> path = {}) -> const Node*;
		auto seek_backward() -> const Node*;
		auto seek_forward(index_t next_node) -> const Node*;
		auto seek_forward() -> const Node*;
		auto seek_forward_if_ready() -> const Node*;
		auto seek_later_sibling() -> const Node*;
		auto seek_earlier_sibling() -> const Node*;

		bool can_seek_backward() const;
		bool can_seek_forward() const;
		bool can_seek_forward_if_ready() const;

		[[nodiscard]] index_t successor_count() const;

		[[nodiscard]] auto graph() -> auto& { return *graph_; }
		[[nodiscard]] auto graph() const -> const auto& { return *graph_; }

		[[nodiscard]] auto graph_root() const -> const Node&;
		[[nodiscard]] auto node() const -> std::shared_ptr<const Node> { return node_.lock(); }
		[[nodiscard]] auto path() const -> std::span<const index_t> { return path_; }
		[[nodiscard]] auto world() const -> std::shared_ptr<const World>;
		[[nodiscard]] auto config() const -> std::shared_ptr<const Dictionary>;

		[[nodiscard]] bool invalidated() const { return node_.expired(); }
		[[nodiscard]] bool ready() const;
		[[nodiscard]] bool editable() const;
		void               editable(bool e) { editable_ = e; }

		friend bool operator==(const Cursor& lhs, const Cursor& rhs)
		{
			return !lhs.node_.owner_before(rhs.node_) && !rhs.node_.owner_before(lhs.node_);
		}
		friend bool operator==(const Cursor& lhs, const std::weak_ptr<const Node>& rhs)
		{
			return !lhs.node_.owner_before(rhs) && !rhs.owner_before(lhs.node_);
		}
		friend bool operator==(const Cursor& lhs, const Node& rhs) { return lhs.node_address_ == &rhs; }

	  private:
		Graph*                    graph_;
		std::vector<index_t>      path_;
		std::weak_ptr<const Node> node_;
		const Node*               node_address_;
		bool                      editable_ = true;

		auto               seek_sibling(bool later) -> const Node*;
		[[nodiscard]] auto find_first_child(std::span<const std::shared_ptr<Node>> container,
		                                    bool                                   reverse,
		                                    int steps) const -> std::shared_ptr<const Node>;
	};

	template <typename F>
	void foreach_node(const Cursor& to, F&& f)
	{
		auto* node = &to.graph_root();
		for(auto i : to.path()) {
			f(*node);
			node = &node->successors()[i];
		}

		f(*node);
	}

	struct last_leaf_node {
		const Cursor& cursor;
	};

	template <typename F>
	void foreach_node(const last_leaf_node& to, F&& f)
	{
		foreach_node(to.cursor, f);

		auto to_node = to.cursor.node();
		if(!to_node)
			return;

		auto* node = to_node.get();
		while(!node->successors().empty()) {
			node = &node->successors().back();
			f(*node);
		}
	}

	template <typename F>
	void foreach_node(const Cursor& from, const Cursor& to, F&& f)
	{
		assert(from.is_before(to));

		auto from_node = from.node();
		if(!from_node)
			return;

		auto node = from_node.get();
		for(auto i = from.path().size(); i < to.path().size(); ++i) {
			f(*node);
			node = &node->successors()[to.path()[i]];
		}

		f(*node);
	}

} // namespace yggdrasill::simulation
