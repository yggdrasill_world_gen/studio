#include "graph.hpp"

#include "module_commands.hpp"

#include <yggdrasill/serialization.hpp>


namespace yggdrasill::simulation {

	Graph::Graph()
	  : root_{std::make_shared<Node>(Node::Private_api_token{},
	                                 std::make_unique<Empty_command>(String("Root"), Command_type::root),
	                                 nullptr)}
	  , main_branch_(root_.get())
	{
		root_->state_.emplace(World{}, std::make_shared<Dictionary>());
		root_->execution_state_ = Node::Execution_state::executed;

		register_command<Empty_command>();

		// start execution thread
		thread_ = std::thread{[&] { simulation_thread_main(); }};
	}
	Graph::~Graph()
	{
		// finish any ongoing IO operations
		if(io_thread_.joinable())
			io_thread_.join();

		std::cout << "Request simulation thread to stop\n";
		// notify the thread that it should shut down and wait for its completion
		if(thread_state_ != Thread_state::done) {
			// acquire the lock, to ensure that the thread is currently not in the fetching state
			auto lock     = std::unique_lock{mutex_};
			thread_state_ = Thread_state::quitting;
			lock.unlock();
			thread_state_cv_.notify_all();
		}
		thread_.join();
		std::cout << "Graph stopped\n";

		// TODO: manually destruct nodes to prevent stackoverflow for large graphs (the same has to be done in clear() undo())
	}

	void Graph::register_command(std::string                                                    id,
	                             std::function<std::unique_ptr<Command>(Const_dictionary_view)> factory)
	{
		command_factories_.emplace(std::move(id), std::move(factory));
	}

	void Graph::start_load(std::string path, std::function<void()> continuation)
	{
		auto expected = Io_state::inactive;
		if(!io_operation_state_.compare_exchange_strong(expected, Io_state::active))
			throw Not_editable{};

		struct Shared {
			std::vector<std::shared_ptr<Node>> roots;
			std::shared_ptr<Node>              cursor;
		};
		auto shared = std::make_shared<Shared>();

		io_continuation_ = [this, shared, continuation = std::move(continuation)] {
			main_branch_ = shared->cursor ? shared->cursor.get() : root_.get();

			// update the tree
			for(auto&& node : shared->roots) {
				add_node(std::move(node), Branching_mode::branch_empty);
			}

			if(continuation)
				continuation();
		};

		io_thread_ = std::thread{[this, shared, path = std::move(path)] {
			try {
				// note: no lock is required here, because the new nodes only become visible right at the end when add_node() is called

				auto       deserializer = Deserializer(path);
				const auto graph        = deserializer.read_config("graph");
				const auto node_data    = graph["nodes"].get<Const_array_view<Const_dictionary_view>>();

				auto read_configs = std::unordered_map<std::int32_t, std::shared_ptr<Dictionary>>{};
				auto read_nodes   = std::unordered_map<std::int32_t, std::shared_ptr<Node>>{};
				read_nodes[-1]    = root_;
				auto last_id      = std::int32_t(-1);

				for(auto data : node_data) {
					auto id        = data["_id"].get<std::int32_t>();
					auto parent_id = data["_pid"].get<std::int32_t>();

					// load the command
					auto command = std::unique_ptr<Command>();
					if(auto command_id = data["_cid"]; command_id.exists()) {
						if(auto factory = command_factories_.find(command_id.get<std::string_view>());
						   factory != command_factories_.end()) {
							try {
								command = factory->second(data);
							} catch(const std::exception& e) {
								std::cerr << "Error in command-definition '" << command_id << "' of node '"
								          << id << "' in file '" << path << "': " << e.what() << '\n';
								// => continue and create an "Unknown Command" node instead
							}
						}
					}
					if(!command) {
						if(auto command_name = data["_cname"]; command_name.exists())
							command = std::make_unique<Empty_command>(command_name.get<String>(),
							                                          Command_type::unknown);
						else
							command = std::make_unique<Empty_command>(String("Unknown Command"),
							                                          Command_type::unknown);
					}

					// create the node
					auto& parent = read_nodes.at(parent_id);
					assert(parent);
					auto node = create_node(std::move(command), parent.get());
					if(parent_id == -1)
						shared->roots.push_back(node);
					else
						parent->successors_.push_back(node); // add to our parent

					// load the world state (if there is one)
					auto cfg   = data["_cfg"];
					auto world = data["_world"];
					if(cfg.convertible(Type::int32_t) && world.convertible(Type::int32_t)) {
						try {
							const auto cfg_id  = cfg.get<std::int32_t>();
							auto&      cfg_obj = read_configs[cfg_id];
							if(!cfg_obj) {
								cfg_obj = std::make_shared<Dictionary>();
								deserializer.read_config(std::to_string(cfg_id), *cfg_obj);
							}

							auto world_obj = deserializer.read_world(
							        static_cast<Serialized_world_handle>(world.get<std::int32_t>()));

							node->state_.emplace(std::move(world_obj), cfg_obj);
							node->execution_state_ = Node::Execution_state::executed;
						} catch(const std::exception& e) {
							std::cerr << "Error reading state of node '" << id << "' in file '" << path
							          << "': " << e.what() << '\n';
							// => continue without its state
						}
					}

					read_nodes.emplace(id, std::move(node));
				}

				shared->cursor = read_nodes[graph["cursor"].get<std::int32_t>(last_id)];
				assert(shared->cursor);

				io_operation_state_ = Io_state::done;
			} catch(...) {
				io_error_           = std::current_exception();
				io_operation_state_ = Io_state::error;
			}
		}};
	}
	void Graph::start_save(std::string path, const Node* cursor, std::function<void()> continuation)
	{
		auto expected = Io_state::inactive;
		if(!io_operation_state_.compare_exchange_strong(expected, Io_state::active))
			throw Not_editable{};

		io_continuation_ = std::move(continuation);

		io_thread_ = std::thread{[this, cursor, path = std::move(path)] {
			try {
				auto serializer = Serializer::open_overwrite(path);
				auto graph      = Dictionary{};
				auto node_data  = graph["nodes"].get<Array_view<Dictionary_ref>>();

				const auto traverse_nodes = [&](auto&& body) {
					// iterate over all nodes in level order
					auto next_level_nodes = std::array<std::vector<std::tuple<std::int32_t, const Node*>>, 2>{};
					auto next_level_nodes_index = 0;
					for(auto& successor : root().successors_)
						next_level_nodes[next_level_nodes_index].emplace_back(-1, successor.get());

					while(!next_level_nodes[next_level_nodes_index].empty()) {
						// iterate over one vector and fill the other with the next level nodes
						auto& nodes = next_level_nodes[next_level_nodes_index];
						next_level_nodes_index ^= 1;
						auto& next_nodes = next_level_nodes[next_level_nodes_index];
						next_nodes.clear();

						for(auto&& [parent_id, node] : nodes) {
							const auto id = body(parent_id, *node);

							for(auto& successor : node->successors_)
								next_nodes.emplace_back(id, successor.get());
						}
					}
				};

				auto written_configs = std::unordered_map<const YGDL_Dict*, std::int32_t>{};
				auto next_cfg_id     = std::int32_t(1);

				auto next_id = std::int32_t(0);
				traverse_nodes([&](std::int32_t parent_id, const Node& node) -> std::int32_t {
					auto data = node_data.push_back();

					const auto id = next_id++;
					data["_id"]   = id;
					data["_pid"]  = parent_id;
					if(auto command_id = node.command_->serialize(data); !command_id.empty())
						data["_cid"] = command_id;
					else
						data["_cname"] = node.command_->description();

					if(&node == cursor)
						graph["cursor"] = id;

					if(node.execution_state_ == Node::Execution_state::executed && node.state_) {
						static_assert(sizeof(std::uint64_t) >= sizeof(Serialized_world_handle));

						auto&& cfg     = node.state_->config();
						auto&  cfg_key = written_configs[c_ptr(cfg)];
						if(cfg_key == 0) {
							cfg_key = next_cfg_id++;
							serializer.write_config(std::to_string(cfg_key), node.state_->config());
						}

						data["_cfg"] = cfg_key;
						data["_world"] =
						        static_cast<std::uint64_t>(serializer.write_world(node.state_->world()));
					}

					return id;
				});

				serializer.write_config("graph", graph);

				io_operation_state_ = Io_state::done;
			} catch(...) {
				io_error_           = std::current_exception();
				io_operation_state_ = Io_state::error;
			}
		}};
	}

	void Graph::rethrow_io_error()
	{
		auto state = io_operation_state_.load();
		switch(state) {
			case Io_state::inactive: return;
			case Io_state::active: return;

			case Io_state::error: {
				io_operation_state_ = Io_state::inactive;

				io_continuation_ = {};

				if(io_thread_.joinable())
					io_thread_.join();
				io_thread_ = {};

				auto error = std::move(io_error_);
				io_error_  = nullptr;
				rethrow_exception(error);
				return;
			}
			case Io_state::done: {
				io_operation_state_ = Io_state::inactive;

				if(io_continuation_)
					io_continuation_();
				io_continuation_ = {};

				if(io_thread_.joinable())
					io_thread_.join();
				io_thread_ = {};

				io_error_ = nullptr;
				return;
			}
		}
	}


	void Graph::invalidate(const Node& node)
	{
		check_io_lock();
		version_++;
		auto lock = std::unique_lock{mutex_};

		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast), safe because we gave out the pointer and own the mutable object
		auto& mutable_node = const_cast<Node&>(node);

		invalidate_execution_results(mutable_node, lock);

		if(node.force_synchronous()) {
			// execute the commands right-now
			prepare_execution(mutable_node);
			if(auto exception = execute_task(mutable_node)) {
				std::rethrow_exception(exception);
			}
			if(mutable_node.error_) {
				mutable_node.error_->throw_as_exception();
			}
		}
	}
	void Graph::unsafe_rerun_inplace(const Node& node)
	{
		check_io_lock();
		version_++;
		auto lock = std::unique_lock{mutex_};

		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast), safe because we gave out the pointer and own the mutable object
		auto& mutable_node = const_cast<Node&>(node);

		assert(mutable_node.execution_state_ == Node::Execution_state::executed
		       || mutable_node.execution_state_ == Node::Execution_state::error);

		mutable_node.execution_state_ = Node::Execution_state::executing;
		if(auto exception = execute_task(mutable_node)) {
			std::rethrow_exception(exception);
		}
		if(mutable_node.error_) {
			mutable_node.error_->throw_as_exception();
		}
	}

	auto Graph::duplicate(const Node& node) -> const Node&
	{
		check_io_lock();
		version_++;
		add_node(create_node(node.command_->clone(), &node), Branching_mode::linear);
		main_branch_->previous_dimensions_ = main_branch_->dimensions_;
		return *main_branch_;
	}

	auto Graph::duplicate_branch(const Node& node) -> const Node&
	{
		check_io_lock();
		version_++;
		auto new_node = create_node(node.command_->clone(), node.predecessor_);

		duplicate_children_into(node, *new_node);

		auto& new_node_ref = *new_node;
		add_node(std::move(new_node), Branching_mode::branch_empty);

		// set previous dimensions to those of the first clones node
		traversal_stack_.clear();
		traversal_stack_.push_back(&new_node_ref);

		while(!traversal_stack_.empty()) {
			auto* n = traversal_stack_.back();
			traversal_stack_.pop_back();
			n->previous_dimensions_ = node.dimensions_;

			for(const auto& c : n->successors_) {
				traversal_stack_.push_back(c.get());
			}
		}

		return *main_branch_;
	}
	void Graph::duplicate_children_into(const Node& source, Node& dest)
	{
		/* Recursively copy all child nodes, like so:
		 * f(new_node, old_node):
		 * 		for c in old_node:
		 * 			new_c = new_node.add(c)
		 * 			f(new_c, c)
		 */
		traversal_stack_.clear();
		traversal_stack_.push_back(&dest);
		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast), safe because we gave out the pointer and own the mutable object
		traversal_stack_.push_back(const_cast<Node*>(&source));

		while(!traversal_stack_.empty()) {
			const auto* old_n = traversal_stack_.back();
			traversal_stack_.pop_back();
			auto* new_n = traversal_stack_.back();
			traversal_stack_.pop_back();

			for(const auto& c : old_n->successors_) {
				if(c.get() == &dest)
					continue;

				auto new_c = create_node(c->command_->clone(), new_n);
				traversal_stack_.push_back(new_c.get());
				traversal_stack_.push_back(c.get());
				new_n->successors_.push_back(std::move(new_c));
			}
		}
	}

	auto Graph::add_world(std::string_view description, State state) -> const Node&
	{
		check_io_lock();

		version_++;
		auto node = create_node(std::make_unique<Empty_command>(String(description), Command_type::new_world),
		                        root_.get());
		node->state_           = std::move(state);
		node->execution_state_ = Node::Execution_state::executed;
		add_node(std::move(node), Branching_mode::branch_empty);
		return *main_branch_;
	}

	void Graph::clear()
	{
		check_io_lock();

		version_++;
		auto lock = std::unique_lock{mutex_};
		root_->successors_.clear();
		recalculate_subtree_dimensions(*root_);
		main_branch_ = root_.get();
	}

	void Graph::undo(const Node& node, bool keep_branch)
	{
		check_io_lock();

		version_++;
		assert(node.predecessor_);
		auto& pre = *node.predecessor_;

		auto lock = std::unique_lock{mutex_};

		auto old_position = std::find_if(
		        pre.successors_.begin(), pre.successors_.end(), [&](auto& n) { return n.get() == &node; });
		assert(old_position != pre.successors_.end());

		const auto main_branch_deleted = keep_branch ? &node == main_branch_ : main_branch_->is_on_branch(node);

		if(!keep_branch || node.successors_.empty()) {
			pre.successors_.erase(old_position);

		} else {
			// replace node in its predecessor with all its children
			auto mutable_node = std::move(*old_position);
			invalidate_execution_results(*mutable_node, lock);
			for(auto& s : mutable_node->successors_)
				s->predecessor_ = &pre;

			*old_position = std::move(mutable_node->successors_[0]);
			traversal_stack_.push_back(old_position->get());

			if(mutable_node->successors_.size() > 1) {
				pre.successors_.insert(old_position + 1,
				                       std::make_move_iterator(mutable_node->successors_.begin() + 1),
				                       std::make_move_iterator(mutable_node->successors_.end()));
			}

			recalculate_subtree_dimensions(*node.predecessor_);
		}

		recalculate_subtree_dimensions(*root_); // full recomputation because the tree height changed
		if(main_branch_deleted)
			main_branch_ = &pre;

		if(keep_branch) {
			lock.unlock();
			notify_simulation_thread();
		}
	}

	auto Graph::create_node(std::unique_ptr<Command> command, const Node* after) -> std::shared_ptr<Node>
	{
		// NOLINTNEXTLINE(cppcoreguidelines-pro-type-const-cast), safe because we gave out the pointer and own the mutable object
		auto* predecessor = after ? const_cast<Node*>(after) : main_branch_;

		return std::make_shared<Node>(Node::Private_api_token{}, std::move(command), predecessor);
	}
	void Graph::add_node(std::shared_ptr<Node> new_node, Branching_mode branch_mode)
	{
		check_io_lock();

		version_++;
		assert(new_node->predecessor_);

		const auto synchronous = new_node->force_synchronous()
		                         && new_node->execution_state() == Node::Execution_state::created;
		auto& predecessor = *new_node->predecessor_;
		auto& pre_dim     = predecessor.dimensions_;
		if(synchronous && !ready(&predecessor))
			throw Simulation_not_ready{};

		auto lock = std::unique_lock{mutex_};

		if(synchronous) {
			// execute the commands right-now
			prepare_execution(*new_node);
			if(auto exception = execute_task(*new_node)) {
				std::rethrow_exception(exception);
			}
			if(new_node->error_) {
				new_node->error_->throw_as_exception();
			}
		}


		auto displaced_successors = std::vector<std::shared_ptr<Node>>{};
		if(branch_mode == Branching_mode::linear) {
			// invalidate execution state for all displaced nodes
			traversal_stack_.clear();
			for(auto& s : predecessor.successors_)
				traversal_stack_.push_back(s.get());
			invalidate_tstack_execution_results(lock);

			// extract current successors
			displaced_successors = std::move(predecessor.successors_);
			predecessor.successors_.clear();
		}

		const auto new_branch = !predecessor.successors_.empty();

		// actually insert the new node
		predecessor.successors_.emplace_back(new_node);

		if(branch_mode == Branching_mode::branch_copy) {
			// copy all successors of our parent (except ourselves)
			duplicate_children_into(*new_node->predecessor_, *new_node);
		}

		if(!displaced_successors.empty()) {
			// move them behind the newly created node
			new_node->successors_ = std::move(displaced_successors);
			for(auto& s : new_node->successors_) {
				s->predecessor_ = new_node.get();
			}

			// the structure of the subtree after the insert-position has been modified => recompute all width/height and x/y
			recalculate_subtree_dimensions(predecessor);
		} else if(!new_node->successors_.empty()) {
			// special case: a complete branch was added => just recalculate all dimensions
			recalculate_subtree_dimensions(predecessor);
		} else {
			// calculate x/y of new_node and update width/height of predecessor
			predecessor.change_dimensions(
			        pre_dim.copy()
			                .add_width(new_branch ? new_node->dimensions_.width : 0)
			                .set_height(std::max(pre_dim.height, 1 + new_node->dimensions_.height)));
			new_node->change_dimensions(
			        new_node->dimensions_.copy().set_x(pre_dim.x + pre_dim.width - 1).set_y(pre_dim.y + 1));
		}

		// update width/height in parents
		traversal_stack_.clear();
		auto max_height = pre_dim.height;
		for(Node *parent = predecessor.predecessor_, *prev = &predecessor; parent != nullptr;
		    prev = parent, parent = parent->predecessor_) {
			max_height = std::max(parent->dimensions_.height, max_height + 1);
			parent->change_dimensions(parent->dimensions_.copy().set_height(max_height));
			if(new_branch) {
				parent->change_dimensions(parent->dimensions_.copy().add_width(new_node->dimensions_.width));

				// x of every node right of this one is incremented => recurse over parents + iterate over all successors_ after the node we are coming from
				auto after_current = false;
				for(auto& s : parent->successors_) {
					if(after_current) {
						traversal_stack_.push_back(s.get());
					} else {
						after_current = s.get() == prev;
					}
				}
			}
		}
		while(!traversal_stack_.empty()) {
			auto node = traversal_stack_.back();
			traversal_stack_.pop_back();
			node->change_dimensions(node->dimensions_.copy().add_x(new_node->dimensions_.width));
			for(auto& s : node->successors_)
				traversal_stack_.push_back(s.get());
		}

		main_branch_ = new_node.get();

		main_branch_->previous_dimensions_ = main_branch_->successors_.empty()
		                                             ? main_branch_->predecessor_->dimensions_
		                                             : main_branch_->dimensions_;

		lock.unlock();
		notify_simulation_thread();
	}

	void Graph::notify_simulation_thread()
	{
		// notify the execution thread, if it's currently waiting for work
		if(auto expected_thread_state = Thread_state::waiting;
		   thread_state_.compare_exchange_strong(expected_thread_state, Thread_state::running)) {
			thread_state_cv_.notify_all();
		}
	}

	void Graph::recalculate_subtree_dimensions(Node& start_node)
	{
		/* recursive algorithm:

		 f(x,y) -> (max_x, max_y):
		 	x_ = x
		 	y_ = y

		 	if(leaf):
		 		width_ = height_ = 1
		 		return (x_+1, y_+1);
		 	else:
				y++
				max_x = x_
				max_y = y_
				for c : successors_:
					(mx, my) = c.f(max_x,y)
					max_y = max(my, max_y)
					max_x = mx
				width_ = max_x - x;
				height_ = max_y - y;
		 		return (max_x, max_y)
		*/

		traversal_stack_.clear();
		traversal_stack_.push_back(&start_node);
		subtree_dimensions_stack_.clear();
		subtree_dimensions_stack_.emplace_back(start_node.dimensions_.x, start_node.dimensions_.y, 0);

		int max_x = start_node.dimensions_.x;
		int max_y = start_node.dimensions_.y;

		while(!traversal_stack_.empty()) {
			auto node           = traversal_stack_.back();
			auto& [x, y, index] = subtree_dimensions_stack_.back();

			if(index == 0) {
				node->change_dimensions(node->dimensions_.copy().set_x(x).set_y(y));
			}

			if(node->successors_.empty()) {
				node->change_dimensions(node->dimensions_.copy().set_width(1).set_height(1));
				max_x = std::max(max_x, x + 1);
				max_y = std::max(max_y, y + 1);
				// end/return
				traversal_stack_.pop_back();
				subtree_dimensions_stack_.pop_back();

			} else if(index < int(node->successors_.size())) {
				// recursive calls for this node
				traversal_stack_.push_back(node->successors_[index].get());
				index++;
				subtree_dimensions_stack_.emplace_back(max_x, y + 1, 0);
				// continue with child

			} else {
				// last recursive call for this node (all children visited
				node->change_dimensions(node->dimensions_.copy().set_width(max_x - x).set_height(max_y - y));
				// end/return
				traversal_stack_.pop_back();
				subtree_dimensions_stack_.pop_back();
			}
		}
	}

	void Graph::invalidate_tstack_execution_results(const std::unique_lock<std::mutex>&)
	{
		while(!traversal_stack_.empty()) {
			auto n = traversal_stack_.back();
			traversal_stack_.pop_back();
			n->execution_state_ = Node::Execution_state::created;
			for(auto& s : n->successors_)
				traversal_stack_.push_back(s.get());
		}
	}

	void Graph::rethrow_execution_error()
	{
		if(async_error_set_) {
			auto error       = std::move(async_error_);
			async_error_     = nullptr;
			async_error_set_ = false;
			std::rethrow_exception(std::move(error));
		}
	}

	void Graph::simulation_thread_main()
	{
		while(thread_state_.load() != Thread_state::quitting) {
			auto task = fetch_next_task();
			if(!task)
				break;

			thread_state_ = Thread_state::running;

			if(auto exception = execute_task(*task)) {
				if(async_error_set_ == false) {
					async_error_     = std::current_exception();
					async_error_set_ = true;
				} else {
					std::cerr << "Unhandled exception in simulation, that can't be stored because "
					             "rethrow_execution_error() hasn't been called since the last exception\n";
				}
			}
		}

		thread_state_ = Thread_state::done;
	}

	auto Graph::fetch_next_task() -> std::shared_ptr<Node>
	{
		using enum Node::Execution_state;

		auto lock = std::unique_lock{mutex_};

		do {
			thread_state_ = Thread_state::fetching;

			// first, check the main branch and return the first un-executed node
			if(main_branch_->execution_state_ == created) {
				for(auto n = main_branch_; n->predecessor_; n = n->predecessor_) {
					if(n->execution_state_.load() != created)
						break;

					const auto state = n->predecessor_->execution_state_.load();
					if(state == executed) {
						n->state_.reset();
						n->state_.emplace(*n->predecessor_->state_); // copy the previous state and execute the node
						n->execution_state_ = executing;
						return n->shared_from_this();
					}
				}
			}

			// depth-first-search, to find the next executable node
			fetch_next_task_stack_.clear();
			fetch_next_task_stack_.push_back(&root_);
			while(!fetch_next_task_stack_.empty()) {
				auto node = fetch_next_task_stack_.back();
				fetch_next_task_stack_.pop_back();

				// if any of the successors are ready to be executed => return the last of them (closest to main )
				if((*node)->execution_state_ == created) {
					prepare_execution(**node);
					return *node;
				}

				for(auto& child : (*node)->successors_) {
					const auto s = child->execution_state_.load();
					if(s != error) {
						fetch_next_task_stack_.push_back(&child);
					}
				}
			}

			// wait for new work to be pushed, i.e. new commands to be executed
			thread_state_ = Thread_state::waiting;
			thread_state_cv_.wait(lock, [&] { return thread_state_ != Thread_state::waiting; });

		} while(thread_state_ != Thread_state::quitting);

		return {};
	}

	void Graph::prepare_execution(Node& task) const
	{
		using enum Node::Execution_state;

		assert(task.predecessor_);
		assert(task.predecessor_->execution_state_ == executed);
		assert(task.predecessor_->state_.has_value());

		task.state_.reset();
		task.state_.emplace(*task.predecessor_->state_); // copy the previous state and execute the node
		task.execution_state_ = executing;
	}

	auto Graph::execute_task(Node& task) -> std::exception_ptr
	{
		auto exception = std::exception_ptr{};

		if(!task.state_)
			throw Simulation_not_ready{};

		auto& world = *task.state_;
		auto  error = false;

		try {
			task.command_->execute(world);
		} catch(const Exception& e) {
			task.error_ = e.error();
			error       = true;
		} catch(...) {
			exception = std::current_exception();
			error     = true;
		}

		// set the state, if the node hasn't been modified since we started executing
		auto expected = Node::Execution_state::executing;
		task.execution_state_.compare_exchange_strong(
		        expected, error ? Node::Execution_state::error : Node::Execution_state::executed);

		version_++;

		return exception;
	}


} // namespace yggdrasill::simulation
