#include "cursor.hpp"

#include "graph.hpp"

namespace yggdrasill::simulation {

	namespace {
		template <typename It>
		constexpr auto make_span(It begin, It end)
		{
			return std::span<std::remove_pointer_t<typename It::pointer>>(&(*begin), std::distance(begin, end));
		}
	} // namespace


	Cursor::Cursor(Graph& graph)
	  : graph_(&graph), node_(graph.root().shared_from_this()), node_address_(&graph.root())
	{
	}

	[[nodiscard]] bool Cursor::is_before(const Cursor& rhs) const
	{
		return path_.size() <= rhs.path_.size() && ranges::equal(path_, rhs.path().subspan(0, path_.size()));
	}

	void Cursor::undo(bool keep_branch)
	{
		auto n = node();
		if(!n)
			return;

		if(keep_branch && !n->successors_.empty()) {
			seek_forward();
		} else if(n->predecessor_->successors_.size() > 1) {
			seek_earlier_sibling() || seek_later_sibling() || seek_backward();
		} else {
			seek_backward();
		}

		graph_->undo(*n, keep_branch);

		if(auto new_n = node())
			seek(*new_n); // recalculate path
	}

	auto Cursor::execute(std::unique_ptr<Command> command) -> const Node&
	{
		auto n = node();
		assert(editable_ && n);
		graph_->execute(std::move(command), n.get());
		seek_forward_if_ready();
		return *node();
	}

	auto Cursor::seek(const Node& node, std::span<index_t> path) -> const Node*
	{
		node_         = node.shared_from_this();
		node_address_ = &node;
		if(path.empty()) {
			path_.clear();
			if(node.predecessor_) {
				auto prev = &node;
				for(auto n = node.predecessor_; n; n = n->predecessor_) {
					auto iter = std::find_if(n->successors_.begin(), n->successors_.end(), [&](auto& ptr) {
						return ptr.get() == prev;
					});
					assert(iter != n->successors_.end());
					path_.emplace_back(index_t(std::distance(n->successors_.begin(), iter)));
					prev = n;
				}
				std::reverse(path_.begin(), path_.end());
			}

		} else {
			path_.assign(path.begin(), path.end());
		}

		return &node;
	}
	auto Cursor::seek_backward() -> const Node*
	{
		auto n = node();
		if(!n)
			return nullptr;

		auto node = n->predecessor();
		if(!node)
			return nullptr;

		node_         = node->shared_from_this();
		node_address_ = node;
		path_.pop_back();
		return node;
	}
	auto Cursor::seek_forward(index_t next_node) -> const Node*
	{
		auto n = node();
		if(!n)
			return nullptr;

		if(next_node >= index_t(n->successors_.size()))
			return nullptr;

		auto& new_node = n->successors_[next_node];
		node_          = new_node;
		node_address_  = new_node.get();
		path_.push_back(next_node);
		return new_node.get();
	}
	auto Cursor::seek_forward() -> const Node*
	{
		if(successor_count() == 0)
			return nullptr;
		return seek_forward(successor_count() - 1);
	}
	auto Cursor::seek_forward_if_ready() -> const Node*
	{
		if(!can_seek_forward_if_ready())
			return nullptr;
		return seek_forward(successor_count() - 1);
	}
	auto Cursor::seek_later_sibling() -> const Node*
	{
		return seek_sibling(true);
	}
	auto Cursor::seek_earlier_sibling() -> const Node*
	{
		return seek_sibling(false);
	}

	auto Cursor::seek_sibling(bool later) -> const Node*
	{
		auto n = node();
		if(!n)
			return nullptr;

		auto steps       = 1;
		auto source_node = n.get();
		for(auto parent = n->predecessor_; parent; parent = parent->predecessor_) {
			auto current = std::find_if(parent->successors_.begin(), parent->successors_.end(), [&](auto& ptr) {
				return ptr.get() == source_node;
			});
			assert(current != parent->successors_.end());

			auto child_subrange = later ? make_span(current + 1, parent->successors_.end())
			                            : make_span(parent->successors_.begin(), current);
			if(!child_subrange.empty()) {
				if(auto child = find_first_child(child_subrange, !later, steps)) {
					seek(*child);
					return child.get();
				}
			}

			source_node = parent;
			steps++;
		}
		return nullptr;
	}
	auto Cursor::find_first_child(std::span<const std::shared_ptr<Node>> container, bool reverse, int steps) const
	        -> std::shared_ptr<const Node>
	{
		if(steps <= 1) {
			if(container.empty())
				return {};
			return reverse ? container.back() : container.front();
		}

		if(reverse) {
			for(auto& e : container | ranges::views::reverse)
				if(auto r = find_first_child(e->successors_, reverse, steps - 1))
					return r;

		} else {
			for(auto& e : container)
				if(auto r = find_first_child(e->successors_, reverse, steps - 1))
					return r;
		}

		return nullptr;
	}

	bool Cursor::can_seek_backward() const
	{
		auto n = node_.lock();
		return n && n->predecessor();
	}
	bool Cursor::can_seek_forward() const
	{
		auto n = node_.lock();
		return n && n->successors().size() > 0;
	}
	bool Cursor::can_seek_forward_if_ready() const
	{
		auto n = node_.lock();
		return n && n->successors().size() > 0 && n->successors_[successor_count() - 1]->ready();
	}

	[[nodiscard]] index_t Cursor::successor_count() const
	{
		auto n = node_.lock();
		if(!n)
			return 0;
		return static_cast<index_t>(n->successors().size());
	}

	[[nodiscard]] auto Cursor::world() const -> std::shared_ptr<const World>
	{
		auto n = node();
		return n && n->ready() ? std::shared_ptr<const World>(std::move(n), &n->state().world())
		                       : std::shared_ptr<const World>{};
	}
	[[nodiscard]] auto Cursor::config() const -> std::shared_ptr<const Dictionary>
	{
		auto n = node();
		return n && n->ready() ? std::shared_ptr<const Dictionary>(std::move(n), &n->state().config())
		                       : std::shared_ptr<const Dictionary>{};
	}

	[[nodiscard]] bool Cursor::ready() const
	{
		auto n = node();
		return n && n->ready();
	}
	[[nodiscard]] bool Cursor::editable() const
	{
		if(!graph_ || !graph_->editable())
			return false;

		auto n = node();
		return editable_ && n && n->ready() && n->type() != Command_type::root;
	}

	auto Cursor::graph_root() const -> const Node&
	{
		return graph_->root();
	}

} // namespace yggdrasill::simulation
