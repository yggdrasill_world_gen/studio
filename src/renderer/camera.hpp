#pragma once

#include "../graphic/vertex_object.hpp"
#include "../gui/config.hpp"
#include "../util/string.hpp"

#include <glm/mat4x4.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include <string>
#include <string_view>


namespace yggdrasill::renderer {

	extern const graphic::Vertex_layout vertex_layout_spherical;
	extern const graphic::Vertex_layout vertex_layout_flat;

	constexpr inline std::string_view default_camera_name = "Main";


	enum class View_mode { spherical, equirectangular };

	/// Representation of a viewpoint.
	///
	/// Is compiled to a Compiled_camera before being passed to the rendering code.
	struct Camera {
		float     zoom      = 0.75f;                ///< normalized distance from the surface [0.01, 1.0]
		glm::vec3 rotation  = {0.f, 0.f, 15.f};     ///< euler angles in degrees
		View_mode view_mode = View_mode::spherical; ///< flat projection or spherical mesh

		/// the centered point, in screen coordinates, when using a flat projection
		glm::vec2 viewport_shift = {0.5f, 0.5f};

		/// factor the elevation is multiplied by before adding it to the vertex positions (for spherical view_mode)
		float elevation_relief_scale = 100.f;

		/// automatically rotate the pitch angle of the camera (used for initial view / "presentation/demo mode")
		bool  auto_rotate       = false;
		float auto_rotate_speed = 15.0f;

		float clipping_cutoff = 0.85f;

		void      update(float dt);
		glm::vec2 calc_content_size(glm::vec2 viewport, bool clip_y = true) const;
	};

	extern const Camera default_spherical_camera;
	extern const Camera default_flat_camera;

	class Camera_map {
	  public:
		Camera_map();

		void save();
		void save(std::string_view key);
		void save(std::string_view key, Camera&);

		[[nodiscard]] Camera&       operator[](std::string&);
		[[nodiscard]] const Camera& operator[](std::string_view) const;

		Camera& emplace(std::string, const Camera&);

		auto begin() { return cameras_.begin(); }
		auto end() { return cameras_.end(); }

		auto begin() const { return cameras_.begin(); }
		auto end() const { return cameras_.end(); }

	  private:
		gui::Config*                          cfg_;
		util::String_map<std::string, Camera> cameras_;
	};


	struct Compiled_camera {
		glm::mat4 view      = glm::mat4{1.f};
		glm::mat4 proj      = glm::mat4{1.f};
		glm::mat4 inv_view  = glm::mat4{1.f};
		glm::mat4 view_proj = glm::mat4{1.f};

		glm::vec2 viewport_offset = glm::vec2{0.f, 0.f};
		glm::vec2 viewport_size   = glm::vec2{1.f, 1.f};

		View_mode view_mode;

		float view_distance = 1.f;
		float zoom          = 1.f;

		float elevation_relief_scale;
	};

	extern bool similar(const Compiled_camera&, const Compiled_camera&);

	extern Compiled_camera compile(
	        const Camera&, glm::vec2 viewport_offset, glm::vec2 viewport_size, float world_radius, float scale = 1.f);

} // namespace yggdrasill::renderer
