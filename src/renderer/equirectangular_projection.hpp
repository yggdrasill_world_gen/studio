#pragma once

#include <yggdrasill/mesh.hpp>
#include <yggdrasill/world.hpp>

#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include <vector>

namespace yggdrasill::renderer {

	struct Mesh_data {
		std::vector<Vec2>   positions;
		std::vector<Vertex> indices;
	};

	extern glm::vec2 equirectangular_projection(const glm::vec3& p);
	extern glm::vec2 equirectangular_projection(const glm::vec3& p, const glm::mat4& view);

	/**
	 * Projects the world (with the given view) onto a flat surface using.
	 *
	 * See: https://en.wikipedia.org/wiki/Equirectangular_projection
	 *
	 * The function inserts additional faces for boundary vertices to avoid folding for faces crossing the boundary.
	 * But there may still be deformed or missing faces exactly on the boundary for large faces, because the function won't
	 *   split faces but only duplicate them on the other side of the boundary.
	 */
	extern Mesh_data equirectangular_projection(Const_world_view world, const glm::mat4& view);

} // namespace yggdrasill::renderer
