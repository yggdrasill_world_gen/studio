#include "camera.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <imgui.h>
#include <imgui_internal.h>


namespace yggdrasill::renderer {

	namespace {
		constexpr auto zoom_min = 1.1f;
		constexpr auto zoom_max = 4.f;
	} // namespace

	const graphic::Vertex_layout vertex_layout_spherical =
	        graphic::Vertex_layout{graphic::Vertex_layout::Mode::triangles,
	                               graphic::vertex<std::uint32_t>("index", 0),
	                               graphic::vertex<Vec3>("position", 1)};

	const graphic::Vertex_layout vertex_layout_flat =
	        graphic::Vertex_layout{graphic::Vertex_layout::Mode::triangles,
	                               graphic::vertex<std::uint32_t>("index", 0),
	                               graphic::vertex<Vec2>("position", 1)};


	const Camera default_spherical_camera = Camera{
	        .zoom = 0.75f, .rotation = {0.f, 0.f, 15.f},
                 .view_mode = View_mode::spherical, .auto_rotate = false
    };
	const Camera default_flat_camera = Camera{
	        .zoom = 1.0f, .rotation = {0.f, 0.f, 0.f},
                 .view_mode = View_mode::equirectangular, .auto_rotate = false
    };

	Camera_map::Camera_map() : cfg_(&gui::Config::create(*ImGui::GetCurrentContext(), "cameras"))
	{
		cameras_.emplace(default_camera_name, default_spherical_camera);
		cameras_.emplace("Equirectangular", default_flat_camera);

		for(auto&& [key, props] : *cfg_) {
			auto& cam                  = cameras_[key];
			cam.zoom                   = props.get("zoom", cam.zoom);
			cam.rotation               = props.get("rotation", cam.rotation);
			cam.view_mode              = props.get("view_mode", cam.view_mode);
			cam.viewport_shift         = props.get("viewport_shift", cam.viewport_shift);
			cam.elevation_relief_scale = props.get("elevation_relief_scale", cam.elevation_relief_scale);
			cam.auto_rotate            = props.get("auto_rotate", cam.auto_rotate);
			cam.auto_rotate_speed      = props.get("auto_rotate_speed", cam.auto_rotate_speed);
		}
	}

	void Camera_map::save()
	{
		for(auto&& [key, cam] : cameras_) {
			save(key, cam);
		}
	}
	void Camera_map::save(std::string_view key)
	{
		save(key, cameras_.find(key)->second);
	}
	void Camera_map::save(std::string_view key, Camera& cam)
	{
		assert(&cameras_.find(key)->second == &cam);

		auto& props = (*cfg_)[key];
		props.set("zoom", cam.zoom);
		props.set("rotation", cam.rotation);
		props.set("view_mode", cam.view_mode);
		props.set("viewport_shift", cam.viewport_shift);
		props.set("elevation_relief_scale", cam.elevation_relief_scale);
		props.set("auto_rotate", cam.auto_rotate);
		props.set("auto_rotate_speed", cam.auto_rotate_speed);
	}


	Camera& Camera_map::operator[](std::string& key)
	{
		if(auto iter = cameras_.find(key); iter != cameras_.end()) {
			return iter->second;
		} else {
			key = default_camera_name;
			return cameras_.emplace(key, default_spherical_camera).first->second;
		}
	}
	const Camera& Camera_map::operator[](std::string_view key) const
	{
		if(auto iter = cameras_.find(key); iter != cameras_.end()) {
			return iter->second;
		} else {
			return default_spherical_camera;
		}
	}

	Camera& Camera_map::emplace(std::string key, const Camera& base)
	{
		return cameras_.emplace(std::move(key), base).first->second;
	}


	static bool similar(const glm::mat4& lhs, const glm::mat4& rhs)
	{
		for(int i = 0; i < 4; i++) {
			for(int j = 0; j < 4; j++) {
				if(std::abs(lhs[i][j] - rhs[i][j]) >= 0.0001f)
					return false;
			}
		}
		return true;
	}
	bool similar(const Compiled_camera& lhs, const Compiled_camera& rhs)
	{
		if(lhs.view_mode != rhs.view_mode)
			return false;

		if(std::abs(lhs.view_distance - rhs.view_distance) > 0.001f)
			return false;

		if(std::abs(lhs.zoom - rhs.zoom) > 0.001f)
			return false;

		if(std::abs(lhs.elevation_relief_scale - rhs.elevation_relief_scale) > 0.001f)
			return false;

		if(length2(lhs.viewport_offset - rhs.viewport_offset) > 0.001f)
			return false;

		if(!similar(lhs.view, rhs.view))
			return false;

		if(!similar(lhs.proj, rhs.proj))
			return false;

		return true;
	}

	Compiled_camera compile(
	        const Camera& camera, glm::vec2 viewport_offset, glm::vec2 viewport_size, float world_radius, float scale)
	{
		auto r = Compiled_camera{};

		viewport_offset *= scale;
		viewport_size *= scale;

		const auto content_size = camera.calc_content_size(viewport_size);
		r.viewport_size         = min(content_size, viewport_size);

		r.viewport_offset = viewport_offset + (viewport_size - r.viewport_size) / 2.f;
		r.zoom            = camera.zoom;
		r.view_mode       = camera.view_mode;

		r.view_distance = std::lerp(zoom_min, zoom_max, camera.zoom) * world_radius;

		r.elevation_relief_scale = camera.elevation_relief_scale;

		if(camera.view_mode == View_mode::spherical) {
			r.proj = glm::perspective(glm::radians(std::lerp(20.f, 60.f, camera.zoom)),
			                          float(r.viewport_size.x) / r.viewport_size.y,
			                          std::max(0.2f, r.view_distance - world_radius * 1.2f),
			                          r.view_distance * 1.2f + 2.f);

		} else {
			r.view_distance = zoom_max * world_radius;

			const auto pixel_size = camera.calc_content_size(viewport_size, false);

			const auto pixel_offset = camera.viewport_shift * scale + (pixel_size - content_size) / 2.f;

			const auto left  = pixel_offset.x / pixel_size.x;
			const auto right = (pixel_offset.x + r.viewport_size.x) / pixel_size.x;

			const auto top    = (pixel_size.y - pixel_offset.y) / pixel_size.y;
			const auto bottom = (pixel_size.y - pixel_offset.y - r.viewport_size.y) / pixel_size.y;

			r.proj = glm::ortho(left, right, bottom, top, -100.f, 100.f);
		}

		r.view = glm::lookAt(glm::vec3(0, 0, r.view_distance + 0.02f), // camera position
		                     glm::vec3(0, 0, 0),                       // look at origin
		                     glm::vec3(0, 1, 0)                        // Head is up
		                     )
		         * glm::toMat4(glm::quat{glm::radians(camera.rotation)});

		r.inv_view  = glm::inverse(r.view);
		r.view_proj = r.proj * r.view;

		return r;
	}

	void Camera::update(float dt)
	{
		if(auto_rotate && view_mode == View_mode::spherical) {
			rotation.x = glm::mix(rotation.x, 0.f, std::min(1.f, 10.f * dt));
			rotation.z = glm::mix(rotation.z, 15.f, std::min(1.f, 10.f * dt));
			rotation.y = std::fmod(rotation.y + dt * auto_rotate_speed, 360.f);
		}
	}
	glm::vec2 Camera::calc_content_size(glm::vec2 viewport, bool clip_y) const
	{
		if(view_mode == View_mode::spherical)
			return viewport;

		const auto fd = clip_y ? glm::vec2(2.f, clipping_cutoff) : glm::vec2{2.f, 1.f};

		viewport /= glm::vec2{2.f, 1.f};
		return std::min(viewport.x, viewport.y) * fd / zoom;
	}

} // namespace yggdrasill::renderer
