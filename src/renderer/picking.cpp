#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "picking.hpp"

#include "../util/func_traits.hpp"

#include <glm/gtx/transform.hpp>
#include <yggdrasill_shaders.hpp>

#include <cstring>

namespace yggdrasill::renderer {

	namespace {
		constexpr auto picking_area          = 1;
		constexpr auto half_inv_picking_area = 1.f / (picking_area / 2.f);
		constexpr auto layer_position        = Layer_definition<Vec3, Ref_type::vertex>("position");

		glm::mat4 calc_shift_matrix(const glm::vec2& viewport_offset, const glm::vec2& viewport_size, glm::vec2 mouse)
		{
			mouse -= viewport_offset; // transform mouse to viewport-space

			// construct matrix that transforms the clip-space of our small 4x4 viewport render to that of the passed viewport, so the mouse coordinates line up
			return glm::translate(glm::vec3{-half_inv_picking_area, -half_inv_picking_area, 0})
			       * glm::scale(glm::vec3{half_inv_picking_area, half_inv_picking_area, 1.f})
			       * glm::translate(glm::vec3{-mouse, 0})
			       * glm::scale(glm::vec3{viewport_size.x / 2.f, viewport_size.y / 2.f, 1.f})
			       * glm::translate(glm::vec3{1.f, 1.f, 0});
		}
	} // namespace

	Picking::Picking(graphic::Layer_cache& layer_cache)
	  : shader_flat_(graphic::Shader_compiler("picking (flat)")
	                         .vertex_shader("shaders::picking_flat_vert", shaders::picking_flat_vert)
	                         .fragment_shader("shaders::picking_frag", shaders::picking_frag)
	                         .vertex_layout(vertex_layout_flat)
	                         .build())
	  , shader_sphere_(graphic::Shader_compiler("picking (sphere)")
	                           .vertex_shader("shaders::picking_sphere_vert", shaders::picking_sphere_vert)
	                           .fragment_shader("shaders::picking_frag", shaders::picking_frag)
	                           .vertex_layout(vertex_layout_spherical)
	                           .build())
	  , vertex_positions_(layer_cache.texture(layer_position))
	{
		glBindFramebuffer(GL_FRAMEBUFFER, *frame_buffer_);

		// depth buffer
		glBindRenderbuffer(GL_RENDERBUFFER, *depth_buffer_);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, picking_area, picking_area);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, *depth_buffer_);

		// color buffers with the results
		for(std::size_t i = 0; i < result_buffer_.size(); ++i) {
			glBindRenderbuffer(GL_RENDERBUFFER, *result_buffer_[i]);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA32UI, picking_area, picking_area);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER,
			                          static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + i),
			                          GL_RENDERBUFFER,
			                          *result_buffer_[i]);
		}

		const auto attachments = std::array<GLenum, 2>{GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
		glDrawBuffers(static_cast<GLsizei>(attachments.size()), attachments.data());

		if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
			std::cerr << "Framebuffer created for picking is not complete!\n";
			std::abort();
		}
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// pixel buffers for async transfer
		for(auto& pbos : pixel_buffers_) {
			for(auto& p : pbos) {
				glBindBuffer(GL_PIXEL_PACK_BUFFER, *p);
				glBufferData(GL_PIXEL_PACK_BUFFER, sizeof(std::uint32_t) * 4, nullptr, GL_DYNAMIC_READ);
			}
		}
		glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
	}

	void Picking::update(
	        const Compiled_camera& state, int indices, Const_mesh_view mesh, float world_radius, const glm::vec2& mouse)
	{
		const auto vp_mouse_position = mouse - state.viewport_offset;

		if(vp_mouse_position.x < 0 || vp_mouse_position.y < 0 || vp_mouse_position.x >= state.viewport_size.x
		   || vp_mouse_position.y >= state.viewport_size.y) {
			pixel_buffer_populated_ = false;
			return;
		}


		// setup rendering
		glBindFramebuffer(GL_FRAMEBUFFER, *frame_buffer_);

		auto cleanup = util::scope_guard{[&] {
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			glEnable(GL_BLEND);
			graphic::enable_multisample();
		}};

		glViewport(0, 0, picking_area, picking_area);
		glScissor(0, 0, picking_area, picking_area);

		if(state.view_mode == View_mode::spherical) {
			glEnable(GL_DEPTH_TEST);
			glEnable(GL_POLYGON_OFFSET_FILL);
			glDepthFunc(GL_LEQUAL);
			glEnable(GL_DEPTH_CLAMP);
		} else {
			glDisable(GL_DEPTH_TEST);
		}

		glDisable(GL_BLEND);
		glEnable(GL_SCISSOR_TEST);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);

		glClearColor(1, 0, 0, 0);
		glClear(GL_DEPTH_BUFFER_BIT);
		const auto clear_color = std::array<GLuint, 4>{static_cast<GLuint>(no_vertex.index()),
		                                               static_cast<GLuint>(no_vertex.index()),
		                                               static_cast<GLuint>(no_vertex.index()),
		                                               0};
		const auto clear_pos   = std::array<GLuint, 4>{0, 0, 0, 0};
		glClearBufferuiv(GL_COLOR, 0, clear_color.data());
		glClearBufferuiv(GL_COLOR, 1, clear_pos.data());

		graphic::disable_multisample();


		// draw
		const auto shift     = calc_shift_matrix(state.viewport_offset, state.viewport_size, mouse);
		const auto spherical = state.view_mode == View_mode::spherical;
		auto&      shader    = spherical ? shader_sphere_ : shader_flat_;

		shader.bind();
		shader.set_uniform("indices", 0);
		shader.set_uniform("positions", 1);
		shader.set_uniform("transform", spherical ? shift * state.view_proj : shift * state.proj);
		shader.set_uniform("radius", world_radius);

		vertex_positions_->bind(1);

		glDrawArrays(GL_TRIANGLES, 0, indices);


		// calculate the nearest face/edge/vertex based on the three closest vertices
		last_result_ = Picking_result{};
		read_values().process([&](auto& shader_results) {
			const auto v0 = Vertex(static_cast<index_t>(shader_results[0]));
			const auto v1 = Vertex(static_cast<index_t>(shader_results[1]));
			const auto v2 = Vertex(static_cast<index_t>(shader_results[2]));

			if(v0 != no_vertex && v1 != no_vertex && v2 != no_vertex) {
				if(!v0.valid(mesh) || !v1.valid(mesh) || !v2.valid(mesh)) {
					// This is caused by outdated data returned from the async framebuffer read and can be safely ignored
					return;
				}

				const auto edges     = v0.edges(mesh);
				auto       prev_dest = (--edges.begin())->dest(mesh);

				for(auto e : edges) {
					const auto dest = e.dest(mesh);
					if(dest == v1) {
						auto p = Vec3();
						std::memcpy(&p.x, &shader_results[4], sizeof(p));
						auto meter_per_pixel = 0.f;
						std::memcpy(&meter_per_pixel, &shader_results[3], sizeof(meter_per_pixel));

						last_result_ = Picking_result{.vertex      = v0,
						                              .primal_edge = e,
						                              .dual_edge   = e.inv_rot(),
						                              .face = prev_dest == v2 ? e.right(mesh) : e.left(mesh),
						                              .interpolated_position = normalized(p) * world_radius,
						                              .meter_per_pixel       = meter_per_pixel};
						break;
					}
					prev_dest = dest;
				}
			}
		});
	}

	auto Picking::read_values() -> util::maybe<Shader_output>
	{
		while(glGetError() != GL_NO_ERROR) {
			// ignore any pre-existing error
		}

		swap(pixel_buffers_[0], pixel_buffers_[1]);

		auto _ = util::scope_guard([] { glBindBuffer(GL_PIXEL_PACK_BUFFER, 0); });

		// request data, to be read on next frame
		for(auto pbo_i = std::size_t(0); pbo_i < pixel_buffers_[0].size(); ++pbo_i) {
			glBindBuffer(GL_PIXEL_PACK_BUFFER, *pixel_buffers_[0][pbo_i]);
			glReadBuffer(static_cast<GLenum>(GL_COLOR_ATTACHMENT0 + pbo_i));
			glReadPixels(0, 0, 1, 1, GL_RGBA_INTEGER, GL_UNSIGNED_INT, nullptr);

			if(auto e = glGetError(); e != GL_NO_ERROR) {
				std::cerr << "glReadPixels of picking framebuffer failed: " << e << "\n";
				pixel_buffer_populated_ = false;
				return util::nothing;
			}
		}

		// read back data from last frame
		if(std::exchange(pixel_buffer_populated_, true)) {
			auto result = Shader_output{static_cast<GLuint>(no_vertex.index()),
			                            static_cast<GLuint>(no_vertex.index()),
			                            static_cast<GLuint>(no_vertex.index())};
			for(auto pbo_i = std::size_t(0); pbo_i < pixel_buffers_[1].size(); ++pbo_i) {
				constexpr auto pixel_size = sizeof(std::uint32_t) * 4;

				glBindBuffer(GL_PIXEL_PACK_BUFFER, *pixel_buffers_[1][pbo_i]);

#ifdef __EMSCRIPTEN__
				glGetBufferSubData(GL_PIXEL_PACK_BUFFER, 0, pixel_size, result.data() + 4 * pbo_i);
#else
				if(auto data = glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, pixel_size, GL_MAP_READ_BIT)) {
					std::memcpy(result.data() + 4 * pbo_i, data, pixel_size);
					glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
				} else {
					std::cerr << "Couldn't map the picking buffer to read the result: " << glGetError() << "\n";
					return util::nothing;
				}
#endif

				if(auto e = glGetError(); e != GL_NO_ERROR) {
					std::cerr << "glGetBufferSubData to access picking buffer result failed: " << e << "\n";
					return util::nothing;
				}
			}

			return result;
		}

		return util::nothing;
	}

} // namespace yggdrasill::renderer
