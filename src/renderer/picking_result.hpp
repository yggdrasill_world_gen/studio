#pragma once

#include <yggdrasill/mesh.hpp>

namespace yggdrasill::renderer {

	struct Picking_result {
		Vertex      vertex;
		Primal_edge primal_edge;
		Dual_edge   dual_edge;
		Face        face;
		Vec3        interpolated_position;
		float       meter_per_pixel = 0.f;
	};

} // namespace yggdrasill::renderer
