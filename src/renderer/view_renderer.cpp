#include <GL/glew.h>

#include "view_renderer.hpp"

#include "camera.hpp"
#include "equirectangular_projection.hpp"
#include "picking.hpp"

#include "../layer_renderer.hpp"
#include "../selection.hpp"
#include "../settings.hpp"
#include "../gui/ui_elements.hpp"
#include "../util/func_traits.hpp"
#include "../util/ranges.hpp"

#include <yggdrasill/mesh_utils.hpp>

#include <glm/gtc/constants.hpp>
#include <imgui.h>
#include <imgui_internal.h>
#include <range/v3/all.hpp>

namespace yggdrasill {
	extern ImFont* font_small;

	extern float current_time();
} // namespace yggdrasill

namespace yggdrasill::renderer {

	namespace {
		constexpr auto layer_position  = Layer_definition<Vec3, Ref_type::vertex>("position");
		constexpr auto layer_elevation = Layer_definition<float, Ref_type::vertex>("elevation");

		constexpr auto non_interactive_render_delay = 0.1f; // in seconds

		struct Geometry_data {
			graphic::Buffer  index_buffer  = graphic::create_dynamic_buffer<Vertex>(2048, false);
			graphic::Texture index_texture = {1024, 1, graphic::Texture_format::R_uint, {}, false};
			graphic::Buffer  position_buffer;
			bool             dirty = true;

			template <typename PositionType = Vec3>
			Geometry_data(PositionType = {})
			  : position_buffer(graphic::create_dynamic_buffer<PositionType>(2048, false))
			{
			}
			Geometry_data(Geometry_data&&) noexcept            = default;
			Geometry_data& operator=(Geometry_data&&) noexcept = default;
			virtual ~Geometry_data()                           = default;

			int bind(const graphic::Vertex_layout& layout) const
			{
				layout.build(index_buffer, position_buffer);
				index_texture.bind(0);
				return static_cast<int>(index_buffer.size());
			}
		};
		struct Sphere_geometry_data final : Geometry_data {
			float relief_scale = 1.f;

			Sphere_geometry_data() : Geometry_data(Vec3{}) {}
		};
		struct Projected_geometry_data final : Geometry_data {
			View_mode mode;
			glm::mat4 view_matrix;

			Projected_geometry_data() : Geometry_data(Vec2{}) {}
		};

		template <typename T, typename Predicate>
		std::shared_ptr<T> find_or_construct_geometry_data(std::vector<std::shared_ptr<T>>& v, Predicate&& predicate)
		{
			for(auto& geo : v) {
				if(predicate(*geo)) {
					return geo;
				}
			}

			// no match => reuse currently unused buffers
			for(auto& geo : v) {
				if(geo.use_count() == 1) { // only stored in `v` => safe to reuse
					geo->dirty = true;
					return geo;
				}
			}

			// no match => append
			return v.emplace_back(std::make_unique<T>());
		}

		struct Layer_view_state {
			const void*   id;
			bool          draw;
			float         alpha;
			std::uint32_t settings_version;

			/*implicit*/ Layer_view_state(const std::unique_ptr<Layer_renderer>& v)
			  : id(v.get()), draw(v->draw()), alpha(v->alpha()), settings_version(v->settings_version())
			{
			}

			[[maybe_unused]] friend auto operator<=>(const Layer_view_state&, const Layer_view_state&) = default;
		};

		struct Per_viewport_state {
			std::shared_ptr<Geometry_data> geometry;
			Compiled_camera                camera;

			std::vector<Layer_view_state> last_layer_views;

			util::maybe<graphic::Render_texture> render_target;

			float last_modified_time = 0;

			bool used_in_last_frame = false;

			void mark_dirty() { last_modified_time = current_time(); }

			bool matches(std::span<const std::unique_ptr<Layer_renderer>> layers) const
			{
				if(layers.size() != last_layer_views.size())
					return false;

				for(auto&& [curr, prev] : util::join(layers, last_layer_views)) {
					if(curr != prev)
						return false;
				}

				return true;
			}

			int bind_buffers() const
			{
				assert(geometry);

				switch(camera.view_mode) {
					case View_mode::spherical: return geometry->bind(vertex_layout_spherical);
					case View_mode::equirectangular: return geometry->bind(vertex_layout_flat);
				}
				assert("unreachable (invalid view_mode)");
				std::abort();
			}
		};

		struct Dirty_flags {
			bool mesh       = false;
			bool positions  = false;
			bool elevations = false;

			constexpr auto any() const { return mesh || positions || elevations; }
		};
	} // namespace

	struct View_renderer::State {
		const Settings& settings;

		bool data_changed = true;

		Const_world_view world;
		Selection        selection;
		float            world_radius = 0.f;

		Picking picking;
		bool    any_hovered = false;

		Data_version mesh_version;
		Data_version position_version;
		Data_version elevation_version;
		Dirty_flags  dirty_flags;

		std::vector<std::shared_ptr<Sphere_geometry_data>>    spherical_geometry;
		std::vector<std::shared_ptr<Projected_geometry_data>> projected_geometry;

		std::unordered_map<const void*, Per_viewport_state> per_viewport_states;

		State(const Settings& settings, graphic::Layer_cache& cache) : settings(settings), picking(cache) {}
	};

	View_renderer::View_renderer(const Settings& settings)
	  : state_(std::make_unique<State>(settings, layer_cache_))
	{
	}
	View_renderer::~View_renderer() = default;

	auto View_renderer::world() const -> Const_world_view
	{
		return state_->world;
	}
	auto View_renderer::get_hovered_geometry() const -> const util::maybe<Picking_result>&
	{
		static const util::maybe<Picking_result> none;
		if(!state_->any_hovered)
			return none;
		else
			return state_->picking.result();
	}

	void View_renderer::update(Const_world_view world, const Selection& selection)
	{
		// early return if the current world can't be drawn
		if(!world || world.mesh().face_max_index() < 0 || !world.unstructured_layer("meta")) {
			state_->world = {};
			return;
		}

		for(auto&& [key, cam] : cameras_)
			cam.update(ImGui::GetIO().DeltaTime);

		// check if any data changed, that would force us to recompute the mesh used passed to the shaders
		state_->dirty_flags.mesh       = world.mesh_modified(state_->mesh_version);
		state_->dirty_flags.positions  = world.layer_modified(layer_position, state_->position_version);
		state_->dirty_flags.elevations = world.layer_modified(layer_elevation, state_->elevation_version);

		// check if ANY data changed, that might force us to redraw (pretty blunt tool)
		state_->data_changed = layer_cache_.refresh(world) || state_->dirty_flags.any();
		state_->world        = world;
		state_->selection    = selection;
		state_->world_radius = world.required_unstructured_layer("meta")["radius"].get<float>();
		state_->any_hovered  = false;

		// remove unused data from cache
		std::erase_if(state_->per_viewport_states, [](auto& s) { return !s.second.used_in_last_frame; });

		// mark all cached data as unused
		for(auto&& [_, s] : state_->per_viewport_states)
			s.used_in_last_frame = false;

		// invalidate geometry data
		if(state_->dirty_flags.mesh || state_->dirty_flags.positions) {
			for(auto& g : state_->spherical_geometry)
				g->dirty = true;
			for(auto& g : state_->projected_geometry)
				g->dirty = true;

		} else if(state_->dirty_flags.elevations) {
			for(auto& g : state_->spherical_geometry)
				g->dirty |= g->relief_scale > 0.001f;
		}
	}

	namespace {
		// region Helper Functions
		bool similar(const glm::mat4& lhs, const glm::mat4& rhs)
		{
			for(int i = 0; i < 4; i++) {
				for(int j = 0; j < 4; j++) {
					if(std::abs(lhs[i][j] - rhs[i][j]) >= 0.000001f)
						return false;
				}
			}
			return true;
		}

		auto scaled_vertex_position(Vec3 p, float elevation, float relief_scale, float radius)
		{
			return p * radius + normalized(p) * elevation * relief_scale;
		}
		auto scaled_vertex_position(
		        Vertex v, const auto& vertex_position, auto vertex_elevation, float relief_scale, float radius)
		{
			auto p = vertex_position[v];

			if(!vertex_elevation)
				return p * radius;

			return scaled_vertex_position(p, (*vertex_elevation)[v], relief_scale, radius);
		}
		auto scaled_vertex_position(Vertex v, Vec3 p, auto vertex_elevation, float relief_scale, float radius)
		{
			if(!vertex_elevation)
				return p * radius;

			return scaled_vertex_position(p, (*vertex_elevation)[v], relief_scale, radius);
		}

		glm::vec2 to_glm(const ImVec2& v)
		{
			return v;
		}

		auto push_clip_rect(ImDrawList& draw_list, const Compiled_camera& camera)
		{
			// calculate clipping area (same as during rendering, but imgui uses a different origin for Y than OpenGL, which we have to account for)
			const auto clip_scale = ImGui::GetIO().DisplayFramebufferScale;
			auto       clip_min =
			        ImVec2(camera.viewport_offset.x,
			               ImGui::GetIO().DisplaySize.y - camera.viewport_offset.y - camera.viewport_size.y)
			        / clip_scale;
			auto clip_max = clip_min + camera.viewport_size / clip_scale;
			draw_list.PushClipRect(clip_min, clip_max, true);
			return util::scope_guard{[&] { draw_list.PopClipRect(); }};
		}
		// endregion

		auto build_camera(const Camera& camera, float world_radius) -> std::optional<Compiled_camera>
		{
			// calculate the area we should draw inside with OpenGL (in framebuffer space)
			const auto scale   = to_glm(ImGui::GetIO().DisplayFramebufferScale);
			const auto fb_size = to_glm(ImGui::GetWindowViewport()->Size) * scale;
			const auto fb_pos  = to_glm(ImGui::GetWindowViewport()->Pos);

			const auto top_left     = (to_glm(ImGui::GetWindowDrawList()->GetClipRectMin()) - fb_pos) * scale;
			const auto bottom_right = (to_glm(ImGui::GetWindowDrawList()->GetClipRectMax()) - fb_pos) * scale;

			const auto offset = glm::vec2{top_left.x, fb_size.y - bottom_right.y};
			const auto size   = glm::vec2{bottom_right.x - top_left.x, bottom_right.y - top_left.y};

			if(offset.x < fb_size.x && offset.y < fb_size.y && size.x >= 0.0f && size.y >= 0.0f) {
				return compile(camera, offset, size, world_radius);
			} else {
				return std::nullopt;
			}
		}

		void generate_geometry(Const_world_view world, const Compiled_camera& camera, Sphere_geometry_data& geo)
		{
			const auto radius = world.required_unstructured_layer("meta")["radius"].get<float>();

			// upload new data
			geo.dirty        = false;
			geo.relief_scale = camera.elevation_relief_scale;

			auto mesh             = world.mesh();
			auto vertex_position  = world.required_layer(layer_position);
			auto vertex_elevation = world.layer(layer_elevation);

			auto indices = std::vector<Vertex>();
			indices.reserve((mesh.face_max_index() + 1) * 3);

			auto positions = std::vector<Vec3>();
			positions.reserve((mesh.face_max_index() + 1) * 3);

			for(const Face face : mesh.faces()) {
				for(const auto v : face.vertices(mesh)) {
					indices.push_back(v);
					positions.push_back(scaled_vertex_position(
					        v, vertex_position, vertex_elevation, geo.relief_scale, radius));
				}
			}

			geo.position_buffer.set(positions);
			geo.index_buffer.set(indices);
			geo.index_texture.update_data(std::min(static_cast<int>(indices.size()), graphic::max_texture_size),
			                              1 + static_cast<int>(indices.size()) / graphic::max_texture_size,
			                              indices);
		}
		void generate_geometry(Const_world_view world, const Compiled_camera& camera, Projected_geometry_data& geo)
		{
			geo.dirty       = false;
			geo.mode        = camera.view_mode;
			geo.view_matrix = camera.view;

			const auto mesh = equirectangular_projection(world, camera.view);
			geo.position_buffer.set(mesh.positions);
			geo.index_buffer.set(mesh.indices);
			geo.index_texture.update_data(
			        std::min(static_cast<int>(mesh.indices.size()), graphic::max_texture_size),
			        1 + static_cast<int>(mesh.indices.size()) / graphic::max_texture_size,
			        mesh.indices);
		}

		auto generate_geometry(Const_world_view world, const Compiled_camera& camera)
		        -> std::shared_ptr<Geometry_data>
		{
			switch(camera.view_mode) {
				case View_mode::spherical: {
					auto out = std::make_shared<Sphere_geometry_data>();
					generate_geometry(world, camera, *out);
					return out;
				}
				case View_mode::equirectangular: {
					auto out = std::make_shared<Projected_geometry_data>();
					generate_geometry(world, camera, *out);
					return out;
				}
			}
			assert("unreachable (invalid view_mode)");
			std::abort();
		}

		auto get_geometry(View_renderer::State& state, const Compiled_camera& camera)
		        -> std::shared_ptr<Geometry_data>
		{
			switch(camera.view_mode) {
				case View_mode::spherical: {
					auto geo = find_or_construct_geometry_data(state.spherical_geometry, [&](const auto& g) {
						return std::abs(g.relief_scale - camera.elevation_relief_scale) < 0.01f;
					});
					if(geo->dirty) {
						generate_geometry(state.world, camera, *geo);
					}
					return geo;
				}

				case View_mode::equirectangular: {
					auto geo = find_or_construct_geometry_data(state.projected_geometry, [&](const auto& g) {
						return camera.view_mode == g.mode && similar(camera.view, g.view_matrix);
					});
					if(geo->dirty) {
						generate_geometry(state.world, camera, *geo);
					}
					return geo;
				}
			}
			assert("unreachable (invalid view_mode)");
			std::abort();
		}

		void update_picking(
		        const Per_viewport_state& view_state, Const_mesh_view mesh, float world_radius, Picking& picking)
		{
			const auto fb_size =
			        to_glm(ImGui::GetWindowViewport()->Size) * to_glm(ImGui::GetIO().DisplayFramebufferScale);

			const auto mouse_imgui = ImGui::GetIO().MousePos;
			const auto mouse_gl    = glm::vec2{mouse_imgui.x, fb_size.y - mouse_imgui.y};
			picking.update(view_state.camera, view_state.bind_buffers(), mesh, world_radius, mouse_gl);
		}

		auto draw_layers(Const_world_view                                 world,
		                 const Settings&                                  settings,
		                 Per_viewport_state&                              state,
		                 const Geometry_data&                             geometry,
		                 const Camera&                                    raw_camera,
		                 float                                            vp_width,
		                 float                                            vp_height,
		                 float                                            scale,
		                 float                                            world_radius,
		                 std::span<const std::unique_ptr<Layer_renderer>> layers) -> const graphic::Texture&
		{
			if(state.render_target.is_nothing()
			   || state.render_target.get_or_throw().width() != int(vp_width * scale)
			   || state.render_target.get_or_throw().height() != vp_height) {
				state.render_target.emplace(int(vp_width * scale), int(vp_height * scale), true);

			} else if(state.last_modified_time < current_time()) {
				// state and view didn't change => reuse last result
				return state.render_target.get_or_throw();
			}

			state.last_layer_views.clear();
			for(auto& v : layers) {
				state.last_layer_views.emplace_back(v);
			}

			auto& render_target = state.render_target.get_or_throw();

			const auto camera = compile(raw_camera, {0, 0}, {vp_width, vp_height}, world_radius, scale);

			graphic::disable_multisample();

			auto _ = render_target.bind_target();

			glClearColor(0, 0, 0, 0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

			glScissor(static_cast<int>(camera.viewport_offset.x),
			          static_cast<int>(camera.viewport_offset.y),
			          static_cast<int>(camera.viewport_size.x),
			          static_cast<int>(camera.viewport_size.y));
			glViewport(static_cast<int>(camera.viewport_offset.x),
			           static_cast<int>(camera.viewport_offset.y),
			           static_cast<int>(camera.viewport_size.x),
			           static_cast<int>(camera.viewport_size.y));

			if(camera.view_mode == View_mode::spherical) {
				glEnable(GL_DEPTH_TEST);
				glEnable(GL_POLYGON_OFFSET_FILL);
				glDepthFunc(GL_LEQUAL);

				glEnable(GL_DEPTH_CLAMP);
			}

			glEnable(GL_BLEND);
			glBlendEquation(GL_FUNC_ADD);
			glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE);
			glEnable(GL_SCISSOR_TEST);

			glEnable(GL_CULL_FACE);
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);
			glLineWidth(2);

			const auto index_count = state.bind_buffers();

			// draw the planet
			for(bool first = true; auto& layer : layers) {
				if(layer->draw()) {
					layer->draw_planet(world, camera, index_count);
				}

				if(first) {
					first = false;
					glPolygonOffset(-0.5f, -0.5f);
					glDepthMask(GL_FALSE);
				}
			}

			glPolygonOffset(0, 0);
			glDepthMask(GL_TRUE);

			// draw additional elements
			for(auto& layer : layers) {
				if(layer->draw()) {
					layer->draw_gizmos(world, camera);
				}
			}

			glDisable(GL_SCISSOR_TEST);

			graphic::enable_multisample();

			return render_target;
		}

		void draw_selection(const Compiled_camera& camera, Const_world_view world, const Selection& selection)
		{
			auto& draw_list = *ImGui::GetWindowDrawList();

			const auto fb_size =
			        to_glm(ImGui::GetWindowViewport()->Size) * to_glm(ImGui::GetIO().DisplayFramebufferScale);

			auto       mesh       = world.mesh();
			auto       positions  = world.required_layer(layer_position);
			auto       elevations = world.layer(layer_elevation);
			const auto radius     = world.required_unstructured_layer("meta")["radius"].get<float>();

			auto vertex_p = [&](Vertex v) {
				return scaled_vertex_position(v, positions, elevations, camera.elevation_relief_scale, radius);
			};
			auto scaled_vertex_p = [&](Vertex v, const Vec3& p) {
				if(!v.valid(mesh))
					return p;
				return scaled_vertex_position(v, p, elevations, camera.elevation_relief_scale, 1.f);
			};
			auto imgui_point = [&](Vec3 p) {
				const auto& size   = camera.viewport_size;
				const auto& offset = camera.viewport_offset;

				if(camera.view_mode == View_mode::spherical) {
					auto p_proj = camera.view_proj * glm::vec4(p.x, p.y, p.z, 1.f);
					p_proj /= p_proj.w;

					return ImVec2{(p_proj.x + 1.f) * (size.x / 2.f) + offset.x,
					              fb_size.y - ((p_proj.y + 1.f) * (size.y / 2.f) + offset.y)};
				} else {
					auto flat   = equirectangular_projection(glm::vec3{p.x, p.y, p.z}, camera.view);
					auto p_proj = camera.proj * glm::vec4(flat.x, flat.y, 0.f, 1.f);
					p_proj /= p_proj.w;

					return ImVec2{(p_proj.x + 1.f) * (size.x / 2.f) + offset.x,
					              fb_size.y - ((p_proj.y + 1.f) * (size.y / 2.f) + offset.y)};
				}
			};
			auto draw_arrow = [&](ImVec2 origin, ImVec2 dest, ImU32 color, float thickness) {
				const auto diff         = Vec2(dest.x, dest.y) - Vec2(origin.x, origin.y);
				const auto diff_len     = length(diff);
				const auto arrow_length = std::clamp(0.1f * diff_len, 5.f, 20.f);
				const auto dir          = diff / diff_len;
				const auto p1 = Vec2(dest.x, dest.y) - dir * arrow_length + Vec2(-dir.y, dir.x) * arrow_length;
				const auto p2 = Vec2(dest.x, dest.y) - dir * arrow_length + Vec2(dir.y, -dir.x) * arrow_length;

				draw_list.AddLine(origin, dest, color, thickness);
				draw_list.AddLine(ImVec2(p1.x, p1.y), dest, color, thickness);
				draw_list.AddLine(ImVec2(p2.x, p2.y), dest, color, thickness);
			};

			selection.foreach_highlight(
			        [&](Face f, ImU32 color) {
				        if(f.valid(mesh)) {
					        std::array<ImVec2, 3> face_pos;
					        for(int i = 0; auto v : f.vertices(mesh)) {
						        face_pos[i++] = imgui_point(vertex_p(v));
					        }
					        draw_list.AddTriangleFilled(face_pos[0], face_pos[1], face_pos[2], color);
				        }
			        },
			        [&](Primal_edge e, ImU32 color, float thickness) {
				        if(e.valid(mesh)) {
					        draw_arrow(imgui_point(vertex_p(e.origin(mesh))),
					                   imgui_point(vertex_p(e.dest(mesh))),
					                   color,
					                   thickness);
				        }
			        },
			        [&](Dual_edge e, ImU32 color, float thickness) {
				        if(e.valid(mesh)) {
					        const auto origin_vertices = e.origin(mesh).vertices(mesh);
					        const auto origin_p = circumcenter_non_normalized(vertex_p(origin_vertices[0]),
					                                                          vertex_p(origin_vertices[1]),
					                                                          vertex_p(origin_vertices[2]));
					        const auto dest_vertices = e.dest(mesh).vertices(mesh);
					        const auto dest_p        = circumcenter_non_normalized(vertex_p(dest_vertices[0]),
                                                                            vertex_p(dest_vertices[1]),
                                                                            vertex_p(dest_vertices[2]));

					        draw_arrow(imgui_point(origin_p), imgui_point(dest_p), color, thickness);
				        }
			        },
			        [&](Vertex v, ImU32 color, float thickness) {
				        if(v.valid(mesh)) {
					        draw_list.AddCircleFilled(imgui_point(vertex_p(v)), thickness, color);
				        }
			        },
			        [&](Highlight_world_position p, ImU32 color, float thickness) {
				        draw_list.AddCircleFilled(
				                imgui_point(scaled_vertex_p(p.nearest_vertex, p.world_position)), thickness, color);
			        },
			        [&](Highlight_world_position a,
			            Highlight_world_position b,
			            float                    segment_length,
			            ImU32                    color_a,
			            ImU32                    color_b,
			            float                    thickness) {
				        const auto a_pos        = scaled_vertex_p(a.nearest_vertex, a.world_position);
				        const auto a_pos_length = length(a_pos);
				        const auto a_pos_norm   = a_pos / a_pos_length;
				        const auto b_pos        = scaled_vertex_p(b.nearest_vertex, b.world_position);
				        const auto b_pos_length = length(b_pos);
				        const auto b_pos_norm   = b_pos / b_pos_length;

				        const auto ab_dot   = dot(a_pos_norm, b_pos_norm);
				        const auto ab_angle = std::acos(ab_dot);
				        const auto ab_dist  = ab_angle * (a_pos_length + b_pos_length) / 2.f;
				        const auto segments = static_cast<int>(std::ceil(ab_dist / segment_length));

				        auto _ = push_clip_rect(draw_list, camera);

				        const auto sphere = camera.view_mode == View_mode::spherical;

				        const auto line = [&](auto p1, auto p2, bool even) {
					        if(sphere) {
						        draw_list.AddLine(
						                imgui_point(p1), imgui_point(p2), even ? color_a : color_b, thickness);

					        } else {
						        // To determine if the line should be clipped, we need to check if it crosses from the far left to the far right (or vice versa).
						        // But that check has to happen before the points are projected to screen-space to be zoom independent.
						        // So the below code inlines imgui_point() to add the check after the equirectangular_projection() but before projecting to screen space.
						        const auto p1_flat =
						                equirectangular_projection(glm::vec3{p1.x, p1.y, p1.z}, camera.view);
						        const auto p2_flat =
						                equirectangular_projection(glm::vec3{p2.x, p2.y, p2.z}, camera.view);

						        if(std::abs(p1_flat.x - p2_flat.x) > 0.9f)
							        return; // clip

						        auto project = [&](auto& p) {
							        const auto& size   = camera.viewport_size;
							        const auto& offset = camera.viewport_offset;

							        auto p_proj = camera.proj * glm::vec4(p.x, p.y, 0.f, 1.f);
							        p_proj /= p_proj.w;

							        return ImVec2{(p_proj.x + 1.f) * (size.x / 2.f) + offset.x,
							                      fb_size.y - ((p_proj.y + 1.f) * (size.y / 2.f) + offset.y)};
						        };

						        draw_list.AddLine(
						                project(p1_flat), project(p2_flat), even ? color_a : color_b, thickness);
					        }
				        };

				        auto last_p = a_pos;
				        for(auto i = 1; i < segments; ++i) {
					        // slerp to get next end position
					        const auto alpha = ab_angle * float(i) / segments;
					        const auto p     = a_pos_norm * std::cos(alpha)
					                       + normalized(b_pos_norm - a_pos_norm * ab_dot) * std::sin(alpha);
					        const auto radius = std::lerp(a_pos_length, b_pos_length, float(i) / segments);

					        const auto next_p = normalized(p) * radius;
					        line(last_p, next_p, (i % 2) == 1);
					        last_p = next_p;
				        }
				        line(last_p, b_pos, (segments % 2) == 1);
			        },
			        [&](Highlight_world_position position, float radius, ImU32 color) {
				        constexpr auto brush_border = 2.f;

				        const auto fill_color = gui::apply_alpha(color, 0.2f);

				        const auto center = scaled_vertex_p(position.nearest_vertex, position.world_position);
				        const auto planet_radius = length(center);
				        const auto normal        = center / planet_radius;
				        const auto tangent       = cross(normal, Vec3(-normal.z, normal.x, normal.y));
				        const auto bitangent     = cross(normal, tangent);

				        constexpr auto segments       = 32;
				        constexpr auto step_size      = 2.f * glm::pi<float>() / segments;
				        auto           brush_vertices = std::array<ImVec2, segments>();
				        for(auto i = 0; i < segments; ++i) {
					        const auto a = i * step_size;
					        const auto p =
					                normalized(center
					                           + normalized(tangent * std::sin(a) + bitangent * std::cos(a)) * radius)
					                * planet_radius;
					        brush_vertices[i] = imgui_point(p);
				        }

				        if(camera.view_mode == View_mode::spherical) {
					        draw_list.AddConvexPolyFilled(brush_vertices.data(), segments, fill_color);
					        draw_list.AddPolyline(brush_vertices.data(), segments, color, ImDrawFlags_Closed, 2.f);

				        } else {
					        auto _ = push_clip_rect(draw_list, camera);

					        // coordinates might cross the left/right border
					        //   => special handling if the center is closer to the border than the center
					        const auto center_to_border =
					                std::abs((imgui_point(center).x - camera.viewport_offset.x)
					                                 / camera.viewport_size.x
					                         - 0.5)
					                * 2;
					        const auto center_near_border = center_to_border >= 0.5f;
					        if(center_near_border) {
						        const auto left = camera.viewport_offset.x - brush_border * 2;
						        const auto right =
						                camera.viewport_size.x + camera.viewport_offset.x + brush_border * 2;
						        const auto center_line = camera.viewport_size.x / 2.f + camera.viewport_offset.x;
						        const auto first_buffer_left = brush_vertices[0].x < center_line;

						        // we create a second copy of the vertices and snap all vertices that cross the
						        //   border to the left/right side respectively
						        auto cut                   = false;
						        auto second_brush_vertices = brush_vertices;
						        second_brush_vertices[0].x = first_buffer_left ? right : left;

						        for(auto i = 1; i < segments; ++i) {
							        if(first_buffer_left) {
								        if(brush_vertices[i].x > center_line) {
									        brush_vertices[i].x = left;
									        cut                 = true;
								        } else {
									        second_brush_vertices[i].x = right;
								        }
							        } else {
								        if(brush_vertices[i].x < center_line) {
									        brush_vertices[i].x = right;
									        cut                 = true;
								        } else {
									        second_brush_vertices[i].x = left;
								        }
							        }
						        }

						        if(cut) {
							        // there was at least one vertex that crossed the border
							        //   => we have to draw a second brush
							        draw_list.AddPolyline(
							                second_brush_vertices.data(), segments, color, ImDrawFlags_Closed, 2.f);
						        }
					        }

					        draw_list.AddPolyline(brush_vertices.data(), segments, color, ImDrawFlags_Closed, 2.f);
				        }
			        });

			selection.foreach_label([&](Vertex             nearest_vertex,
			                            util::maybe<Vec3>  world_position,
			                            std::string        text,
			                            util::maybe<ImU32> color) {
				ImGui::PushFont(nullptr);
				ON_EXIT(ImGui::PopFont());

				const auto p =
				        imgui_point(world_position.is_some()
				                            ? scaled_vertex_p(nearest_vertex, world_position.get_or_throw())
				                            : vertex_p(nearest_vertex));
				const auto size = ImGui::CalcTextSize(text.c_str()) + ImGui::GetStyle().WindowPadding;

				draw_list.AddRectFilled(p,
				                        p + size,
				                        gui::apply_alpha(ImGui::GetColorU32(ImGuiCol_PopupBg), 0.6f),
				                        ImGui::GetStyle().WindowRounding,
				                        0);

				draw_list.AddRect(p,
				                  p + size,
				                  ImGui::GetColorU32(ImGuiCol_Border),
				                  ImGui::GetStyle().WindowRounding,
				                  0,
				                  ImGui::GetStyle().WindowBorderSize);

				draw_list.AddText(p + ImGui::GetStyle().WindowPadding / 2.f,
				                  color.get_or(ImGui::GetColorU32(ImGuiCol_Text)),
				                  text.c_str());
			});
		}

	} // namespace

	bool View_renderer::draw(
	        const void* owner, const Camera& camera, std::span<const std::unique_ptr<Layer_renderer>> layers)
	{
		if(!state_->world)
			return false;

		// calculate view area
		ImGui::PushClipRect(ImGui::GetWindowPos(), ImGui::GetWindowPos() + ImGui::GetWindowSize(), true);
		ON_EXIT(ImGui::PopClipRect());
		const auto compiled_camera = build_camera(camera, state_->world_radius);

		// early return, if the viewport isn't visible
		if(!compiled_camera)
			return false;

		auto& view_state = state_->per_viewport_states[owner];
		YGGDRASILL_INVARIANT(!view_state.used_in_last_frame, "ID collision in View_renderer");
		view_state.used_in_last_frame = true;
		if(state_->data_changed || !similar(view_state.camera, *compiled_camera) || !view_state.matches(layers)) {
			view_state.mark_dirty();
		}

		// regenerate buffers and textures, if required
		view_state.camera   = *compiled_camera;
		view_state.geometry = get_geometry(*state_, *compiled_camera);


		const auto vp_size =
		        ImGui::GetWindowDrawList()->GetClipRectMax() - ImGui::GetWindowDrawList()->GetClipRectMin();

		const auto interactive = (current_time() - view_state.last_modified_time) < non_interactive_render_delay;
		const auto scale = interactive ? state_->settings.render_scale_interactive()
		                               : state_->settings.render_scale_static();
		if(interactive) {
			ImGui::SetMaxWaitBeforeNextFrame(camera.auto_rotate ? 0 : non_interactive_render_delay);
		}

		// draw layers into texture (if modified) and send it to ImGui
		auto& drawn = draw_layers(state_->world,
		                          state_->settings,
		                          view_state,
		                          *view_state.geometry,
		                          camera,
		                          vp_size.x,
		                          vp_size.y,
		                          scale,
		                          state_->world_radius,
		                          layers);
		ImGui::GetWindowDrawList()->AddImage(
		        reinterpret_cast<ImTextureID>(static_cast<std::ptrdiff_t>(drawn.unsafe_low_level_handle())),
		        ImGui::GetWindowDrawList()->GetClipRectMin(),
		        ImGui::GetWindowDrawList()->GetClipRectMax(),
		        ImVec2{0, 1},
		        ImVec2{1, 0});

		// draw highlighting
		draw_selection(*compiled_camera, state_->world, state_->selection);

		// if hovered, also calculate hovered geometry
		if(!state_->any_hovered && ImGui::IsWindowHovered()
		   && ImGui::IsMouseHoveringRect(ImGui::GetWindowDrawList()->GetClipRectMin(),
		                                 ImGui::GetWindowDrawList()->GetClipRectMax())) {
			state_->any_hovered = true;
			update_picking(view_state, state_->world.mesh(), state_->world_radius, state_->picking);
		}

		return true;
	}

	auto View_renderer::draw_to_texture(Const_world_view                                 world_in,
	                                    const Camera&                                    camera,
	                                    std::span<const std::unique_ptr<Layer_renderer>> layers,
	                                    int                                              width,
	                                    int height) -> std::optional<graphic::Render_texture>
	{
		auto world = world_in ? world_in : state_->world;

		if(!world || world.mesh().face_max_index() < 0 || !world.unstructured_layer("meta"))
			return std::nullopt;

		const auto world_radius    = world.required_unstructured_layer("meta")["radius"].get<float>();
		const auto compiled_camera = compile(camera, {0, 0}, {width, height}, world_radius);

		auto view_state   = Per_viewport_state{};
		view_state.camera = compiled_camera;
		view_state.geometry =
		        world_in ? generate_geometry(world, compiled_camera) : get_geometry(*state_, compiled_camera);

		if(world_in)
			layer_cache_.refresh(world);

		draw_layers(world,
		            state_->settings,
		            view_state,
		            *view_state.geometry,
		            camera,
		            static_cast<float>(width),
		            static_cast<float>(height),
		            1.f,
		            world_radius,
		            layers);

		if(world_in)
			layer_cache_.refresh(state_->world);

		return std::move(view_state.render_target.get_or_throw());
	}

} // namespace yggdrasill::renderer
