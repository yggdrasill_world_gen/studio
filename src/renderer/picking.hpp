#pragma once

#include <GL/glew.h>

#include "camera.hpp"
#include "picking_result.hpp"

#include "../graphic/gl_handle.hpp"
#include "../graphic/layer_backed.hpp"
#include "../graphic/shader.hpp"
#include "../util/maybe.hpp"

#include <yggdrasill/mesh.hpp>

#include <glm/glm.hpp>

#include <array>


namespace yggdrasill::renderer {

	class Picking {
	  public:
		Picking(graphic::Layer_cache&);

		/// called by the view_renderer
		void update(const Compiled_camera& state,
		            int                    indices,
		            Const_mesh_view        mesh,
		            float                  world_radius,
		            const glm::vec2&       mouse);

		[[nodiscard]] auto& result() const { return last_result_; }

	  private:
		struct Gl_framebuffer_handle_trait {
			using value_type = GLuint;
			static value_type create()
			{
				auto h = value_type(0);
				glGenFramebuffers(1, &h);
				return h;
			}
			static void destroy(value_type h) { glDeleteFramebuffers(1, &h); }
		};
		struct Gl_renderbuffer_handle_trait {
			using value_type = GLuint;
			static value_type create()
			{
				auto h = value_type(0);
				glGenRenderbuffers(1, &h);
				return h;
			}
			static void destroy(value_type h) { glDeleteRenderbuffers(1, &h); }
		};
		struct Gl_pixelbuffer_handle_trait {
			using value_type = GLuint;
			static value_type create()
			{
				auto h = value_type(0);
				glGenBuffers(1, &h);
				return h;
			}
			static void destroy(value_type h) { glDeleteBuffers(1, &h); }
		};
		using Fb_handle  = graphic::Gl_handle<Gl_framebuffer_handle_trait>;
		using Rb_handle  = graphic::Gl_handle<Gl_renderbuffer_handle_trait>;
		using Pbo_handle = graphic::Gl_handle<Gl_pixelbuffer_handle_trait>;

		using Shader_output = std::array<std::uint32_t, 4 * 2>;
		using Pixel_buffers = std::array<Pbo_handle, 2>; //< one for each color attachment

		Fb_handle                    frame_buffer_;
		Rb_handle                    depth_buffer_;
		std::array<Rb_handle, 2>     result_buffer_;
		std::array<Pixel_buffers, 2> pixel_buffers_;
		bool                         pixel_buffer_populated_ = false;
		graphic::Shader              shader_flat_;
		graphic::Shader              shader_sphere_;

		graphic::Layer_backed<graphic::Texture> vertex_positions_;

		util::maybe<Picking_result> last_result_;

		util::maybe<Shader_output> read_values();
	};

} // namespace yggdrasill::renderer
