#include "equirectangular_projection.hpp"

#include "../util/ranges.hpp"

#include <yggdrasill/layer.hpp>
#include <yggdrasill/mesh_utils.hpp>

#include <glm/gtc/matrix_transform.hpp>

#include <bitset>


namespace yggdrasill::renderer {

	namespace {
		constexpr auto layer_position = Layer_definition<Vec3, Ref_type::vertex>("position");

		struct Lat_long {
			float longitude; /// range: +-PI, -PI := right
			float latitude;  /// range: +-PI/2, -PI/2 := top
		};

		Lat_long to_lat_long(const Vec3& p)
		{
			auto atan_res = 0.f;
			if constexpr(std::numeric_limits<float>::is_iec559) {
				atan_res = std::atan2(p.z, p.x);
			} else {
				atan_res = std::abs(p.x) < 1e-8f ? (p.z < 0.f ? 0.f : glm::pi<float>()) : std::atan2(p.z, p.x);
			}
			return {atan_res, std::asin(p.y)};
		}
		Vec3 project(const glm::mat4& view, const Vec3& p)
		{
			const auto p_4d = view * glm::vec4(p.x, p.y, p.z, 0.f);
			return normalized(Vec3(p_4d.x, p_4d.y, p_4d.z));
		}

		/// map the geographic coordinates to the range (0-1)
		Vec2 to_uv(Lat_long p)
		{
			// includes a small factor <1 on Y component, to avoid artifacts by missing texture clamping in exports
			return {(-p.longitude / glm::pi<float>() / 2.f + 0.5f),
			        (p.latitude * 0.98f / glm::pi<float>() + 0.5f)};
		}

		template <typename... args>
		auto make_bitset(args... bools) -> std::bitset<sizeof...(bools)>
		{
			char data[] = {bools ? '1' : '0' ..., '\0'};
			return std::bitset<sizeof...(bools)>(data, sizeof...(bools));
		}

		inline bool is_face_folded(const Lat_long& a, const Lat_long& b, const Lat_long& c)
		{
			return yggdrasill::is_wrong_winding_order({to_uv(a), 1}, {to_uv(b), 1}, {to_uv(c), 1});
		}
	} // namespace

	glm::vec2 equirectangular_projection(const glm::vec3& p, const glm::mat4& view)
	{
		auto r = to_uv(to_lat_long(project(view, {p.x, p.y, p.z})));
		return {r.x, r.y};
	}
	glm::vec2 equirectangular_projection(const glm::vec3& p)
	{
		auto r = to_uv(to_lat_long({p.x, p.y, p.z}));
		return {r.x, r.y};
	}

	Mesh_data equirectangular_projection(Const_world_view world, const glm::mat4& view)
	{
		auto result = Mesh_data();

		const auto& mesh           = world.mesh();
		const auto& position_layer = world.required_layer(layer_position);
		const auto  radius         = world.required_unstructured_layer("meta")["radius"].get<float>();

		result.positions.reserve((mesh.face_max_index() + 1) * 3);
		result.indices.reserve((mesh.face_max_index() + 1) * 3);
		const auto add_face = [&](auto face_vertex, const std::array<Lat_long, 3> lat_long) {
			const auto uvs = util::map(lat_long, to_uv);
			const auto max_len =
			        std::max(std::max(dist(uvs[0], uvs[1]), dist(uvs[1], uvs[2])), dist(uvs[2], uvs[0]));
			if(max_len > 0.5f)
				return; // skip long faces, which are usually caused by degenerated geometry and may cause artefacts

			result.indices.insert(result.indices.end(), face_vertex.begin(), face_vertex.end());
			result.positions.insert(result.positions.end(), uvs.begin(), uvs.end());
		};

		// generate index array, adding faces where required to avoid seam/artifacts
		for(const Face face : mesh.faces()) {
			const auto face_vertex = face.vertices(mesh);
			const auto vertex_pos =
			        util::map(face_vertex, [&](auto v) { return project(view, position_layer[v] * radius); });
			const auto lat_long = util::map(vertex_pos, to_lat_long);

			if(is_wrong_winding_order(vertex_pos[0], vertex_pos[1], vertex_pos[2]))
				continue;

			constexpr auto long_max = glm::pi<float>();

			const auto long_top = make_bitset(lat_long[0].longitude < -long_max / 3,
			                                  lat_long[1].longitude < -long_max / 3,
			                                  lat_long[2].longitude < -long_max / 3);

			const auto long_bottom = make_bitset(lat_long[0].longitude > long_max / 3,
			                                     lat_long[1].longitude > long_max / 3,
			                                     lat_long[2].longitude > long_max / 3);

			if(!is_face_folded(lat_long[0], lat_long[1], lat_long[2]) && !(long_top.any() && long_bottom.any())) {
				add_face(face_vertex, lat_long);

			} else {
				// face crosses uv seam => we need to insert two triangles, one for each side of the seam
				auto lat_long_1 = lat_long;
				auto lat_long_2 = lat_long;

				for(int i = 0; i < 3; i++) {
					if(std::abs(lat_long_2[i].longitude) < long_max / 4)
						continue;

					if(lat_long_2[i].longitude >= 0.f) {
						lat_long_2[i].longitude = -long_max - (long_max - lat_long[i].longitude);
					} else {
						lat_long_1[i].longitude = long_max + (long_max + lat_long[i].longitude);
					}
				}

				add_face(face_vertex, lat_long_1);
				add_face(face_vertex, lat_long_2);
			}
		}

		return result;
	}

} // namespace yggdrasill::renderer
