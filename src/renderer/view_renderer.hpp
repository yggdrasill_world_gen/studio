#pragma once

#include "camera.hpp"
#include "picking_result.hpp"

#include "../graphic/layer_backed.hpp"
#include "../util/maybe.hpp"

#include <memory>
#include <span>

namespace yggdrasill {
	class Const_world_view;
	class Layer_renderer;
	class Selection;
	class Settings;
} // namespace yggdrasill

namespace yggdrasill::renderer {
	struct Camera;

	class View_renderer {
	  public:
		struct State;

		View_renderer(const Settings&);
		View_renderer(View_renderer&&)                 = delete;
		View_renderer(const View_renderer&)            = delete;
		View_renderer& operator=(View_renderer&&)      = delete;
		View_renderer& operator=(const View_renderer&) = delete;
		~View_renderer();

		auto& layer_cache() { return layer_cache_; }
		auto  world() const -> Const_world_view;
		auto& cameras() { return cameras_; }

		/// Returns the vertex/face/edge that was under the mouse cursor in the previous frame.
		/// \returns std::nullopt if the current view is not visible, the cursor is outside its area or there has been an error
		auto get_hovered_geometry() const -> const util::maybe<Picking_result>&;

		/// should be called exactly once per frame
		void update(Const_world_view, const Selection&);

		/// Draws the given layers into the current ImGui window.
		/// The actual rendering happens in a callback, inside the ImGui rendering loop, so the passed state
		///   has to survive at least until the end of the current frame.
		/// \returns true if a redraw was required
		bool draw(const void* owner, const Camera&, std::span<const std::unique_ptr<Layer_renderer>>);

		/// one-of draw of the given layers into a texture
		auto draw_to_texture(
		        const Camera& cam, std::span<const std::unique_ptr<Layer_renderer>> views, int width, int height)
		        -> std::optional<graphic::Render_texture>
		{
			return draw_to_texture({}, cam, views, width, height);
		}
		auto draw_to_texture(Const_world_view,
		                     const Camera&,
		                     std::span<const std::unique_ptr<Layer_renderer>>,
		                     int width,
		                     int height) -> std::optional<graphic::Render_texture>;

	  private:
		graphic::Layer_cache   layer_cache_;
		Camera_map             cameras_;
		std::unique_ptr<State> state_;
	};

} // namespace yggdrasill::renderer
