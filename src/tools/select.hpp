#pragma once

#include "../gui/config.hpp"

#include <memory>

namespace yggdrasill::simulation {
	class Graph;
}

namespace yggdrasill::tools {
	class Tool;

	extern std::unique_ptr<Tool> select(simulation::Graph&, gui::Config::Properties& cfg);

} // namespace yggdrasill::tools
