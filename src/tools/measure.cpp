#include "measure.hpp"

#include "../tool.hpp"
#include "../gui/ui_elements.hpp"

#include <yggdrasill_icons.hpp>

namespace yggdrasill::tools {

	namespace {
		constexpr auto marker_radius = 6.f;

		Vec3 closest_point(const Vec3& a, const Vec3& b, const Vec3& new_point)
		{
			const auto g = cross(normalized(a), normalized(b));
			const auto f = cross(normalized(new_point), g);
			return normalized(cross(g, f)) * length(new_point);
		}

		class Measure : public Tool {
		  private:
			float                                           segment_length_km_ = 100.f;
			std::vector<Highlight_world_position>           markers_;
			ImGuiButtonFlags                                prev_buttons_;
			std::vector<Highlight_world_position>::iterator hovered_marker_;
			bool                                            marker_dragged_ = false;

			util::String_literal button_label() const override { return yggdrasill::icons::tool_measure; }
			util::String_literal tooltip() const override { return "Measure distances between points"_str; }
			auto                 used_buttons() const -> ImGuiButtonFlags override
			{
				return ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight;
			}
			auto readonly() const -> bool override { return true; }

			void update(simulation::Cursor&, Selection& selection) override
			{
				if(!marker_dragged_) {
					const auto hover_distance = marker_radius * selection.meter_per_pixel() * 2.f;
					hovered_marker_ = std::find_if(markers_.begin(), markers_.end(), [&](const auto& m) {
						return dist2(selection.world_position(), m.world_position)
						       < hover_distance * hover_distance;
					});
				}

				ImGui::SliderFloat("Segment Length", &segment_length_km_, 10.f, 1'000.f, "%.0f km");

				ImGui::Spacing();
				ImGui::TextWrapped("Left click/drag to place or move a marker.");
				ImGui::Spacing();
				ImGui::TextWrapped("Right click to remove markers.");


				// draw lines
				for(auto i = std::size_t(1); i < markers_.size(); ++i) {
					selection.highlight_subdivided_line(markers_[i - 1],
					                                    markers_[i],
					                                    segment_length_km_ * 1000.f,
					                                    "#f00"_color,
					                                    "#fff"_color);
				}

				// draw markers
				for(auto i = std::size_t(0); i < markers_.size(); ++i) {
					selection.highlight(markers_[i], "#aaa"_color, marker_radius);
				}
				if(hovered_marker_ != markers_.end()) {
					selection.highlight(*hovered_marker_, marker_dragged_ ? "#fff"_color : "#aaa"_color, 10.f);
				}

				// draw labels and print distances
				if(markers_.size() > 1) {
					auto distance_sum = 0.f;
					for(auto i = std::size_t(1); i < markers_.size(); ++i) {
						const auto& a = markers_[i - 1].world_position;
						const auto& b = markers_[i].world_position;
						const auto  distance =
						        std::acos(dot(normalized(a), normalized(b))) * (length(a) + length(b)) / 2.f;
						const auto distance_str = util::concat(static_cast<int>(distance / 1000.f), "km");

						distance_sum += distance;
						selection.add_label({.world_position = slerp(a, b, 0.5f)}, distance_str.c_str());
					}

					ImGui::Spacing();
					ImGui::Separator();
					ImGui::Spacing();
					ImGui::TextWrapped(
					        "%s",
					        util::concat("Total distance: ", static_cast<int>(distance_sum / 1000.f), "km").c_str());
				}
			}

			// check if new points lies on an existing connection => insert point there instead of at the end
			auto find_insert_position(const Vec3& p, float max_distance)
			{
				if(markers_.size() < 2)
					return markers_.end();

				auto insert_pos         = markers_.end();
				auto closest_distance_2 = max_distance * max_distance;
				for(auto iter = markers_.begin() + 1; iter != markers_.end(); ++iter) {
					const auto line_p = closest_point((iter - 1)->world_position, iter->world_position, p);
					if(const auto d = dist2(line_p, p); d < closest_distance_2) {
						insert_pos         = iter;
						closest_distance_2 = d;
					}
				}

				return insert_pos;
			}

			void on_use(simulation::Cursor& state, Selection& selection, ImGuiButtonFlags buttons) override
			{
				if(selection.hovered() && selection.vertex() != no_vertex) {
					// left button down => place new marker or begin dragging
					if(!(prev_buttons_ & ImGuiButtonFlags_MouseButtonLeft)
					   && (buttons & ImGuiButtonFlags_MouseButtonLeft)) {
						if(hovered_marker_ == markers_.end()) {
							// nothing hovered => create new marker
							hovered_marker_ = markers_.emplace(
							        find_insert_position(selection.world_position(),
							                             selection.meter_per_pixel() * marker_radius),
							        Highlight_world_position{selection.vertex(), selection.world_position()});
						}
						marker_dragged_ = true;
					}

					// move currently dragged marker to mouse position
					if(marker_dragged_) {
						*hovered_marker_ =
						        Highlight_world_position{selection.vertex(), selection.world_position()};
					}

					// right button released => remove marker
					if((prev_buttons_ & ImGuiButtonFlags_MouseButtonRight)
					   && !(buttons & ImGuiButtonFlags_MouseButtonRight)) {
						if(hovered_marker_ != markers_.end()) {
							markers_.erase(hovered_marker_);
							hovered_marker_ = markers_.end();
						}
					}
				}

				// left button released => stop dragging
				if((prev_buttons_ & ImGuiButtonFlags_MouseButtonLeft)
				   && !(buttons & ImGuiButtonFlags_MouseButtonLeft)) {
					marker_dragged_ = false;
				}

				prev_buttons_ = buttons;
			}
		};
	} // namespace

	std::unique_ptr<Tool> measure(simulation::Graph&)
	{
		return std::make_unique<Measure>();
	}

} // namespace yggdrasill::tools
