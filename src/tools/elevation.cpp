#include "elevation.hpp"

#include "../tool.hpp"
#include "../gui/ui_elements.hpp"

#include <yggdrasill_icons.hpp>

#include <unordered_set>

namespace yggdrasill {
	extern ImFont* font_larger;
} // namespace yggdrasill


namespace yggdrasill::tools {

	namespace {
		enum class Crust_type : int8_t { none, oceanic, continental };

		constexpr auto layer_elevation = Layer_definition<float, Ref_type::vertex>("elevation");
		constexpr auto layer_type      = Layer_definition<Crust_type, Ref_type::vertex>("crust_type");

		constexpr auto min_elevation    = -1e4f;
		constexpr auto min_elevation_km = min_elevation / 1000.f;
		constexpr auto max_elevation    = 1e4f;
		constexpr auto max_elevation_km = max_elevation / 1000.f;
		constexpr auto land_elevation   = -100.f;

		float brush_falloff(float distance, float brush_size)
		{
			const auto x = std::clamp(std::abs(distance / brush_size), 0.f, 1.f);
			return 1.f - x * x * (3.f - 2.f * x);
		}

		class Paint_elevation_cmd : public simulation::Command_base<Paint_elevation_cmd> {
		  public:
			explicit Paint_elevation_cmd(float target_elevation)
			  : min_(target_elevation), max_(target_elevation), update_crust_type_(true)
			{
			}
			Paint_elevation_cmd(float min, float max, bool update_crust_type)
			  : min_(min), max_(max), update_crust_type_(update_crust_type)
			{
			}

			void add(Vertex v, float diff) { diff_[v] += diff; }

			[[nodiscard]] std::string_view         description() const override { return "Elevation Change"; }
			[[nodiscard]] simulation::Command_type type() const override
			{
				return simulation::Command_type::user_world_edit;
			}
			void execute(simulation::State& state) override
			{
				auto mesh              = state.world().mesh();
				auto [elevation, type] = state.world().lock_layer(layer_elevation, layer_type);
				for(auto& [v, diff] : diff_) {
					if(!v.valid(mesh))
						continue;

					const auto value     = elevation[v];
					auto       new_value = value + (min_ != max_ || value < min_ ? diff : -diff);

					if(new_value < min_ && value >= min_)
						new_value = min_;
					else if(new_value > max_ && value <= max_)
						new_value = max_;

					elevation[v] = new_value;

					if(update_crust_type_) {
						if(value < land_elevation && new_value >= land_elevation)
							type[v] = Crust_type::continental;
						else if(value > land_elevation && new_value <= land_elevation)
							type[v] = Crust_type::oceanic;
					}
				}
			}

			[[nodiscard]] bool force_synchronous() const override { return true; }
			[[nodiscard]] bool repeatable() const override { return false; }
			[[nodiscard]] bool noop() const { return diff_.empty(); }

			static constexpr std::string_view id = "elevation";
			std::string_view                  serialize(Dictionary_view cfg) override
			{
				cfg["min"]    = min_;
				cfg["max"]    = max_;
				cfg["uct"]    = update_crust_type_;
				auto cfg_diff = cfg["diff"].get<Array_ref<Dictionary_ref>>();
				cfg_diff.reserve(static_cast<index_t>(diff_.size()));
				for(auto [v, diff] : diff_) {
					auto e = cfg_diff.push_back();
					e["v"] = v.index();
					e["d"] = diff;
				}
				return id;
			}
			static std::unique_ptr<Command> deserialize(Const_dictionary_view cfg)
			{
				auto cmd = std::make_unique<Paint_elevation_cmd>(
				        cfg["min"].get<float>(), cfg["max"].get<float>(), cfg["uct"].get<bool>());
				auto diff = cfg["diff"].get<Const_array_view<Const_dictionary_view>>();
				cmd->diff_.reserve(diff.size());
				for(auto e : diff) {
					cmd->diff_[Vertex{e["v"].get<std::int32_t>()}] = e["d"].get<float>();
				}
				return cmd;
			}

		  private:
			std::unordered_map<Vertex, float> diff_;
			float                             min_, max_;
			bool                              update_crust_type_;
		};


		class Elevation : public Cmd_based_tool<Paint_elevation_cmd> {
		  private:
			enum class Preset { raise_lower, mountain, valley, water, ocean, set };
			static constexpr auto preset_labels_ = std::array{yggdrasill::icons::tool_elevation_manual,
			                                                  yggdrasill::icons::tool_elevation_mountain,
			                                                  yggdrasill::icons::tool_elevation_hill,
			                                                  yggdrasill::icons::tool_elevation_water,
			                                                  yggdrasill::icons::tool_elevation_ocean,
			                                                  yggdrasill::icons::cog};
			static constexpr auto preset_tooltips_ =
			        std::array{"Raise (left click) and lower (right click) the elevation",
			                   "Draw Mountains",
			                   "Draw Valleys",
			                   "Draw shallow oceans (continental shelfs)",
			                   "Draw deep oceans",
			                   "Draw a user-defined elevation"};
			static constexpr auto preset_count_ = preset_labels_.size();

			static constexpr auto fixed_preset_amount_ = 2000.f;
			static constexpr auto fixed_preset_target_height_ = std::array{0.f, 8'000.f, 50.f, -50.f, -8'000.f};

			Preset preset_            = Preset::mountain;
			float  target_elevation_  = 0.f;
			float  min_elevation_     = min_elevation;
			float  max_elevation_     = max_elevation;
			float  meter_per_second_  = 500.f;
			float  brush_radius_km_   = 1'000.f;
			bool   update_crust_type_ = true;
			bool   lowering_          = false;

			std::unordered_set<Vertex> tmp_seen_vertices_;

		  public:
			using Cmd_based_tool::Cmd_based_tool;

			util::String_literal button_label() const override { return yggdrasill::icons::tool_elevation; }
			util::String_literal tooltip() const override { return "Changes the terrain elevation"_str; }
			auto                 used_buttons() const -> ImGuiButtonFlags override
			{
				if(preset_ == Preset::raise_lower) {
					return ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight;
				} else {
					return ImGuiButtonFlags_MouseButtonLeft;
				}
			}

			void update(simulation::Cursor&, Selection& selection) override
			{
				draw_preset_buttons();

				ImGui::SliderFloat("Brush Size", &brush_radius_km_, 10.f, 2'000.f, "%.1f km");

				if(preset_ == Preset::raise_lower) {
					ImGui::SliderFloat("Amount", &meter_per_second_, 100.f, 5000.f, "%.0f m");

					auto l_min_elevation_km = min_elevation_ / 1'000.f;
					ImGui::SliderFloat(
					        "Minimum", &l_min_elevation_km, min_elevation_km, max_elevation_km, "%.1f km");
					min_elevation_ = l_min_elevation_km * 1'000.f;

					auto l_max_elevation_km = max_elevation_ / 1'000.f;
					ImGui::SliderFloat(
					        "Maximum", &l_max_elevation_km, min_elevation_km, max_elevation_km, "%.1f km");
					max_elevation_ = l_max_elevation_km * 1'000.f;

					ImGui::Checkbox("Update Crust Type", &update_crust_type_);

				} else if(preset_ == Preset::set) {
					ImGui::SliderFloat(
					        "Elevation", &target_elevation_, min_elevation, max_elevation, "%.0f m");
				}

				const auto pure_color = [&] {
					switch(preset_) {
						case Preset::mountain: return "#753"_color;
						case Preset::valley: return "#bda"_color;
						case Preset::water: return "#8bd"_color;
						case Preset::ocean: return "#348"_color;
						case Preset::set: return "#888"_color;
						case Preset::raise_lower:
							return command_active() ? (lowering_ ? "#977"_color : "#8b8"_color) : "#aaa"_color;
					}
					return "#f00"_color;
				}();
				const auto color = gui::apply_alpha(pure_color, command_active() ? 1.f : 0.6f);
				selection.highlight_area(
				        {selection.vertex(), selection.world_position()}, brush_radius_km_ * 1000.f, color);
			}

			void draw_preset_buttons()
			{
				ImGui::PushFont(font_larger);
				ON_EXIT(ImGui::PopFont());

				gui::radio_buttons([&](auto&& button) {
					for(int i = 0; i < static_cast<int>(preset_count_); ++i) {
						if(button(static_cast<int>(preset_) == i, preset_labels_[i], preset_tooltips_[i]))
							preset_ = static_cast<Preset>(i);
					}
				});
			}

			bool do_on_use(simulation::Cursor& state, Selection& selection, ImGuiButtonFlags buttons) override
			{
				if((buttons & used_buttons()) == 0)
					return false;
				if(selection.vertex() == no_vertex)
					return true;

				const auto diff = (preset_ == Preset::raise_lower ? meter_per_second_ : fixed_preset_amount_)
				                  * ImGui::GetIO().DeltaTime;


				auto& cmd = [&]() -> decltype(auto) {
					switch(preset_) {
						case Preset::raise_lower:
							return command(min_elevation_, max_elevation_, update_crust_type_);
						case Preset::set: return command(target_elevation_);
						default: return command(fixed_preset_target_height_[static_cast<int>(preset_)]);
					}
				}();

				lowering_ = (buttons & ImGuiButtonFlags_MouseButtonLeft) == 0;

				const auto radius = brush_radius_km_ * 1000.f;

				foreach_vertex_in_range(state, selection, radius, [&](const Vertex v, const float dist2) {
					auto amount = diff * brush_falloff(std::sqrt(dist2), radius);
					if(v == selection.vertex()) {
						// ensure that at least one vertex is affected
						amount = std::max(amount, 0.5f);
					}

					if(preset_ == Preset::raise_lower) {
						amount *= lowering_ ? -1 : 1;
					}

					cmd.add(v, amount);
				});
				return true;
			}
		};
	} // namespace

	std::unique_ptr<Tool> elevation(simulation::Graph& graph)
	{
		return std::make_unique<Elevation>(graph);
	}

} // namespace yggdrasill::tools
