#include "plate_id.hpp"

#include "../tool.hpp"
#include "../gui/ui_elements.hpp"

#include <yggdrasill_icons.hpp>


namespace yggdrasill::tools {

	namespace {
		using Plate_id          = std::int32_t;
		constexpr auto no_plate = std::numeric_limits<Plate_id>::max();

		constexpr auto layer_plate_id = Layer_definition<Plate_id, Ref_type::vertex>("plate_id")
		                                        .interpolation(Interpolation::max_weight)
		                                        .invalidation(Invalidation::reset_affected);

		class Paint_plate_id_cmd : public simulation::Command_base<Paint_plate_id_cmd> {
		  public:
			explicit Paint_plate_id_cmd(Plate_id plate_id) : plate_id_(plate_id) {}

			void add(Vertex v) { vertices_.insert(v); }

			[[nodiscard]] std::string_view description() const override { return "Plate Assignment Change"; }
			[[nodiscard]] simulation::Command_type type() const override
			{
				return simulation::Command_type::user_world_edit;
			}
			void execute(simulation::State& state) override
			{
				auto mesh    = state.world().mesh();
				auto [plate] = state.world().lock_layer(layer_plate_id);

				for(auto v : vertices_) {
					if(v.valid(mesh))
						plate[v] = plate_id_;
				}
			}

			[[nodiscard]] bool force_synchronous() const override { return true; }
			[[nodiscard]] bool repeatable() const override { return false; }
			[[nodiscard]] bool noop() const { return vertices_.empty(); }

			[[nodiscard]] auto plate_id() const { return plate_id_; }

			static constexpr std::string_view id = "plate_id";
			std::string_view                  serialize(Dictionary_view cfg) override
			{
				cfg["id"]     = plate_id_;
				auto vertices = cfg["v"].get<Array_ref<std::int32_t>>();
				for(auto v : vertices_) {
					vertices.push_back(v.index());
				}
				return id;
			}
			static std::unique_ptr<Command> deserialize(Const_dictionary_view cfg)
			{
				auto cmd      = std::make_unique<Paint_plate_id_cmd>(cfg["id"].get<Plate_id>());
				auto vertices = cfg["v"].get<Const_array_view<std::int32_t>>();
				cmd->vertices_.reserve(vertices.size());

				for(auto v : vertices) {
					cmd->vertices_.emplace(v);
				}
				return cmd;
			}

		  private:
			std::unordered_set<Vertex> vertices_;
			Plate_id                   plate_id_;
		};

		class Plate_id_tool : public Cmd_based_tool<Paint_plate_id_cmd> {
		  private:
			util::maybe<Plate_id> locked_plate_;
			bool                  advanced_used_   = false;
			float                 brush_radius_km_ = 400.f;


		  public:
			using Cmd_based_tool::Cmd_based_tool;

			util::String_literal button_label() const override
			{
				return yggdrasill::icons::tool_plate_assignment;
			}
			util::String_literal tooltip() const override
			{
				return "Changes what tectonic plate a piece of crust belongs to"_str;
			}
			auto used_buttons() const -> ImGuiButtonFlags override
			{
				return ImGuiButtonFlags_MouseButtonLeft
				       | (advanced_used_ ? ImGuiButtonFlags_MouseButtonRight : 0);
			}

			void update(simulation::Cursor&, Selection& selection) override
			{
				ImGui::SliderFloat("Brush Size", &brush_radius_km_, 10.f, 2'000.f, "%.1f km");

				ImGui::TextWrapped("Drag with left mouse button to expand a plate");

				ImGui::Spacing();
				advanced_used_ = ImGui::CollapsingHeader("Advanced");
				if(advanced_used_) {

					if(ImGui::Button("Reset")) {
						locked_plate_.reset();
					}
					ImGui::SameLine();
					auto plate = locked_plate_.get_or(no_plate);
					if(gui::input_int_with_hint("Plate ID", "<none>", plate, no_plate)) {
						locked_plate_ = plate;
					}

					ImGui::TextWrapped(
					        "Right click to pick a plate. Left click to assign a piece of crust to this "
					        "plate.");
				} else {
					locked_plate_.reset();
				}

				selection.highlight_area({selection.vertex(), selection.world_position()},
				                         brush_radius_km_ * 1000.f,
				                         gui::apply_alpha("#fff"_color, command_active() ? 1.f : 0.6f));
			}

			bool do_on_use(simulation::Cursor& state, Selection& selection, ImGuiButtonFlags buttons) override
			{
				auto world = state.world();
				if(!world)
					return false;

				auto ids = world->layer(layer_plate_id);
				if(!ids)
					return false;

				if(buttons & ImGuiButtonFlags_MouseButtonRight) {
					if(selection.vertex().valid(world->mesh())) {
						locked_plate_ = (*ids)[selection.vertex()];
					} else {
						locked_plate_.reset();
					}

					return true;
				} else if(buttons & ImGuiButtonFlags_MouseButtonLeft) {
					auto& cmd = command(locked_plate_.get_or([&] {
						if(selection.vertex().valid(world->mesh())) {
							return (*ids)[selection.vertex()];
						} else {
							return Plate_id(0);
						}
					}));

					foreach_vertex_in_range(state, selection, brush_radius_km_ * 1000.f, [&](const Vertex v, float) {
						if((*ids)[v] != cmd.plate_id())
							cmd.add(v);
					});

					return true;

				} else {
					return false;
				}
			}
		};
	} // namespace

	std::unique_ptr<Tool> plate_id(simulation::Graph& graph)
	{
		return std::make_unique<Plate_id_tool>(graph);
	}

} // namespace yggdrasill::tools
