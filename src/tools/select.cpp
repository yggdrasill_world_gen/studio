#include "select.hpp"

#include "../tool.hpp"
#include "../gui/ui_elements.hpp"
#include "../gui/yggdrasill_type_widgets.hpp"

#include <yggdrasill_icons.hpp>

#include <any>
#include <sstream>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

namespace yggdrasill {
	extern ImFont* font_small;
}

namespace yggdrasill::tools {

	// helper functions to print layers
	namespace {
		template <Any_layer_definition DataLayer>
		class Modify_mesh_layer_cmd : public simulation::Command_base<Modify_mesh_layer_cmd<DataLayer>> {
		  public:
			Modify_mesh_layer_cmd(const DataLayer&                      layer_def,
			                      typename DataLayer::key_type          key,
			                      const typename DataLayer::value_type& value)
			  : description_(generate_description(layer_def, key, value))
			  , layer_def_(layer_def)
			  , key_(key)
			  , value_(value)
			{
			}

			void value(const typename DataLayer::value_type& value)
			{
				value_       = value;
				description_ = generate_description(layer_def_, key_, value_);
			}
			auto value() const { return value_; }

			[[nodiscard]] std::string_view         description() const override { return description_; }
			[[nodiscard]] simulation::Command_type type() const override
			{
				return simulation::Command_type::user_world_edit;
			}
			void execute(simulation::State& state) override
			{
				auto mesh    = state.world().mesh();
				auto [layer] = state.world().lock_layer(layer_def_);
				if(key_.valid(mesh))
					layer[key_] = value_;
			}

			[[nodiscard]] bool force_synchronous() const override { return true; }
			[[nodiscard]] bool repeatable() const override { return false; }

			/// not serialized
			std::string_view serialize(Dictionary_view cfg) override { return {}; }


		  private:
			std::string                    description_;
			const DataLayer                layer_def_;
			typename DataLayer::key_type   key_;
			typename DataLayer::value_type value_;

			static std::string generate_description(const DataLayer&                      layer_def,
			                                        typename DataLayer::key_type          key,
			                                        const typename DataLayer::value_type& value)
			{
				return util::concat("Set ", key, " of ", layer_def.id(), " to ", value);
			}
		};


		template <typename T, Ref_type RefType>
		std::string_view layer_name(const Layer_definition<T, RefType>& layer)
		{
			static const auto mapping = std::unordered_map<std::string_view, std::string_view>{
			        {"crust_created", "Crust Age"  },
                    {"position",      "Coordinates"},
                    {"elevation",     "Elevation"  }
            };

			return util::find_maybe(mapping, layer.id()).get_or(layer.id());
		}

		constexpr auto crust_type_labels     = std::array{"[none]", "[oceanic]", "[continental]"};
		constexpr auto plate_boundary_labels = std::array{"[transform]",
		                                                  "[subducting_origin]",
		                                                  "[subducting_dest]",
		                                                  "[collision]",
		                                                  "[joined]",
		                                                  "[ridge]"};

		//region string_element function set
		void enum_string_element(const auto& value, const auto& labels)
		{
			if(value >= 0 && std::size_t(value) < labels.size())
				ImGui::TextUnformatted(crust_type_labels[value]);
			else
				ImGui::TextColored(ImVec4(1, 0, 0, 1), "<invalid>");
		}


		template <typename T>
		void string_element(std::string_view id, Const_world_view, const T& value)
		{
			ImGui::TextUnformatted(gui::pretty_print(value).c_str());
		}

		void string_element(std::string_view id, Const_world_view, const std::int8_t& value)
		{
			if(id == "crust_type") {
				enum_string_element(value, crust_type_labels);

			} else if(id == "primal_plate_boundaries" || id == "dual_plate_boundaries") {
				enum_string_element(value, plate_boundary_labels);

			} else {
				ImGui::TextUnformatted(gui::pretty_print(value).c_str());
			}
		}

		void string_element(std::string_view id, Const_world_view, const Vec3& value)
		{
			if(id == "position") {
				auto pos = normalized(value);
				auto lat = glm::degrees(std::asin(pos.y));
				auto lon = std::abs(pos.x) < 1e-5f ? (pos.z < 0 ? -180.f : 180.f)
				                                   : glm::degrees(-std::atan2(pos.z, pos.x));

				auto ss = std::ostringstream();
				ss << std::fixed << std::setprecision(4);
				ss << (lat > 0 ? 'N' : 'S') << std::abs(lat) << "° ";
				ss << (lon <= 0 ? 'W' : 'E') << std::abs(lon) << "°";

				auto str_value = std::move(ss).str();
				ImGui::TextUnformatted(str_value.data(), str_value.data() + str_value.size());

			} else {
				auto ss = std::ostringstream();
				ss << std::fixed << std::setprecision(4);
				ss << value.x << '/' << value.y << '/' << value.z;

				auto str_value = std::move(ss).str();
				ImGui::TextUnformatted(str_value.data(), str_value.data() + str_value.size());
			}
		}

		void string_element(std::string_view id, Const_world_view, const Vec2& value)
		{
			auto ss = std::ostringstream();
			ss << std::fixed << std::setprecision(4);
			ss << value.x << '/' << value.y;

			auto str_value = std::move(ss).str();
			ImGui::TextUnformatted(str_value.data(), str_value.data() + str_value.size());
		}

		void string_element(std::string_view id, Const_world_view world, const float& value)
		{
			if(id == "crust_created") {
				const auto meta = world.unstructured_layer("meta");
				const auto now  = meta ? (*meta)["age"].get(0.f) : 0.f;

				const auto age     = now - value;
				const auto age_str = gui::pretty_print(static_cast<int>(age / 1'000'000)) + " MJ";
				ImGui::TextUnformatted(age_str.c_str());

			} else {
				ImGui::TextUnformatted(gui::pretty_print(value).c_str());
			}
		}
		//endregion


		//region layer_input function set
		bool layer_input(std::string_view id, Const_world_view, auto key, YGDL_Bool& value)
		{
			auto v = value == YGDL_TRUE;
			auto r = ImGui::Checkbox("", &v);
			value  = v ? YGDL_TRUE : YGDL_FALSE;
			return r;
		}
		bool layer_input(std::string_view id, Const_world_view, auto key, int8_t& value)
		{
			if(id == "crust_type") {
				constexpr auto labels = std::array{"[none]", "[oceanic]", "[continental]"};
				return gui::dropdown("##input", value, labels);

			} else if(id == "primal_plate_boundaries" || id == "dual_plate_boundaries") {
				constexpr auto labels = std::array{"[transform]",
				                                   "[subducting_origin]",
				                                   "[subducting_dest]",
				                                   "[collision]",
				                                   "[joined]",
				                                   "[ridge]"};
				return gui::dropdown("##input", value, labels);
			} else {
				auto v = static_cast<int>(value);
				ImGui::InputInt("", &v);
				value = static_cast<int8_t>(v);
				return ImGui::IsItemDeactivatedAfterEdit();
			}
		}
		bool layer_input(std::string_view id, Const_world_view, auto key, int32_t& value)
		{
			auto v = static_cast<int>(value);
			ImGui::InputInt("", &v);
			value = static_cast<int32_t>(v);
			return ImGui::IsItemDeactivatedAfterEdit();
		}
		bool layer_input(std::string_view id, Const_world_view world, auto key, float& value)
		{
			const auto last_value = value;
			if(id == "crust_created") {
				const auto meta = world.unstructured_layer("meta");
				const auto now  = meta ? (*meta)["age"].get(0.f) : 0.f;

				auto age = (now - value) / 1'000'000;
				ImGui::InputFloat("", &age, 0.f, 0.f, "%.3f MJ");
				value = now - age * 1'000'000;

			} else if(id == "elevation") {
				ImGui::InputFloat("", &value, 0.f, 0.f, "%.1f meter");

			} else {
				ImGui::InputFloat("", &value);
			}

			return ImGui::IsItemDeactivatedAfterEdit()
			       && std::abs(value - last_value) > std::numeric_limits<float>::epsilon() * 10.f;
		}
		bool layer_input(std::string_view id, Const_world_view, auto key, Vec2& value)
		{
			ImGui::InputFloat2("", &value.x);
			return ImGui::IsItemDeactivatedAfterEdit();
		}
		bool layer_input(std::string_view id, Const_world_view world, auto key, Vec3& value)
		{
			if(id == "position") {
				// not editable by the user, because it would be too easy to accidentally destroy the mesh structure
				auto str_value = gui::pretty_print_position(value);
				gui::input_text_lazy("", str_value, ImGuiInputTextFlags_ReadOnly);
				return false;

			} else {
				ImGui::InputFloat3("", &value.x);
				return ImGui::IsItemDeactivatedAfterEdit();
			}
		}
		// endregion
	} // namespace

	namespace {
		constexpr auto config_prefix = std::string_view("select_show_sb_");

		class Select : public Tool {
		  private:
			gui::Config::Properties& cfg_;
			Const_world_view         last_simulation_state_;

			simulation::Command*                                 last_mesh_layer_cmd_ = nullptr;
			std::optional<std::tuple<std::string_view, index_t>> last_mesh_layer_cmd_key_;
			const simulation::Node*                              last_mesh_layer_edit_ = nullptr;

			std::tuple<Vertex, Face, Primal_edge, Dual_edge> selection_;
			bool                                             selection_locked_    = false;
			bool                                             lock_button_pressed_ = false;

			util::String_map<std::string, bool> show_in_statusbar_ = {
			        {"position",      true},
                    {"crust_created", true},
                    {"elevation",     true},
                    {"plate_id",      true}
            };

			std::unordered_map<const void*, gui::Command_based_edit_state> ulayer_edit_state_;

		  public:
			Select(gui::Config::Properties& cfg) : cfg_(cfg)
			{
				cfg.foreach([&](auto& key, auto& value) {
					if(key.starts_with(config_prefix)) {
						show_in_statusbar_[key.substr(config_prefix.size())] = value == "true";
					}
				});
			}

			util::String_literal button_label() const override { return yggdrasill::icons::tool_select; }
			util::String_literal tooltip() const override
			{
				return "Move/Rotate the view and inspect information about the world (values of mesh- and unstructured layers)"_str;
			}
			auto used_buttons() const -> ImGuiButtonFlags override
			{
				return ImGuiButtonFlags_MouseButtonRight;
			}
			auto readonly() const -> bool override { return true; }

			void draw_status_always(simulation::Cursor& state, Selection& selection) override
			{
				const auto world = state.world();
				if(!world)
					return;

				const auto& mesh = world->mesh();

				auto print_data = [&](const char* id, auto key) {
					world->foreach_layer([&](auto layer) {
						if constexpr(std::is_same_v<typename std::decay_t<decltype(layer)>::key_type,
						                            std::decay_t<decltype(key)>>) {
							const auto id = layer.definition().id();

							if(util::find_maybe(show_in_statusbar_, id).get_or(false)) {
								const auto name = layer_name(layer.definition());

								ImGui::TextUnformatted(name.data(), name.data() + name.size());
								ImGui::SameLine();
								ImGui::TextUnformatted(":");
								ImGui::SameLine();
								string_element(id, *world, layer[key]);
								ImGui::Separator();
							}
						}
					});
				};

				if(selection.vertex().valid(mesh))
					print_data("vertex_data", selection.vertex());

				if(selection.face().valid(mesh))
					print_data("face_data", selection.face());

				if(selection.primal_edge().valid(mesh))
					print_data("p_edge_data", selection.primal_edge());
				if(selection.dual_edge().valid(mesh))
					print_data("d_edge_data", selection.dual_edge());
			}

			void update(simulation::Cursor& state, Selection& selection) override
			{
				auto world = state.world();
				if(!world)
					return;


				if(!selection_locked_) {
					selection_ = {
					        selection.vertex(), selection.face(), selection.primal_edge(), selection.dual_edge()};
				}
				const auto a = selection_locked_ ? 1.f : 0.5f;
				selection.highlight(std::get<Face>(selection_), gui::apply_alpha("#afa6"_color, a));
				selection.highlight(std::get<Dual_edge>(selection_), gui::apply_alpha("#aaf"_color, a), 4.f);
				selection.highlight(std::get<Primal_edge>(selection_), gui::apply_alpha("#faa"_color, a), 4.f);
				selection.highlight(std::get<Vertex>(selection_), gui::apply_alpha("#fff"_color, a), 8.f);

				ImGui::TextWrapped("Move/Rotate the view by dragging with any mouse button.");
				ImGui::TextWrapped(
				        "When other tools are used, the view can be moved by holding down the [Ctrl] key.");
				ImGui::Spacing();
				ImGui::TextWrapped(
				        "Right click to lock the selection, so it doesn't change until you click again.");
				ImGui::Spacing();
				ImGui::Separator();
				ImGui::Spacing();

				if(ImGui::TreeNodeEx("Mesh", ImGuiTreeNodeFlags_Framed | ImGuiTreeNodeFlags_DefaultOpen)) {
					auto mesh = world->mesh();
					ImGui::Text("Vertices : %i (%i)", mesh.vertex_used_count(), mesh.vertex_max_index() + 1);
					ImGui::Text("Faces    : %i (%i)", mesh.face_used_count(), mesh.face_max_index() + 1);
					ImGui::Text("Edges    : %i (%i)", mesh.edge_used_count(), mesh.edge_max_index() + 1);
					ImGui::TreePop();
				}

				if(ImGui::TreeNodeEx("Unstructured layers", ImGuiTreeNodeFlags_Framed)) {
					unstructured_layers(state);
					ImGui::TreePop();
				}

				if(ImGui::TreeNodeEx("Mesh layers", ImGuiTreeNodeFlags_Framed)) {
					mesh_layers(state);
					ImGui::TreePop();
				}
			}

			void on_use(simulation::Cursor&, Selection&, ImGuiButtonFlags button) override
			{

				if(button & ImGuiButtonFlags_MouseButtonRight)
					lock_button_pressed_ = true;
				else if(lock_button_pressed_) {
					selection_locked_    = !selection_locked_;
					lock_button_pressed_ = false;
				}
			}

			void mesh_layers(simulation::Cursor& state)
			{
				const auto& world = state.world();
				if(!world)
					return;

				auto mesh = world->mesh();

				ImGui::BeginDisabled(!state.editable());
				ON_EXIT(ImGui::EndDisabled());

				auto heading = [&](const char* name, auto& key) {
					using T = std::decay_t<decltype(key)>;
					ImGui::AlignTextToFramePadding();
					ImGui::TextUnformatted(name);
					ImGui::SameLine(150);
					const auto valid = key.valid(mesh);
					if(!valid) {
						ImGui::PushStyleColor(ImGuiCol_TextDisabled, "#f55"_color);
						ImGui::PushStyleColor(ImGuiCol_Text, "#f55"_color);
					}

					ImGui::PushID(name);
					ImGui::SetNextItemWidth(100);
					int i = key.index();
					if(gui::input_int_with_hint("##", "<none>", i, T{}.index()) && key != T(i)) {
						key               = T(i);
						selection_locked_ = true;
					}
					ImGui::PopID();

					if(!valid) {
						ImGui::PopStyleColor(2);
					}
				};

				auto data_table = [&](const char* id, auto key) {
					ImGui::Indent();
					ON_EXIT(ImGui::Unindent());
					if(ImGui::BeginTable(id, 3, ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg)) {
						ImGui::TableSetupColumn("ID", ImGuiTableColumnFlags_WidthFixed);
						ImGui::TableSetupColumn("SB", ImGuiTableColumnFlags_WidthFixed);
						ImGui::TableSetupColumn("Value", ImGuiTableColumnFlags_WidthStretch);

						world->foreach_layer([&](auto layer) {
							if constexpr(std::is_same_v<typename std::decay_t<decltype(layer)>::key_type,
							                            std::decay_t<decltype(key)>>) {
								const auto id = layer.definition().id();

								ImGui::PushID(id.data(), id.data() + id.size());
								auto _ = util::scope_guard([] { ImGui::PopID(); });

								ImGui::TableNextColumn();
								const auto name = layer_name(layer.definition());
								ImGui::AlignTextToFramePadding();
								ImGui::TextUnformatted(name.data(), name.data() + name.size());

								ImGui::TableNextColumn();
								auto show_sb = util::find_maybe(show_in_statusbar_, id).get_or(false);
								if(ImGui::Checkbox("##show_statusbar", &show_sb)) {
									auto id_str = std::string(id);
									cfg_.set(std::string(config_prefix) + id_str, show_sb);
									show_in_statusbar_.insert_or_assign(std::move(id_str), show_sb);
								}
								if(ImGui::IsItemHovered()) {
									ImGui::SetTooltip("Show value in status bar");
								}

								ImGui::TableNextColumn();
								if(key.valid(mesh)) {
									ImGui::SetNextItemWidth(ImGui::GetColumnWidth());
									auto value_copy = layer[key];
									if(layer_input(id, *world, key, value_copy) && value_copy != layer[key]) {
										const auto cmd_key = std::make_tuple(id, key.index());
										auto       applied = false;

										using Cmd_t = Modify_mesh_layer_cmd<decltype(layer.definition())>;

										// reuse the last history-node, if it's the last command we created, and it modifies the same layer/key
										if(cmd_key == last_mesh_layer_cmd_key_ && *last_mesh_layer_edit_ == state) {
											if(auto* cmd = dynamic_cast<Cmd_t*>(last_mesh_layer_cmd_)) {
												cmd->value(value_copy);
												state.graph().invalidate(*last_mesh_layer_edit_);
												applied = true;
											}
										}

										if(!applied) {
											auto cmd = std::make_unique<Cmd_t>(layer.definition(), key, value_copy);
											last_mesh_layer_cmd_     = cmd.get();
											last_mesh_layer_cmd_key_ = cmd_key;
											last_mesh_layer_edit_    = &state.execute(std::move(cmd));
										}
									}
								} else {
									ImGui::AlignTextToFramePadding();
									ImGui::TextUnformatted("<none>");
								}
							}
						});

						ImGui::EndTable();
					}
				};


				heading("Vertex:", std::get<Vertex>(selection_));
				data_table("vertex_data", std::get<Vertex>(selection_));

				heading("Face:", std::get<Face>(selection_));
				data_table("face_data", std::get<Face>(selection_));

				const auto cp_edge = ImGui::GetCursorScreenPos();
				heading("Edge:", std::get<Primal_edge>(selection_));
				if(auto& e = std::get<Primal_edge>(selection_);
				   e.valid(mesh) && state.editable() && gui::begin_context_popup_area("edge_ctx", cp_edge)) {
					ImGui::TextUnformatted("Traverse:");
					ImGui::Indent();
					constexpr auto row_offset = 150;

					if(ImGui::Button("sym()"))
						e = e.sym();
					if(ImGui::Button("base()"))
						e = e.base();

					if(ImGui::Button("origin_next()"))
						e = e.origin_next(mesh);
					ImGui::SameLine(row_offset);
					if(ImGui::Button("origin_prev()"))
						e = e.origin_prev(mesh);

					if(ImGui::Button("dest_next()"))
						e = e.dest_next(mesh);
					ImGui::SameLine(row_offset);
					if(ImGui::Button("dest_prev()"))
						e = e.dest_prev(mesh);

					if(ImGui::Button("left_next()"))
						e = e.left_next(mesh);
					ImGui::SameLine(row_offset);
					if(ImGui::Button("left_prev()"))
						e = e.left_prev(mesh);

					if(ImGui::Button("right_next()"))
						e = e.right_next(mesh);
					ImGui::SameLine(row_offset);
					if(ImGui::Button("right_prev()"))
						e = e.right_prev(mesh);

					ImGui::Unindent();
					gui::end_context_popup_area();
				}
				data_table("p_edge_data", std::get<Primal_edge>(selection_));

				const auto cp_dedge = ImGui::GetCursorScreenPos();
				heading("Dual Edge:", std::get<Dual_edge>(selection_));
				if(auto& e = std::get<Dual_edge>(selection_);
				   e.valid(mesh) && state.editable() && gui::begin_context_popup_area("dedge_ctx", cp_dedge)) {
					ImGui::TextUnformatted("Traverse:");
					ImGui::Indent();
					constexpr auto row_offset = 150;

					if(ImGui::Button("sym()"))
						e = e.sym();

					if(ImGui::Button("origin_next()"))
						e = e.origin_next(mesh);
					ImGui::SameLine(row_offset);
					if(ImGui::Button("origin_prev()"))
						e = e.origin_prev(mesh);

					if(ImGui::Button("dest_next()"))
						e = e.dest_next(mesh);
					ImGui::SameLine(row_offset);
					if(ImGui::Button("dest_prev()"))
						e = e.dest_prev(mesh);

					if(ImGui::Button("left_next()"))
						e = e.left_next(mesh);
					ImGui::SameLine(row_offset);
					if(ImGui::Button("left_prev()"))
						e = e.left_prev(mesh);

					if(ImGui::Button("right_next()"))
						e = e.right_next(mesh);
					ImGui::SameLine(row_offset);
					if(ImGui::Button("right_prev()"))
						e = e.right_prev(mesh);

					ImGui::Unindent();
					gui::end_context_popup_area();
				}
				data_table("d_edge_data", std::get<Dual_edge>(selection_));
			}

			void unstructured_layers(simulation::Cursor& state)
			{
				const auto& world = state.world();
				if(!world)
					return;

				ON_EXIT(last_simulation_state_ = *world);
				if(*world != last_simulation_state_ || !state.editable()) {
					ulayer_edit_state_.clear();
				}

				world->foreach_unstructured_layer([&](std::string_view layer_id) {
					ImGui::PushID(layer_id.data(), layer_id.data() + layer_id.size());

					if(auto layer = world->unstructured_layer(layer_id)) {
						ImGui::SetNextItemOpen(true, ImGuiCond_Once);
						if(ImGui::TreeNodeEx(static_cast<const void*>(layer_id.data()),
						                     ImGuiTreeNodeFlags_Framed,
						                     "%.*s",
						                     static_cast<int>(layer_id.size()),
						                     layer_id.data())) {

							ImGui::Spacing();

							if(auto edit_state = ulayer_edit_state_.find(layer_id.data());
							   edit_state != ulayer_edit_state_.end()) {

								if(gui::right_aligned_button(YGGDRASILL_ICON_LOCK " Lock")) {
									ulayer_edit_state_.erase(edit_state);

								} else {
									gui::dictionary_edit("dict", *layer, edit_state->second);
								}

							} else {
								if(gui::right_aligned_button(YGGDRASILL_ICON_LOCK_OPEN " Unlock",
								                             state.editable())) {
									ulayer_edit_state_.emplace(
									        layer_id.data(),
									        gui::Command_based_edit_state{
									                "'" + std::string(layer_id) + "' Modified",
									                simulation::Command_type::user_world_edit,
									                [=](simulation::State& state, auto&& operation) {
										                auto [layer_mut] =
										                        state.world().lock_unstructured_layer(layer_id);
										                operation(layer_mut);
									                },
									                [state, layer_id] {
										                return state.world()->required_unstructured_layer(layer_id);
									                },
									                state,
									                [this](const simulation::Node& new_node) {
										                if(last_simulation_state_
										                   == new_node.predecessor()->state().world())
											                last_simulation_state_ = new_node.state().world();
									                }});
								}

								gui::dictionary_view("dict", *layer);
							}

							ImGui::TreePop();
						}
					}

					ImGui::PopID();
				});
			}
		};
	} // namespace

	std::unique_ptr<Tool> select(simulation::Graph& graph, gui::Config::Properties& cfg)
	{
		return std::make_unique<Select>(cfg);
	}

} // namespace yggdrasill::tools

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
