#include "mesh_resolution.hpp"

#include "../tool.hpp"
#include "../gui/ui_elements.hpp"

#include <yggdrasill_icons.hpp>

#include <unordered_set>

namespace yggdrasill {
	extern ImFont* font_larger;
} // namespace yggdrasill


namespace yggdrasill::tools {

	namespace {
		constexpr auto layer_position = Layer_definition<Vec3, Ref_type::vertex>("position");

		class Change_resolution_cmd : public simulation::Command_base<Change_resolution_cmd> {
		  public:
			explicit Change_resolution_cmd(bool increase, float brush_radius_m)
			  : increase_(increase), brush_radius_m_(brush_radius_m)
			{
			}

			void add(Vertex v, Vec3 p, Primal_edge e)
			{
				seed_edges_.emplace(e);
				if(seed_vertices_.emplace(v).second)
					seed_positions_.push_back(p);
			}

			[[nodiscard]] std::string_view description() const override
			{
				return increase_ ? "Increase Resolution" : "Decrease Resolution";
			}
			[[nodiscard]] simulation::Command_type type() const override
			{
				return simulation::Command_type::user_world_edit;
			}
			void execute(simulation::State& state) override
			{

				const auto radius_2 = brush_radius_m_ * brush_radius_m_;

				auto [mesh]          = state.world().lock_mesh();
				auto       positions = state.world().required_layer(layer_position);
				const auto world_radius =
				        state.world().required_unstructured_layer("meta")["radius"].get<float>();

				const auto in_range = [&](const Vertex v) {
					auto p = positions[v] * world_radius;
					return std::any_of(seed_positions_.begin(), seed_positions_.end(), [&](auto& pp) {
						return dist2(p, pp) <= radius_2;
					});
				};

				auto seen_edges = std::unordered_set<Primal_edge>();

				auto openlists = std::array<std::vector<Vertex>, 2>{
				        std::vector(seed_vertices_.begin(), seed_vertices_.end())};
				auto current = 0;

				while(!openlists[current].empty()) {
					const auto next = current == 0 ? 1 : 0;

					for(auto v : openlists[current]) {
						if(!v.valid(mesh))
							continue;

						for(auto e : v.edges(mesh)) {
							const auto dest = e.dest(mesh);
							if(in_range(dest)) {
								if(seen_edges.insert(e.base()).second) {
									openlists[next].push_back(dest);
								}
							}
						}
					}

					openlists[current].clear();
					current = next;
				}

				seen_edges.insert(seed_edges_.begin(), seed_edges_.end());

				if(increase_) {
					for(auto& e : seen_edges) {
						if(e.valid(mesh))
							mesh.try_split(e, 0.5f);
					}
				} else {
					for(auto& e : seen_edges) {
						if(!e.valid(mesh))
							continue;

						try_collapse(mesh, e, 0.5f, positions) || try_collapse(mesh, e, 0.333f, positions)
						        || try_collapse(mesh, e, 0.666f, positions);
					}
				}
			}

			[[nodiscard]] bool force_synchronous() const override { return true; }
			[[nodiscard]] bool repeatable() const override { return false; }
			[[nodiscard]] bool noop() const { return seed_vertices_.empty(); }

			static constexpr std::string_view id = "resolution";
			std::string_view                  serialize(Dictionary_view cfg) override
			{
				cfg["incr"]   = increase_;
				cfg["r"]      = brush_radius_m_;
				auto vertices = cfg["v"].get<Array_ref<std::int32_t>>();
				vertices.reserve(static_cast<index_t>(seed_vertices_.size()));
				for(auto v : seed_vertices_)
					vertices.push_back(v.index());

				cfg["p"].get<Array_ref<Vec3>>() = seed_positions_;

				auto edges = cfg["e"].get<Array_ref<std::int32_t>>();
				edges.reserve(static_cast<index_t>(seed_edges_.size()));
				for(auto e : seed_edges_)
					edges.push_back(e.index());

				return id;
			}
			static std::unique_ptr<Command> deserialize(Const_dictionary_view cfg)
			{
				auto cmd      = std::make_unique<Change_resolution_cmd>(cfg["incr"].get<float>(),
                                                                   cfg["r"].get<float>());
				auto vertices = cfg["v"].get<Const_array_view<std::int32_t>>();
				cmd->seed_vertices_.reserve(vertices.size());
				for(auto i : vertices)
					cmd->seed_vertices_.emplace(Vertex{i});

				auto positions = cfg["p"].get<Const_array_view<Vec3>>();
				cmd->seed_positions_.resize(positions.size());
				std::copy(positions.begin(), positions.end(), cmd->seed_positions_.begin());

				auto edges = cfg["e"].get<Const_array_view<std::int32_t>>();
				cmd->seed_edges_.reserve(edges.size());
				for(auto i : edges)
					cmd->seed_edges_.emplace(Primal_edge{i});

				return cmd;
			}

		  private:
			std::unordered_set<Vertex>      seed_vertices_;
			std::vector<Vec3>               seed_positions_;
			std::unordered_set<Primal_edge> seed_edges_;
			bool                            increase_;
			float                           brush_radius_m_;
		};


		class Mesh_resolution : public Cmd_based_tool<Change_resolution_cmd> {
		  private:
			enum class Mode { inactive, increase, decrease };
			float brush_radius_km_ = 1'000.f;
			Mode  mode_            = Mode::inactive;

			std::unordered_set<Vertex> tmp_seen_vertices_;


		  public:
			using Cmd_based_tool::Cmd_based_tool;

			util::String_literal button_label() const override
			{
				return yggdrasill::icons::tool_mesh_subdivision;
			}
			util::String_literal tooltip() const override { return "Change Local Mesh Resolution"_str; }
			auto                 used_buttons() const -> ImGuiButtonFlags override
			{
				return ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight;
			}

			void update(simulation::Cursor&, Selection& selection) override
			{
				ImGui::SliderFloat("Brush Size", &brush_radius_km_, 10.f, 2'000.f, "%.1f km");

				const auto pure_color = [&] {
					switch(mode_) {
						case Mode::inactive: return "#aaa"_color;
						case Mode::increase: return "#8f8"_color;
						case Mode::decrease: return "#f88"_color;
					}
					return "#f00"_color;
				}();
				const auto color = gui::apply_alpha(pure_color, command_active() ? 1.f : 0.6f);
				selection.highlight_area(
				        {selection.vertex(), selection.world_position()}, brush_radius_km_ * 1000.f, color);
			}

			bool do_on_use(simulation::Cursor& state, Selection& selection, ImGuiButtonFlags buttons) override
			{
				if((buttons & used_buttons()) == 0) {
					mode_ = Mode::inactive;
					return false;
				}

				mode_ = (buttons & ImGuiButtonFlags_MouseButtonLeft) != 0 ? Mode::increase : Mode::decrease;

				auto& cmd = command(mode_ == Mode::increase, brush_radius_km_ * 1000.f);

				if(selection.vertex() != no_vertex) {
					cmd.add(selection.vertex(), selection.world_position(), selection.primal_edge());
				}
				return true;
			}
		};
	} // namespace

	std::unique_ptr<Tool> mesh_resolution(simulation::Graph& graph)
	{
		return std::make_unique<Mesh_resolution>(graph);
	}

} // namespace yggdrasill::tools
