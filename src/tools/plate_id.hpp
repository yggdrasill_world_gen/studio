#pragma once

#include <memory>

namespace yggdrasill::simulation {
	class Graph;
}

namespace yggdrasill::tools {
	class Tool;

	extern std::unique_ptr<Tool> plate_id(simulation::Graph&);

} // namespace yggdrasill::tools
