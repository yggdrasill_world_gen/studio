#include "velocity.hpp"

#include "../tool.hpp"
#include "../gui/ui_elements.hpp"

#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <yggdrasill_icons.hpp>


namespace yggdrasill::tools {

	namespace {
		using Plate_id = std::int32_t;

		constexpr auto layer_position = Layer_definition<Vec3, Ref_type::vertex>("position");
		constexpr auto layer_velocity =
		        Layer_definition<Vec3, Ref_type::vertex>("velocity").interpolation(Interpolation::max_weight);
		constexpr auto layer_plate_id = Layer_definition<Plate_id, Ref_type::vertex>("plate_id")
		                                        .interpolation(Interpolation::max_weight)
		                                        .invalidation(Invalidation::reset_affected);

		constexpr auto max_velocity    = 0.2f;
		constexpr auto velocity_factor = 1e-7f;

		constexpr auto normalize_dt = 10'000.f;

		Vec3 normalized_velocity(const Vec3& position, const Vec3& velocity)
		{
			// move position one step with given velocity and normalize it back to spheres surface
			const auto next_position = normalized(position + velocity * normalize_dt) * length(position);
			return (next_position - position) / normalize_dt; // calculate actual velocity
		}

		auto to_glm(const Vec3 v)
		{
			return glm::vec3{v.x, v.y, v.z};
		}
		auto from_glm(const glm::vec3 v)
		{
			return Vec3{v.x, v.y, v.z};
		}

		class Paint_velocity_cmd : public simulation::Command_base<Paint_velocity_cmd> {
		  public:
			Paint_velocity_cmd(Vertex vertex, Vertex pivot) : vertex_(vertex), pivot_(pivot) {}

			void change(Vec3 position_difference) { position_difference_ = position_difference; }

			[[nodiscard]] std::string_view description() const override { return "Plate Direction Change"; }
			[[nodiscard]] simulation::Command_type type() const override
			{
				return simulation::Command_type::user_world_edit;
			}
			void execute(simulation::State& state) override
			{
				if(position_difference_.is_nothing())
					return;

				auto mesh = state.world().mesh();
				if(!vertex_.valid(mesh))
					return;

				auto [positions, plate_ids] = state.world().required_layer(layer_position, layer_plate_id);
				auto [velocities]           = state.world().lock_layer(layer_velocity);
				const auto radius = state.world().required_unstructured_layer("meta")["radius"].get<float>();

				const auto plate_id = plate_ids[vertex_];
				auto       seen     = std::unordered_set<Vertex>{};

				// velocity change with pivot, i.e. part translation and part rotation
				if(pivot_ != no_vertex && pivot_ != vertex_) {
					const auto pivot_p         = positions[pivot_] * radius;
					const auto pivot_to_vertex = positions[vertex_] * radius - pivot_p;
					const auto base_axis       = normalized(pivot_to_vertex);

					// determine the part of the new velocity that should be realized through translation (dragged vertex away from pivot)
					const auto translation =
					        base_axis * dot(pivot_to_vertex + position_difference_.get_or_throw(), base_axis)
					        - pivot_to_vertex;
					const auto velocity = translation * velocity_factor;

					// determine the part of the new velocity that should be realized through rotation (dragged vertex perpendicular to pivot)
					// premultiplied by the scale factors, for use in flood-fill below
					const auto rotation = slerp(
					        glm::quat(1, 0, 0, 0),
					        glm::rotation(to_glm(base_axis),
					                      to_glm(normalized(pivot_to_vertex + position_difference_.get_or_throw()
					                                        - translation))),
					        normalize_dt * velocity_factor);

					// flood fill plate and set velocity, normalized to each vertex
					// clang-format off
					flood_fill(
							mesh,
							std::vector{std::pair{vertex_, velocity}},
							Neighbor_type::just_primal_edges,
							[&](Vertex v, Vec3 local_velocity) -> std::optional<Vec3> {
						if(plate_ids[v] != plate_id || !seen.insert(v).second)
							return std::nullopt;

						const auto p = positions[v]*radius;
						local_velocity = normalized_velocity(p, local_velocity);

						auto next_position = from_glm(rotation*to_glm(p-pivot_p)) + pivot_p; // rotate
						next_position += local_velocity * normalize_dt; // move
						next_position = normalized(next_position) * length(p); // re-normalize to sphere surface
						velocities[v] = limit_length((next_position - p) / normalize_dt, max_velocity); // calculate actual velocity

						return local_velocity;
					});
					// clang-format on

				} else { // velocity change without pivot, i.e. just translation
					     // flood fill plate and set velocity, normalized to each vertex

					auto velocity      = position_difference_.get_or_throw() * velocity_factor;
					auto velocity_len2 = length2(velocity);
					if(velocity_len2 > max_velocity * max_velocity)
						velocity = velocity / std::sqrt(velocity_len2) * max_velocity;

					// clang-format off
					flood_fill(
							mesh,
							std::vector{std::pair{vertex_, velocity}},
							Neighbor_type::just_primal_edges,
							[&](Vertex v, Vec3 new_vel) -> std::optional<Vec3> {
						if(plate_ids[v] != plate_id || !seen.insert(v).second)
							return std::nullopt;

						return velocities[v] = normalized_velocity(positions[v]*radius, new_vel);
					});
					// clang-format on
				}
			}

			[[nodiscard]] bool force_synchronous() const override { return true; }
			[[nodiscard]] bool repeatable() const override { return false; }
			[[nodiscard]] bool noop() const { return position_difference_.is_nothing(); }

			static constexpr std::string_view id = "velocity";
			std::string_view                  serialize(Dictionary_view cfg) override
			{
				cfg["vertex"] = vertex_.index();
				cfg["pivot"]  = pivot_.index();
				if(position_difference_.is_some())
					cfg["diff"] = position_difference_.get_or_throw();
				return id;
			}
			static std::unique_ptr<Command> deserialize(Const_dictionary_view cfg)
			{
				auto cmd = std::make_unique<Paint_velocity_cmd>(Vertex{cfg["vertex"].get<std::int32_t>()},
				                                                Vertex{cfg["pivot"].get<std::int32_t>()});
				if(auto diff = cfg["diff"]; diff.exists())
					cmd->change(diff.get<Vec3>());
				return cmd;
			}

		  private:
			Vertex            vertex_;
			Vertex            pivot_;
			util::maybe<Vec3> position_difference_;
		};

		class Velocity : public Cmd_based_tool<Paint_velocity_cmd> {
		  private:
			util::maybe<Vec3> initial_vertex_position_;
			Vertex            initial_vertex_;
			Vertex            pivot_vertex_;
			bool              right_mouse_pressed_ = false;


		  public:
			using Cmd_based_tool::Cmd_based_tool;

			util::String_literal button_label() const override
			{
				return yggdrasill::icons::tool_plate_velocity;
			}
			util::String_literal tooltip() const override
			{
				return "Changes the velocity of tectonic plates"_str;
			}
			auto used_buttons() const -> ImGuiButtonFlags override
			{
				return ImGuiButtonFlags_MouseButtonLeft | ImGuiButtonFlags_MouseButtonRight;
			}

			void update(simulation::Cursor&, Selection& selection) override
			{
				ImGui::TextWrapped("Left click and drag to override the movement direction of a plate.");
				ImGui::TextWrapped(
				        "Right click to select a pivot point, around which the plate should rotate.");

				if(pivot_vertex_ != no_vertex) {
					selection.highlight(pivot_vertex_, "#db1"_color, 8.f);
					selection.add_label(pivot_vertex_, "Pivot Point");
				}
				if(initial_vertex_ != no_vertex) {
					selection.highlight(initial_vertex_, "#fff"_color, 4.f);
				} else if(selection.vertex() != no_vertex) {
					selection.highlight(selection.vertex(), "#aaa"_color, 4.f);
				}
			}

			bool do_on_use(simulation::Cursor& state, Selection& selection, ImGuiButtonFlags buttons) override
			{
				auto world = state.world();
				if(!world)
					return false;

				if(buttons & ImGuiButtonFlags_MouseButtonRight) {
					right_mouse_pressed_ = true;
				} else if(right_mouse_pressed_) {
					pivot_vertex_        = pivot_vertex_ != no_vertex ? no_vertex : selection.vertex();
					right_mouse_pressed_ = false;
				}

				if(buttons & ImGuiButtonFlags_MouseButtonLeft) {
					if(initial_vertex_position_.is_nothing()) {
						auto positions = world->layer(layer_position);
						if(!positions || !selection.vertex().valid(world->mesh()))
							return true;

						const auto radius = world->required_unstructured_layer("meta")["radius"].get<float>();

						initial_vertex_          = selection.vertex();
						initial_vertex_position_ = (*positions)[selection.vertex()] * radius;
					}

					auto& cmd = command(initial_vertex_, pivot_vertex_);

					if(selection.vertex() != no_vertex)
						cmd.change(selection.world_position() - initial_vertex_position_.get_or_throw());

					return true;

				} else {
					initial_vertex_position_.reset();
					initial_vertex_ = no_vertex;
				}

				return false;
			}
		};
	} // namespace

	std::unique_ptr<Tool> velocity(simulation::Graph& graph)
	{
		return std::make_unique<Velocity>(graph);
	}

} // namespace yggdrasill::tools
