#include "editor.hpp"

#include "gui/file_dialogs.hpp"
#include "gui/popup.hpp"
#include "gui/ui_elements.hpp"
#include "util/ranges.hpp"
#include "views/base.hpp"
#include "views/grid.hpp"
#include "views/interpolated_area_views.hpp"
#include "views/map.hpp"
#include "views/solid_area_views.hpp"
#include "views/vector_views.hpp"
#include "views/wireframe.hpp"
#include "windows/config.hpp"
#include "windows/export.hpp"
#include "windows/timeline.hpp"
#include "windows/tools.hpp"
#include "windows/viewport.hpp"

#include <yggdrasill/layer.hpp>
#include <yggdrasill/serialization.hpp>

#include <imgui.h>
#include <imgui_internal.h>
#include <range/v3/all.hpp>
#include <yggdrasill_icons.hpp>

#include <fstream>
#include <random>
#include <sstream>


namespace yggdrasill {
	extern float current_time();
	extern void  set_window_title_suffix(const std::string& str);
} // namespace yggdrasill

namespace yggdrasill {

	namespace {
		constexpr auto show_error_seconds = 5;

		constexpr auto debug_seed = yggdrasill::Seed::from({
		        2148334203,
		        346516426,
		        1904038130,
		        3887960964,
		});
	} // namespace


	auto Editor::create_viewport(gui::Config& cfg, const std::string& id) -> std::unique_ptr<windows::Viewport>
	{
		return std::make_unique<windows::Viewport>(&main_dock_,
		                                           cfg,
		                                           id,
		                                           std::as_const(camera_buttons_),
		                                           renderer_,
		                                           views::map,
		                                           windows::hidden(views::plates),
		                                           windows::hidden(views::elevation),
		                                           windows::hidden(views::age),
		                                           windows::hidden(views::target_density),
		                                           windows::hidden(windows::alpha(views::wireframe, 0.2f)),
		                                           windows::hidden(windows::alpha(views::grid_cells, 0.2f)),
		                                           windows::hidden(windows::alpha(views::grid_hex, 0.2f)),
		                                           windows::alpha(views::grid_lat_long, 0.2f),
		                                           views::plate_velocities);
	}

	Editor::Editor(const std::string& plugin_dir)
	  : viewport_cfg_(gui::Config::create(*ImGui::GetCurrentContext(), "viewports"))
	  , cfg_(gui::Config::create(*ImGui::GetCurrentContext(), "editor"))
	  , global_settings_(cfg_["global"])
	  , command_manager_(plugin_dir)
	  , graph_()
	  , current_state_(graph_)
	  , renderer_(global_settings_)
	{
		command_manager_.register_commands(graph_);

		graph_.add_world("New World", command_manager_.create_initial_state(create_world_cfg(debug_seed)));
		command_manager_.execute(command_manager_.list()[0], graph_);

		dark_mode_       = cfg_["main"].get("dark_mode", true);
		validate_mesh_   = cfg_["main"].get("validate_mesh", false);
		validate_layers_ = cfg_["main"].get("validate_layer", true);
		validate_mesh_on_release(validate_mesh_);
		validate_layers_on_release(validate_layers_);

		viewport_cfg_.foreach_entry(
		        [&](auto& key) { viewports_.push_back(create_viewport(viewport_cfg_, key)); });

		if(viewports_.empty()) {
			viewports_.push_back(create_viewport(viewport_cfg_));
		}

		std::sort(viewports_.begin(), viewports_.end(), [](auto& lhs, auto& rhs) {
			return lhs->id() < rhs->id();
		});

		windows_.push_back(windows::timeline(
		        &bottom_dock_, cfg_, true, graph_, command_manager_, current_state_, dark_mode_));
		windows_.push_back(
		        windows::create_world_config(&bottom_dock_, cfg_, false, current_state_, command_manager_));
		create_world_window_ = windows_.back().get();
		windows_.push_back(windows::config(
		        &bottom_dock_, cfg_, false, current_state_, command_manager_, global_settings_));
		windows_.push_back(windows::tools(&left_dock_, cfg_, true, current_state_, selection_, camera_buttons_));

		update_style();

		saved_version_ = graph_.version();
	}
	Editor::~Editor() = default;

	void Editor::update_style()
	{
		cfg_["main"].set("dark_mode", dark_mode_);

		ImGuiStyle& style       = ImGui::GetStyle();
		style.WindowPadding     = ImVec2(8.00f, 8.00f);
		style.FramePadding      = ImVec2(5.00f, 2.00f);
		style.CellPadding       = ImVec2(6.00f, 6.00f);
		style.ItemSpacing       = ImVec2(6.00f, 6.00f);
		style.ItemInnerSpacing  = ImVec2(6.00f, 6.00f);
		style.TouchExtraPadding = ImVec2(0.00f, 0.00f);
		style.AntiAliasedLines  = true;
		style.AntiAliasedFill   = true;
		style.IndentSpacing     = 25;
		style.ScrollbarSize     = 15;
		style.GrabMinSize       = 10;
		style.WindowBorderSize  = 1;
		style.ChildBorderSize   = 1;
		style.PopupBorderSize   = 1;
		style.FrameBorderSize   = 1;
		style.TabBorderSize     = 1;
		style.WindowRounding    = 7;
		style.ChildRounding     = 4;
		style.FrameRounding     = 3;
		style.PopupRounding     = 4;
		style.ScrollbarRounding = 9;
		style.GrabRounding      = 3;
		style.LogSliderDeadzone = 4;
		style.TabRounding       = 4;

		if(dark_mode_) {
			ImGui::StyleColorsDark();
			ImVec4* colors                         = ImGui::GetStyle().Colors;
			colors[ImGuiCol_Text]                  = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
			colors[ImGuiCol_TextDisabled]          = ImVec4(0.70f, 0.70f, 0.70f, 1.00f);
			colors[ImGuiCol_WindowBg]              = ImVec4(0.10f, 0.10f, 0.10f, 0.95f);
			colors[ImGuiCol_ChildBg]               = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
			colors[ImGuiCol_PopupBg]               = ImVec4(0.19f, 0.19f, 0.19f, 0.92f);
			colors[ImGuiCol_Border]                = ImVec4(0.19f, 0.19f, 0.19f, 0.29f);
			colors[ImGuiCol_BorderShadow]          = ImVec4(0.00f, 0.00f, 0.00f, 0.24f);
			colors[ImGuiCol_FrameBg]               = ImVec4(0.0f, 0.0f, 0.0f, 0.6f);
			colors[ImGuiCol_FrameBgHovered]        = ImVec4(0.19f, 0.19f, 0.19f, 0.54f);
			colors[ImGuiCol_FrameBgActive]         = ImVec4(0.20f, 0.22f, 0.23f, 1.00f);
			colors[ImGuiCol_TitleBg]               = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_TitleBgActive]         = ImVec4(0.06f, 0.06f, 0.06f, 1.00f);
			colors[ImGuiCol_TitleBgCollapsed]      = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_MenuBarBg]             = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
			colors[ImGuiCol_ScrollbarBg]           = ImVec4(0.05f, 0.05f, 0.05f, 0.54f);
			colors[ImGuiCol_ScrollbarGrab]         = ImVec4(0.34f, 0.34f, 0.34f, 0.54f);
			colors[ImGuiCol_ScrollbarGrabHovered]  = ImVec4(0.40f, 0.40f, 0.40f, 0.54f);
			colors[ImGuiCol_ScrollbarGrabActive]   = ImVec4(0.56f, 0.56f, 0.56f, 0.54f);
			colors[ImGuiCol_CheckMark]             = ImVec4(0.4f, 0.6f, 0.2f, 1.00f);
			colors[ImGuiCol_SliderGrab]            = ImVec4(0.34f, 0.34f, 0.34f, 0.54f);
			colors[ImGuiCol_SliderGrabActive]      = ImVec4(0.56f, 0.56f, 0.56f, 0.54f);
			colors[ImGuiCol_Button]                = ImVec4(0.05f, 0.05f, 0.05f, 0.54f);
			colors[ImGuiCol_ButtonHovered]         = ImVec4(0.19f, 0.19f, 0.19f, 0.54f);
			colors[ImGuiCol_ButtonActive]          = ImVec4(0.20f, 0.22f, 0.23f, 1.00f);
			colors[ImGuiCol_Header]                = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_HeaderHovered]         = ImVec4(0.00f, 0.00f, 0.00f, 0.36f);
			colors[ImGuiCol_HeaderActive]          = ImVec4(0.20f, 0.22f, 0.23f, 0.33f);
			colors[ImGuiCol_Separator]             = ImVec4(0.28f, 0.28f, 0.28f, 0.29f);
			colors[ImGuiCol_SeparatorHovered]      = ImVec4(0.44f, 0.44f, 0.44f, 0.29f);
			colors[ImGuiCol_SeparatorActive]       = ImVec4(0.40f, 0.44f, 0.47f, 1.00f);
			colors[ImGuiCol_ResizeGrip]            = ImVec4(0.28f, 0.28f, 0.28f, 0.29f);
			colors[ImGuiCol_ResizeGripHovered]     = ImVec4(0.44f, 0.44f, 0.44f, 0.29f);
			colors[ImGuiCol_ResizeGripActive]      = ImVec4(0.40f, 0.44f, 0.47f, 1.00f);
			colors[ImGuiCol_Tab]                   = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_TabHovered]            = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
			colors[ImGuiCol_TabActive]             = ImVec4(0.20f, 0.20f, 0.20f, 0.36f);
			colors[ImGuiCol_TabUnfocused]          = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_TabUnfocusedActive]    = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
			colors[ImGuiCol_DockingPreview]        = ImVec4(0.5f, 0.8f, 0.4f, 0.70f);
			colors[ImGuiCol_PlotLines]             = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_PlotLinesHovered]      = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_PlotHistogram]         = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_PlotHistogramHovered]  = ImVec4(1.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_TableHeaderBg]         = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_TableBorderStrong]     = ImVec4(0.00f, 0.00f, 0.00f, 0.52f);
			colors[ImGuiCol_TableBorderLight]      = ImVec4(0.28f, 0.28f, 0.28f, 0.29f);
			colors[ImGuiCol_TableRowBg]            = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
			colors[ImGuiCol_TableRowBgAlt]         = ImVec4(1.00f, 1.00f, 1.00f, 0.06f);
			colors[ImGuiCol_TextSelectedBg]        = ImVec4(0.20f, 0.22f, 0.23f, 1.00f);
			colors[ImGuiCol_DragDropTarget]        = ImVec4(0.33f, 0.67f, 0.86f, 1.00f);
			colors[ImGuiCol_NavHighlight]          = ImVec4(0.80f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_NavWindowingHighlight] = ImVec4(0.80f, 0.00f, 0.00f, 0.70f);
			colors[ImGuiCol_NavWindowingDimBg]     = ImVec4(0.80f, 0.00f, 0.00f, 0.20f);
			colors[ImGuiCol_ModalWindowDimBg]      = ImVec4(0.1f, 0.00f, 0.00f, 0.8f);

		} else {
			ImGui::StyleColorsLight();
			ImVec4* colors                         = style.Colors;
			colors[ImGuiCol_Text]                  = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_TextDisabled]          = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
			colors[ImGuiCol_WindowBg]              = ImVec4(0.86f, 0.86f, 0.86f, 1.00f);
			colors[ImGuiCol_ChildBg]               = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
			colors[ImGuiCol_PopupBg]               = ImVec4(0.93f, 0.93f, 0.93f, 0.98f);
			colors[ImGuiCol_Border]                = ImVec4(0.71f, 0.71f, 0.71f, 0.08f);
			colors[ImGuiCol_BorderShadow]          = ImVec4(0.00f, 0.00f, 0.00f, 0.04f);
			colors[ImGuiCol_FrameBg]               = ImVec4(0.71f, 0.71f, 0.71f, 0.55f);
			colors[ImGuiCol_FrameBgHovered]        = ImVec4(0.94f, 0.94f, 0.94f, 0.55f);
			colors[ImGuiCol_FrameBgActive]         = ImVec4(0.71f, 0.78f, 0.69f, 0.98f);
			colors[ImGuiCol_TitleBg]               = ImVec4(0.85f, 0.85f, 0.85f, 1.00f);
			colors[ImGuiCol_TitleBgCollapsed]      = ImVec4(0.82f, 0.78f, 0.78f, 0.51f);
			colors[ImGuiCol_TitleBgActive]         = ImVec4(0.78f, 0.78f, 0.78f, 1.00f);
			colors[ImGuiCol_MenuBarBg]             = ImVec4(0.86f, 0.86f, 0.86f, 1.00f);
			colors[ImGuiCol_ScrollbarBg]           = ImVec4(0.20f, 0.30f, 0.25f, 0.61f);
			colors[ImGuiCol_ScrollbarGrab]         = ImVec4(0.90f, 0.90f, 0.90f, 0.30f);
			colors[ImGuiCol_ScrollbarGrabHovered]  = ImVec4(0.92f, 0.92f, 0.92f, 0.78f);
			colors[ImGuiCol_ScrollbarGrabActive]   = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
			colors[ImGuiCol_CheckMark]             = ImVec4(0.184f, 0.407f, 0.193f, 1.00f);
			colors[ImGuiCol_SliderGrab]            = ImVec4(0.184f, 0.407f, 0.193f, 1.f);
			colors[ImGuiCol_SliderGrabActive]      = ImVec4(0.184f, 0.6f, 0.193f, 1.f);
			colors[ImGuiCol_Button]                = ImVec4(0.71f, 0.78f, 0.69f, 0.40f);
			colors[ImGuiCol_ButtonHovered]         = ImVec4(0.725f, 0.805f, 0.702f, 1.00f);
			colors[ImGuiCol_ButtonActive]          = ImVec4(0.793f, 0.900f, 0.836f, 1.00f);
			colors[ImGuiCol_Header]                = ImVec4(0.71f, 0.78f, 0.69f, 0.31f);
			colors[ImGuiCol_HeaderHovered]         = ImVec4(0.71f, 0.78f, 0.69f, 0.80f);
			colors[ImGuiCol_HeaderActive]          = ImVec4(0.71f, 0.78f, 0.69f, 1.00f);
			colors[ImGuiCol_TableHeaderBg]         = ImVec4(0.71f, 0.78f, 0.69f, 0.31f);
			colors[ImGuiCol_Tab]                   = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
			colors[ImGuiCol_TabHovered]            = ImVec4(0.75f, 1.0f, 0.72f, 0.78f);
			colors[ImGuiCol_TabActive]             = ImVec4(0.75f, 0.84f, 0.72f, 1.00f);
			colors[ImGuiCol_TabUnfocusedActive]    = ImVec4(0.75f, 0.84f, 0.72f, 1.00f);
			colors[ImGuiCol_TabUnfocused]          = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
			colors[ImGuiCol_DockingPreview]        = ImVec4(0.537f, 0.858f, 0.419f, 1.00f);
			colors[ImGuiCol_Separator]             = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
			colors[ImGuiCol_SeparatorHovered]      = ImVec4(0.14f, 0.80f, 0.44f, 0.78f);
			colors[ImGuiCol_SeparatorActive]       = ImVec4(0.14f, 0.80f, 0.44f, 1.00f);
			colors[ImGuiCol_ResizeGrip]            = ImVec4(1.00f, 1.00f, 1.00f, 0.00f);
			colors[ImGuiCol_ResizeGripHovered]     = ImVec4(0.26f, 0.98f, 0.59f, 0.45f);
			colors[ImGuiCol_ResizeGripActive]      = ImVec4(0.26f, 0.98f, 0.59f, 0.78f);
			colors[ImGuiCol_PlotLines]             = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
			colors[ImGuiCol_PlotLinesHovered]      = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
			colors[ImGuiCol_PlotHistogram]         = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
			colors[ImGuiCol_PlotHistogramHovered]  = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
			colors[ImGuiCol_TextSelectedBg]        = ImVec4(0.26f, 0.98f, 0.59f, 0.35f);
			colors[ImGuiCol_ModalWindowDimBg]      = ImVec4(0.20f, 0.20f, 0.20f, 0.35f);
			colors[ImGuiCol_DragDropTarget]        = ImVec4(0.26f, 0.98f, 0.59f, 0.95f);
			colors[ImGuiCol_NavHighlight]          = ImVec4(0.80f, 0.00f, 0.00f, 1.00f);
			colors[ImGuiCol_NavWindowingHighlight] = ImVec4(0.80f, 0.00f, 0.00f, 0.70f);
			colors[ImGuiCol_NavWindowingDimBg]     = ImVec4(0.00f, 0.1f, 0.00f, 0.8f);
		}
	}


	void Editor::update()
	{
		update_generator();

		if(reset_layout_) {
			reset_layout();
			reset_layout_ = false;
		}

		if(!ImGui::GetIO().WantTextInput
		   && (ImGui::GetIO().ConfigMacOSXBehaviors ? ImGui::GetIO().KeySuper : ImGui::GetIO().KeyCtrl)) {
			if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Z))) {
				current_state_.seek_backward();
			} else if(ImGui::IsKeyPressed(ImGui::GetKeyIndex(ImGuiKey_Y))) {
				current_state_.seek_forward_if_ready();
			}
		}

		draw_dockspace();

		for(auto& w : windows_) {
			w->update();
		}

		if(auto node = current_state_.node(); node && node->ready())
			renderer_.update(node->state().world(), selection_);
		else
			renderer_.update(Const_world_view{}, selection_);

		for(auto& v : viewports_)
			v->update();

		selection_.update(renderer_.get_hovered_geometry());

		draw_menu();
		draw_statusbar();

		if(save_file_.has_value() && !modified_since_save_ && saved_version_ != graph_.version()) {
			modified_since_save_ = true;
			set_window_title_suffix("*" + *save_file_);
		}

		if(!graph_.editable()) {
			gui::spinner(*ImGui::GetForegroundDrawList(),
			             ImGui::GetIO().MousePos + ImVec2{8 * 2, 8 * 3},
			             8.f,
			             4.f,
			             ImGui::GetColorU32(ImGuiCol_CheckMark),
			             false);
		}
	}

	void Editor::update_generator()
	{
		try {
			graph_.rethrow_io_error();
		} catch(const std::exception& e) {
			status_message_ = Status_message{"Load/Save failed", ImVec4(1, 0, 0, 1), show_error_seconds};
			gui::show_error_dialog("Load/Save failed",
			                       std::string("The operation couldn't complete due to an "
			                                   "internal error:\n")
			                               + e.what());
		}

		try {
			graph_.rethrow_execution_error();
		} catch(const std::runtime_error& e) {
			std::cerr << "Unexpected error in generator: " << e.what() << "\n";
			status_message_ = Status_message{std::string("Unexpected error in generator: ") + e.what(),
			                                 ImVec4(1, 0, 0, 1),
			                                 show_error_seconds};
		}
	}

	void Editor::draw_dockspace()
	{
		ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
		ImGuiViewport*   viewport     = ImGui::GetMainViewport();
		ImGui::SetNextWindowPos(viewport->Pos);
		ImGui::SetNextWindowSize(viewport->Size - ImVec2{0, ImGui::GetFrameHeight()});
		ImGui::SetNextWindowViewport(viewport->ID);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
		ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
		window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize
		                | ImGuiWindowFlags_NoMove;
		window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;

		ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
		ImGui::Begin("main_dock_win", nullptr, window_flags);
		ImGui::PopStyleVar();

		ImGui::PopStyleVar(2);

		if(ImGui::DockBuilderGetNode(ImGui::GetID("main_dock")) == nullptr) {
			create_dockspace();
		}

		ImGuiID dockspace_id = ImGui::GetID("main_dock");
		ImGui::DockSpace(dockspace_id, ImVec2(0.0f, 0.0f), 0);
		ImGui::End();
	}

	void Editor::create_dockspace()
	{
		ImGuiID        dockspace_id = ImGui::GetID("main_dock");
		ImGuiViewport* viewport     = ImGui::GetMainViewport();

		ImGui::DockBuilderRemoveNode(dockspace_id);                            // Clear out existing layout
		ImGui::DockBuilderAddNode(dockspace_id, ImGuiDockNodeFlags_DockSpace); // Add empty node
		ImGui::DockBuilderSetNodeSize(dockspace_id, viewport->Size);

		main_dock_   = dockspace_id;
		left_dock_   = ImGui::DockBuilderSplitNode(main_dock_, ImGuiDir_Left, 0.20f, nullptr, &main_dock_);
		bottom_dock_ = ImGui::DockBuilderSplitNode(main_dock_, ImGuiDir_Down, 0.30f, nullptr, &main_dock_);
		ImGui::DockBuilderGetNode(main_dock_)->LocalFlags |= ImGuiDockNodeFlags_AutoHideTabBar;
		ImGui::DockBuilderGetNode(left_dock_)->LocalFlags |= ImGuiDockNodeFlags_AutoHideTabBar;
		ImGui::DockBuilderGetNode(bottom_dock_)->LocalFlags |= ImGuiDockNodeFlags_AutoHideTabBar;

		ImGui::DockBuilderFinish(dockspace_id);
	}

	void Editor::reset_layout()
	{
		ImGui::DockBuilderRemoveNode(ImGui::GetID("main_dock"));

		viewports_.clear();
		yggdrasill::windows::reset_viewport_ids();

		cfg_.clear(gui::Config_clear::values);
		viewport_cfg_.clear(gui::Config_clear::all);
		ImGui::ClearIniSettings();

		create_dockspace();

		viewports_.push_back(create_viewport(viewport_cfg_));

		for(auto& w : windows_) {
			w->reset_position();
		}
		for(auto& v : viewports_) {
			v->reset_position();
		}
	}

	void Editor::draw_menu()
	{
		if(ImGui::IsKeyPressed(ImGuiKey_S) && (ImGui::GetIO().KeyMods & ImGuiModFlags_Ctrl) != 0
		   && graph_.editable()) {
			quick_save();
		}

		if(ImGui::BeginMainMenuBar()) {
			if(ImGui::BeginMenu("File")) {
				if(ImGui::MenuItem(YGGDRASILL_ICON_FILE " New", nullptr, false, graph_.editable())) {
					create_world_window_->reset_position();
					create_world_window_->visible(true);
				}

				if(ImGui::MenuItem(YGGDRASILL_ICON_TRASH " Discard Timeline", nullptr, false, graph_.editable())) {
					safe_destructive_operation([&] { graph_.clear(); });
				}

				if(ImGui::MenuItem(YGGDRASILL_ICON_FOLDER_OPEN " Load...",
				                   nullptr,
				                   false,
				                   graph_.editable() && yggdrasill::serialization_supported())) {
					safe_destructive_operation([&] {
						gui::load_dialog("Load",
						                 save_file_.value_or("world.ygs"),
						                 {"Yggdrasill Save File", "*.ygs"},
						                 [&](std::string_view path, std::function<void()> continuation) {
							                 if(!graph_.editable())
								                 return;

							                 save_file_ = path;
							                 graph_.clear();
							                 status_message_ = Status_message{
							                         "Load started. Please wait...", ImVec4(1, 1, 1, 1), 99999};
							                 graph_.start_load(std::string(path),
							                                   [this, continuation = std::move(continuation)] {
								                                   current_state_.seek(graph_.main_branch());
								                                   set_window_title_suffix(*save_file_);
								                                   modified_since_save_ = false;
								                                   saved_version_       = graph_.version();
								                                   status_message_      = Status_message{
                                                                           "File successfully loaded",
                                                                           ImVec4(0, 1, 0, 1),
                                                                           show_error_seconds};

								                                   if(continuation)
									                                   continuation();
							                                   });
						                 });
					});
				}
				if(ImGui::MenuItem(YGGDRASILL_ICON_SAVE " Save",
				                   "Ctrl + S",
				                   false,
				                   save_file_.has_value() && graph_.editable()
				                           && yggdrasill::serialization_supported())) {
					quick_save();
				}
				if(ImGui::MenuItem(YGGDRASILL_ICON_SAVE " Save As...",
				                   nullptr,
				                   false,
				                   graph_.editable() && yggdrasill::serialization_supported())) {
					save_as();
				}

				// export menu options
				if(ImGui::BeginMenu(YGGDRASILL_ICON_FILE_EXPORT " Export...")) {
					// complete current world
					if(ImGui::MenuItem("World", nullptr, false, yggdrasill::serialization_supported())) {
						gui::save_dialog("Export Current World - Save Location",
						                 "world.ygw",
						                 {"Yggdrasill World File", "*.ygw"},
						                 [&](std::string_view path) {
							                 try {
								                 auto node = current_state_.node();
								                 if(!node)
									                 return;

								                 auto s = Serializer::open_overwrite(path);
								                 s.write_world(node->state().world());
								                 s.write_config("config", node->state().config());
								                 status_message_ =
								                         Status_message{"World successfully exported",
								                                        ImVec4(0, 1, 0, 1),
								                                        show_error_seconds};

							                 } catch(const std::exception& e) {
								                 gui::show_error_dialog("Export failed",
								                                        std::string("The world couldn't be "
								                                                    "exported because of an "
								                                                    "internal failure:\n")
								                                                + e.what());
							                 }
						                 });
					}

					auto count_active = 0;
					auto viewport     = std::find_if(viewports_.begin(), viewports_.end(), [&](auto& vp) {
                        if(!vp->visible())
                            return false;
                        count_active++;
                        return true;
                    });

					// screenshot of a single view
					if(count_active == 1) {
						if(ImGui::MenuItem("View")) {
							windows::open_image_export_popup(renderer_, **viewport);
						}
					} else if(count_active > 1) {
						if(ImGui::BeginMenu("View...")) {
							for(auto& vp : viewports_)
								if(ImGui::MenuItem(vp->id()))
									windows::open_image_export_popup(renderer_, *vp);

							ImGui::EndMenu();
						}
					} else {
						ImGui::BeginMenu("View...", false);
					}

					// mesh + textures, based on a single view
					if(count_active == 1) {
						if(ImGui::MenuItem("Mesh")) {
							windows::open_mesh_export_popup(renderer_, **viewport);
						}
					} else if(count_active > 1) {
						if(ImGui::BeginMenu("Mesh...")) {
							for(auto& vp : viewports_)
								if(ImGui::MenuItem(vp->id()))
									windows::open_mesh_export_popup(renderer_, *vp);

							ImGui::EndMenu();
						}
					} else {
						ImGui::BeginMenu("Mesh...", false);
					}

					ImGui::EndMenu();
				}

				if(ImGui::MenuItem(YGGDRASILL_ICON_FILE_IMPORT " Import World",
				                   nullptr,
				                   false,
				                   graph_.editable() && yggdrasill::serialization_supported())) {
					gui::load_dialog(
					        "Import World File - Load Location",
					        "world.ygw",
					        {"Yggdrasill World File", "*.ygw"},
					        [&](std::string_view path) {
						        if(!graph_.editable())
							        return;
						        try {
							        auto file   = Deserializer(path);
							        auto loaded = false;
							        file.foreach_world([&](auto world_handle) {
								        if(loaded)
									        return;

								        auto world  = file.read_world(world_handle);
								        auto config = std::make_shared<Dictionary>();
								        file.read_config("config", *config);
								        current_state_.seek(
								                graph_.add_world(util::concat("Imported \"", path, "\""),
								                                 {std::move(world), std::move(config)}));
								        status_message_ = Status_message{"World successfully imported",
								                                         ImVec4(0, 1, 0, 1),
								                                         show_error_seconds};
								        loaded          = true;
							        });
							        if(!loaded) {
								        gui::show_error_dialog(
								                "Invalid export",
								                "The given file doesn't contain any "
								                "world definitions. It is either not a valid export "
								                "file or has been corrupted.");
							        }
						        } catch(const std::exception& e) {
							        gui::show_error_dialog(
							                "Import failed",
							                std::string("The given file couldn't be opened because of an "
							                            "internal failure:\n")
							                        + e.what());
						        }
					        });
				}

				ImGui::Separator();

				if(ImGui::MenuItem(YGGDRASILL_ICON_SYNC " Reload Modules", nullptr, false, graph_.editable())) {
					command_manager_.refresh();
				}

				if(ImGui::MenuItem(YGGDRASILL_ICON_LIGHTBULB "Dark Mode", nullptr, &dark_mode_, true)) {
					update_style();
				}

				if(ImGui::MenuItem(YGGDRASILL_ICON_CHECK_DOUBLE "Validate Mesh", nullptr, &validate_mesh_, true)) {
					validate_mesh_on_release(validate_mesh_);
				}
				gui::ui_help_tooltip("Validate the mesh after each modification");

				if(ImGui::MenuItem(
				           YGGDRASILL_ICON_CHECK_DOUBLE "Validate Layers", nullptr, &validate_layers_, true)) {
					validate_layers_on_release(validate_layers_);
				}
				gui::ui_help_tooltip("Validate the data layers after each modification");

				if(ImGui::MenuItem(YGGDRASILL_ICON_WINDOW_CLOSE " Quit")) {
					quit_ = true;
				}
				ImGui::EndMenu();
			}

			if(ImGui::BeginMenu("Edit")) {
				if(ImGui::MenuItem(
				           YGGDRASILL_ICON_UNDO " Undo", nullptr, false, current_state_.can_seek_backward())) {
					current_state_.seek_backward();
				}
				if(ImGui::MenuItem(YGGDRASILL_ICON_REDO " Redo",
				                   nullptr,
				                   false,
				                   current_state_.can_seek_forward_if_ready())) {
					current_state_.seek_forward_if_ready();
				}
				ImGui::EndMenu();
			}

			if(ImGui::BeginMenu("Window")) {
				for(auto& w : windows_) {
					auto visible = w->visible();
					if(ImGui::MenuItem(w->id(), nullptr, &visible, true)) {
						w->visible(visible);
					}
				}
				ImGui::Separator();

				for(auto& v : viewports_) {
					auto visible = v->visible();
					if(ImGui::MenuItem(v->id(), nullptr, &visible, true)) {
						v->visible(visible);
					}
				}

				ImGui::Separator();

				if(ImGui::MenuItem(YGGDRASILL_ICON_PLUS_SQUARE "Add View", nullptr, false, true)) {
					viewports_.push_back(create_viewport(viewport_cfg_));
				}

				ImGui::Separator();

				if(ImGui::MenuItem(YGGDRASILL_ICON_WINDOW_RESTORE " Reset Layout")) {
					reset_layout_ = true;
				}

				ImGui::EndMenu();
			}

			ImGui::EndMainMenuBar();
		}
	}

	void Editor::draw_statusbar()
	{
		if(ImGui::BeginViewportSideBar("##status_bar",
		                               nullptr,
		                               ImGuiDir_Down,
		                               ImGui::GetFrameHeight(),
		                               ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoSavedSettings
		                                       | ImGuiWindowFlags_MenuBar)) {
			if(ImGui::BeginMenuBar()) {
				if(status_message_.time > 0) {
					if(!ImGui::IsWindowHovered()) {
						status_message_.time -= ImGui::GetIO().DeltaTime;
					}
					ImGui::TextColored(status_message_.color, "%s", status_message_.message.c_str());
					ImGui::Separator();
				}

				for(auto& w : windows_)
					if(w->draw_status())
						break;

				ImGui::EndMenuBar();
			}
			ImGui::End();
		}
	}

	auto Editor::create_world_cfg(const Seed& seed) const -> std::shared_ptr<Dictionary>
	{
		return command_manager_.create_base_config(seed);
	}


	void Editor::safe_destructive_operation(std::function<void()> execute)
	{
		if(saved_version_ == graph_.version()) {
			// no unsaved changes
			execute();
			return;
		}

		gui::open_popup("Save Changes?", [this, execute = std::move(execute)]() -> bool {
			ImGui::TextUnformatted(
			        "If you don't save your changes before this operation, they will be permanently lost.");

			if(ImGui::Button("Save") || ImGui::IsKeyPressed(ImGuiKey_Enter, false)) {
				save_as(std::move(execute));
				return true;
			}
			ImGui::SameLine();
			if(ImGui::Button("Discard Changes")) {
				execute();
				return true;
			}
			ImGui::SameLine();
			if(ImGui::Button("Cancel")) {
				return true;
			}
			return false;
		});
	}
	void Editor::save_as(std::function<void()> after)
	{
		if(!graph_.editable())
			return;

		gui::save_dialog("Save",
		                 save_file_.value_or("world.ygs"),
		                 {"Yggdrasill Save File", "*.ygs"},
		                 [&, after = std::move(after)](std::string_view path, std::function<void()> continuation) {
			                 save_to(path, [after = std::move(after), continuation = std::move(continuation)] {
				                 if(continuation)
					                 continuation();
				                 if(after)
					                 after();
			                 });
		                 });
	}
	void Editor::quick_save(std::function<void()> after)
	{
#ifdef __EMSCRIPTEN__
		save_as(std::move(after));
#else
		if(!save_file_) {
			save_as(std::move(after));
			return;
		}

		if(saved_version_ == graph_.version()) {
			if(after)
				after();
			return;
		}

		save_to(*save_file_, std::move(after));
#endif
	}

	void Editor::save_to(std::string_view path, std::function<void()> after)
	{
		if(!graph_.editable())
			return;

		save_file_      = path;
		status_message_ = Status_message{"Save started. Please wait...", ImVec4(1, 1, 1, 1), 99999};
		graph_.start_save(std::string(path), current_state_.node().get(), [this, after = std::move(after)] {
			set_window_title_suffix(*save_file_);
			modified_since_save_ = false;
			saved_version_       = graph_.version();
			status_message_ = Status_message{"File successfully saved", ImVec4(0, 1, 0, 1), show_error_seconds};
			if(after)
				after();
		});
	}

} // namespace yggdrasill
