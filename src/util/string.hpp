#pragma once

#include <algorithm>
#include <iomanip>
#include <locale>
#include <sstream>
#include <string>
#include <string_view>
#include <unordered_map>

namespace yggdrasill::util {
	class String_literal;
}
consteval yggdrasill::util::String_literal operator""_str(const char*, size_t);

namespace yggdrasill::util {

	struct Transparent_string_hash {
		using hash_type      = std::hash<std::string_view>;
		using is_transparent = void;

		std::size_t operator()(const char* str) const { return hash_type{}(str); }
		std::size_t operator()(std::string_view str) const { return hash_type{}(str); }
		std::size_t operator()(const std::string& str) const { return hash_type{}(str); }
	};
	template <typename Key, typename Value>
	using String_map = std::unordered_map<Key, Value, Transparent_string_hash, std::equal_to<>>;


	class String_literal {
	  public:
		constexpr String_literal() : data_(""), size_(0) {}
		template <std::size_t N>
		constexpr String_literal(const char (&data)[N]) : data_(data), size_(N - 1)
		{
		}
		consteval String_literal(const char* data) : data_(data), size_(0)
		{
			while(data_[size_] != 0)
				size_++;
		}
		template <std::size_t N>
		String_literal(const char8_t (&data)[N]) : data_(reinterpret_cast<const char*>(data)), size_(N - 1)
		{
		}

		constexpr operator const char*() const { return c_str(); }
		constexpr operator std::string_view() const { return view(); }

		constexpr const char*      c_str() const { return data_; }
		constexpr std::string_view view() const { return {data_, size_}; }
		std::string                str() const { return {data_, size_}; }

	  private:
		friend consteval String_literal(::operator""_str)(const char*, size_t);

		const char* data_;
		std::size_t size_;

		constexpr String_literal(const char* data, std::size_t size) : data_(data), size_(size) {}
	};


	inline void ltrim(std::string& s)
	{
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) { return !std::isspace(ch); }));
	}

	inline void rtrim(std::string& s)
	{
		s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) { return !std::isspace(ch); }).base(),
		        s.end());
	}

	inline void trim(std::string& s)
	{
		ltrim(s);
		rtrim(s);
	}

	inline std::string ltrim_copy(std::string s)
	{
		ltrim(s);
		return s;
	}

	inline std::string rtrim_copy(std::string s)
	{
		rtrim(s);
		return s;
	}

	inline std::string trim_copy(std::string s)
	{
		trim(s);
		return s;
	}
	inline bool contains(std::string_view s, char c)
	{
		return s.find(c) != std::string_view::npos;
	}

	inline std::string replace_all(std::string_view in, std::string_view search, std::string_view replace)
	{
		auto out = std::string();
		out.reserve(in.size());

		const auto end     = in.end();
		auto       current = in.begin();
		auto       next    = std::search(current, end, search.begin(), search.end());
		while(next != end) {
			out.append(current, next);
			out.append(replace);
			current = next + search.size();
			next    = std::search(current, end, search.begin(), search.end());
		}
		out.append(current, next);
		return out;
	}

	template <typename... T>
	std::string concat(const T&... values)
	{
		struct facet : std::numpunct<char> {
			char        do_thousands_sep() const { return '\''; }
			std::string do_grouping() const { return "\03"; }
		};
		static const auto loc = std::locale(std::locale::classic(), new facet);

		auto ss = std::ostringstream();
		ss.imbue(loc);
		ss << std::boolalpha << std::setprecision(8);
		(ss << ... << values);
		return std::move(ss).str();
	}

} // namespace yggdrasill::util

consteval yggdrasill::util::String_literal operator""_str(const char* data, size_t count)
{
	return {data, count};
}
