/** simple wrappers for iterator pairs ***************************************
 *                                                                           *
 * Copyright (c) 2018 Florian Oetke                                          *
 *  This file is distributed under the MIT License                           *
 *  See LICENSE file for details.                                            *
\*****************************************************************************/

#pragma once

#include <tuple>
#include <utility>
#include <vector>

namespace yggdrasill::util {

	template <class Iter>
	class Iter_range {
	  public:
		Iter_range() noexcept = default;
		Iter_range(Iter begin, Iter end) noexcept : begin_(begin), end_(end) {}

		bool operator==(const Iter_range& o) noexcept { return begin_ == o.begin_ && end_ == o.end_; }

		Iter begin() const noexcept { return begin_; }
		Iter end() const noexcept { return end_; }

		[[nodiscard]] std::size_t size() const noexcept { return std::distance(begin_, end_); }

	  private:
		Iter begin_, end_;
	};
	template <class T>
	using vector_range = Iter_range<typename std::vector<T>::iterator>;

	template <class T>
	using cvector_range = Iter_range<typename std::vector<T>::const_iterator>;


	template <class Range>
	class Skip_range {
	  public:
		Skip_range() noexcept = default;
		Skip_range(std::size_t skip, Range range) noexcept : skip_(skip), range_(std::move(range)) {}

		bool operator==(const Skip_range& o) noexcept { return range_ == o.range_; }

		auto begin() const noexcept
		{
			auto i       = range_.begin();
			auto skipped = std::size_t(0);
			while(i != range_.end() && skipped++ < skip_)
				++i;
			return i;
		}
		auto end() const noexcept { return range_.end(); }

		std::size_t size() const noexcept
		{
			auto s = range_.size();
			return s > skip_ ? s - skip_ : 0;
		}

	  private:
		std::size_t skip_;
		Range       range_;
	};
	template <class Range>
	auto skip(std::size_t skip, Range&& range)
	{
		return Skip_range<std::remove_reference_t<Range>>(skip, std::forward<Range>(range));
	}


	namespace detail {
		struct deref_join {
			template <class... Iter>
			[[maybe_unused]] auto operator()(Iter&&... iter) const
			{
				return std::tuple<decltype(*iter)...>(*iter...);
			}
		};

		template <typename T>
		class Proxy_holder {
		  public:
			Proxy_holder(const T& value) : value_(value) {}
			T* operator->() { return &value_; }
			T& operator*() { return value_; }

		  private:
			T value_;
		};
	} // namespace detail

	template <class... Iter>
	class Join_iterator {
	  public:
		Join_iterator() = default;
		template <class... T>
		explicit Join_iterator(std::size_t index, T&&... iters)
		  : iters_(std::forward<T>(iters)...), index_(index)
		{
		}

		auto operator==(const Join_iterator& rhs) const { return index_ == rhs.index_; }
		auto operator!=(const Join_iterator& rhs) const { return !(*this == rhs); }

		auto operator*() const { return std::apply(detail::deref_join{}, iters_); }
		auto operator->() const { return proxy_holder(**this); }

		auto operator++()
		{
			foreach_in_tuple(iters_, [](auto, auto& iter) { ++iter; });
			++index_;
		}
		auto operator++(int)
		{
			auto v = *this;
			++*this;
			return v;
		}

	  private:
		std::tuple<Iter...> iters_;
		std::size_t         index_{};
	};

	namespace detail {
		struct get_join_begin {
			template <class... Range>
			[[maybe_unused]] auto operator()(Range&&... ranges) const
			{
				return Join_iterator<std::remove_reference_t<decltype(ranges.begin())>...>(0, ranges.begin()...);
			}
		};
		struct get_join_iterator {
			template <class... Range>
			[[maybe_unused]] auto operator()(std::size_t offset, Range&&... ranges) const
			{
				return Join_iterator<std::remove_reference_t<decltype(ranges.begin())>...>(
				        offset, (ranges.begin() + offset)...);
			}
		};
	} // namespace detail

	template <class... Range>
	class Join_range {
	  public:
		Join_range() noexcept {}
		template <class... T>
		Join_range(T&&... ranges) noexcept
		  : ranges_(std::forward<T>(ranges)...), size_(std::min({ranges.size()...}))
		{
		}

		auto begin() const noexcept { return std::apply(detail::get_join_begin{}, ranges_); }
		auto begin() noexcept { return std::apply(detail::get_join_begin{}, ranges_); }
		auto end() const noexcept
		{
			return std::apply(detail::get_join_iterator{}, std::tuple_cat(std::make_tuple(size_), ranges_));
		}
		auto end() noexcept
		{
			return std::apply(detail::get_join_iterator{}, std::tuple_cat(std::make_tuple(size_), ranges_));
		}

		auto size() const noexcept { return size_; }

	  private:
		std::tuple<Range...> ranges_;
		std::size_t          size_;
	};

	template <class... Range>
	auto join(Range&&... range)
	{
		if constexpr((std::is_lvalue_reference_v<Range> && ...)) {
			return Join_range<Range...>(range...); // & or const& based ranges

		} else {
			// one of the ranges was a temporary => copy/value based ranges
			return Join_range<std::remove_reference_t<Range>...>(std::forward<Range>(range)...);
		}
	}


	namespace detail {
		template <class Arg>
		auto construct_index_tuple(std::int64_t index, Arg arg)
		{
			return std::tuple<std::int64_t, Arg>(index, arg);
		}
		template <class, class... Args>
		auto construct_index_tuple(std::int64_t index, std::tuple<Args...> arg)
		{
			return std::tuple<std::int64_t, Args...>(index, std::get<Args>(arg)...);
		}


		template <class BaseIter>
		class Indexing_range_iterator {
		  public:
			Indexing_range_iterator(BaseIter&& iter, std::int64_t index) : iter_(iter), index_(index) {}

			auto operator==(const Indexing_range_iterator& rhs) const { return iter_ == rhs.iter_; }
			auto operator!=(const Indexing_range_iterator& rhs) const { return !(*this == rhs); }

			auto operator*() { return construct_index_tuple<decltype(*iter_)>(index_, *iter_); }
			auto operator->() { return Proxy_holder(**this); }

			auto operator*() const { return construct_index_tuple<decltype(*iter_)>(index_, *iter_); }
			auto operator->() const { return Proxy_holder(**this); }

			auto operator++()
			{
				++iter_;
				++index_;
			}
			auto operator++(int)
			{
				auto v = *this;
				++*this;
				return v;
			}

		  private:
			BaseIter     iter_;
			std::int64_t index_;
		};
		template <class BaseIter>
		auto make_indexing_range_iterator(BaseIter&& iter, std::int64_t index)
		{
			return Indexing_range_iterator<std::remove_reference_t<BaseIter>>(std::forward<BaseIter>(iter), index);
		}
	} // namespace detail

	template <class Range>
	class Indexing_range {
	  public:
		Indexing_range(Range&& range) noexcept : range_(std::move(range)) {}

		bool operator==(const Indexing_range& rhs) noexcept { return range_ == rhs.range_; }

		auto begin() noexcept { return detail::make_indexing_range_iterator(range_.begin(), 0u); }
		auto end() noexcept { return detail::make_indexing_range_iterator(range_.end(), std::int64_t(-1)); }
		auto begin() const noexcept { return detail::make_indexing_range_iterator(range_.begin(), 0u); }
		auto end() const noexcept
		{
			return detail::make_indexing_range_iterator(range_.end(), std::int64_t(-1));
		}

	  private:
		Range range_;
	};
	template <class Range>
	auto with_index(Range&& r) -> Indexing_range<std::remove_reference_t<Range>>
	{
		return {std::move(r)};
	}

	template <class Range>
	class Indexing_range_view {
	  public:
		Indexing_range_view(Range& range) noexcept : range_(&range) {}

		bool operator==(const Indexing_range_view& rhs) noexcept { return *range_ == *rhs.range_; }

		auto begin() noexcept { return detail::make_indexing_range_iterator(range_->begin(), 0u); }
		auto end() noexcept { return detail::make_indexing_range_iterator(range_->end(), std::int64_t(-1)); }
		auto begin() const noexcept { return detail::make_indexing_range_iterator(range_->begin(), 0u); }
		auto end() const noexcept
		{
			return detail::make_indexing_range_iterator(range_->end(), std::int64_t(-1));
		}

	  private:
		Range* range_;
	};
	template <class Range>
	auto with_index(Range& r) -> Indexing_range_view<std::remove_reference_t<Range>>
	{
		return {r};
	}


	template <class T>
	class Numeric_range {
		struct iterator {
			using iterator_category = std::random_access_iterator_tag;
			using value_type        = T;
			using difference_type   = std::ptrdiff_t;
			using pointer           = T*;
			using reference         = T&;

			T value;
			T step;
			constexpr iterator(T value, T step = 1) noexcept : value(value), step(step) {}
			constexpr iterator(const iterator&) noexcept = default;
			constexpr iterator(iterator&&) noexcept      = default;
			iterator& operator++() noexcept
			{
				value += step;
				return *this;
			}
			iterator operator++(int) noexcept
			{
				auto t = *this;
				*this ++;
				return t;
			}
			iterator& operator--() noexcept
			{
				value -= step;
				return *this;
			}
			iterator operator--(int) noexcept
			{
				auto t = *this;
				*this --;
				return t;
			}
			bool     operator==(const iterator& rhs) const noexcept { return value == rhs.value; }
			bool     operator!=(const iterator& rhs) const noexcept { return value != rhs.value; }
			const T& operator*() const noexcept { return value; }
		};
		using const_iterator = iterator;

	  public:
		constexpr Numeric_range() noexcept {}
		constexpr Numeric_range(T begin, T end, T step = 1) noexcept : begin_(begin), end_(end), step_(step)
		{
		}
		constexpr Numeric_range(Numeric_range&&) noexcept      = default;
		constexpr Numeric_range(const Numeric_range&) noexcept = default;

		Numeric_range& operator=(const Numeric_range&) noexcept = default;
		Numeric_range& operator=(Numeric_range&&) noexcept      = default;
		bool           operator==(const Numeric_range& o) noexcept
		{
			return begin_ == o.begin_ && end_ == o.end_ && step_ == o.step_;
		}

		constexpr iterator begin() const noexcept { return begin_; }
		constexpr iterator end() const noexcept { return end_; }

	  private:
		T begin_, end_, step_;
	};
	template <class Iter, typename = std::enable_if_t<!std::is_arithmetic<Iter>::value>>
	constexpr Iter_range<Iter> range(Iter b, Iter e)
	{
		return {b, e};
	}
	template <class B, class E, typename = std::enable_if_t<std::is_arithmetic<B>::value>>
	constexpr auto range(B b, E e, std::common_type_t<B, E> s = 1)
	{
		using T = std::common_type_t<B, E>;
		return Numeric_range<T>{T(b), std::max(T(e + 1), T(b)), T(s)};
	}
	template <class T, typename = std::enable_if_t<std::is_arithmetic<T>::value>>
	constexpr Numeric_range<T> range(T num)
	{
		return {0, static_cast<T>(num)};
	}
	template <class Container, typename = std::enable_if_t<!std::is_arithmetic<Container>::value>>
	auto range(Container& c) -> Iter_range<typename Container::iterator>
	{
		using namespace std;
		return {begin(c), end(c)};
	}
	template <class Container, typename = std::enable_if_t<!std::is_arithmetic<Container>::value>>
	auto range(const Container& c) -> Iter_range<typename Container::const_iterator>
	{
		using namespace std;
		return {begin(c), end(c)};
	}

	template <class Container, typename = std::enable_if_t<!std::is_arithmetic<Container>::value>>
	auto range_reverse(Container& c) -> Iter_range<typename Container::reverse_iterator>
	{
		using namespace std;
		return {rbegin(c), rend(c)};
	}
	template <class Container, typename = std::enable_if_t<!std::is_arithmetic<Container>::value>>
	auto range_reverse(const Container& c) -> Iter_range<typename Container::const_reverse_iterator>
	{
		using namespace std;
		return {rbegin(c), rend(c)};
	}

	template <typename... Ts>
	auto vector_from(Ts&&... args)
	{
		auto r = std::vector<std::common_type_t<Ts...>>();
		r.reserve(sizeof...(Ts));
		(r.push_back(std::forward<Ts>(args)), ...);
		return r;
	}
	template <typename... Ts>
	auto array_from(Ts&&... args)
	{
		auto r = std::array<std::common_type_t<Ts...>, sizeof...(Ts)>{{std::forward<Ts>(args)...}};
		return r;
	}

	template <typename std::size_t Size, typename F, std::size_t... Is>
	constexpr auto make_array_impl(F&& f, std::index_sequence<Is...>)
	{
		// <std::invoke_result_t<F, std::size_t>, Size>
		return std::array{f(Is)...};
	}

	template <typename std::size_t Size, typename F>
	constexpr auto make_array(F&& f)
	{
		return make_array_impl<Size>(std::forward<F>(f), std::make_index_sequence<Size>());
	}

	template <typename T, std::size_t Size, typename F>
	constexpr auto map(const std::array<T, Size>& array, F&& f)
	{
		return make_array<Size>([&](auto i) {
			if constexpr(std::is_invocable_v<F, T>)
				return std::invoke(f, array[i]);
			else
				return std::apply(f, array[i]);
		});
	}

	template <typename T, std::size_t Size>
	constexpr auto shift(const std::array<T, Size>& array, int offset)
	{
		return make_array<Size>([&](auto i) {
			auto shifted_i = static_cast<int>(i) - offset;
			if(shifted_i < 0)
				shifted_i = Size + shifted_i;
			return array[shifted_i % Size];
		});
	}

	template <typename C, typename V>
	int index_of(const C& container, const V& value)
	{
		const auto it = std::find(container.begin(), container.end(), value);
		return it != container.end() ? std::distance(container.begin(), it) : -1;
	}

} // namespace yggdrasill::util
