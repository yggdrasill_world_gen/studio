/** simple wrapper for optional values or references *************************
 *                                                                           *
 * Copyright (c) 2014 Florian Oetke                                          *
 *  This file is distributed under the MIT License                           *
 *  See LICENSE file for details.                                            *
\*****************************************************************************/

#pragma once

#include <functional>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <type_traits>

#define YGGDRASILL_FAIL(M) \
	do {                   \
		(std::cerr << M);  \
		std::cerr << "\n"; \
		std::abort();      \
	} while(false)

#define YGGDRASILL_INVARIANT(C, M)                \
	do {                                          \
		if(!(C)) {                                \
			std::cerr << "Invariant violation: "; \
			(std::cerr << M);                     \
			std::cerr << "\n";                    \
			std::abort();                         \
		}                                         \
	} while(false)

namespace yggdrasill::util {

	template <typename T>
	class maybe;

	struct nothing_t;

	namespace details {
		struct maybe_else_callable {
			bool is_nothing;

			template <typename Func>
			void on_nothing(Func f)
			{
				if(is_nothing)
					f();
			}
		};

		template <typename T, typename... Args>
		constexpr bool is_first_maybe()
		{
			if constexpr(sizeof...(Args) == 0)
				return true;
			else {
				using First_arg =
				        std::remove_cv_t<std::remove_reference_t<typename std::tuple_element<0, std::tuple<Args...>>::type>>;
				return std::is_same_v<maybe<T>, First_arg> || std::is_same_v<nothing_t, First_arg>;
			}
		}
	} // namespace details

	template <typename T>
	class maybe {
	  public:
		constexpr maybe() noexcept : valid_(false) {}
		template <class... Args, typename = std::enable_if_t<!details::is_first_maybe<T, Args...>()>>
		explicit constexpr maybe(Args&&... args) noexcept : valid_(true), data_{std::forward<Args>(args)...}
		{
		}
		/*implicit*/ constexpr maybe(T&& data_) noexcept : valid_(true), data_(std::move(data_)) {}
		/*implicit*/ constexpr maybe(const T& data_) noexcept : valid_(true), data_(data_) {}
		constexpr maybe(const maybe& o) noexcept : valid_(o.valid_)
		{
			if(o.valid_) {
				new(&data_) T(o.data_);
			}
		}
		constexpr maybe(maybe&& o) noexcept : valid_(o.valid_)
		{
			if(o.valid_) {
				new(&data_) T(std::move(o.data_));
			}
			o.valid_ = false;
		}

		constexpr ~maybe() noexcept
		{
			if(is_some())
				data_.~T();
		}

		operator maybe<const T>() const noexcept
		{
			return is_some() ? maybe<const T>(data_) : maybe<const T>::nothing();
		}
		constexpr bool operator!() const noexcept { return is_nothing(); }

		maybe& operator=(const maybe& o) noexcept
		{
			valid_ = o.valid_;
			data_  = o.data_;
			return *this;
		}
		maybe& operator=(maybe&& o) noexcept
		{
			if(o.valid_) {
				if(valid_)
					data_ = std::move(o.data_);
				else
					new(&data_) T(std::move(o.data_));

				o.data_.~T();
			}

			valid_   = o.valid_;
			o.valid_ = false;
			return *this;
		}
		template <class... Args>
		T& emplace(Args&&... args)
		{
			if(valid_)
				data_.~T();

			new(&data_) T(std::forward<Args>(args)...);
			valid_ = true;

			return data_;
		}
		void reset()
		{
			if(valid_) {
				data_.~T();
				valid_ = false;
			}
		}

		static constexpr maybe nothing() noexcept { return maybe(); }


		[[nodiscard]] constexpr bool is_some() const noexcept { return valid_; }
		[[nodiscard]] constexpr bool is_nothing() const noexcept { return !is_some(); }

		T get_or_throw() &&
		{
			YGGDRASILL_INVARIANT(is_some(), "Called getOrThrow on nothing.");

			return std::move(data_);
		}
		template <typename... Ms, typename = std::enable_if_t<sizeof...(Ms) >= 1>>
		T get_or_throw(Ms&&... ms) &&
		{
			YGGDRASILL_INVARIANT(is_some(), ... << std::forward<Ms>(ms));

			return std::move(data_);
		}

		T& get_or_throw() &
		{
			YGGDRASILL_INVARIANT(is_some(), "Called getOrThrow on nothing.");

			return data_;
		}
		template <typename... Ms, typename = std::enable_if_t<sizeof...(Ms) >= 1>>
		T& get_or_throw(Ms&&... ms) &
		{
			YGGDRASILL_INVARIANT(is_some(), ... << std::forward<Ms>(ms));

			return data_;
		}

		const T& get_or_throw() const&
		{
			YGGDRASILL_INVARIANT(is_some(), "Called getOrThrow on nothing.");

			return data_;
		}
		template <typename... Ms, typename = std::enable_if_t<sizeof...(Ms) >= 1>>
		const T& get_or_throw(Ms&&... ms) const&
		{
			YGGDRASILL_INVARIANT(is_some(), ... << std::forward<Ms>(ms));

			return data_;
		}

		constexpr T&       get_ref_or(T& other) noexcept { return is_some() ? data_ : other; }
		constexpr const T& get_ref_or(const T& other) const noexcept { return is_some() ? data_ : other; }
		T                  get_or(T other) const noexcept { return is_some() ? data_ : other; }

		template <typename Func,
		          typename = std::enable_if_t<!std::is_convertible_v<Func&, const T&>>,
		          typename = decltype(std::declval<Func>())>
		T get_or(Func&& f) const noexcept
		{
			return is_some() ? data_ : f();
		}

		template <typename Func, class = std::enable_if_t<std::is_same<std::invoke_result_t<Func, T&>, void>::value>>
		void process(Func&& f) const
		{
			if(is_some())
				std::invoke(f, data_);
		}
		template <typename Func, class = std::enable_if_t<!std::is_same<std::invoke_result_t<Func, T&>, void>::value>>
		auto process(Func&& f) const -> maybe<std::invoke_result_t<Func, const T&>>
		{
			if(is_some())
				return std::invoke(f, data_);
			else
				return {};
		}

		template <typename Func, class = std::enable_if_t<std::is_same<std::invoke_result_t<Func, T&>, void>::value>>
		void process(Func&& f)
		{
			if(is_some())
				std::invoke(f, data_);
		}
		template <typename Func, class = std::enable_if_t<!std::is_same<std::invoke_result_t<Func, T&>, void>::value>>
		auto process(Func&& f) -> maybe<std::invoke_result_t<Func, T&>>
		{
			if(is_some())
				return std::invoke(f, data_);
			else
				return maybe<std::invoke_result_t<Func, T&>>{};
		}

		template <typename RT, typename Func>
		auto process(RT def, Func&& f) -> RT
		{
			if(is_some())
				return std::invoke(f, data_);

			return def;
		}

		template <typename RT, typename Func>
		auto process(RT def, Func&& f) const -> RT
		{
			if(is_some())
				return std::invoke(f, data_);

			return def;
		}

		template <typename Func>
		auto map(Func&& f) const -> std::decay_t<std::invoke_result_t<Func, const T&>>
		{
			if(is_some())
				return std::invoke(f, data_);

			return {};
		}

	  private:
		bool valid_;
		union {
			std::remove_const_t<T> data_;
		};
	};

	struct nothing_t {
		template <typename T>
		constexpr operator maybe<T>() const noexcept
		{
			return maybe<T>::nothing();
		}
	};
	constexpr nothing_t nothing;

	template <typename T>
	maybe<std::remove_reference_t<T>> just(T&& inst)
	{
		return maybe<std::remove_reference_t<T>>(std::forward<T>(inst));
	}
	template <typename T>
	maybe<T> just_copy(const T& inst)
	{
		return maybe<T>(inst);
	}
	template <typename T>
	constexpr maybe<T&> just_ptr(T* inst)
	{
		return inst != nullptr ? maybe<T&>(*inst) : nothing;
	}

	template <typename T, typename Func>
	auto operator>>(const maybe<T>& t, Func f)
	        -> std::enable_if_t<!std::is_same<void, decltype(f(t.get_or_throw()))>::value,
	                            maybe<decltype(f(t.get_or_throw()))>>
	{
		return t.is_some() ? just(f(t.get_or_throw())) : nothing;
	}

	template <typename T, typename Func>
	auto operator>>(const maybe<T>& t, Func f)
	        -> std::enable_if_t<std::is_same<void, decltype(f(t.get_or_throw()))>::value, void>
	{
		if(t.is_some())
			f(t.get_or_throw());
	}

	template <typename T>
	constexpr bool operator!(const maybe<T>& m)
	{
		return !m.is_some();
	}


	template <typename T>
	class maybe<T&> {
	  public:
		constexpr maybe() : ref_(nullptr) {}
		/*implicit*/ constexpr maybe(T& data) noexcept : ref_(&data) {}
		template <typename U, class = std::enable_if_t<std::is_convertible<U*, T*>::value>>
		constexpr maybe(U& o) noexcept : ref_(&o)
		{
		}
		constexpr maybe(const maybe& o) noexcept : ref_(o.ref_) {}
		constexpr maybe(maybe&& o) noexcept : ref_(o.ref_) { o.ref_ = nullptr; }
		template <typename U, class = std::enable_if_t<std::is_convertible<U*, T*>::value>>
		constexpr maybe(const maybe<U&>& o) noexcept : ref_(o.ref_)
		{
		}
		constexpr ~maybe() noexcept = default;

		constexpr operator maybe<const T&>() const noexcept
		{
			return is_some() ? maybe<const T&>(*ref_) : maybe<const T&>::nothing();
		}
		constexpr bool operator!() const noexcept { return is_nothing(); }

		template <typename U, class = std::enable_if_t<std::is_convertible<U*, T*>::value>>
		constexpr maybe& operator=(U& o) noexcept
		{
			ref_ = &o;
			return *this;
		}
		constexpr maybe& operator=(const maybe& o) noexcept
		{
			ref_ = o.ref_;
			return *this;
		}
		constexpr maybe& operator=(maybe&& o) noexcept
		{
			std::swap(ref_ = nullptr, o.ref_);
			return *this;
		}
		constexpr void reset() { ref_ = nullptr; }

		static constexpr maybe nothing() noexcept { return maybe(); }

		[[nodiscard]] constexpr bool is_some() const noexcept { return ref_ != nullptr; }
		[[nodiscard]] constexpr bool is_nothing() const noexcept { return !is_some(); }

		constexpr T& get_or_throw() const
		{
			YGGDRASILL_INVARIANT(is_some(), "Called getOrThrow on nothing.");

			return *ref_;
		}
		template <typename... Ms, typename = std::enable_if_t<sizeof...(Ms) >= 1>>
		constexpr T& get_or_throw(Ms&&... ms) const
		{
			YGGDRASILL_INVARIANT(is_some(), ... << std::forward<Ms>(ms));

			return *ref_;
		}
		constexpr T& get_or(std::remove_const_t<T>& other) const noexcept
		{
			return is_some() ? *ref_ : other;
		}
		constexpr const T& get_or(const T& other) const noexcept { return is_some() ? *ref_ : other; }

		template <typename Func,
		          typename = std::enable_if_t<!std::is_convertible_v<Func&, const T&>>,
		          typename = decltype(std::declval<Func>())>
		constexpr T& get_or(Func&& f) const noexcept
		{
			return is_some() ? *ref_ : f();
		}

		template <typename Func, class = std::enable_if_t<std::is_same<std::invoke_result_t<Func, T&>, void>::value>>
		void process(Func&& f) const
		{
			if(is_some())
				f(*ref_);
		}
		template <typename Func, class = std::enable_if_t<!std::is_same<std::invoke_result_t<Func, T&>, void>::value>>
		auto process(Func&& f) const -> maybe<std::invoke_result_t<Func, const T&>>
		{
			if(is_some())
				return std::invoke(f, *ref_);
			else
				return {};
		}

		template <typename Func, class = std::enable_if_t<std::is_same<std::invoke_result_t<Func, T&>, void>::value>>
		void process(Func&& f)
		{
			if(is_some())
				std::invoke(f, *ref_);
		}
		template <typename Func, class = std::enable_if_t<!std::is_same<std::invoke_result_t<Func, T&>, void>::value>>
		auto process(Func&& f) -> maybe<std::invoke_result_t<Func, T&>>
		{
			if(is_some())
				return std::invoke(f, *ref_);
			else
				return {};
		}

		template <typename RT, typename Func>
		auto process(RT def, Func&& f) -> RT
		{
			if(is_some())
				return std::invoke(f, *ref_);

			return def;
		}

		template <typename RT, typename Func>
		auto process(RT def, Func&& f) const -> RT
		{
			if(is_some())
				return std::invoke(f, *ref_);

			return def;
		}

		template <typename Func>
		auto map(Func&& f) const -> std::invoke_result_t<Func, const T&>
		{
			if(is_some())
				return std::invoke(f, *ref_);

			return {};
		}

	  private:
		T* ref_;
	};

	namespace details {
		template <std::size_t...>
		struct seq {};

		template <std::size_t N, std::size_t... S>
		struct gens : gens<N - 1u, N - 1u, S...> {};

		template <std::size_t... S>
		struct gens<0u, S...> {
			typedef seq<S...> type;
		};

		template <typename... T>
		struct processor {
			std::tuple<T&&...> args;

			template <typename Func>
			void operator>>(Func&& f)
			{
				call(std::forward<Func>(f), typename gens<sizeof...(T)>::type());
			}

		  private:
			template <typename Func, std::size_t... S>
			void call(Func&& f, seq<S...>)
			{
				call(std::forward<Func>(f), std::forward<decltype(std::get<S>(args))>(std::get<S>(args))...);
			}

			template <typename Func, typename... Args>
			void call(Func&& f, Args&&... m)
			{
				for(bool b : {m.is_some()...})
					if(!b)
						return;

				f(m.get_or_throw()...);
			}
		};
	} // namespace details

	/*
	 * Usage:
	 * maybe<bool> b = true;
	 *	maybe<int> i = nothing();
	 *	maybe<float> f = 1.0f;
	 *
	 *	process(b,i,f)>> [](bool b, int i, float& f){
	 *		// ...
	 *	};
	 */
	template <typename... T>
	auto process(T&&... m) -> details::processor<T...>
	{
		return details::processor<T...>{std::tuple<decltype(m)...>(std::forward<T>(m)...)};
	}

	template <class Map, class Key>
	auto find_maybe(Map& map, const Key& key)
	{
		auto iter = map.find(key);
		return iter != map.end() ? just_ptr(&iter->second) : nothing;
	}

	template <typename T1, typename T2>
	requires(requires { std::declval<const T1>() == std::declval<const T2>(); })
	inline bool operator==(const maybe<T1>& lhs, const maybe<T2>& rhs)
	{
		return lhs.is_some() == rhs.is_some() && (lhs.is_nothing() || lhs.get_or_throw() == rhs.get_or_throw());
	}
	template <typename T1, typename T2>
	requires(requires { std::declval<const T1>() <=> std::declval<const T2>(); })
	inline auto operator<=>(const maybe<T1>& lhs, const maybe<T2>& rhs)
	        -> decltype(lhs.get_or_throw() <=> rhs.get_or_throw())
	{
		using RT = decltype(lhs.get_or_throw() <=> rhs.get_or_throw());

		if(lhs.is_nothing() && rhs.is_nothing())
			return RT::equivalent;
		else if(lhs.is_nothing())
			return RT::less;
		else if(rhs.is_nothing())
			return RT::greater;
		else
			return lhs.get_or_throw() <=> rhs.get_or_throw();
	}


	template <typename T>
	class lazy {
	  public:
		using source_t = std::function<T()>;

		/*implicit*/ lazy(source_t s) : source_(s) {}

		operator T() { return source_; }

	  private:
		source_t source_;
	};

	template <typename T>
	inline lazy<T> later(typename lazy<T>::source_t f)
	{
		return lazy<T>(f);
	}


	template <class F>
	struct return_type;

	template <class R, class T, class... A>
	struct return_type<R (T::*)(A...)> {
		typedef R type;
	};
	template <class R, class... A>
	struct return_type<R (*)(A...)> {
		typedef R type;
	};

	template <typename S, typename T>
	inline lazy<T> later(S* s, T (S::*f)())
	{
		std::weak_ptr<S> weak_s = s->shared_from_this();

		return lazy<T>([weak_s, f]() {
			auto shared_s = weak_s.lock();
			if(shared_s) {
				auto s = shared_s.get();
				return (s->*f)();
			} else {
				return T{};
			}
		});
	}
} // namespace yggdrasill::util
