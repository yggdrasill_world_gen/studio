[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](/LICENSE)

## Yggdrasill Studio

Editor and debugger for the yggdrasill framework

Latest build:

- [WebGL](https://cranky-swartz-3250cb.netlify.app) (requires a recent version Chrome [>=68], Firefox [>=79] or Edge [>=79])
- [Windows](https://cranky-swartz-3250cb.netlify.app/windows.zip)
- [Linux](https://cranky-swartz-3250cb.netlify.app/linux.zip)
